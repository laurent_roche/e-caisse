#!/bin/bash
#
# Télécharger le script par :
# wget https://framagit.org/laurent_roche/e-caisse/raw/master/Scripts/controle.sh && chmod u+x controle.sh
#pour la toute dernière version, et
# wget https://framagit.org/laurent_roche/e-caisse/repository/v0.8.00/Scripts/controle.sh && chmod u+x controle.sh
#pour la version v0.8.00
#
# Contrôle la version téléchargée
# 1/ Télécharge la version
# 2/ Contrôle sha1sum
# 3/ Décompresse
# 4/ Comparaison INTÉGRALE 
# 5/ Comparaison des fichiers gérant le contrôle

pVersion=$1
pDir=$2
f_Usage () {
	echo "Usage: controle.sh no_version répertoire_à_comparer" >&2
	[ "X$1" != "X" ] && echo $1 >&2
	exit 2
}


[ "X$pVersion" == "X" ] && f_Usage "Donnez un numéro de version"
[ "X$pDir" == "X" ] && f_Usage "Donnez le répertoire à comparer"

### Télécharge la version
echo "=== Télécharge la version de référence ==="
wget http://framagit.org/laurent_roche/e-caisse/repository/$pVersion/archive.tar.gz

### Contrôle sha1sum
# Crée fichier sha1sum
case $pVersion in
	"v0.8.00")
		echo 'b39f6a8b6c08d8af6669a468035c087129754df7  archive.tar.gz' > ctrl.txt
		;;
	"v00.08.01")
		#echo 'dae503dd9e96d3cfadf693b3ba97860486f790e5  archive.tar.gz' > ctrl.txt
		echo '0473d824bad439bbf505b011bf4514b4d3dd1d84  archive.tar.gz' > ctrl.txt
		;;
		
	*)
		echo "Version inconnu ! ! !" >&2
		;;
esac

#Contrôle sha1sum
if [ -e ctrl.txt ]
then 
	echo "=== Contrôle du fichier téléchargé ==="
	sha1sum -c ctrl.txt
	[ $? -ne 0 ] && echo "ERREUR: mauvais fichier " >&2 && exit 3
fi

### Décompresse
tar -zxvf  archive.tar.gz > /dev/null
[ $? -ne 0 ] && echo "ERREUR à la décompression " >&2 && exit 4

mv e-caisse-* Diff_E-Caisse

### diff -qr
echo "=== Compare INTÉGRALEMENT les 2 versions ==="
diff -qr Diff_E-Caisse $pDir

### Compare seulement les fichiers du contrôle
sCtrlFPN=$(grep -r '#e-Caisse_CTRL#' Diff_E-Caisse/* | cut -d: -f1)
iNbCtrlFPN=`echo $sCtrlFPN |wc -l`
echo "=== Comparaison des $iNbCtrlFPN fichiers de contrôle ==="
for fic in $sCtrlFPN
do
	diff $fic ${pDir}/${fic#*/}
done

### Nettoyage
rm -R Diff_E-Caisse
rm ctrl.txt archive.tar.gz
