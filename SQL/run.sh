#!/bin/bash
. ./my.conf


for myf in  *.sql; do
	echo ".... running $myf"
	mysql -h $MYSQL_SERVER -u $MYSQL_LOGIN -p"$MYSQL_PWD" -D ${MYSQL_DB} < $myf
	if [ $? -ne 0 ]; then
		echo " >>> ERREUR <<<"
		break
	fi
done
