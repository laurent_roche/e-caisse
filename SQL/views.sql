CREATE OR REPLACE VIEW VaffichageFavoris AS select Affichage.aff_parent AS VafF_parent,count(Affichage.aff_id) AS VafF_nb from Affichage group by Affichage.aff_parent;

CREATE OR REPLACE VIEW Vanalytique AS select Codes.cod_id AS Vana_id,Codes.cod_nom AS Vana_nom,Codes.cod_type AS Vana_type from Codes where Codes.cod_type = 3;

CREATE OR REPLACE VIEW Varticle_editeur AS select Articles.art_id AS Vart_id,Articles.art_cb AS Vart_cb,Titres.tit_nom AS Vart_titre,Articles.art_rayon AS Vart_rayon,Articles.art_stk AS Vart_stk,Articles.art_unite AS Vart_unite,Articles.art_pht AS Vart_pht,Articles.art_tva AS Vart_tva,Articles.art_statut AS Vart_statut,Editeurs.edi_utilisateur AS Vart_utilisateur,Editeurs.edi_nom AS Vart_nomediteur,Editeurs.edi_id AS Vart_idediteur from ((Articles join Editeurs on(Articles.art_editeur = Editeurs.edi_id)) join Titres on(Titres.tit_article = Articles.art_id)) where Titres.tit_niveau = 1;

CREATE OR REPLACE VIEW Varticles_editeurs AS select Articles.art_editeur AS Vadi_editeur,count(Articles.art_editeur) AS Vadi_ct,Articles.art_id AS Vadi_article,Editeur_serveur.eds_serveur AS Vadi_serveur,Editeur_serveur.eds_utilisateur AS Vadi_utilisateur from (Articles join Editeur_serveur on(Editeur_serveur.eds_editeur = Articles.art_editeur)) group by Articles.art_editeur order by Editeur_serveur.eds_serveur;

CREATE OR REPLACE VIEW Varticles_rayon AS select Articles.art_id AS Var_article,Articles.art_cb AS Var_cb,Articles.art_stk AS Var_stk,Articles.art_rayon AS Var_rayon,Editeurs.edi_utilisateur AS Var_utilisateur,Rayons.ray_nom AS Var_raynom from ((Articles join Rayons on(Articles.art_rayon = Rayons.ray_id)) join Editeurs on(Articles.art_editeur = Editeurs.edi_id)) where Articles.art_statut < 3;

CREATE OR REPLACE VIEW Varticles_secteurs AS select Articles.art_id AS Vas_article,Rayons.ray_secteur AS Vas_secteur from (Articles join Rayons on(Rayons.ray_id = Articles.art_rayon)) order by Rayons.ray_secteur;

CREATE OR REPLACE VIEW Varticles_utilisateur AS select Articles.art_id AS Vart_id,Articles.art_statut AS Vart_statut,Articles.art_cb AS Vart_cb,Editeurs.edi_utilisateur AS Vart_utilisateur,Articles.art_creation AS Vart_creation,Articles.art_stk AS Vart_stk from (Articles join Editeurs) where Articles.art_editeur = Editeurs.edi_id;

CREATE OR REPLACE VIEW Vcana AS select Codes.cod_id AS Vca_id,Codes.cod_nom AS Vca_nom,Codes.cod_type AS Vca_type from Codes where Codes.cod_type = 3;

CREATE OR REPLACE VIEW Vcodes_entree AS select Codes.cod_id AS Vce_id,Codes.cod_nom AS Vce_nom,Codes.cod_description AS Vce_description from Codes where Codes.cod_type = 1 order by Codes.cod_ordre;

CREATE OR REPLACE VIEW Vcodes_tvadeductible AS select Codes.cod_id AS Vct_id,Codes.cod_nom AS Vct_nom,Codes.cod_description AS Vct_description from Codes where Codes.cod_type = 6 order by Codes.cod_ordre;

CREATE OR REPLACE VIEW Vfavoris AS select Favoris.fav_parent AS Vfav_parent,count(Favoris.fav_article) AS Vfav_ct from Favoris group by Favoris.fav_parent;

CREATE OR REPLACE VIEW Vfonction_utlisateur AS select Utilisateurs.uti_fonction AS Vfon_id,count(Utilisateurs.uti_fonction) AS Vfon_ct from Utilisateurs group by Utilisateurs.uti_fonction;

CREATE OR REPLACE VIEW Vhistorique AS select UnionTable.art AS art from (select Tickets_2022.tic_article AS art from ((Tickets_2022 join Articles on(Articles.art_id = Tickets_2022.tic_article)) join Editeurs on(Articles.art_editeur = Editeurs.edi_id)) where Editeurs.edi_utilisateur = 4 and Articles.art_stk <> 0.00 union all select Commandes_2022.com_article AS art from ((Commandes_2022 join Articles on(Articles.art_id = Commandes_2022.com_article)) join Editeurs on(Articles.art_editeur = Editeurs.edi_id)) where Editeurs.edi_utilisateur = 4 and Articles.art_stk <> 0.00) UnionTable group by UnionTable.art;

CREATE OR REPLACE VIEW Vserveurs AS select Editeurs.edi_id AS Vser_id,Editeurs.edi_nom AS Vser_nom,count(Editeurs.edi_id) AS Vser_ct from (Editeurs join Editeur_serveur on(Editeur_serveur.eds_serveur = Editeurs.edi_id)) group by Editeur_serveur.eds_serveur;

CREATE OR REPLACE VIEW Vtit1 AS select Titres.tit_id AS Vt1_id,Titres.tit_nom AS Vt1_nom,Titres.tit_recherche AS Vt1_recherche,Titres.tit_article AS Vt1_article,Titres.tit_niveau AS Vt1_niveau from Titres where Titres.tit_niveau = 1;

CREATE OR REPLACE VIEW Vtit2 AS select Titres.tit_id AS Vt2_id,Titres.tit_nom AS Vt2_nom,Titres.tit_recherche AS Vt2_recherche,Titres.tit_article AS Vt2_article,Titres.tit_niveau AS Vt2_niveau from Titres where Titres.tit_niveau = 2;

CREATE OR REPLACE VIEW Vtit3 AS select Titres.tit_id AS Vt3_id,Titres.tit_nom AS Vt3_nom,Titres.tit_recherche AS Vt3_recherche,Titres.tit_article AS Vt3_article,Titres.tit_niveau AS Vt3_niveau from Titres where Titres.tit_niveau = 3;

CREATE OR REPLACE VIEW Vtit4 AS select Titres.tit_id AS Vt4_id,Titres.tit_nom AS Vt4_nom,Titres.tit_recherche AS Vt4_recherche,Titres.tit_article AS Vt4_article,Titres.tit_niveau AS Vt4_niveau from Titres where Titres.tit_niveau = 4;

