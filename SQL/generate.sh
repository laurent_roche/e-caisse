#!/bin/bash
. ./my.conf

myExclViews=$(mysql -h $MYSQL_SERVER -u $MYSQL_LOGIN -p"$MYSQL_PWD" --skip-column-names --batch -e 'SELECT CONCAT(" --ignore-table=", TABLE_SCHEMA, ".",TABLE_NAME) FROM information_schema.views')
mysqldump -h $MYSQL_SERVER -u $MYSQL_LOGIN -p"$MYSQL_PWD" --no-data  --skip-lock-tables --skip-add-locks --skip-disable-keys --skip-dump-date --skip-quote-names --skip-set-charset --comments $myExclViews ${MYSQL_DB} |grep -v ' SET *character_set_client' |grep -v '@@character_set_client' |grep -v ${MYSQL_SERVER} | sed 's/ AUTO_INCREMENT=[0-9]*//g' > ddl.sql

#mysqldump -h $MYSQL_SERVER -u $MYSQL_LOGIN -p"$MYSQL_PWD" --skip-extended-insert --no-create-info --skip-dump-date --single-transaction --quick --complete-insert --skip-quote-names --skip-lock-tables ${MYSQL_DB} |grep -v "LOCK TABLE" |grep -v ${MYSQL_SERVER}  > mydata.sql #INSERT à chaque ligne
mysqldump -h $MYSQL_SERVER -u $MYSQL_LOGIN -p"$MYSQL_PWD" --extended-insert --no-create-info --skip-dump-date --single-transaction --quick --complete-insert --skip-quote-names --skip-lock-tables ${MYSQL_DB} |grep -v "LOCK TABLE" |grep -v ${MYSQL_SERVER} | sed 's/),(/),\n(/g'  > mydata.sql


#mysql -h $MYSQL_SERVER -u $MYSQL_LOGIN -p"$MYSQL_PWD" --skip-column-names --batch -e 'SELECT REPLACE(REPLACE(CONCAT("DROP TABLE IF EXISTS ", TABLE_NAME, ";@CREATE OR REPLACE VIEW ", TABLE_NAME, " AS ", VIEW_DEFINITION, ";@"), CONCAT("`",TABLE_SCHEMA,"`."), ""),"`","" ) table_name from information_schema.views' | tr '@' '\n'  #DROP TABLE avant CREATE VIEW
mysql -h $MYSQL_SERVER -u $MYSQL_LOGIN -p"$MYSQL_PWD" --skip-column-names --batch -e 'SELECT REPLACE(REPLACE(CONCAT("CREATE OR REPLACE VIEW ", TABLE_NAME, " AS ", VIEW_DEFINITION, ";@"), CONCAT("`",TABLE_SCHEMA,"`."), ""),"`","" ) table_name FROM information_schema.views WHERE TABLE_SCHEMA="'${MYSQL_DB}'"' | tr '@' '\n' > views.sql
