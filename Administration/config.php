<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
require $incpath . "mysql/connect.php";
require $incpath . "php/fonctions.php";
connexobjet();
// print_r($config);
$req_conf = "SELECT * FROM Config";
$r_conf = $idcom->query($req_conf);
$rq_conf = $r_conf->fetch_object(); //modif(id,tb,vl,cp,my)
$cka = ($rq_conf->con_arrondi == 1) ? "checked" : "";
$ckd = ($rq_conf->con_don == 1) ? "checked" : "";
?>
<script>
    $(document).ready(function() {
        $('table#config input').on('keydown', function(event) {
            if (event.which == 13) {
                if ($(this).attr('type') == 'text') {
                    if ($(this).hasClass('jaune')) {
                        modif(0, 9, $(this).val(), $(this).attr('id'), 1);
                        if ($(this).attr('conf') == 'txt') {
                            //on regenère le fichier config.php
                            charge('regenere', '', 'panneau_d');
                        }
                    }
                }
            } else {
                $(this).addClass('jaune');
            }
        });

        $("table#config input[type=checkbox]").on("click", (function() {
            champ = $(this).attr('id');
            if ($(this).prop('checked') == true) {
                modif(0, 9, 1, champ, 1);
            } else {
                modif(0, 9, 0, champ, 1);
            }
            //on regenère le fichier config.php
            charge('regenere', '', 'panneau_d');
        }));
    });
</script>

<img src="/images/aide.png" style="float:right" onclick="charge('aide',2,'panneau_d')">

<h3>Configuration générale</h3>
<CENTER>
    <table id='config' style="clear:both">
        <tr>
            <TH>Mode sortie</TH>
            <td><input conf='txt' type='text' value='<?php echo $rq_conf->con_mode ?>' style='width:50px' id='mode'>
            </td>
            <td>1 : sortie sur tva<br>2 : sortie sur secteurs</td>
        </tr>
        <tr>
            <TH>Favoris</TH>
            <td><input conf='txt' type='text' value='<?php echo $rq_conf->con_favoris ?>' style='width:50px' id='favoris'>
            </td>
            <td>1 : Gestion fine des favoris<br>2 : Favoris basés sur un rayon dédié</td>
        </tr>
        <tr>
            <TH>Année de départ</TH>
            <td><input conf='txt' type='text' value='<?php echo $rq_conf->con_debut ?>' style='width:50px' id='debut'> <button onclick="charge('purge','','panneau_d')">Purge</button>
            </td>
            <td>Date de mise en service de l'application</td>
        </tr>
        <TR>
            <TH>Fond de caisse</TH>
            <TD><input conf='txt' type='text' value='<?php echo $rq_conf->con_caisse ?>' style='width:50px' id='caisse'> €</TD>
        </TR>
        <TR>
            <TH>Arrondi</TH>
            <TD><input conf='txt' id="arrondi" type='checkbox' <?php echo $cka ?>></TD>
            <TD></TD>
        </TR>
        <TR>
            <TH>Don</TH>
            <TD><input conf='txt' id="don" type='checkbox' <?php echo $ckd ?>></TD>
            <TD></TD>
        </TR>
        <TR>
            <TH>Id Article don/echange</TH>
            <TD><input conf='txt' type='text' value='<?php echo $rq_conf->con_articledon ?>' id="articledon" style='width:50px'></TD>
            <TD></TD>
        </TR>
        <TR>
            <TH>Préfixe Factures</TH>
            <TD><input type='text' value='<?php echo $rq_conf->con_prefixfacture ?>' style='width:50px' id='prefixfacture'></TD>
        </TR>
        <TR>
            <TH>Préfixe Devis</TH>
            <TD><input type='text' value='<?php echo $rq_conf->con_prefixdevis ?>' style='width:50px' id='prefixdevis'></TD>
        </TR>
        <TR>
            <TH>Préfixe Journal de factures</TH>
            <TD><input type='text' value='<?php echo $rq_conf->con_prefixjournal ?>' style='width:50px' id='prefixjournal'></TD>
        </TR>
        <TR>
            <TH>Mois de l'inventaire</TH>
            <TD>
                <select onchange="modif(0,9,this.value,'inventaire','1')">
                    <option>Mois</option>
                    <?php
                    for ($i = 1; $i < 13; $i++) {
                        if ($i == $rq_conf->con_inventaire) {
                            $sel = "selected";
                        } else {
                            $sel = "";
                        }
                        echo "<option " . $sel . ">" . $i . "</option>";
                    }
                    ?>
                </select>
            </TD>
        </TR>
        <TR>
            <TH>remise sur livre</TH>
            <TD><input type='text' value='<?php echo $rq_conf->con_livre ?>' style='width:50px' id='livre'>%</TD>
        </TR>
        <tr>
            <TH>Secteur livre</TH>
            <TD><select onchange="modif(0,9,this.value,'secteur','1')">
                    <option></option>
                    <?php
                    $req_secteur = 'SELECT * FROM Secteurs ORDER BY sec_id';
                    $r_secteur = $idcom->query($req_secteur);
                    while ($rq_secteur = $r_secteur->fetch_object()) {
                        $sel = ($rq_secteur->sec_id == $rq_conf->con_secteur) ? " selected" : "";
                        echo "<option value='" . $rq_secteur->sec_id . "'" . $sel . ">" . $rq_secteur->sec_nom . "</option>";
                    }
                    ?>
                </select>
            </TD>
        </TR>
        <TR>
            <TH>Identification</TH>
            <TD><input type='text' value='<?php echo $rq_conf->con_token ?>' style='width:250px' id='token'></TD>
        </TR>

    </table>
</CENTER>