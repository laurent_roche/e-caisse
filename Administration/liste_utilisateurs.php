<?php
if (!isset($idcom)) {
    session_start();
    if (!isset($incpath)) {
        $p=preg_split("[/]", $_SERVER['PHP_SELF']);
        $incpath="";
        for ($i=1;$i<sizeof($p)-1;$i++) {
            $incpath='../'.$incpath;
        }
        unset($p, $i);
    }
    include $incpath."mysql/connect.php";
    connexobjet();
    $req_uti="SELECT * FROM Utilisateurs JOIN Fonctions ON fon_id=uti_fonction ORDER BY uti_fonction";
    $r_uti=$idcom->query($req_uti);
}
$n = 0;
$fonc = '';
while ($rq_uti=$r_uti->fetch_object()) {
    $nom =($rq_uti->uti_etat == 2 )?"<em>".$rq_uti->uti_nom."</em>":$rq_uti->uti_nom;
    // print_r($rq_uti);
    if (($fonc != $rq_uti->uti_fonction)&&($n != 0)) {
        echo "</div><div class='cadre'><h3>".$rq_uti->fon_nom."</h3>";
    } else if ($n == 0) {
        echo "<h3>".$rq_uti->fon_nom."</h3>";
    }
    echo "<span class='nom' onclick=charge('utilisateur',".$rq_uti->uti_id.",'panneau_d')>".$nom."</span><br>";
    $fonc=$rq_uti->uti_fonction;
    $n++;
}
//recherche des vendeurs connectés
$req_vendeur = "SELECT uti_nom, uti_id, COUNT(tic_num) AS articles, uti_id, tic_num, rst_id
                    FROM Utilisateurs
                    JOIN Resume_ticket_".ANNEE." ON rst_utilisateur = uti_id
                    LEFT JOIN Tickets_".ANNEE." ON rst_id = tic_num
                      WHERE uti_etat = 1 AND rst_validation IS NULL
                        GROUP BY tic_num";
$r_vendeur=$idcom->query($req_vendeur);
if ($r_vendeur->num_rows <> 0) {
    ?>
    <script>
    function aller(id,n){
    if (n == 0){
    charge("nonvalide",id+'&mode=suppression',"panneau_d");
    }
    else{
    charge("nonvalide",id,"panneau_d");
    }
    // alert(id);
    }</script>
    <?php
    $erreur = "<h3>Utilisateurs connectés</h3>";

    while ($rq_vendeur=$r_vendeur->fetch_object()) {
        $erreur .=$rq_vendeur->uti_nom;
        if ($rq_vendeur->articles > 0) {
            $erreur .=" (".$rq_vendeur->articles." article".$s.")<br>";
            $erreur .="<button ".$l." article".$s." onclick='aller(".$rq_vendeur->tic_num.",1)'>Voir ".$l." article".$s." non validé".$s."</button><br>";
        } else {
            $erreur .=" (ticket vide)<br>";
        }
        $erreur .="<button onclick='aller(".$rq_vendeur->rst_id.",0)'>Déconnecter l'utilisateur</button><br><br>";
    }
    ?>
    <script>$('#panneau_d').html("<?php echo $erreur?>");</script>
    <?php
}
?>
