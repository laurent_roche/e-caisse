<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$uti= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
connexobjet();
$req_uti="SELECT * FROM Utilisateurs LEFT JOIN Fonctions ON fon_id=uti_fonction WHERE uti_id=$uti";
$r_uti=$idcom->query($req_uti);
$rq_uti=$r_uti->fetch_object();
//    modif (id,tb,vl,cp,my)

?>
<link rel='stylesheet' href='/bgrins-spectrum/spectrum.css' />
<script src='/bgrins-spectrum/spectrum.js'></script>

<script>
$(".fond").spectrum({
    change: function(color) {
        col=color.toHexString();
        or=$(this).attr('id');
        col=col.replace('#','');
        if (or == 'fondF') {
            $('body').css('backgroundColor','#'+col);
        }
        else if (or == 'fondC') {
            $('#menu').css('backgroundColor','#'+col);
            $('#affichage').css('backgroundColor','#'+col);
        }
        else if (or == 'titre') {
            $('h3').css('backgroundColor','#'+col);
        }
        $('#actualiser').css('visibility','visible');
        modif (<?php echo $uti?>,1,col,or,1);
    }
});

$(document).ready(function() {
    $('#pass').on('keyup',function(event){
        if (isNaN($(this).val())) {
            $('#info').html('<span style="font-size:20px;color:red">Uniquement des chiffres</span>');
            $(this).val('');
        }
      });
    $('table#config input').on('keydown',function(event){
        $(this).addClass('jaune');    
        if (event.which == 13){
            if(($(this).attr('type') == 'text')||($(this).attr('type') == 'password')){
                modif (<?php echo $uti?>,1,$(this).val(),$(this).attr('id'),1);
            }
        }
    });
});
</script>
<h3>Modification de l'utilisateur</h3>
<table id="config">
  <TR>
    <TH>Nom</TH><TD><input type="text" id='nom' style="width:180px" value="<?php echo $rq_uti->uti_nom?>"></TD>
  </TR>
  <TR>
    <TH>Modules</TH><TD>
    <select onchange="modif (<?php echo $uti?>,1,this.value,'fonction','1')"><option></option>
    <?php
    $req_fonction="SELECT * FROM Fonctions";
    $r_fonction=$idcom->query($req_fonction);
    while ($rq_fonction=$r_fonction->fetch_object()) {
        $sel=($rq_fonction->fon_id == $rq_uti->uti_fonction)?" selected":"";
        echo "<option value='".$rq_fonction->fon_id."'".$sel.">".$rq_fonction->fon_nom."</option>\n";
    }
    ?></select>
    </TD>
  </TR>
  <TR>
    <TH>Couleur Foncée</TH><TD><input type='text' value="<?php echo $rq_uti->uti_fondF?>" class="fond" id="fondF"></TD>
  </TR>
  <TR>
    <TH>Couleur Claire</TH><TD><input type='text' value="<?php echo $rq_uti->uti_fondC?>" class="fond" id="fondC"></TD>
  </TR>
  <TR>
    <TH>Titre</TH><TD><input type='text' value="<?php echo $rq_uti->uti_titre?>" class="fond" id="titre"></TD>
  </TR>
  <TR>
    <TH>Surlignage</TH><TD><input type='text' value="<?php echo $rq_uti->uti_surligne?>" class="fond" id="surligne"></TD>
  </TR>
  <TR>
    <TH>Mot de Passe</TH><TD><input id='pass' onclick="this.value=''" type="password" value="<?php echo ($rq_uti->uti_pass != "")?'****':''?>"></TD>
  </TR>
  <TR>
    <TH>Email</TH><TD><input type="email" id='email' value="<?php echo $rq_uti->uti_email?>"></TD>
  </TR>
    <?php
    if ($rq_uti->fon_nom == "Ventes") {
        ?>
    <tr>
    <TH>Clavier</TH><TD>
    <select onchange="modif (<?php echo $uti?>,1,this.value,'clavier','1')">
    <option></option>
    <?php
    $tab_etat=array("Azerty","Alphabétique");
        for ($i=0; $i<2; $i++) {
            $sel=($i == $rq_uti->uti_clavier)?" selected":"";
            echo "<option value='".$i."'".$sel.">".$tab_etat[$i]."</option>";
        } ?>
    </select></TD>
    </TR>
    <TH>État</TH><TD>
    <select onchange="modif (<?php echo $uti?>,1,this.value,'etat','1')">
    <option></option>
    <?php
    $tab_etat=array("Déconnecté","Connecté","Inactif");
        for ($i=0; $i<3; $i++) {
            $sel=($i == $rq_uti->uti_etat)?" selected":"";
            echo "<option value='".$i."'".$sel.">".$tab_etat[$i]."</option>";
        } ?>
    </select></TD>
    </TR>
    <?php
    $sel1 ='';
        $sel2 ='';
        if ($rq_uti->uti_fonction == 2) {//Ventes
    
            if ($rq_uti->uti_droits == 0) {
                $sel1=" selected";
            } else {
                $sel2=" selected";
            } ?>
    <tr><TH>Gestion des comptes</TH>
    <td>
    <select onchange="modif (<?php echo $uti?>,1,this.value,'droits','1')">
    <option value="0"<?php echo $sel1?>>Non</option>
    <option value="1"<?php echo $sel2?>>Oui</option>
    </select>
    </td>
    </tr>
    <?php
        }
    }
?><tr><th colspan="2" id="info"></th></tr>
</table>
<?php
/*recherche de l'utilisation de cet utilisateur
dans les ventes, Editieurs
il y a obligatiorement utilisateur dans chaque module
*/
// $req_utilise = "SELECT ";
// $r_utilise = $idcom->query($req_utilise);
// $rq_utilise =$r_->fetch_object();
// // if ($rq_uti->fon_id == '') echo
?>