<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
$id = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath . "mysql/connect.php";
require $incpath . "php/fonctions.php";
connexobjet();
$req_artpseudo = "SELECT art_cb,
                        art_id,
                        art_pseudo,
                        Vt1_nom,
                        pse_nom,
                        art_stk
                            FROM Articles
                                JOIN Vtit1 ON Vt1_article  = art_id
                                JOIN Pseudos on pse_id = art_pseudo
                                    WHERE art_pseudo = " . $id;
$r_artpseudo = $idcom->query($req_artpseudo);
$rq_artpseudo = $r_artpseudo->fetch_object();
if ($r_artpseudo->num_rows == 0) {
    echo "<img src='/images/attention.png'> Il n'y a pas d'article dans cette catégorie";
    exit;
}
$req_pseudo = "SELECT * FROM Pseudos ORDER BY pse_id";
$r_pseudo = $idcom->query($req_pseudo);
$combo = '';
while ($rq_pseudo = $r_pseudo->fetch_object()) {
    $combo .= "<option value='" . $rq_pseudo->pse_id . "'>" . $rq_pseudo->pse_nom . "</option>";
}
?>
<style>
    #articles {
        width: 90%
    }

    .jaune {
        background-color: yellow;
    }

    #articles.tablesorter tbody td {
        font-size: 11pt;
        color: #3D3D3D;
        padding: 0 4px 0 4px;
        background-color: <?php echo $coulFF ?>;
        vertical-align: middle;
    }

    #articles.tablesorter tbody tr.odd td {
        /*   text-align:left; */
        background-color: <?php echo $coulCC ?>;
        vertical-align: middle;
    }

    #articles.tablesorter tbody tr.odd.orange td {
        /*   text-align:left; */
        background-color: orange;
        vertical-align: middle;
    }

    #articles.tablesorter thead tr .headerSortDown,
    table.tablesorter thead tr .headerSortUp {
        background-color: #8dbdd8;
    }
</style>
<script type="text/javascript" src="/js/jquery.tablesorter.js"></script>
<script>
    $(document).ready(function() {
        $("select").change(function() {
            id = $(this).attr("id").substr(2);
            // alert($(this).val());
            modif(id, 11, $(this).val(), "pseudo", 1);
            charge('art_pseudos', '<?php echo $id ?>', 'panneau_d')
        });
        $("#articles").tablesorter({
            widgets: ['zebra']
        });
    });
</script>
<h3><?php echo $rq_artpseudo->pse_nom ?></h3>
<table id='articles' class="tablesorter">
    <thead>
        <tr>
            <th>CB</th>
            <th>Titre</th>
            <th>Stock</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $r_artpseudo->data_seek(0);
        $tab_pseudos = "0,";
        $tab_article = "0,";
        $n = 1;
        while ($rq_artpseudo = $r_artpseudo->fetch_object()) {
            if ($rq_artpseudo->art_pseudo == 1) {
                echo "Statut normal de l'article, la liste serait trop longue.";
                break;
            } else {
                $tab_article .= ($n == 1) ? $rq_artpseudo->art_id : "," . $rq_artpseudo->art_id;
                $tab_pseudos .= ($n == 1) ? $rq_artpseudo->art_pseudo : "," . $rq_artpseudo->art_pseudo;
                echo "<tr><td>" . $rq_artpseudo->art_cb . "</td>
    <td>" . $rq_artpseudo->Vt1_nom . "</td>
    <td>" . $rq_artpseudo->art_stk . "</td>
    <td><select id='PS" . $rq_artpseudo->art_id . "'></select></td>
    </tr>";
                $n++;
            }
        }
        ?>
    </tbody>
    <tfoot></tfoot>
</table>
<script>
    $(document).ready(function() {
        var tab_pseudos = [<?php echo $tab_pseudos ?>];
        var tab_article = [<?php echo $tab_article ?>];
        var combo = "<option></option><?php echo $combo ?>";
        for (i = 1; i < tab_pseudos.length; i++) {
            // alert('#C'+tab_article[i]);
            $('#PS' + tab_article[i]).html(combo);
            $('#PS' + tab_article[i] + ' option[value=' + tab_pseudos[i] + ']').prop('selected', true);
        }
    });
    $("#panneau_g").css('max-height', $('#affichage').height());
</script>