<?php
session_start();

if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
$uti = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath . "mysql/connect.php";
connexobjet();
$req_exc = "SELECT * FROM Soldes WHERE YEAR(sol_debut) = " . date('Y') . " ORDER BY sol_id";
$r_exc = $idcom->query($req_exc);
//    modif(id,tb,vl,cp,my)
?>
<script>
    $(document).ready(function() {
        $('table#soldes input').change(function() {
            modif($(this).attr('id'), 33, $(this).val(), $(this).attr('alt'), 1);
        });
    });
</script>
<img src="/images/aide.png" style="float:right" onclick="charge('aide',5,'panneau_d')">
<h3>Dates des soldes de l'année <?php echo date('Y') ?></h3>
<img src="/images/attention.png"> <br>Les dates des soldes sont imposées par l'administration, on les trouve sur le site du gouvernement :
<a href="https://www.economie.gouv.fr/particuliers/dates-soldes" target="_blank">https://www.economie.gouv.fr/particuliers/dates-soldes</a>
<table id='soldes'>
    <TR>
        <TH>Début</TH>
        <th>Fin</th>
    </TR>
    <?php
    if ($r_exc->num_rows != 0) {
        while ($rq_exc = $r_exc->fetch_object()) {
    ?>
            <tr>
                <td><input id="<?php echo $rq_exc->sol_id ?>" alt="debut" type="date" min="<?php echo date('Y') ?>-01-01" max="<?php echo date('Y') ?>-12-31" value="<?php echo $rq_exc->sol_debut ?>"></td>
                <td><input id="<?php echo $rq_exc->sol_id ?>" alt="fin" type="date" min="<?php echo date('Y') ?>-01-01" max="<?php echo date('Y') ?>-12-31" value="<?php echo $rq_exc->sol_fin ?>"></td>
            </tr>
    <?php
            if ((date('Y-m-d') >= $rq_exc->sol_debut) && (date('Y-m-d') <= $rq_exc->sol_fin)) {
                echo '<br><h3> Soldes activées</h3>';
            }
        }
    }
    ?>
</table>
<button onclick="modif('',33,'<?php echo date('Y') ?>-01-01','debut','0');setTimeout(function(){charge('soldes','','panneau_g')},300);">Nouvelle date de soldes</button>