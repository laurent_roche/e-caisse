<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$nv= filter_input(INPUT_GET, "nv", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
connexobjet();
if (isset($nv)) {
    $idcom->query("INSERT INTO Codes (cod_type) VALUE ($req)");
}
if (!$req) {
    exit;
}
$req_codes="SELECT * FROM Codes WHERE  cod_type = $req ORDER BY cod_ordre";
$r_codes=$idcom->query($req_codes);
$nb = $r_codes->num_rows;
//modif(id,tb,vl,cp,my)
?>
<script>
$(document).ready(function() {
    $('table#codes input').on('keydown',function(event){
        if(event.which == 13){
            if($(this).attr('type') == 'text') {
                if ($(this).hasClass('jaune')) {
                    modif($(this).attr('cod'),27,$(this).val(),$(this).attr('alt'),1);
                }
            }
        } else {
            $(this).addClass('jaune');
        }
    });
});
</script>
<table id='codes'>
<?php

while ($rq_codes=$r_codes->fetch_object()) {
    echo "<tr><td><input alt='nom' cod='".$rq_codes->cod_id."' type='text' value='".$rq_codes->cod_nom."'></td>
    <td><input alt='description' cod='".$rq_codes->cod_id."' type='text' value='".$rq_codes->cod_description."'></td>
    <td><select onchange=\"modif(".$rq_codes->cod_id.",27,$(this).val(),'ordre',1)\"><option></option>";
    for ($i = 1; $i <= $nb; $i++) {
        $sel=($i == $rq_codes->cod_ordre)?" selected":"";
        echo "<option".$sel.">".$i."</option>";
    }
    echo "</select></td>";
}
?></tr>
<tr><TD><button onclick="charge('detail_code','<?php echo $req?>&nv=1','detail_code')">Nouveau</button></TD>
</tr>
</table>
<script>$('.sp-container').remove();</script>