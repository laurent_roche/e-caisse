<?php
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
switch($req) {
case 1 :
    ?>
    <h3>Info sur la TVA</h3>
    <h4>Valeur</h4>
    Quand un taux de tva change de valeur, il ne faut pas en créer un nouveau mais modifier l'existant.<br>
    Les articles ont le taux de tva de leur date d'achat. Il est réactualisé à chaque livraison.<br>
    L'inventaire étant fait sur les articles HT, seul l'ID est indiqué, la valeur étant la valeur du moment, non celle de l'achat.<br>
    On ne peut supprimer un taux de tva.
    <h4>La valeur Ac 'Actif', 'Désactiver' gère l'affichage de plusieurs éléments :</h4>
    <ul style="margin-left:30px">
    <LI>liste de sélection de la page de création/modifcation des articles</LI>
    <li>le résumé par taux de tva en fin de journée </li>
    <li>le résumé par taux de tva dans les factures</li>
    <li>le résumé par taux de tva dans l'inventaire</li>
    </ul>
    <h4>Article</h4>
    Indiquer le N° de l'article qui permettra d'insérer automatiquement un article de port associé au bon taux de tva.
    <h4>Code achat	</h4>
    Code qui servira à l'export/import des commandes dans EBP.
    <h4>Couleur</h4>
    Ces couleurs seront utilisées dans les tableaux du mois, elles mettent en valeur les colonnes. En finissant la première couleur, on définit la même couleur pour l'ensemble des valeurs. Cela facilte la création d'un dégradé.
    <?php
    break;
case 2 :
    ?>
    <h3>Aide sur la configuration générale</h3>
    <h4>Mode de sortie</h4>
    Il s'agit de l'impression des fins de journée<br>&nbsp;
    <h4>Favoris</h4>
    Mode de gestion des articles utilisé dans le module Ventes.<br>&nbsp;
    <h4>Année de départ et historique</h4>
    La législation impose un historique informatique de 6 ans plus l'année en cours. Les années affichées et analysées commenceront à la date indiquée. Si la purge n'a pas été faite, les années antérieurs seront ignorées.
    <h4>Arrondi</h4>
    Si coché, on ne pas décaisse pas les unités, ex. : <br>
    Valeur caisse : 653 €, valeur de fond de caisse 500 €<br>
    décaissement 150 €, fond de caisse 503 €.<br>&nbsp;
    <h4>Don</h4>
    Si votre magasin est habilité à recevoir des dons, cochez cette case.<br>
    Quand le vendeur mettra une valeur supérieur au montant dû, une ligne don sera ajoutée.<br>
    Si la case n'est pas cochée, pour les Chèques et CB, une ligne sera ajouté au ticket et un ticket de sortie de caisse sera automatiquement généré.<br>&nbsp;
    L'identifiant de l'article est à indiquer dnas le champ en dessous
    <h4>Préfixe</h4>
    Pour ces trois champs, indiquer le préfixe suivi d'un - et de 0 pour le nombre voulu ex. :<br> Fac-000 pour obtenir un numéro de facture Fac2017001.
    <br>Ce paramètre ne devra pas être modifié car il donne aussi le nom aux pdf des factures et devis.<br>&nbsp;
    <?php
    break;
case 3 :
    ?>
    <h3>Aide sur les fonctions</h3>
    <h4>Les noms</h4>
    Le nom des fonctions est aussi le nom des dossiers de l'application.<br>
    Les 4 premières fonctions sont celles de l'application, elle ne sont pas modifiables.<br>
    Si vous être programmeur, vous pouvez en ajouter, vous les retrouverez alors sur la page d'accueil.
    <?php
    break;
case 4 :
    ?>
    <h3>Aide sur les modes de reglement</h3>
    <h4>Principe</h4>
    L'application ne génère pas les écritures comptables.<br>
    Toute vente est active pour le jour.<br>
    En cas de paiement différé, l'entrer en Différé et éditer une facture comme pièce justificative en précisant que le règlement n'a pas été fait<br>
    Les articles mis en compte seront comptabilisés au moment de la 'remise à zero' du compte.<br>
    Exceptionnellement, il est possible de faire une sortie de caisse dont la valeur à été entrée en carte bancaire sans achat (distributeur de billet)<br>
    <h4>Décaissement</h4>
    En fonction des choix du service comptable
    <h4>Ordre</h4>
    Ordre permet d'imposer l'ordre d'affichage dans les tableaux de fin de journée et de mois.<br>
    <h4>État</h4>
    On ne peut supprimer un mode de règlement. S'il n'est plus utilisé, le désactiver.
    <h4>Seuil</h4>
    Permet d'imposer un prix minimum en fonction du mode de règlement.
    <h4>Couleur</h4>
    Ces couleurs seront utilisées dans les tableaux du mois, elles mettent en valeur les colonnes. En finissant la première couleur, on définit la même couleur pour l'ensemble des valeurs. Cela facilte la création d'un dégradé.
    <h4>Droits</h4>
    Correspond à Gestion des comptes dans Utilisateurs. Permet de limiter certains mode de règlement en fonction du vendeurs.
    <?php
    break;
case 5 :
    ?><h3>Aide sur les exclusions</h3>
    Ce paramètre alimente le combo d'exclusion de la fiche de l'article, permettant ainsi de connaître la raison de son exclusion
    <?php
        break;
case 6 :
    ?><h3>Aide sur les secteurs et Rayons</h3>
    Les secteurs et rayons sont communs à tous les utilisateurs.
    <br>Le classement se fait sur "Abrégé".
    <?php
    break;
case 7 :
    ?><h3>Aide sur les Favoris</h3>
    Les Favoris que vous définissez ici sont indépendants des secteurs et rayons.
    <br>Les Favoris dont la liste déroulante indique 'Parent' sont visibles directement dans l'interface de vente.
    <br>Pour être parent, le favoris ne doit pas contenir d'article. Dans le menu déroulant, ne sont disponibles que les favoris remplissants cette condition.
    <br>
    <?php
    break;
case 8 :
    ?><h3>Aide sur les Pseudos</h3>
    La marque pseudo permet de gérer les cas particulier de certains articles.<br>
    Les deux premiers sont indispensables à l'application.<br>
    <em>article normal</em> = tous les articles qui correspondent à un 'objet réel'.<br>
    <em>Pseudo générique</em> = article de substitution lié à la tva, il permert de vendre un article qu'on ne peut saisir sur le point de vente, mauvais code barre ou autre<br>
    Les suivants sont en fonctions des besoins
    <?php
    break;
case 9 :
    ?><h3>Aide sur la purge</h3>
    La législation impose l'historique de 6 années plus l'année en cours.<br>
    Si la purge est proposée, c'est que certaines données peuvent être supprimé.<br>
    Ce n'est pas une obligation si vous voulez gardez un historique plus ancien.<br>
    <?php
    break;
}
//
?>
