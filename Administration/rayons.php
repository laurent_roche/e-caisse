<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$secteur= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$id= filter_input(INPUT_GET, "id", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$pa= filter_input(INPUT_GET, "pa", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
if ($pa =="") {
    $pa = "fr";
}
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
if ($secteur == 'sup') {
    $req_rayon = "SELECT ray_nom, sec_nom, sec_id FROM Rayons JOIN Secteurs ON sec_id = ray_secteur WHERE ray_id = $id";
    $r_rayon = $idcom->query($req_rayon);
    $rq_rayon =$r_rayon->fetch_object(); ?>
    <h3>Supression de <em><?php echo $rq_rayon->ray_nom?></em> du secteur <em><?php echo $rq_rayon->sec_nom ?></em></h3>
    <h2>Voulez-vous vraiment supprimer ce rayon ?</h2>
    <br>
    <button onclick="modif(<?php echo $id?>,7,'','id','2');setTimeout(function(){charge('rayons',<?php echo $rq_rayon->sec_id?>,'panneau_d')},300);">Oui</button> <button onclick="charge('rayons',<?php echo $rq_rayon->sec_id?>,'panneau_d')">Non</button>
    <?php
    exit;
}
$req_ray="SELECT Rayons.*, COUNT(art_rayon) AS NB, sec_nom FROM Rayons JOIN Secteurs ON sec_id=ray_secteur LEFT JOIN Articles ON art_rayon = ray_id WHERE ray_secteur=$secteur GROUP BY art_rayon ORDER BY ray_nom" ;
$r_ray=$idcom->query($req_ray);
$rq_ray=$r_ray->fetch_object();
if ($r_ray->num_rows == 0) {
    ?>
    <h3>Il n'a pas de rayon dans ce secteur</h3>
    <button onclick="modif('',7,'<?php echo $secteur?>','secteur','0');setTimeout(function(){charge('rayons',<?php echo $secteur?>,'panneau_d')},300);">Nouveau rayon</button>
    <button style='float:right' onclick="modif('<?php echo $secteur?>',6,'','id','2');setTimeout(function(){charge('secteurs','','panneau_g')},300);">Supprimer le secteur</button>
    <?php
    exit;
}$ray_id=$rq_ray->ray_id;
require 'combo_codes.inc.php';
?>
<script>
$(document).ready(function() {
    $('table#rayons input').on('keydown',function(event) {
         if(event.which == 13){
            if($(this).attr('type') == 'text') {
                if ($(this).hasClass('jaune')) {
                    tab=$(this).attr('id').split('-');                
                    modif(tab[1], 7, $(this).val(), tab[0], 1);
                }
            }
        } else {
            $(this).addClass('jaune');
        }
    });
    $('table#rayons img').click(function(){
        var id = $(this).attr('alt');
        if ($(this).hasClass('supprimer') == true ) {
            charge('rayons','sup&id='+id,'panneau_d');
        } else {
            window.open('upload.php?req=rayons&id='+id);
        }
    });

    $("table#rayons input[type=checkbox]").on("click",(function(){
        var id = $(this).attr('fav');
        if($(this).prop('checked') == true) {
        modif(id, 7, 1, 'fav', 1);
        $('#R_'+id).css('visibility','visible');
        window.open('upload.php?req=rayons&id='+id);
//         charge('upload','rayons&id='+id,'ins_rayon');
        } else {
        modif(id, 7, 0, 'fav', 1);
        $('#popup').css('visibility','hidden');
        $('#R_'+id).css('visibility','hidden');
        }
    }));
    $('table#rayons button').click(function() {
        charge("rayons",<?php echo $secteur?>+'&pa='+$(this).attr('id'),"panneau_d");
    })
    $('#<?php echo $pa?>').prop( "disabled", true );
});
</script>
<style>
#rayons th {
    text-align:center;
}
</style>
<h3>Rayons de <?php echo $rq_ray->sec_nom?></h3>
<table id='rayons'>
  <TR>
    <TH>Nom</TH>
    <TH>Abrégé</TH>
    <TH>Code Entrée<br><button id="fr">Fr.</button><button id='eu'>EU</button><button id='neu'>!EU</button></TH>
    <TH>Code Sort.</TH>
    <TH>Code Ana.</TH>
    <TH>Nb articles</TH>
    <?php
    if ($config['favoris'] == 2) {
        ?>
        <TH>Favoris</TH>
        <TH></TH>
        <?php
    }
    ?>
  </TR>
<?php
$r_ray->data_seek(0);
$n = 0;
while ($rq_ray=$r_ray->fetch_object()) {
    $coul=($n%2 == 0)?$coulCC:$coulFF; ?>
    <TR style="background-color:<?php echo $coul?>">
    <TD><input id='nom-<?php echo $rq_ray->ray_id?>' style='width:200px' value="<?php echo $rq_ray->ray_nom?>" type="text"></TD>
    <TD><input id='abrege-<?php echo $rq_ray->ray_id?>' class="pt" value="<?php echo $rq_ray->ray_abrege?>" type="text"></TD>
    <TD>
    <select id="E_<?php echo $rq_ray->ray_id?>" onchange="modif(<?php echo $rq_ray->ray_id?>,7,this.value,'ent','1')">
    <script>combo(<?php echo $rq_ray->ray_id.','.$rq_ray->ray_ent?>,1)</script>
    </select>
    </TD>
    <TD>
    <select id="S_<?php echo $rq_ray->ray_id?>" onchange="modif(<?php echo $rq_ray->ray_id?>,7,this.value,'sort','1')">
    <script>combo(<?php echo $rq_ray->ray_id.','.$rq_ray->ray_sort?>,2)</script>
    </select>
    </TD>
    <TD>
    <select id="A_<?php echo $rq_ray->ray_id?>" onchange="modif(<?php echo $rq_ray->ray_id?>,7,this.value,'ana','1')">
    <script>combo(<?php echo $rq_ray->ray_id.','.$rq_ray->ray_ana?>,3)</script>
    </select>
    </TD>
    <td class="droite"><?php
    if ($rq_ray->NB > 0) {
        echo $rq_ray->NB;
    } else {
        ?>
<img src="/images/annuler.png" class="supprimer" width="30" alt='<?php echo $rq_ray->ray_id?>'>
        <?php
    } ?></td>
    <?php
    if ($config['favoris'] == 2) {
        $check=($rq_ray->ray_fav == 1)?" checked":""; ?>
        <td><input type="checkbox" <?php echo $check?> fav='<?php echo $rq_ray->ray_id?>'></td>
        <td>
        <?php
        if ($rq_ray->ray_fav == 1) {
            if (file_exists($incpath."images/rayons/ray_".$rq_ray->ray_id.".png")) {
                echo "<img id='R_".$rq_ray->ray_id."' alt='".$rq_ray->ray_id."' height=32 src='/images/rayons/ray_".$rq_ray->ray_id.".png'";
                "/images/rayons/inc.png";
            }
        } ?>
    </td>
    <?php
    } ?>
    </TR>
    <?php
    $n++;
}
?>
</table>
<button onclick="modif('',7,'<?php echo $secteur?>','secteur','0');setTimeout(function(){charge('rayons',<?php echo $secteur?>,'panneau_d')},300);">Nouveau rayon</button>
<script>
$("#panneau_g").css('max-height', $('#affichage').height());
</script>
