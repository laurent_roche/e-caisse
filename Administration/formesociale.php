<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
require $incpath."mysql/connect.php";
connexobjet();
?>
<script>
$(document).ready(function() {
    $('table#formes input').on('keydown',function(event){
        if(event.which == 13){
            if($(this).attr('type') == 'text') {
                if ($(this).hasClass('jaune')) {
                    tab=$(this).attr('id').split('-');
                    modif(tab[1],3,$(this).val(),tab[0],1);
                }
            }
        } else {
            $(this).addClass('jaune');
        }
    });
});
</script>

<h3>Les formes sociales</h3>
<CENTER>
  <table id='formes'>
    <tr>
      <TH>Id</TH>
      <TH>Nom</TH>
      <TH>Nom long</TH>
    </tr>
    <?php
    $req_soc="SELECT * FROM Formesociale";
    $r_soc=$idcom->query($req_soc);
    while ($rq_soc=$r_soc->fetch_object()) {
        echo "<tr><td>".$rq_soc->fos_id."</td><td><input id='nom-".$rq_soc->fos_id."' type='text' value='".$rq_soc->fos_nom."' style='width:150px' ></td><td><input id='long-".$rq_soc->fos_id."' type='text' value='".$rq_soc->fos_long."'></td></tr>\n";
    }
    //modif(id,tb,vl,cp,my)
    ?>
  </table>
<button onclick="modif('',3,'','nom','0');setTimeout(function(){charge('formesociale','','panneau_d')},300);">Nouvelle forme</button>
</CENTER>
