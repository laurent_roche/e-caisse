<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$uti= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
connexobjet();
$req_tva="SELECT * FROM Tva ORDER BY tva_ordre";
$r_tva=$idcom->query($req_tva);
$nb=$r_tva->num_rows;
//combo codes achats
$req_code = "SELECT * FROM Codes WHERE cod_type = 6 order BY cod_ordre";
$r_cod = $idcom->query($req_code);
// combo article
$req_art = 'SELECT art_id, Vt1_nom FROM Articles JOIN Vtit1 ON Vt1_article = art_id WHERE art_statut = 7';
$r_art = $idcom->query($req_art);

require 'combo_codes.inc.php';
//    modif(id,tb,vl,cp,my)

?>
<img src="/images/aide.png" style="float:right" onclick="charge('aide',1,'panneau_d')">
<link rel='stylesheet' href='/bgrins-spectrum/spectrum.css' />
<script src='/bgrins-spectrum/spectrum.js'></script>
<script>
$(".fond").spectrum({
    change: function(color) {
    col=color.toHex();
    or=$(this).attr('id');
    $('#'+or).parent().parent().css('backgroundColor','#'+col);
    modif(or,8,col,'couleur',1);
    }
});

$(document).ready(function() {
    $('table#tva input').on('keydown',function(event){
        $(this).addClass('jaune');
        if(event.which == 13){
            if($(this).attr('type') == 'text') {
                if ($(this).hasClass('jaune')) {
                    tab=$(this).attr('id').split('-');
                    modif(tab[1],8,$(this).val(),tab[0],1);
                }
            }
        } else {
            $(this).addClass('jaune');
        }
    });

});
</script>

<h3>Tva</h3>
<table id='tva'>
  <TR>
    <TH>ID</TH>
    <TH>Valeur</TH>
    <th>Ac</th>
    <th>Article</th>
    <th>Ordre</th>
    <th>Code ana</th>
    <th style ='paddin-left:0px;width:100px'>Code achat</th>
    <th>Couleur</th>
  </TR>
  <?php
  $id = 0;
while ($rq_tva=$r_tva->fetch_object()) {
    if ($rq_tva->tva_id > $id) {
        $id = $rq_tva->tva_id;
    }
    if ($rq_tva->tva_etat == 0) {
        $sel0 = " selected";
        $sel1 = "";
    } else {
        $sel0 = "";
        $sel1 = " selected";
    } ?>
  <TR style='background-color:#<?php echo $rq_tva->tva_couleur?>'>
        <td><?php echo $rq_tva->tva_id?></td>
        <TD><input id='nom-<?php echo $rq_tva->tva_id?>' class='cent' value="<?php echo $rq_tva->tva_nom?>" type="text"></TD>
        <td>
        <select onchange="modif(<?php echo $rq_tva->tva_id?>,8,this.value,'etat','1')">
        <OPTION></OPTION>
        <OPTION value="0"<?php echo $sel0?>>Désactivé</OPTION>
        <OPTION value="1"<?php echo $sel1?>>Actif</OPTION>
        <?php
        
        ?>
        </select>
        </td>
        <TD><select onchange="modif(<?php echo $rq_tva->tva_id?>,8,this.value,'article','1')"><option></option>
        <?php
        $r_art->data_seek(0);
    while ($rq_art =$r_art->fetch_object()) {
        $sel=($rq_art->art_id == $rq_tva->tva_article)?" selected":"";
        echo "<option value='".$rq_art->art_id."'".$sel.">".$rq_art->Vt1_nom."</option>";
    } ?>
        </select></TD>
        <td>
        <select onchange="modif(<?php echo $rq_tva->tva_id?>,8,this.value,'ordre','1')">
        <option></option>
        <?php
        for ($i=0;$i<=$nb;$i++) {
            if ($i == $rq_tva->tva_ordre) {
                $sel=" selected";
            } else {
                $sel="";
            }
            echo "<option".$sel.">".$i."</option>";
        } ?></select></td>
        <TD>
        <select id="T_<?php echo $rq_tva->tva_id?>" onchange="modif(<?php echo $rq_tva->tva_id?>,8,this.value,'code','1')">
        <script>combo(<?php echo $rq_tva->tva_id.','.$rq_tva->tva_code?>,5)</script>
        </select>
        </TD>
        <td><select onchange="modif(<?php echo $rq_tva->tva_id?>,8,this.value,'codeachat','1')"><option></option>
        <?php
        $r_cod->data_seek(0);
    while ($rq_cod =$r_cod->fetch_object()) {
        switch ($pa) {
                case 'eu': $champ = 'tva_codeachateu';
                    break;
                case 'neu': $champ = 'tva_codeachatneu';
                    break;
                default: $champ = 'tva_codeachat';
            }
        $sel=($rq_cod->cod_id == $rq_tva->$champ)?" selected":"";
        echo "<option value='".$rq_cod->cod_id."'".$sel.">".$rq_cod->cod_nom."</option>";
    } ?>
        </select>
        </td>
        <TD><input type='text' value="#<?php echo $rq_tva->tva_couleur?>" class="fond" id="<?php echo $rq_tva->tva_id?>">
        </TD>
  </TR>
  <?php
}
  ?>
</table>
<button onclick="modif('',8,'<?php echo($id + 1)?>','id','0');setTimeout(function(){charge('tva','','panneau_g')},300);">Nouveau Taux de tva</button>