<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$uti= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
connexobjet();
$req_sec="SELECT * FROM Secteurs ORDER BY sec_abrege";
$r_sec=$idcom->query($req_sec);
$nb=$r_sec->num_rows;

$req_tva="SELECT * FROM Tva ORDER BY tva_ordre";
$r_tva=$idcom->query($req_tva);
?>
<script>
$(document).ready(function() {
    $('table#secteurs input').on('keydown',function(event){	
        if(event.which == 13){
            if($(this).attr('type') == 'text') {
                if ($(this).hasClass('jaune')) {
                    tab=$(this).attr('id').split('-');
                    modif(tab[1],6,$(this).val(),tab[0],1);
                }
            }
        } else {
            $(this).addClass('jaune');
        }
    });
    
    $('table#secteurs img').click(function(){
    var id = $(this).parent().attr('sec');
//     alert($(this).attr('alt'));
        if ($(this).attr('alt') == 'voir' ) {
            charge('rayons',id,'panneau_d');
        } else {
            window.open('upload.php?req=secteur&id='+id);
        }
    });
});
</script>

<img src="/images/aide.png" style="float:right" onclick="charge('aide',6,'panneau_d')">
<h3>Définition des secteurs</h3>
<table id="secteurs">
  <TR><TH>Nom</TH><TH>Abrégé</TH><th>Ordre</th><th>Tva</th><th>Rayons</th>
  </TR>
<?php
while ($rq_sec=$r_sec->fetch_object()) {
//    modif(id,tb,vl,cp,my) ?>
  <TR style='background-color:#<?php echo $rq_sec->sec_couleur?>'>
    <TD><input id='nom-<?php echo $rq_sec->sec_id?>' type="text" style="width:200px" value="<?php echo $rq_sec->sec_nom?>"></TD>

    <TD><input id='abrege-<?php echo $rq_sec->sec_id?>' type="text" class="pt" value="<?php echo $rq_sec->sec_abrege?>">
    </TD>
    <td>  <select onchange="modif(<?php echo $rq_sec->sec_id?>,6,this.value,'ordre','1')">
  <option></option>
  <?php
  for ($i=0;$i<=$nb;$i++) {
      if ($i == $rq_sec->sec_ordre) {
          $sel=" selected";
      } else {
          $sel="";
      }
      echo "<option".$sel.">".$i."</option>";
  } ?></select></td>
    <td>  <select onchange="modif(<?php echo $rq_sec->sec_id?>,6,this.value,'tva','1')">
  <option></option>
  <?php
  $r_tva->data_seek(0);
    while ($rq_tva=$r_tva->fetch_object()) {
        if ($rq_sec->sec_tva == $rq_tva->tva_id) {
            $sel=" selected";
        } else {
            $sel="";
        }
        echo "<option".$sel." value='".$rq_tva->tva_id."'>".$rq_tva->tva_nom."</option>";
    } ?></select></td>
    <TD sec='<?php echo $rq_sec->sec_id?>'>
    <?php
//     echo "<img scr='/images/secteurs/".$rq_sec->sec_abrege.".png'>";
    $file=$incpath.'images/secteurs/sec_'.$rq_sec->sec_id.'.png';
    if (file_exists($file)) {
        echo "<img src='/images/secteurs/sec_".$rq_sec->sec_id.".png'  alt='secteur'>";
    } else {
        echo "<img src='/images/secteurs/inc.png'>";
    } ?> <img src='/images/voir.png' alt="voir" style='float:right;width:40px'>
    </td>
  </TR>
<?php
}
?>
</table>
<button onclick="modif('',6,'','nom','0');setTimeout(function(){charge('secteurs','','panneau_g');$('#panneau_d').empty()},300);">Nouveau secteur</button>
</div>
<script>
$("#panneau_g").css('max-height', $('#affichage').height());
</script>
