<?php
session_start();

if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$uti= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
connexobjet();
$req_exc="SELECT * FROM Statut ORDER BY sta_id";
$r_exc=$idcom->query($req_exc);
//    modif(id,tb,vl,cp,my)
?>
<script>
$(document).ready(function() {
    $('table#statut input').on('keydown',function(event){
        if(event.which == 13){
            if($(this).attr('type') == 'text') {
                if ($(this).hasClass('jaune')) {
                    modif($(this).attr('id'),15,$(this).val(),'nom',1);
                }
            }
        } else {
            $(this).addClass('jaune');
        }
    });
    $('table#statut img').click(function(){
        var id = $(this).attr('alt');
        charge('art_statut',id,'panneau_d');
        }
    );
});

</script>
<img src="/images/aide.png" style="float:right" onclick="charge('aide',5,'panneau_d')">
<h3>Statuts des articles</h3>
<table id='statut'>
  <TR>
    <TH>Valeur</TH><th></th>
  </TR>
    <?php
    while ($rq_exc=$r_exc->fetch_object()) {
        if($rq_exc->sta_id < 3) { //valeur système ne peut être modifié
            echo "<tr><td>".$rq_exc->sta_nom."</td><td></td></tr>";
        } else {
        ?>
        <TR>
        <TD><input id='<?php echo $rq_exc->sta_id?>' value="<?php echo $rq_exc->sta_nom?>"  type="text"></TD>
        <td><img src="/images/voir.png" alt='<?php echo $rq_exc->sta_id?>'></td>
        </TR>
        <?php
        }
    }
    ?>
</table>
<button onclick="modif('',15,'','nom','0');setTimeout(function(){charge('exclus','','panneau_g')},300);">Nouvelle exclusion</button>