<?php
session_start();
//page non utilisé pour l'instant, copié de exclus
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
$uti = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath . "mysql/connect.php";
connexobjet();
$req_exc = "SELECT * FROM Pseudos";
$r_exc = $idcom->query($req_exc);

//    modif(id,tb,vl,cp,my)
?>
<script>
    $(document).ready(function() {
        $('table#pseudos input').on('keydown', function(event) {
            if (event.which == 13) {
                if ($(this).attr('type') == 'text') {
                    if ($(this).hasClass('jaune')) {
                        modif($(this).attr('id').substr(1), 32, $(this).val(), 'nom', 1);
                    }
                }
            } else {
                $(this).addClass('jaune');
            }
        });
        $('table#pseudos img').click(function() {
            var id = $(this).attr('alt');
            charge('art_pseudos', id, 'panneau_d');
        });
    });
</script>
<img src="/images/aide.png" style="float:right" onclick="charge('aide',8,'panneau_d')">
<h3>Les pseudos articles</h3>
<h3><img src='/images/attention.png'> Voir l'aide avant modification</h3>
<table id='pseudos'>
    <TR>
        <TH>Valeur</TH>
        <th>En stock</th>
        <th>Remisable</th>
    </TR>
    <?php
    while ($rq_exc = $r_exc->fetch_object()) {
        $selS = ($rq_exc->pse_stock == 1) ? " selected" : "";
        $selR = ($rq_exc->pse_remise == 1) ? " selected" : "";
        ?>
        <TR>
            <TD><input id='P<?php echo $rq_exc->pse_id ?>' value="<?php echo $rq_exc->pse_nom ?>" type="text"></td>
            <TD><select onchange="modif(<?php echo $rq_exc->pse_id ?>,32,$(this).val(),'stock',1)">
                    <option value="0">Non</option>
                    <option value="1" <?php echo $selS ?>>Oui</option>
                </select></TD>
            <TD><select onchange="modif(<?php echo $rq_exc->pse_id ?>,32,$(this).val(),'remise',1)">
                    <option value="0">Non</option>
                    <option value="1" <?php echo $selR ?>>Oui</option>
                </select></TD>
            <td><img src="/images/voir.png" alt='<?php echo $rq_exc->pse_id ?>'></td>
        </TR>
        <?php
    }
    ?>
</table>
<button onclick="modif('',32,'','nom','0');setTimeout(function(){charge('pseudos','','panneau_g')},300);">Nouveau pseudo</button>
<script>
    $("#panneau_d").height($("#affichage").height() - 10);
</script>