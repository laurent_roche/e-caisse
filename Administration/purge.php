<?php
session_start();

if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$annee = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/config.php";
connexobjet();
$legal = date('Y')-7;
$test = '';
if ($annee == "") {
    unset($annee);
}
//recherche de la plus ancienne table
// echo "------------".$config['debut'];

echo $req_anc = "SELECT SUBSTRING(TABLE_NAME,-4) AS debut 
            FROM INFORMATION_SCHEMA.TABLES 
                WHERE TABLE_NAME LIKE '%Tickets%' 
                    ORDER BY TABLE_NAME 
                        LIMIT 0,1;";
$r_anc = $idcom->query($req_anc);
$rq_anc =$r_anc->fetch_object();
echo $debut = $rq_anc->debut;
$erreur = 0;
if ($debut < $legal) { //il y a des tables à supprimer
   echo $req_table = "SHOW TABLES LIKE '%".($debut)."'";
    $r_table = $idcom->query($req_table);
    $nb = $r_table->num_rows +1 ;
    if (isset($annee)) {
        if ($annee < $legal) {
            echo "<h3>Résultat du traitement</h3>";
            while ($row = $r_table->fetch_array(MYSQLI_NUM)) {
                if ($row[0] == "Stock_".($debut)) {
                    $row[0] = "Stock_".($debut -1);
                }
                echo "DROP TABLE ".$row[0]."<br>";
                $test=$idcom->query("DROP TABLE ".$row[0]);
                echo "Suppression de ".$row[0]."<br>";
                if (!$test) {
                    echo "Suppression de ".$row[0]." <b>Erreur n° ".$idcom->errno."</b> <em>".$idcom->error."</em><br>";
                    $erreur = 1;
                } else {
                    echo "Suppression de ".$row[0]."<br>";
                }
            }
            //mise à jour de la base de donnée si nécéssaire
            $idcom->query("UPDATE Config SET con_debut =if(con_debut <  $debut, $debut, con_debut)");
            //ecriture du fichier
            if ($idcom->affected_rows > 0) {
                file_get_contents($incpath.'regenere.php');
            }
        } ?>
        <h3>Purge de la base de données</h3>
        <?php
        if ($erreur == 1) {
            echo "<img src='/images/attention.png' Il y a eut des erreurs dans le traitement. Copier les erreurs ci-dessus pour le service technique.";
        } ?>
        La base de données à été mise à jour ainsi que la date début. Vous devez fermer toutes les fenêtres du navigateur pour que la modification soit prise en compte par l"application.
        <?php
        exit;
    }
}
//    modif(id,tb,vl,cp,my)
?>

<h3>Purge de la base de données</h3>
<?php
if (($legal) < ($config['debut'] - 7)) {
    echo "Il n'y aura pas de purge avant : ".($config['debut'] + 7);
} elseif (isset($nb)) {
    if($nb == 1) { //la table stock de l'année doit rester
        echo "<img src='/images/attention.png'> il n'y a pas de donnée à supprimer";
        exit;
    }
    ?>
    Les <?php echo $nb?> tables de l'année <?php echo($debut)?> peuvent être supprimées, cela permettra de retirer les articles non utilisés depuis cette date.
    <br><button onclick="charge('purge',<?php echo($debut)?>,'panneau_d')">Suppression de l'année <?php echo($debut)?></button>
    <?php
} else {
        echo "La base de données est à jour.";
    }
    ?>
<div id="aideP"></div>
