<?php
session_start();

if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$uti= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
connexobjet();
$req_exc="SELECT * FROM Mode_reglement ORDER BY mdr_ordre";
$r_exc=$idcom->query($req_exc);
$nb=$r_exc->num_rows;
$tab_dec=array("","Hebdomadaire","Mensuel");
// $tab_etat=array("","Actif","Désactivé");

require 'combo_codes.inc.php';
?>
<link rel='stylesheet' href='/bgrins-spectrum/spectrum.css' />
<script src='/bgrins-spectrum/spectrum.js'></script>
<script>
$("#mode_reglement .fond").spectrum({
    change: function(color) {
        col=color.toHex();
        or=$(this).attr('id');
        modif(or,19,col,'couleur',1);
        $('#'+or).parent().parent().css('backgroundColor','#'+col);
    }
});

$(document).ready(function() {
    $('table#mode_reglement input').on('keydown',function(event){
        if(event.which == 13){
            if($(this).attr('type') == 'text') {
                if ($(this).hasClass('jaune')) {
                    tab=$(this).attr('id').split('-');
                    modif(tab[1],19,$(this).val(),tab[0],1);
                }
            }
        } else {
            $(this).addClass('jaune');
        }
    });
        $('table#mode_reglement img').click(function(){
        var id = $(this).attr('alt');
        if ($(this).hasClass('supprimer') == true ) {
            charge('rayons','sup&id='+id,'panneau_d');
        } else {
            window.open('upload.php?req=reglement&id='+id);
        }
    });

});
</script>
<img src="/images/aide.png" style="float:right" onclick="charge('aide',4,'panneau_d')">

<h3>Modes de règlements</h3>
<table id='mode_reglement'>
  <TR>
    <TH>Valeur</TH><TH>Ab.</TH><!-- <th>Décaissement</th> --><th>Cor.</th><th>Ordre</th><th>État</th><th>Seuil</th><th>Code</th><th>Couleur</th><th>Droits</th><th></th>
  </TR>
<?php
while ($rq_exc=$r_exc->fetch_object()) {
    ?>
    <TR style='background-color:#<?php echo $rq_exc->mdr_couleur?>'>
        <TD><input id='nom-<?php echo $rq_exc->mdr_id?>' value="<?php echo $rq_exc->mdr_nom?>" type="text" style="width:90px"></TD>
        <TD><input id='abrege-<?php echo $rq_exc->mdr_id?>' class='pt' value="<?php echo $rq_exc->mdr_abrege?>" type="text"></TD>
        <!-- <TD>
            <select onchange="modif(<?php echo $rq_exc->mdr_id?>,19,this.value,'decaissement','1')">
            <?php
            for ($i=0;$i<3;$i++) {
                if ($i == $rq_exc->mdr_decaissement) {
                    $sel=" selected";
                } else {
                    $sel="";
                }
                echo "<option value='".$i."'".$sel.">".$tab_dec[$i]."</option>";
            } ?>
            </select>
            </TD> -->
            <td><input id='correction-<?php echo $rq_exc->mdr_id?>' type="text" class='pt' value="<?php echo $rq_exc->mdr_correction?>">
            </td>
            <td>
            <select onchange="modif(<?php echo $rq_exc->mdr_id?>,19,this.value,'ordre','1')">
            <option></option>
            <?php
            for ($i=1;$i<=$nb;$i++) {
                if ($i == $rq_exc->mdr_ordre) {
                    $sel=" selected";
                } else {
                    $sel="";
                }
                echo "<option".$sel.">".$i."</option>";
            }
            if ($rq_exc->mdr_etat == 1) {
                $sel1 = "selected";
                $sel2 = "";
            } else {
                $sel1 ="";
                $sel2 = "selected";
            } ?></select>
        </td>
        <td>
            <select onchange="modif(<?php echo $rq_exc->mdr_id?>,19,this.value,'etat','1')">
            <option value="0">0</option>
            <option value="1" <?php echo $sel1?>>Actif</option>
            <option value="2" <?php echo $sel2?>>Désactivé</option>
            </select>
        </td>
        <TD><input id='seuil-<?php echo $rq_exc->mdr_id?>' type='text' class="pt" value="<?php echo $rq_exc->mdr_seuil?>" class="cent">
        <TD>
            <select id="M_<?php echo $rq_exc->mdr_id?>" onchange="modif(<?php echo $rq_exc->mdr_id?>,19,this.value,'code','1')">
            <script>combo(<?php echo $rq_exc->mdr_id.','.$rq_exc->mdr_code?>,4)</script>
            </select>
        </TD>
        <TD><input type='text' value="#<?php echo $rq_exc->mdr_couleur?>" class="fond" id="<?php echo $rq_exc->mdr_id?>"></TD>
        <td><input id='droits-<?php echo $rq_exc->mdr_id?>' type='text' class="pt" value="<?php echo $rq_exc->mdr_droits?>"></td>
        <td>
        <?php
        if (file_exists($incpath."images/reglements/mdr_".$rq_exc->mdr_id.".png")) {
            echo "<img id='mdr_".$rq_exc->mdr_id."' alt='".$rq_exc->mdr_id."' height=32 src='/images/reglements/mdr_".$rq_exc->mdr_id.".png'";
        } else {
            echo "<img src='/images/rayons/inc.png' alt='".$rq_exc->mdr_id."'>";
        } ?></td>
    </TR>
    <?php
}
?>
</table>
<button onclick="modif('',19,'','nom','0');setTimeout(function(){charge('mode_reglement','','panneau_g')},300);">Nouveau mode</button>