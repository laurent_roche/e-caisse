<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
$req_afficheur = "SELECT * FROM Externe ORDER BY ext_id";
$r_afficheur = $idcom->query($req_afficheur);
// modif (id,tb,vl,cp,my)
?>
<script>
$(document).ready(function() {
    $('table#afficheurs input[type=text]').on('keydown',function(event){	
    $(this).addClass('jaune');
    
    if(event.which == 13){
            id=$(this).parent().parent().attr('id');
            if($(this).attr('type') == 'text'){
            modif(id,28,$(this).val(),$(this).attr('chp'),1);
            } else {
                alert($(this).attr('type'));
            }
        }
    });

    $('table#afficheurs td').on('click',function() {
        id=$(this).parent().attr('id');
        if($(this).children().attr('type') == 'checkbox') {
            if($(this).children().prop('checked') ==  true){
                modif(id,28,1,'auto','1');
            } else {
                modif(id,28,0,'auto','1');
            }
        } else {
            if($(this).children().attr('type') == 'img') {
                modif(id,28,0,'id','2');
                charge('afficheurs','','panneau_g');
            }
        }
    });
});
</script>
<h3>Les afficheurs externes</h3>
<table class="generique" id='afficheurs'><thead><th>id</th><th>ip afficheur</th><th>ip poste associé</th><th>Connexion auto</th><th style='width:25%'></th></thead>
<tbody>
<?php
while ($rq_afficheur =$r_afficheur->fetch_object()) {
    $check=($rq_afficheur->ext_auto == 1)?' checked':'';
    echo "<tr id='".$rq_afficheur->ext_id."'><td style='text-align:center;width:25%'>".$rq_afficheur->ext_id."</td>
    <td><input class='demi' type='text' chp='ip' value='".$rq_afficheur->ext_ip."'></td>
    <td><input class='demi' type='text'chp='poste' value='".$rq_afficheur->ext_poste."'></td>
    <td><input type='checkbox'".$check."></td>
    <td><img type='img' src='/images/supprimer.png'></td>
    </tr>";
}
?>
</tbody>
</table>
<button onclick="modif('',28,'0','auto','0');setTimeout(function(){charge('afficheurs','','panneau_g')},300);">Nouvel afficheur externe</button>