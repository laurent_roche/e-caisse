<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
require $incpath."mysql/connect.php";
connexobjet();
$req_soc="SELECT * FROM Societe";
$r_soc=$idcom->query($req_soc);
$rq_soc=$r_soc->fetch_object();
?>
<script>
$(document).ready(function() {
    $('table#config input').on('keydown',function(event){
        if(event.which == 13){
            if($(this).attr('type') == 'text') {
                if ($(this).hasClass('jaune')) {
                    modif(1,2,$(this).val(),$(this).attr('id'),1);
                }
            }
        } else {
            $(this).addClass('jaune');
        }
            
    });
});
</script>
<h3>Coordonnées de la société</h3>
<CENTER><table id='config'>
<tr><th>Nom</th><td><input type="text" id="nom" value="<?php echo $rq_soc->soc_nom?>"></td></tr>
<tr><th>Forme sociale</th><td>
    <select onchange="modif(1,2,this.value,'formesociale','1')">
        <option></option>
<?php
$req_form="SELECT * FROM Formesociale ORDER BY fos_id";
$r_form=$idcom->query($req_form);
while ($rq_form=$r_form->fetch_object()) {
    if ($rq_form->fos_id == $rq_soc->soc_formesociale) {
        $sel= "selected";
    } else {
        $sel ="";
    }
    echo "<option value='".$rq_form->fos_id."'".$sel.">".$rq_form->fos_nom."</option>\n";
}
?></select></td></tr>
<tr><th>Raison sociale</th><td><input type="text" id="raisonsociale" value="<?php echo $rq_soc->soc_raisonsociale?>"></td></tr>
<tr><th>Adresse1</th><td><input type="text" id="adr1" value="<?php echo $rq_soc->soc_adr1?>"></td></tr>
<tr><th>Adresse2</th><td><input type="text" id="adr2" value="<?php echo $rq_soc->soc_adr2?>"></td></tr>
<tr><th>Cp</th><td><input type="text" id="cp" value="<?php echo $rq_soc->soc_cp?>"></td></tr>
<tr><th>Ville</th><td><input type="text" id="ville" value="<?php echo $rq_soc->soc_ville?>"></td></tr>
<tr><th>Pays</th><td><input type="text" id="pays" value="<?php echo $rq_soc->soc_pays?>"></td></tr>
<tr><th>Capital</th><td><input type="text" id="capital" value="<?php echo $rq_soc->soc_capital?>" onchange="modif(1,2,this.value,'','1')"></td></tr>
<tr><th>Rcs</th><td><input type="text" id="rcs" value="<?php echo $rq_soc->soc_rcs?>"></td></tr>
<tr><th>tel</th><td><input type="text" id="tel" value="<?php echo $rq_soc->soc_tel?>"></td></tr>
<tr><th>Fax</th><td><input type="text" id="fax" value="<?php echo $rq_soc->soc_fax?>"></td></tr>
<tr><th>Siret</th><td><input type="text" id="siret" value="<?php echo $rq_soc->soc_siret?>"></td></tr>
<tr><th>Ape</th><td><input type="text" id="ape" value="<?php echo $rq_soc->soc_ape?>"></td></tr>
<tr><th>Banque</th><td><input type="text" id="banque" value="<?php echo $rq_soc->soc_banque?>" onchange="modif(1,2,this.value,'','1')"></td></tr>
<tr><th>RIB</th><td><input type="text" id="rib" value="<?php echo $rq_soc->soc_rib?>"></td></tr>
<tr><th>Clé</th><td><input type="text" id="cle" value="<?php echo $rq_soc->soc_cle?>"></td></tr>
<tr><th>TVA</th><td><input type="text" id="tva" value="<?php echo $rq_soc->soc_tva?>"></td></tr>
<tr><th>IBAN</th><td><input type="text" id="iban" value="<?php echo $rq_soc->soc_iban?>"></td></tr>
<tr><th>BIC</th><td><input type="text" id="bic" value="<?php echo $rq_soc->soc_bic?>"></td></tr>
</table>
<button onclick="charge('formesociale','','panneau_d');">Les formes sociales</button></CENTER>

<script>
    var w=$(window).height();
    $("#affichage").height(w -40);
    $("#panneau_g").height($('#affichage').height());
</script>
