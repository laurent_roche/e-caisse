<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
/*
Validation du panier en mode différé ou mise en compte:
création d'une ligne dans Resume_ticket_$an
| rst_id          | int(11)       | NO   |     | 0       |       |
| rst_num         | int(4)        | NO   |     | 0       |       |
| rst_utilisateur | tinyint(2)    | NO   |     | 0       |       |
| rst_etat        | varchar(1)    | NO   |     | 0       |       | mode de reglement
| rst_total       | decimal(18,2) | NO   |     | 0.00    |       |
| rst_validation  | datetime      | YES  |     | NULL    |       |
| rst_mod         | enum('0','1') | NO   |     | 0       |       |
recupération de l'id et update de tickets_$an, cet id remplaçant le numero du panier

*/
$du= filter_input(INPUT_GET, "du", FILTER_SANITIZE_STRING); //valeur du ticket
$vl= filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);//valeur reglée
$mode= filter_input(INPUT_GET, "md", FILTER_SANITIZE_STRING);

require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
// on vérifie si la valeur envoyé correspond au calcul sur le serveur. Le ticket à pu être modifié depuis un autre poste

echo "<br>1".$req_valeur="SELECT SUM(tic_tt) AS vlT FROM Tickets_".ANNEE." WHERE tic_num =".$_SESSION['panier_'.$_SESSION[$dossier]];
$r_valeur=$idcom->query($req_valeur);
$rq_valeur=$r_valeur->fetch_object();
if ($rq_valeur->vlT != $du) {
    ?>
    <script>
    charge('erreurs',1,'references');
    </script>
    <?php
    exit;
}
/*

- pour les reglements en espèce ou en différé, on ne tient compte que du montant dû recalculé sur le serveur
- pour les autres :
si le magasin est habilité à recevoir des dons :
  si la valeur donnée n'est pas égale à la valeur dûe,
    on ajoute le pseudo-article don/echange avec la valeur dans le ticket
s'il ne peut recevoir des dons
    on ajoute l'article le pseudo-article don/echange avec la valeur dans le ticket
    et on créer un nouveau ticket avec le pseudo-article don/echange en sortie de caisse avec la valeur et qt -1

recherche du dernier ticket enregistré pour incrémenter le numéro journalier

*/
/*echo "<br>2".*/$req_numero="SELECT rst_id FROM Resume_ticket_".ANNEE." WHERE DATE(rst_validation) = DATE(NOW())";
$r_numero = $idcom->query($req_numero);
$rq_numero = $r_numero->num_rows + 1;

if ($_SESSION['don']==1) {//accepte les dons sauf si c'est un règlement en caisse ou différé
    if (($vl != $du)&&(($mode != 1)&&($mode != 5))) {
        $diff = $vl - $du;
        $du = $vl;
        // 		echo 'ici';
        // 		exit;
        // 	insertion du pseudo-article $_SESSION['articledon']
        echo "<br>3".$req="INSERT INTO Tickets_".ANNEE." (tic_num, tic_article, tic_quantite, tic_quantiteS, tic_prix, tic_prixS, tic_pht, tic_tva, tic_ntva) VALUES(".$_SESSION['panier_'.$_SESSION[$dossier]].",".$_SESSION['articledon'].",1,1,$diff,$diff,$diff, 0.00, 0)";
    
        $res=$idcom->query($req);
        if (!$res) {
            ?>
            <script>$('#mysql').css('visibility','visible')</script>
            <?php
            echo $idcom->errno." ".$idcom->error;
            echo "<br>".$req;
        }
    }
}
if (($mode == 1)||($mode != 5)) {//caisse et différé
    echo	"<br>4".$req_insert="INSERT INTO Resume_ticket_".ANNEE." (rst_utilisateur,rst_num, rst_etat, rst_total, rst_validation) VALUES($_SESSION[$dossier],$rq_numero,$mode,$du,NOW())";
    $idcom->query($req_insert);
    /*echo " <br>erreur ".*/$idcom->errno." ".$idcom->error;
    /*echo	"<br>insert ".*/$id_insert = $idcom->insert_id;
} else {
    echo	"<br>5".$req_insert="INSERT INTO Resume_ticket_".ANNEE." (rst_utilisateur,rst_num, rst_etat, rst_total, rst_validation) VALUES($_SESSION[$dossier],$rq_numero,$mode,$vl,NOW())";
    $idcom->query($req_insert);
    /*echo " <br>erreur ".*/$idcom->errno." ".$idcom->error;
    /*echo	"<br>insert ".*/$id_insert = $idcom->insert_id;
}

echo	"<br>6".$req_insert="UPDATE Tickets_".ANNEE." SET tic_num = ".$id_insert." WHERE tic_num =".$_SESSION['panier_'.$_SESSION[$dossier]];
$idcom->query($req_insert);
//on retire les articles du stock si ce ne sont pas des pseudoarticles : 1,2,3,4,2797,4389,4390,5060,5193
echo	"<br>7".$req_stock="UPDATE Articles JOIN Tickets_".ANNEE." ON art_id = tic_article SET art_stock =  art_stock - tic_quantite WHERE art_pseudo = 1 AND tic_num = ".$rq_numero;
$idcom->query($req_stock);

unset($_SESSION['panier_'.$_SESSION[$dossier]]);

// header("location:liste_paniers.php");

?>
