<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
/*
les articles sont stockés dans la table Tickets_$an avec dans le champ tic_num, le n° de l'utilisateur suivi du n° du panier
Au moment du règlement, une ligne est ajoutée à Resume_ticket_$an, on met à jour les numéros panier avec cet ID et on ajuste les stocks.
Pour les panier mis en comptes, c'est la date du jour(timestamp) qui prend la palce du numéro du ticket
On incrémente le panier ou on récupère un panier en attente.
Le panier en cours d'utilisation est dans la variable $_SESSION['panier_'.$_SESSION[$dossier]].
Tous les boutons reglements, sélections etc se réfèrent à cette variable
*/
require_once $incpath . "mysql/connect.php";
require_once $incpath . "php/fonctions.php";
$req = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
if ($req == 'impression') {
    unset($_SESSION['impression_' . $_SESSION[$dossier]]);
}
// print_r($config);
connexobjet();
$req_panier = "SELECT Vt1_nom AS tit_nom,
                    tic_id, 
                    tic_prix, 
                    tic_prixS, 
                    tic_quantite, 
                    tic_tva,
                    tic_ntva, 
                    tic_num, 
                    art_unite,
                    ray_secteur,
                    art_stk,
                    art_ttc,
                    art_pht,
                    tic_tt,
                    unv_abrege,
                    pse_remise,
                    pro_valeur
                        FROM Tickets_" . ANNEE . " 
                            JOIN Vtit1 ON Vt1_article = tic_article 
                            JOIN Articles ON art_id = tic_article
                            JOIN Rayons ON ray_id = art_rayon
                            JOIN Unites_vente ON unv_id = art_unite
                            JOIN Pseudos ON pse_id = art_pseudo
                            LEFT JOIN Promo ON pro_article = art_id
                                WHERE tic_num LIKE '" . (3155 - $_SESSION[$dossier]) . "%' 
                                    AND LENGTH(tic_num)=9 
                                    ORDER BY tic_id DESC";

$r_panier = $idcom->query($req_panier);

$nb = $r_panier->num_rows; //contient l'ensembles des paniers, seul l'actif sera affiché. Cela permet de gérer le bouton de déconnexion
if ($nb == 0) {
    ?>
    <script>
        $('#deconnexion').css('visibility', 'visible');
        <?php
}
?>
    </script>
    <table id="tableTicket" class="pagination" number-per-page="6" current-page="0">
        <thead>
            <tr>
                <th style="width:40px"></th>
                <th id='nom'>Articles (<span id='unites'></span>)</th>
                <th style="width:80px">Prix&nbsp;U.&nbsp;TTC</th>
                <th style="width:100px" colspan="3">Qt.</th>
                <th style="width:80px">
                    <center>Tot. TTC</center>
                </th>
                <th style="width:50px"></th>
            </tr>
        </thead>
        <tbody>
            <?php
            $n_ligne = 1;
            $ligne_reelle = 0; //pour permettre l'affichage dans le cas d'une seule ligne négative
            $n_article = 1;
            $nb_unite = 0;
            $total = 0;
            $diff_prix = '';
            $prix_or = '';
            while ($rq_panier = $r_panier->fetch_object()) {
                if ($n_ligne % 2 == 0) {
                    $coul = $coulCC;
                    if ($rq_panier->tic_prix != $rq_panier->tic_prixS) {
                        $diff_prix = " style='background-color:Bisque'";
                        $prix_or = "<br><span class='pt8'>" . $rq_panier->tic_prixS . "</span>";
                    }
                } else {
                    $coul = $coulFF;
                    if ($rq_panier->tic_prix != $rq_panier->tic_prixS) {
                        $diff_prix = " style='background-color:Orange'";
                        $prix_or = "<br><span class='pt8'>" . $rq_panier->tic_prixS . "</span>";
                    }
                }
                //--------------------------------------------------------------------------------------------------
                //bouton négatif
                if ($rq_panier->tic_quantite == 1) { //insertion de l'article, possibilité de faire un retour
                    $retourn = 'R';  //affichage
                    $retournV = 'R'; //valeur
                } else {
                    $retourn = '-';
                    $retournV = 'moins';
                }
                //--------------------------------------------------------------------------------------------------
                //bouton positif
                if ($rq_panier->tic_quantite == -1) {
                    $retourpV = '1';
                } else {
                    $retourpV = 'plus';
                }
                if ($rq_panier->tic_prix == '0.00') { //c'est un pseudo article ou on a oublié de lui mettre un prix
                    include 'prix_pseudo.php';
                    exit;
                }
                //--------------------------------------------------------------------------------------------------
                if ($rq_panier->ray_secteur == 2) {
                    $class = "class='livre'";
                    // 	prix TTC/(1 + taux de TVA)
                    $prix_minimum = $rq_panier->art_ttc / (1 + ($rq_panier->tic_tva / 100));
                } else {
                    $class = "";
                    $prix_minimum = $rq_panier->art_pht * (1 + ($rq_panier->tic_tva / 100));
                }
                //--------------------------------------------------------------------------------------------------
                if ($rq_panier->art_ttc == 0.00) { //c'est un article sans prix de base, on désactive le changement de prix
                    $changement = ''; //\"$('#references').html("Cet article n'est pas remisable")\"
                } elseif ($rq_panier->pse_remise == 0) {
                    $changement = ' onclick="$(\'#references\').html(\' <b>Pas de remise possible sur cette article</b>\')"';
                } elseif ($rq_panier->pro_valeur > 0) {
                    $changement = ' onclick="$(\'#references\').html(\' <b>Pas de remise sur les articles en promotion ou solde</b>\')"';
                } else {
                    $changement = ' onclick="voir_clavierp(' . $rq_panier->tic_id . ',' . $prix_minimum . ')"';
                }
                //--------------------------------------------------------------------------------------------------
                if ($rq_panier->tic_quantite == '0.00') { //c'est un article au poids
                    include 'quantite_poids.php';
                    exit;
                }
                //--------------------------------------------------------------------------------------------------

                if ($rq_panier->tic_num == $_SESSION['panier_' . $_SESSION[$dossier]]) {
                    if ($rq_panier->tic_quantite > 0) { //on ne comptabilise pas les retours pour l'affichage des quantité dans la ligne de titre
                        if ($rq_panier->art_unite == 1) {
                            $quantite = $rq_panier->tic_quantite;
                        } else {
                            $quantite = 1;
                        }
                        $nb_unite += $quantite;
                        $n_article++;
                        $td_dup =  "<td class='sup' onclick=\"charge('update','" . $rq_panier->tic_id . "&qt=sup','panier')\">" . $n_ligne . "</td>";
                    } else {
                        $td_dup =  "<td class='sup' onclick=\"charge('update','" . $rq_panier->tic_id . "&qt=sup','panier')\"></td>";
                        $n_ligne--;
                    }
                    //echo $rq_panier->art_unite;
                    //------------------unités-----------------
                    if ($rq_panier->art_unite == 1) { //1 = non divisible, bouton plus et moins
                        echo '<tr id="' . $rq_panier->tic_id . '" style="background-color:' . $coul . '">' . $td_dup . '
            <td ' . $class . '><div class="libelle"><span class="stock">( ' . sprintf("%d", $rq_panier->art_stk) . ') </span>' . $rq_panier->tit_nom . '</div> </td>
            <td ' . $diff_prix . ' class="align_d" alt="' . $rq_panier->tic_prixS . '" id="prix_' . $rq_panier->tic_id . '"' . $changement . ' >' . monetaireF($rq_panier->tic_prix) . $prix_or . '</td>
            <td class="align_d"><button class="plus" onclick="charge(\'update\',\'' . $rq_panier->tic_id . '&qt=' . $retournV . '\',\'panier\')">' . $retourn . '</button></td>
            <td class="align_d" unite="' . $rq_panier->art_unite . '" alt="' . sprintf("%d", $rq_panier->tic_quantite) . '" id="quantite_' . $rq_panier->tic_id . '">' . sprintf("%d", $rq_panier->tic_quantite) . '</td>
            <td><button class="plus" style="margin-left:10px" onclick="charge(\'update\',\'' . $rq_panier->tic_id . '&qt=' . $retourpV . '\',\'panier\')">+</button></td>
            <td class="align_d">' . monetaireF($stotal = $rq_panier->tic_tt) . '</td>
            <td class="edit" onclick="voir_clavierqt(' . $rq_panier->tic_id . ')"></td></tr>';
                    } else { //pas de plus et moins, demande de quantité et retour

                        $retourn = 'R';
                        $retournV = 'R';


                        echo '<tr style="background-color:' . $coul . '">' . $td_dup . '
            <td ' . $class . '><div class="libelle"><span class="stock">( ' . $rq_panier->art_stk . ') </span>' . $rq_panier->tit_nom . '</div> </td>
            <td ' . $diff_prix . ' class="align_d" alt="' . $rq_panier->tic_prixS . '" id="prix_' . $rq_panier->tic_id . '"' . $changement . ' >' . monetaireF($rq_panier->tic_prix) . $prix_or . '</td>';
                        if ($rq_panier->tic_quantite > 0) {
                            echo '<td class="align_d"><button class="plus" onclick="charge(\'update\',\'' . $rq_panier->tic_id . '&qt=R\',\'panier\')">R</button></td>';
                        } else {
                            echo '<td></td>';
                        }
                        echo '<td class="saisie" unite="' . $rq_panier->art_unite . '" alt="' . $rq_panier->tic_quantite . '" id="quantite_' . $rq_panier->tic_id . '" onclick="voir_clavierqt(' . $rq_panier->tic_id . ')">' . $rq_panier->tic_quantite . ' ' . $rq_panier->unv_abrege . '</td>
            <td></td>
            <td class="align_d">' . monetaireF($stotal = $rq_panier->tic_tt) . '</td>
            <td></td></tr>';
                    }
                    //------------------ fin de unités-----------------
                    $total = $stotal + $total;
                    $diff_prix = '';
                    $prix_or = '';
                    $n_ligne++;
                    $ligne_reelle++;
                }
            }
            ?>
        </tbody>
    </table>
    <?php
    // print_r($_SESSION);
    if ($ligne_reelle == 0) { //pas d'article dans ce panier
        $total = '0.00';
        //bouton de déconnexion visible
        ?>
        <script>
            $('.reglem').css('visibility', 'hidden');
            $('#nouveau_panier').css('visibility', 'hidden');
            $('#b_compte').attr('alt', 'comptes');
            $('.pagination').css('visibility', 'hidden');
            var num = 0.00;
            var n = num.toFixed(2);
            $('#prix').html(n);
            charge('cb', '', 'references');
        </script>
        <?php
        exit;
    } else {
        ?>
        <script>
            $('.reglem').css('visibility', 'visible');
            $('#b_compte').attr('alt', 'panier');
            $('#deconnexion').css('visibility', 'hidden');
            $('#nouveau_panier').css('visibility', 'visible');
            $('#mode').css('visibility', 'visible');
            $('#references').css('visibility', 'visible');
            $('.panier_actif').attr('alt', <?php echo $nb ?>);
            $('.libelle').width($('#nom').width());
            <?php
            $nb = $n_article - 1;
            include 'tableTicketjs.php'; ?>
            //----------------------------------------------------------------------------------------------------------
            var act = 'prix';

            function action(ac) {
                $('#n_prix').html('');
                $('#remise').html('');
                if (ac == 'remise') {
                    $('#remise').css('backgroundColor', '<?php echo $_SESSION['surligne_' . $_SESSION[$dossier]] ?>');
                    $('#n_prix').css('backgroundColor', 'white');
                    act = 'remise';
                } else {
                    $('#n_prix').css('backgroundColor', '<?php echo $_SESSION['surligne_' . $_SESSION[$dossier]] ?>');
                    $('#remise').css('backgroundColor', 'white');
                    act = 'prix';
                }
            }
            //----------------------------------------------------------------------------------------------------------
            function voir_clavierp(id, min) {
                $('#prix_' + id).css('backgroundColor', '<?php echo $_SESSION['surligne_' . $_SESSION[$dossier]] ?>');
                $('#references').html("<div id='limit'>Prix minimum imposé en dehors de soldes : <span id=\"min\">" + min.toFixed(2) + "</span> €</div>");
                $('#references').append(n_prix);
                $('#references').append(clavierp);
                $('#n_prix').css('backgroundColor', '<?php echo $_SESSION['surligne_' . $_SESSION[$dossier]] ?>');

                $('#keypad').css('top', 140);
                $('#keypad').css('display', 'block');

                $("#keypad .key").click(function() {

                    if ($(this).html() == 'Annuler') {
                        $('#prix_' + id).css('backgroundColor', 'transparent');
                        $("#references").empty();
                        charge('cb', '', 'references');
                    } else if ($(this).html() == 'Valider') {
                        charge('update', id + '&px=' + $('#n_prix').html(), 'panier');
                    } else if ($(this).html() == 'Effacer') {
                        $('#n_prix').html('');
                        $('#remise').html('');
                    } else {
                        if (act == 'remise') {
                            $("#remise").append($(this).html());
                            p = $('#prix_' + id).attr('alt');
                            pp = p - (p * $("#remise").html() / 100);
                            $('#n_prix').html(pp.toFixed(2));
                        } else if (act == 'prix') {
                            $("#n_prix").append($(this).html());
                            p = $('#prix_' + id).attr('alt');
                            pp = 100 - ($("#n_prix").html() * 100 / p);
                            $('#remise').html(pp.toFixed(2));
                        }
                    }
                    //    le bouton n'est visible que si la valeur est supérieur au minimum  
                    if (+$('#n_prix').html() >= +$('#min').html()) {
                        $('#valider').css('visibility', 'visible');
                    } else $('#valider').css('visibility', 'hidden');
                })
            }
            //----------------------------------------------------------------------------------------------------------
            function voir_clavierqt(id) {
                $("#quantite_" + id).html("");
                $("#quantite_" + id).css('backgroundColor', '<?php echo $_SESSION['surligne_' . $_SESSION[$dossier]] ?>')
                //suivant unité de vente
                if ($("#quantite_" + id).attr('unite') == 1) $('#references').html(clavierqt);
                else $('#references').html(clavierp);

                $('#keypad').css('top', 50);
                $('#keypad').css('display', 'block');

                $("#keypad .key").click(function() {
                    if ($(this).html() == 'Valider') {
                        charge('update', id + '&qt=' + $('#quantite_' + id).html(), 'panier');
                    } else if ($(this).html() == 'Annuler') {
                        $('#quantite_' + id).html($('#quantite_' + id).attr('Alt'));
                        $('#quantite_' + id).css('backgroundColor', 'transparent');
                    } else if ($(this).html() == 'Effacer') {
                        $('#quantite_' + id).html('');
                    } else if ($(this).html() == '0') {
                        if (($('#quantite_' + id).html() == '') && ($("#quantite_" + id).attr('unite') == 1)) {
                            $('#references').html('<h1>Il ne peut y avoir une quantité 0; veuillez supprimer la ligne.</h1>');
                            $('#quantite_' + id).html($('#quantite_' + id).attr('Alt'));
                        } else $("#quantite_" + id).append($(this).html());
                    } else {
                        $("#quantite_" + id).append($(this).html());
                        $('#valider').css('visibility', 'visible');
                    }
                });
            }
            //----------------------------------------------------------------------------------------------------------

            $('.page').click(function() {
                $('#keypad').css('display', 'none')
            });

            $('#unites').html('<?php echo ($n_article - 1) . "/" . $nb_unite ?>');

            var num = <?php echo $total ?>;
            var n = num.toFixed(2);
            $('#prix').html(n);
            charge('cb', '', 'references');
        </script>
        <?php
    }
