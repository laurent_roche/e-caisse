<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$det= filter_input(INPUT_GET, "det", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$sup= filter_input(INPUT_GET, "sup", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
if ($sup) {
    $idcom->query("DELETE FROM Comptes WHERE cpt_id=$sup");
    echo '<h1>Le compte à bien été supprimé</h1>';
    ?>
    <script>
    $('#C_<?php echo $sup?>').empty();
    </script>
    <?php
    exit;
}
if ($req == "000") {//creation de compte
    $insert=" INSERT INTO Comptes (cpt_nom) VALUE ('NOUVEAU')";
    $r_comptes=$idcom->query($insert);
    $req=$idcom->insert_id;
    include 'detail_compte.inc.php';
    exit;
}
if ($det) {//le compte a été choisi on affiche le détail
    include 'detail_compte.inc.php';
    exit;
}
 else {
    $req_comptes="SELECT Comptes.*, COUNT(tic_article) AS nb_article FROM Comptes LEFT JOIN Tickets_".ANNEE." ON tic_cp = cpt_id WHERE cpt_nom LIKE '".$req."%' GROUP BY cpt_id ORDER BY cpt_nom";
    $r_comptes=$idcom->query($req_comptes);
    $nb = $r_comptes->num_rows;
    if ($idcom->error) {
        echo "<br>".$idcom->errno." ".$idcom->error."<br>";
    }
    $n = 0;
    echo '<table id="tableCompte" class="pagination" number-per-page="6" current-page="0"><thead><tr><th>Remise</th><th>Nom</th><th>Ville</th><th>Adresse</th><th>Art. cp</th><th></th></thead><tbody>';
    while ($rq_comptes=$r_comptes->fetch_object()) {
        $coul=($n%2 == 0)?$coulCC:$coulFF;
        if ($rq_comptes->nb_article > 0) {
            $voir="class='voirC' onclick=\"charge('detail_articlescompte',".$rq_comptes->cpt_id.",'references')\"";
        } else {
            $voir='';
        }
        if ($rq_comptes->cpt_remise == 0) {//il n'y a pas de remise, on peut mettre en compte directement sinon il faut passer par l'édition pour calculer la remise
            $remise = '';
        } else { 
            $remise = '<span style="font-size:15px;">'.$rq_comptes->cpt_remise.'&nbsp%</span>';
        }    
        ?>
        <tr style='background-color:<?php echo $coul?>'><td><?php echo $remise?></td>
            <TD><?php echo $rq_comptes->cpt_nom?></TD>
            <TD><?php echo $rq_comptes->cpt_ville?></TD>
            <TD><?php echo $rq_comptes->cpt_adr1?></TD>
            <td <?php echo $voir?> onclick="charge('detail_articlescompte',<?php echo $rq_comptes->cpt_id?>,'references')"></td>
            <td class="edit" onclick="charge('detail_compte','&det=<?php echo $rq_comptes->cpt_id?>','panier')"></td>
        </tr>
        <?php
        $n++;
    }
    echo '</tbody></table>';
}
?>
<script>
nb_ligne = Math.floor($("#affichage").height()/70);

if (nb_ligne == <?php echo ($nb)?>){
hauteur_td = ($("#affichage").height()-40)/(nb_ligne);
$("#tableCompte td").height(hauteur_td);
}
//on tient compte de la place des boutons de navigation
if (nb_ligne <= <?php echo ($nb -1)?>){
// alert(nb_ligne)
$("#tableCompte").attr('number-per-page',nb_ligne -1);
hauteur_td = ($("#affichage").height()-40)/(nb_ligne);
$("#tableCompte td").height(hauteur_td);
$("#tableCompte").pagination();
$('.pager').css('margin-top',20);
}</script>


