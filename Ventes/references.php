<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$ref= filter_input(INPUT_GET, "ref", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$lng= filter_input(INPUT_GET, "lng", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();

/*
on recherche toutes les références internes (tit_niveau = 3) en groupant sur le premier caractère
le clic sur le bouton obtenu affiche la liste dans le panneau gauche
*/
$n_panier=$_SESSION['panier_'.$_SESSION[$dossier]];

$req_references="SELECT Vt3_article, LEFT(Vt3_nom ,1) AS ref, Vt3_nom, COUNT(Vt3_article) AS ct FROM Vtit3 WHERE Vt3_nom LIKE '".$ref."%' GROUP BY ref ORDER BY ref";

$r_references=$idcom->query($req_references);
if ($idcom->error) {
    echo "<br>".$idcom->errno." ".$idcom->error."<br>";
}
$nb = $r_references->num_rows;

echo "<h3>Articles par code</h3>";
while ($rq_references=$r_references->fetch_object()) {
    if ($ref != $rq_references->ref) {
        ?>
        <button class="boutref" onclick="charge('divers_requete','&ref=<?php echo $rq_references->ref?>','panier')"><?php echo $rq_references->ref?></button>
        <?php
        $ref=$rq_references->ref;
    }
}
?>

