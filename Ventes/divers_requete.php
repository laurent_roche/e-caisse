<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$ref= filter_input(INPUT_GET, "ref", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();

if ($req!='') {
    $requete="SELECT Vt1_nom AS tit_nom,
                    art_id,
                    art_ttc,
                    art_pseudo,
                    art_cb,
                    art_unite, if (unv_abrege != '',CONCAT('0.00 ',unv_abrege),'') AS unv_abrege,
                    aff_nom,
                    Vt3_nom
                        FROM Articles 
                        JOIN Vtit1 ON Vt1_article = art_id 
                        LEFT JOIN Vtit3 ON Vt3_article = art_id 
                        JOIN Unites_vente ON art_unite = unv_id 
                        JOIN Favoris ON fav_article = art_id 
                        JOIN Affichage ON aff_id = fav_parent 
                            WHERE fav_parent =".$req." 
                                ORDER BY Vt3_nom, Vt1_nom";
} elseif ($ref!='') {
    $requete="SELECT art_id,
                    art_cb,
                    art_pseudo,
                    CONCAT(Vt1_nom,' / ',Vt3_nom) AS tit_nom,
                    art_ttc,
                    art_unite,
                    if (unv_abrege != '',CONCAT('0.00 ',unv_abrege),'') AS unv_abrege
                     FROM Vtit3
                     JOIN Articles ON art_id = Vt3_article
                     JOIN Vtit1 ON Vt1_article = Vt3_article
                     JOIN Unites_vente ON art_unite = unv_id
                      WHERE Vt3_nom LIKE '".$ref."%' 
                      AND art_statut < 3 
                        ORDER BY Vt3_nom";
} else {
    echo "erreur interne";
}
$r_cartes=$idcom->query($requete);
// echo $idcom->errno." ".$idcom->error;
$rq_cartes=$r_cartes->fetch_object();
$titre = '';//$rq_cartes->aff_nom;
$r_cartes->data_seek(0);
if ($idcom->error) {
    echo "<br>".$idcom->errno." ".$idcom->error."<br>";
}
$nb = $r_cartes->num_rows;
if ($nb == 1) {//insertion direct de l'article
    $rq_cartes = $r_cartes->fetch_object()
    ?>
    <script>charge('insert','&cb=<?php echo $rq_cartes->art_id?>','panier');</script>
    <?php
    exit;
}
// $rq_cartes=$r_cartes->fetch_object();
?>
<table id="tableTicket" class="pagination" number-per-page="6" current-page="0">
    <thead>
        <TR><TH colspan="2"><?php echo $titre?></TH></TR>
    </thead>
<?php
$n=0;
$art_pseudoP = 0;
$art_pseudoQT = '';
$abrege = '';
while ($rq_cartes=$r_cartes->fetch_object()) {
    $coul = ($n % 2 == 0)? $coulCC:$coulFF;
    $prix = ($rq_cartes->art_ttc == 0.00)?'Prix à définir':$rq_cartes->art_ttc." €";
    $img = (file_exists($incpath."Saisie/images/".$rq_cartes->art_cb.".png"))?$incpath."Saisie/images/".$rq_cartes->art_cb.".png":$incpath."Saisie/images/bord.png";
    $larg = getimagesize($img)[0];
    $comp ="style=\"padding-left:".$larg."px;background-image:url(".$img.")";
    
    echo "<tr style='background-color:".$coul."'>";//ligne
    echo "<td class='abbaye' ".$comp."\">".$rq_cartes->tit_nom." (<i>".$prix."</i>)";//titre article
    //article à l'unité
    if ($rq_cartes->art_unite == 1) {
        $abrege = '';
        if ($rq_cartes->art_ttc == 0.00) {//prix à définir
            echo "</td>
            <td unite='".$rq_cartes->art_unite."' onclick='voir_clavierpi(".$rq_cartes->art_id.")' class='saisie' id='".$rq_cartes->art_id."' pseudo='".$rq_cartes->art_pseudo."'><span style='color:silver'>Valeur</span></td>";
            $art_pseudoP = 1;//gestion des claviers virtuels
        } else {
            // echo "<td class='abbaye' ".$comp."\">".$n."-2/".$rq_cartes->tit_nom." (<i>".$prix."</i>)";
            echo "<button class='plus1' onclick='plus1(".$rq_cartes->art_id.")'>+</button>";
            echo "</td><td unite='".$rq_cartes->art_unite."' onclick='voir_clavierqt(".$rq_cartes->art_id.")' class='saisie' id='".$rq_cartes->art_id."' pseudo='".$rq_cartes->art_pseudo."'>".$abrege."</td>";
            $art_pseudoQT = 0;//gestion des claviers virtuels
        }
    } else { //article à la quantité
        echo "<td unite='".$rq_cartes->art_unite."' onclick='voir_clavierpi(".$rq_cartes->art_id.")' class='saisie' id='".$rq_cartes->art_id."' pseudo='".$rq_cartes->art_pseudo."'><span style='color:silver'>Quantité</span></td>";
        $art_pseudoP = 1;//gestion des claviers virtuels
    }
    echo "</tr>";
    $n++;
}
?>
</table>
<script type="text/javascript" src="/js/simplepagination.js"></script>
<script>
<?php
require 'tableTicketjs.php';
if ($art_pseudoQT == 0) {
    ?>
min = 'test';
function plus1(id){
    $('#'+id).html((+$('#'+id).html())+1);
    $('#'+id).addClass('calcul');
    voir_clavierqt();
    }
    
function voir_clavierqt(id){
        $("#"+id).html("");
        $( ".saisie" ).each(function() {
            $( ".saisie" ).css("backgroundColor","white");
            });
        $("#"+id).css("backgroundColor","<?php echo $_SESSION['surligne_'.$_SESSION[$dossier]]?>");
        $('#'+id).addClass('calcul');
        
        if ($('#'+id).attr('unite') == 1)$('#references').html(clavierqt);
        else $('#references').html(clavierp);
        
        $('#keypad').css('top',50);
        $('#keypad').css('display','block');
        
        $("#keypad .key").click(function(){
        if ($(this).html() == 'Valider'){
            var tableauCarte="";
            $(".calcul").each(function(){ 
            if ($(this).html() != 0){
                tableauCarte=tableauCarte+$(this).attr('id')+"-"+$(this).html()+"-"+$(this).attr('pseudo')+",";
                }
                });
            charge('insert',"&carte="+tableauCarte,'panier');
            }
        else if ($(this).html() == 'Annuler'){
            $('#'+id).html($('#'+id).attr('Alt'));
            }
        else if ($(this).html() == 'Effacer'){
            $('#'+id).html('');
            }
            
        else if (($(this).html() == '0')&&($(this).attr('unite') == 1)){
            if ($('#'+id).html() == ''){
                $('#references').html('<h1>Il ne peut y avoir une quantité 0.</h1>');
                $('#'+id).html($('#'+id).attr('Alt'));
            } else $("#"+id).append($(this).html());
        } else $("#"+id).append($(this).html());
        $('#valider').css('visibility', 'visible');
    });
    //gestion du bouton valider
    var tt='';
    $( ".saisie" ).each(function() {
    tt +=  $(this).text();
    });
    if (tt =='')$('#valider').css('visibility', 'hidden');
    else $('#valider').css('visibility', 'visible');
    }

<?php
}

if ($art_pseudoP == 1) {
    ?>
    function voir_clavierpi(id) {
        $(".saisie" ).each(function() {
            $(".saisie" ).css("backgroundColor","white");
            });
        $('#'+id).empty();
        $('#'+id).css('backgroundColor','<?php echo $_SESSION['surligne_'.$_SESSION[$dossier]]?>');
        $('#'+id).addClass('calcul');
        $('#references').empty();
        $('#references').html(clavierp);
        $('#keypad').css('top',50);
        $('#keypad').css('display','block');
        $("#keypad .key").click(function(){
        
            if ($(this).html() == 'Valider'){
                var tableauCarte="";
                $(".calcul").each(function(){ 
                    tableauCarte=tableauCarte+$(this).attr('id')+"-"+$(this).html()+"-"+$(this).attr('pseudo')+",";
                    });
                charge('insert',"&carte="+tableauCarte,'panier');
                }
            else if ($(this).html() == 'Annuler'){
                $('#'+id).css('color','black');
                $("#references").empty();
                charge('cb','','references');
                }
            else if ($(this).html() == 'Valider'){
                charge('update',id+'&px='+$('#n_prix').html(),'panier');
                }
            else if ($(this).html() == 'Effacer'){
                $('#'+id).html('');
                }
            else{
                $('#'+id).append($(this).html());
                $('#valider').css('visibility', 'visible');
                }
            })
        var tt='';
        $( ".calcul" ).each(function() {
        tt +=  $( this ).text();
        });
        if (tt =='')$('#valider').css('visibility', 'hidden');
        else $('#valider').css('visibility', 'visible');
        }
    <?php
}
?>
</script>
