<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}

//***********************************************Attention encodage cp1252******************************

$facture= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);//numero du ticket
$an= filter_input(INPUT_GET, "an", FILTER_SANITIZE_FULL_SPECIAL_CHARS);//numero du ticket
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";

connexobjet();

require_once $incpath.'/fpdf181/fpdf.php';
$Mois=array("","janvier","f�vrier","mars","avril","mai","juin","juillet","ao�t","septembre","octobre","novembre","d�cembre");
require "../info_facture.php";
// global $an;
if ($an == '') {
    $an = ANNEE;
}
$cemois = '';
//Create new pdf file
class PDF_FacturePF extends FPDF
{
    public $ligne = 0;
    protected $an;
    protected $cemois;
    protected $Ligne_Hauteur = 15;
    protected $Colonne_Largeur;
    protected $colonne = 1;

    public function __construct($margeG, $margeH, $an, $cemois)
    {
        parent::__construct();

        $this->SetAutoPageBreak(false);
        $this->SetFontSize(7);
        $this->FontSize = 7;
        $this->FontFamily = "Arial";

        $this->Ligne_Hauteur = $this->FontSize * .47;
        $this->AddPage('P', 'A4');

        $this->lMargin = $margeG;
        $this->tMargin = $margeH;
        $this->Colonne_Largeur = ($this->w - $this->lMargin - $this->rMargin);
        $this->ligne = 0;
        $this->colonne = 1;
        $this->cemois = $cemois;
        $this->an = $an;
    }

    // Give the height for a char size given.
    protected function _get_height_chars($pt = null)
    {
        // Tableau de concordance entre la hauteur des caract�res et de l'espacement entre les lignes
        if ($pt === null) {
            $pt = $this->FontSize;
        }
        return $pt * .52;
    }

    public function addLigne($type, $valeurs)
    {
        if ($type == "entete") {
            //**********************en tete******************************
            $this->SetFont('Arial', 'B', 14);
            $this->SetX($this->lMargin);
            $this->MultiCell(100, 6, $valeurs['soc_nom'].'
'.$valeurs['soc_adr1'].'
'.$valeurs['soc_adr2'].'
'.$valeurs['soc_cp'].' '.$valeurs['soc_ville'].'
'.$valeurs['soc_pays']);
            $this->SetFont('Arial', 'I', 8);
            $this->SetX($this->lMargin);
            $this->MultiCell(100, 4, 'SIRET : '.$valeurs['soc_siret'].'
N TVA : '.$valeurs['soc_tva'].'
'.$valeurs['soc_banque'].'
			RIB : '.$valeurs['soc_iban']);
            $this->SetFont('Arial', 'B', 10);
            $this->SetX($this->lMargin);
            $this->MultiCell(100, 4, 'TEL. : '. $valeurs['soc_tel'].'
FAX. : '.$valeurs['soc_fax']);
            //****************fin de l'entete***********************
            
            //**************on affiche les coordonnes de l'acheteur*****************
        if ($valeurs['totalF'] < 0) {//c'est un avoir
        $libel = 'AVOIR ';
        } else {
            $libel = 'FACTURE ';
        }
            //   print_r($rq_facture);
            $debut=30;
            $mar_g=$this->lMargin;
            $this->SetFont('Arial', 'B', 16);
            $this->SetY(20);
            $this->SetX($mar_g+100);
            $this->Cell(100, 6, $libel.'N� L'.(($this->an * 1000)+$valeurs['fac_id']));
            $this->SetFont('Arial', 'I', 10);
            $this->SetY($debut);
            $this->SetX($mar_g+100);
            $this->Cell(80, 6, $valeurs['cpt_nom']);
            $this->SetY($debut+6);
            $this->SetX($mar_g+100);
            if ($valeurs['cpt_adr2'] !="") {
                $this->Cell(80, 6, $valeurs['cpt_adr1']);
                $this->SetY($debut+12);
                $this->SetX($mar_g+100);
                $this->Cell(80, 6, $valeurs['cpt_adr2']);
                $this->SetY($debut+18);
                $this->SetX($mar_g+100);
                $this->Cell(80, 6, $valeurs['cpt_cp']);
                $this->SetY($debut+24);
                $this->SetX($mar_g+100);
                $this->Cell($debut, 6, $valeurs['cpt_ville']);
            } else {
                $this->Cell(80, 6, $valeurs['cpt_adr1']);
                $this->SetY($debut+12);
                $this->SetX($mar_g+100);
                $this->Cell(80, 6, $valeurs['cpt_cp']);
                $this->SetY($debut+18);
                $this->SetX($mar_g+100);
                $this->Cell($debut, 6, $valeurs['cpt_ville']);
            }
        } elseif ($type == "titre_tableau") {
            $y_axis_initial=70;
            $this->SetFillColor(255, 255, 255);
            $this->SetFont('Arial', 'B', 14);
            $this->SetY($y_axis_initial);
            $this->SetX($this->lMargin);
            $this->Cell(180, 7, 'Ticket N� '.$valeurs['ticket'].' du '.$valeurs['jour']." ".$valeurs['mois']." ".$valeurs['annee'], 1, 0, 'C', 1);
            $hauteur_tableau = 214;
            $this->Line(15, 75, 15, $hauteur_tableau);
            $this->Line(110, 83, 110, $hauteur_tableau);
            $this->Line(120, 83, 120, $hauteur_tableau);
            $this->Line(140, 83, 140, $hauteur_tableau);
            $this->Line(155, 83, 155, $hauteur_tableau);
            $this->Line(175, 83, 175, $hauteur_tableau);
            $this->Line(195, 75, 195, $hauteur_tableau);
            $this->Line(15, $hauteur_tableau, 180, $hauteur_tableau);
            //deuxieme ligne, titre des colonnes
            $this->SetY($y_axis_initial+6);
            $this->SetX($this->lMargin);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(95, 6, 'D�signation', 1, 0, 'C', 1);
            $this->Cell(10, 6, 'Qte', 1, 0, 'C', 1);
            $this->Cell(20, 6, 'Prix Uni. HT', 1, 0, 'C', 1);
            $this->Cell(15, 6, 'Rem.', 1, 0, 'C', 1);
            $this->Cell(20, 6, 'Taux TVA', 1, 0, 'C', 1);
            $this->Cell(20, 6, 'Total HT', 1, 0, 'C', 1);
        } elseif ($type == "titre_suivant") {
            $y_axis_initial=12;
            $hauteur_tableau = 260;
            $this->Line(15, 22, 15, $hauteur_tableau);
            $this->Line(110, 22, 110, $hauteur_tableau);
            $this->Line(120, 22, 120, $hauteur_tableau);
            $this->Line(140, 22, 140, $hauteur_tableau);
            $this->Line(155, 22, 155, $hauteur_tableau);
            $this->Line(175, 22, 175, $hauteur_tableau);
            $this->Line(195, 22, 195, $hauteur_tableau);
            $this->Line(15, $hauteur_tableau, 195, $hauteur_tableau);
            //deuxieme ligne, titre des colonnes
            $this->SetY($y_axis_initial+6);
            $this->SetX($this->lMargin);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(160, 6, 'Ticket N� '.$valeurs['ticket'].' du '.$valeurs['jour']." ".$valeurs['mois']." ".$valeurs['annee'].', suite', 1, 0, 'C', 1);
            $this->Cell(20, 6, 'Page '.$valeurs['n_page'], 1, 0, 'C', 1);
            
            $this->SetY($y_axis_initial+12);
            $this->SetX($this->lMargin);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(95, 6, 'D�signation', 1, 0, 'C', 1);
            $this->Cell(10, 6, 'Qte', 1, 0, 'C', 1);
            $this->Cell(20, 6, 'Prix Uni. HT', 1, 0, 'C', 1);
            $this->Cell(15, 6, 'Rem.', 1, 0, 'C', 1);
            $this->Cell(20, 6, 'Taux TVA', 1, 0, 'C', 1);
            $this->Cell(20, 6, 'Total HT', 1, 0, 'C', 1);
        } elseif ($type == "article") {
            $prx_u = $valeurs["prix"]/(1+($valeurs["tva"]/100));//prix unitaire HT avant remise
//       if($valeurs["prix"]==$valeurs["prixS"]) $remise = 0.00;
//       elseif($valeurs["prixS"] != 0.00) $remise = floor(100 - ($valeurs["prix"] * 100 / $valeurs["prixS"]));
//       else  $remise = 0.00;
            
            $Total_HT = $prx_u * $valeurs["quantite"];
            // 			$Total_HT = $valeurs["prix"] * $valeurs["quantite"];
            
            $this->SetY(82+$valeurs["dec"]);
            $this->SetX($this->lMargin);
            $this->SetFont('Arial', 'B', 8);
            $this->Cell(95, 6, " ".$valeurs["titre"], 1, 0, 'L', 1);
            $this->Cell(10, 6, $valeurs["quantite"], 1, 0, 'C', 1);
            $this->Cell(20, 6, monetaireF($prx_u)." �", 1, 0, 'R', 1);
            $this->Cell(15, 6, monetaireF($valeurs['remise'])." %", 1, 0, 'R', 1);//.
            $this->Cell(20, 6, monetaireF($valeurs["tva"])." %", 1, 0, 'R', 1);
            $this->Cell(20, 6, monetaireF($Total_HT)." �", 1, 0, 'R', 1);
        } elseif ($type == "total") {
            //***************************************ligne tva*******************************
            $mar_g=$this->lMargin;
            $bas = $valeurs['bas'];
            $this->SetY($bas);
            $this->SetX($mar_g);
            $this->SetFont('Arial', 'B', 7);
            $this->Cell(60, 4, 'D�tail de la TVA', 1, 0, 'C', 1);
            $this->SetY($bas += 4);
            $this->SetX($mar_g);
            $this->SetFont('Arial', 'B', 7);
            $this->Cell(20, 4, 'Montant HT', 1, 0, 'C');
            $this->Cell(20, 4, 'Taux', 1, 0, 'C');
            $this->Cell(20, 4, 'Montant TVA', 1, 1, 'C');
            //*****************************affichage des tva utilis�s********************************
            $TTtva = 0;
            $THT = 0;
            foreach ($valeurs['afftva'] as $key => $value) {
                if ($value != 0.00) {
                    $tva=($value*(1+($key/100)))-$value;
                    $monTHT = $value;
                } else {
                    $tva =0.00;
                    $monTHT = $value;
                }
                $this->SetY($bas += 4);
                $this->SetX($mar_g);
                $this->SetFont('Arial', '', 7);
                $this->Cell(20, 4, monetaireF($monTHT)." �", "L", 0, 'R');
                $this->Cell(20, 4, monetaireF($key).' %', 0, 0, 'R');
                $this->Cell(20, 4, monetaireF($tva)." �", 'R', 1, 'R');
                $TTtva += $tva;
                $THT += $monTHT;
            }
            $this->Line(15, $bas+4, 180, $bas+4);
            //*****************************fin affichage des tva utilis�s********************************
     
            $bas=$valeurs['bas'];
            //ligne du total
            //ligne du total
            $this->SetY($bas);
            $this->SetX($mar_g);
            $this->SetFont('Arial', 'B', 11);
            $this->Cell(140, 6);
            $this->Cell(20, 6, 'Total HT', 1);
            $this->Cell(20, 6, monetaireF($THT)." �", 1, 0, 'R', 1);
            //ligne titre tva
            $bas=$bas+6;
            $this->SetY($bas);
            $this->SetX($mar_g);
            $this->Cell(140, 6);
            $this->Cell(20, 6, 'Total TVA', 1, 0, 'R');
            $this->Cell(20, 6, monetaireF($TTtva)." �", 1, 0, 'R', 1);
            //ligne titre tva
            $bas=$bas+6;
            $this->SetY($bas);
            $this->SetX($mar_g);
            $this->SetFont('Arial', '', 8);
            $this->Cell(140, 6);
            $this->SetFont('Arial', 'B', 11);
            $this->Cell(20, 6, 'Total TTC', 1, 0, 'R', 1);
            $this->Cell(20, 6, monetaireF($THT+$TTtva)." �", 1, 0, 'R', 1);
            //mode de regelement
            $bas += 6;
            $this->SetY($bas);
            $this->SetX($mar_g);
            $this->SetFont('Arial', '', 8);
            $this->Cell(60, 6);
            $this->Cell(120, 6, $valeurs['mdr'], 'RB', 0, 'R');
        } elseif ($type == "infos_legales") {
            //position fixe
            $this->SetY(240);
            $this->SetX($this->lMargin);
            $this->SetFont('Arial', '', 7);
            $this->MultiCell(180, 3, utf8_decode($valeurs['info']), 1, 'J');
        } elseif ($type == "finTableau") {
            $hauteur_tableau = $valeurs['fin'];
            $debut = $valeurs['debut'];
            $this->Line(15, $debut, 15, $hauteur_tableau);
            $this->Line(110, $debut, 110, $hauteur_tableau);
            $this->Line(120, $debut, 120, $hauteur_tableau);
            $this->Line(140, $debut, 140, $hauteur_tableau);
            $this->Line(155, $debut, 155, $hauteur_tableau);
            $this->Line(175, $debut, 175, $hauteur_tableau);
            $this->Line(195, $debut, 195, $hauteur_tableau);
            $this->Line(15, $hauteur_tableau, 195, $hauteur_tableau);
        }
    }
}
//coordonn�es de la soci�t�
$req_societe="SELECT * FROM Societe";
$r_societe=$idcom->query($req_societe);
$rq_societe=$r_societe->fetch_object();



//coordonn�es de l'acheteur
$req_facture="SELECT Comptes.*, fac_id, rst_id, UNIX_TIMESTAMP(rst_validation) AS rst_validation FROM Comptes JOIN Factures_$an ON fac_cp = cpt_id JOIN Resume_ticket_$an ON fac_ticket = rst_id WHERE fac_id = $facture LIMIT 0,1";
$r_facture=$idcom->query($req_facture);
$rq_facture=$r_facture->fetch_object();

//v�rification du total de la facture, si n�gatif, c'est un avoir
$req_total = "SELECT SUM(tic_tt) AS totalF FROM Tickets_$an WHERE tic_num=".$rq_facture->rst_id;
$r_total=$idcom->query($req_total);
$rq_total=$r_total->fetch_object();
// exit;
$FacturePF=new PDF_FacturePF(15, 6, $an, $cemois);

//debut de facture, coordonn�es et lignes de titre
$FacturePF->addLigne("entete", array("soc_nom"=>utf8_decode($rq_societe->soc_nom),"soc_adr1"=>utf8_decode($rq_societe->soc_adr1),"soc_adr2"=>utf8_decode($rq_societe->soc_adr2),"soc_cp"=>$rq_societe->soc_cp,"soc_ville"=>utf8_decode($rq_societe->soc_ville),"soc_pays"=>utf8_decode($rq_societe->soc_pays),"soc_siret"=>$rq_societe->soc_siret,"soc_tva"=>$rq_societe->soc_tva,"soc_banque"=>$rq_societe->soc_banque,"soc_iban"=>$rq_societe->soc_iban,"soc_tel"=>$rq_societe->soc_tel,"soc_fax"=>$rq_societe->soc_fax,"cpt_nom"=>utf8_decode($rq_facture->cpt_nom),"fac_id"=>$rq_facture->fac_id,"cpt_adr1"=>utf8_decode($rq_facture->cpt_adr1),"cpt_adr2"=>utf8_decode($rq_facture->cpt_adr2),"cpt_cp"=>$rq_facture->cpt_cp,"cpt_ville"=>utf8_decode($rq_facture->cpt_ville),"totalF"=>$rq_total->totalF));

// contenu de la commande
$req_articles="SELECT tit_nom, 
                    tic_prixS, 
                    tic_quantite, 
                    tic_tva, 
                    tic_ntva, 
                    tic_prix,
                    tic_tt, 
                    tic_cp, 
                    tic_num, 
                    mdr_nom, 
                    mdr_id,
                    art_unite 
                        FROM Tickets_$an 
                        JOIN Titres ON tit_article = tic_article 
                        JOIN Resume_ticket_$an ON rst_id = tic_num 
                        JOIN Mode_reglement ON mdr_id = rst_etat 
                        JOIN Articles ON art_id = tic_article 
                            WHERE tit_niveau = 1 
                            AND tic_num =".$rq_facture->rst_id;
$r_articles=$idcom->query($req_articles);

$rq_articles=$r_articles->fetch_object();
if ($rq_articles->mdr_id != 5) {
    $mdr = "Mode de r�glement utilis� : ".utf8_decode($rq_articles->mdr_nom);
} else {
    $mdr = "";
}
// exit;
$FacturePF->addLigne("titre_tableau", array('ticket'=>$rq_articles->tic_num,'jour'=>date('j', $rq_facture->rst_validation),'mois'=>$Mois[date('n', $rq_facture->rst_validation)],'annee'=>date('Y', $rq_facture->rst_validation)));
$r_articles->data_seek(0);

/*
Il y a toujours le cadre info sur la premi�re page. S'il n'y a <= 22 une seule page avec les totaux s'il ya plus de 22 lignes les totaux seront sur la deuxi�me page.

totaux : 4 lignes
info : 8 lignes
premi�re page seule 22 lignes
page complete 47 lignes

nombre de page
si plus de 22

*/
  //creation dynamique des lignes tva
$req_tva = "SELECT tva_id, tva_nom FROM Tva WHERE tva_etat = 1 ORDER BY tva_ordre";
$r_tva=$idcom->query($req_tva);
$afftva=array();
while ($rq_tva=$r_tva->fetch_object()) {
    array_push($afftva, $rq_tva->tva_nom);
}
$afftva = array_fill_keys($afftva, '0.00');

$n_ligne = 0;
$page_encours = 1;
$nombre_de_ligne = $r_articles->num_rows-1;
$dec=0;
$page = 1;
$numero_ligne = 1;//ligne de la page
$reste = $nombre_de_ligne;
$TT = 0.00;
while ($rq_articles=$r_articles->fetch_object()) {
    $prx_u = $rq_articles->tic_prix/(1+($rq_articles->tic_tva/100));//prix unitaire HT avant remise
    if ($rq_articles->tic_prix == $rq_articles->tic_prixS) {
        $remise = 0.00;
    } elseif ($rq_articles->tic_prixS != 0.00) {
        $remise = round(100 - ($rq_articles->tic_prix * 100 / $rq_articles->tic_prixS),1);
        // $remise = floor(100 - ($rq_articles->tic_prix * 100 / $rq_articles->tic_prixS));
    } else {
        $remise = 0.00;
    }
            
    if ($rq_articles->art_unite == 1) {
        $quantite = sprintf("%d", $rq_articles->tic_quantite);
    } else {
        $quantite = $rq_articles->tic_quantite;
    }
    $titre = utf8_decode($rq_articles->tit_nom);
    $titre = str_replace('&#39;',"'",$titre);          
    $FacturePF->addLigne("article", array("titre"=>$titre,"remise"=>$remise,"prix"=>$rq_articles->tic_prix,"quantite"=>$quantite,"tva"=>$rq_articles->tic_tva,"dec"=>$dec,"numero_ligne"=>$numero_ligne));
    
    $afftva[$rq_articles->tic_tva] += ($prx_u*$rq_articles->tic_quantite);

    $dec += 6;
    $reste = $nombre_de_ligne--;
    if ($page_encours == 1) {
        if (($numero_ligne <= 22)&&($reste == 0)) {//page unique : articles/totaux/info
            $bas=214;
            $FacturePF->addLigne('infos_legales', array("info"=>$info));
            $FacturePF->addLigne('total', array('bas'=>$bas, 'TT'=>$TT,'afftva'=>$afftva,'mdr'=>$mdr));
        } elseif (($numero_ligne <= 26)&&($reste == 0)) {//premi�re page : articles/info
            if ($numero_ligne != 26) {
                $FacturePF->addLigne('finTableau', array('debut'=>($dec),'fin'=>238));
            }
            $FacturePF->addLigne('infos_legales', array("info"=>$info));
            $FacturePF->AddPage('P', 'A4');
            $page_encours = 2;
            $dec=0;
            $bas = 260;
            $FacturePF->addLigne('titre_suivant', array('ticket'=>$rq_articles->tic_num,'jour'=>date('j', $rq_facture->rst_validation),'mois'=>$Mois[date('n', $rq_facture->rst_validation)],'annee'=>date('Y', $rq_facture->rst_validation),'n_page'=>$page_encours));
        } elseif (($numero_ligne == 26)&&($reste > 0)) {//premi�re page : articles/info
            $FacturePF->addLigne('infos_legales', array("info"=>$info));
            $FacturePF->AddPage('P', 'A4');
            $page_encours++;
            $bas = 260;
            $dec = -53;
            $numero_ligne = 1;
            $FacturePF->addLigne('titre_suivant', array('ticket'=>$rq_articles->tic_num,'jour'=>date('j', $rq_facture->rst_validation),'mois'=>$Mois[date('n', $rq_facture->rst_validation)],'annee'=>date('Y', $rq_facture->rst_validation),'n_page'=>$page_encours));
        }
    } else {//toutes les pages suivantes
        if (($numero_ligne >= 39)&&($reste == 0)) {
            if ($reste == 0) {
                if ($numero_ligne != 43) {
                    $FacturePF->addLigne('finTableau', array('debut'=>($dec),'fin'=>270));
                }
                $FacturePF->AddPage('P', 'A4');
                $page_encours++;
                $bas = 260;
                $dec = -53;
                $numero_ligne = 1;
                $FacturePF->addLigne('titre_suivant', array('ticket'=>$rq_articles->tic_num,'jour'=>date('j', $rq_facture->rst_validation),'mois'=>$Mois[date('n', $rq_facture->rst_validation)],'annee'=>date('Y', $rq_facture->rst_validation),'n_page'=>$page_encours));
            }
        } elseif ($numero_ligne == 43) {
            $FacturePF->AddPage('P', 'A4');
            $page_encours++;
            $bas = 260;
            $dec = -53;
            $numero_ligne = 1;
            $FacturePF->addLigne('titre_suivant', array('ticket'=>$rq_articles->tic_num,'jour'=>date('j', $rq_facture->rst_validation),'mois'=>$Mois[date('n', $rq_facture->rst_validation)],'annee'=>date('Y', $rq_facture->rst_validation),'n_page'=>$page_encours));
        }
    }
    $numero_ligne ++;
}
    //---------------------------------------------------------
 
$FacturePF->addLigne('total', array('bas'=>$bas,'afftva'=>$afftva,'mdr'=>$mdr));
$chemin = $incpath."pdf/factures/facture_".(($an * 1000)+$facture).".pdf";
$FacturePF->Output('F', $chemin);//
unset($_SESSION['impression_'.$_SESSION[$dossier]]);
?>
<script>
// location.href='<?php echo $chemin?>';
window.open('<?php echo $chemin?>');
</script>
