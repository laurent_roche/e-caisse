<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}

$term = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
$nom=$term;//mb_strtolower($term,'UTF8');//exit;
$compS=($_SESSION['soldes'] == 'oui')?"OR pro_mode >= 1":"OR pro_mode < 2";

$req_recher="SELECT tit_article,
                        tit_nom,
                        tit_niveau,
                        art_stk,
                        CASE
                            WHEN pro_valeur IS NOT NULL THEN pro_valeur
                            ELSE art_ttc
                        END AS art_ttc,
                        art_cb,
                        ray_secteur,
                        Vt1_nom,
                        Vt3_nom,
                        art_unite,
                        art_id,
                        pro_valeur,
                        pro_mode
                        FROM Titres
                        JOIN Articles ON art_id = tit_article
                        JOIN Rayons ON ray_id = art_rayon
                        LEFT JOIN Vtit1 ON Vt1_article = art_id
                        LEFT JOIN Vtit3 ON Vt3_article = art_id
                        LEFT JOIN Promo ON pro_article = art_id
                        	WHERE tit_recherche
                                LIKE '%$nom%' AND art_statut < 3
                                AND pro_mode IS NULL
                                $compS
                                    GROUP BY tit_nom ,tit_niveau
                                    ORDER BY tit_niveau, tit_nom";
$r_recher=$idcom->query($req_recher);
$nb = $r_recher->num_rows;
if ($nb == 0) {
    // exit;
    ?>
    <script>
    // alert('ici');
    if (<?php echo $nb?> == 0) {
        $("#resultat").html("<h1>Pas de d'article avec la recherche '<?php echo $nom?>'</h1>");
        $('#resultat').width($('#affichage').width()-750);
    }
    </script>
    <?php
    exit;
}
?>
<table id=clavier class="pagination" number-per-page="6" current-page="0"><thead>
<TR>
<TH id="nb_titre" class="auteur"></TH>
</TR>
</thead>
<tbody>
<?php
// echo "<ul>\t" . '<li>' .$nb . "\n";
$nb_titre = $nb;
$comp3 = '';
$nbA = 0;
$t = 0;//limitation de l'affichage si trop d'articles
while ($resu=$r_recher->fetch_object()) {
    $coul=($t%2 == 0)?$coulCC:$coulFF;
    $n = 0;//sert à l'affichage des auteurs
    $qt=($resu->art_unite == 1)?sprintf('%d', $resu->art_stk):$resu->art_stk;
    $comp=" (".$resu->art_ttc." €, qt : ".$qt.")";
    if ($resu->tit_niveau == 3) {
        $comp3= " <i>(".$resu->Vt3_nom.")</i> ";
    }    
    if ($resu->ray_secteur == 2) {
        $class="class='livre'";
    } else {
        $class="";
    }
    
    if (($resu->tit_niveau != 1)&&($resu->tit_niveau != 4)) {//ni titre principal ni auteur
        echo "<tr style='background-color:".$coul."'><td alt='".$resu->art_id."' ".$class.">".stripslashes($resu->Vt1_nom).$comp3.$comp."</td></tr>\n";
    } elseif ($resu->tit_niveau == 4) {//auteur, on recherche tous les livres de cet auteur, actuellement basé sur le nom
        $nom = addslashes($resu->tit_nom);
        $req_auteur="SELECT Vt1_nom, Vt1_article 
                        FROM Vtit1 
                        JOIN Vtit4 ON Vt1_article = Vt4_article 
                        JOIN Articles ON art_id = Vt1_article
                            WHERE Vt4_nom LIKE '%$nom%' 
                            AND art_statut < 3
                                ORDER BY Vt1_nom";
        $r_auteur=$idcom->query($req_auteur);
        $nbA= $r_recher->num_rows;
        while ($rq_auteur=$r_auteur->fetch_object()) {
            if ($n == 0) {
                echo "<tr><td alt='' class='auteur'>".stripslashes($resu->tit_nom)."</td></tr>\n";
            }
            $coul=($t%2 == 0)?$coulCC:$coulFF;
            echo "<tr style='background-color:".$coul."'><td alt='".$rq_auteur->Vt1_article."' ".$class.">".stripslashes($rq_auteur->Vt1_nom).$comp."</td></tr>\n";
            $n++;
            $t++;
            if ($t > 19) { 
                break;
            }
        }
    } else {
        echo "<tr style='background-color:".$coul."'><td alt='".$resu->art_id."' ".$class.">".stripslashes($resu->tit_nom).$comp."</td></tr>\n";
        $n++;
        if ($t > 19) {
            break;
        }
    }
    $t++;
    if ($t > 19) {
        break;
    }
}
// echo $t;
?>
</tbody>
</table>

<!-- <script type="text/javascript" src="/js/simplepagination.js"></script> -->
<script>
$(document).ready(function() {
    $('#clavier td').click(function() {
        if($(this).attr('alt')!='') charge('insert','&cb='+$(this).attr('alt'),'panier');
    });
});

if(<?php echo ($nb_titre+$nbA)?> > 20) {
    $("#nb_titre").html("trop de résultat (<?php echo ($nb_titre+$nbA)?>, 20 affichés)");
} else if(<?php echo ($nb_titre+$nbA)?> > 1) {
    $("#nb_titre").html("<?php echo ($nb_titre+$nbA)?> articles trouvés");
} else if(<?php echo ($nb_titre+$nbA)?> == 1) {
    $("#nb_titre").html("Un article trouvé");
}
nb_ligne = Math.floor($("#affichage").height()/60);
// alert(nb_ligne);
if(nb_ligne < <?php echo ($nb_titre+$nbA)?>){
$("#clavier").attr('number-per-page',nb_ligne);
$("#clavier").pagination();
$("#clavier th").height('50px');
$("#clavier td").height(($("#affichage").height()-40)/(nb_ligne+2));
$('.pager').css('margin-top','20px');
}
$('#resultat').height($('#references').height()-60);
// setTimeout(function(){$('#resultat').width($('#affichage').width()-750)},100)
$('#resultat').width($('#affichage').width()-750);
</script>
