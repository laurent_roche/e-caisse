<?php
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
require "../entete.php";
$utilisateur=$_SESSION[$dossier];
// print_r($_SESSION);
//on indique que l'utilisateur est connecté
$req_uti="UPDATE Utilisateurs SET uti_etat='1' WHERE uti_id=".$utilisateur;
$idcom->query($req_uti);
require $incpath."php/fonctions.php";
require $incpath."php/config.php";
?>

<div id="conteneur" style="background-color:<?php echo $_SESSION['fondF_'.$utilisateur]?>">
<!--  bandeau supérieur -->
    <div id="entete" style="height:100px;">
    <div id='liste_panier'>
        <?php
        require "liste_paniers.php";//gestion des paniers
        ?>
        </div>
        <div id="prix"></div>
        <?php
        require "afficheur_externe.php";//gestion d'un affichage déporté de la valeur du panier, voir explication dans le fichier
        ?>
        <button id='liste_tickets' onclick="charge('tickets', '', 'panier')">Mes tickets</button>
    </div>
 <!--  fin bandeau supérieur -->
 
 <!--  partie inférieur -->
    <div id="affichage" style="background-color:<?php echo $_SESSION['fondC_'.$utilisateur]?>">
        <div id='panier'></div><!--div gauche-->
        <div id="references"></div><!--div droite-->
    </div>
    <!--  bandeau droit -->   
    <div id="mode">
    <?php
    $nb_bt = 0;//nombre de bouton pour le calcul de leur dimention en fonction de l'écran
    //on recherche les mode de regelement actifs pour les afficher
    $limit=$_SESSION["droits_".$utilisateur];
    $req_mdr="SELECT mdr_id, mdr_abrege, mdr_nom, mdr_droits FROM Mode_reglement WHERE mdr_etat = 1 AND mdr_droits <= $limit ORDER BY mdr_id";
    $r_mdr=$idcom->query($req_mdr);
    while ($rq_mdr=$r_mdr->fetch_object()) {
        echo '<img class="bt_action reglem" src="../images/reglements/mdr_'.$rq_mdr->mdr_id.'.png" onclick="clic_reglement('.$rq_mdr->mdr_id.')">';
        $nb_bt++;
    }
    if ($limit > 0) {
        ?>
        <img id="b_compte" class="bt_action" src="../images/reglements/compte.png" alt="compte" onclick="compte_panier()" >
        <?php
        $nb_bt++;
    }
    ?>
    <div id="type" style="vertical-align: bottom;position: absolute;bottom: 0px;margin-right:5px;width:100px">
    <?php
    //liste des favoris, gestion en fonction de $config['favoris']
    if ($config['favoris'] == 1) {
        $req_favoris = "SELECT * FROM Affichage LEFT JOIN VaffichageFavoris ON VafF_parent = aff_id ORDER BY aff_ordre;";
        $r_favoris = $idcom->query($req_favoris);
        while ($rq_favoris =$r_favoris->fetch_object()) {
            $file=$incpath.'images/affichage/aff_'.$rq_favoris->aff_id.'.png';
            $imgfavoris=(file_exists($file))?'/images/affichage/aff_'.$rq_favoris->aff_id.'.png':"/images/secteurs/inc.png";
            if ($rq_favoris->aff_id == 1) {//articles par codes en premier
                echo '<img class="bt_action" src="..'.$imgfavoris.'" onclick="charge(\'references\', \'\', \'references\')">';
                $nb_bt++;
            } elseif (($rq_favoris->aff_parent == 0)&&($rq_favoris->VafF_parent == '')) {//affichage dans la fenêtre principale
                echo '<img class="bt_action" src="..'.$imgfavoris.'" onclick="charge(\'divers_requete\', \''.$rq_favoris->aff_id.'\', \'panier\');$(\'#references\').empty();">';
                $nb_bt++;
            } elseif (($rq_favoris->aff_parent == 0)&&($rq_favoris->VafF_parent != '')) {//liste des favoris dans la div de droite
                echo '<img class="bt_action" src="..'.$imgfavoris.'" onclick="charge(\'divers\', \''.$rq_favoris->aff_id.'\', \'references\')">';
                $nb_bt++;
            }
        }
    } else {
        ?>
    <img class="bt_action" src="../images/eCcatLib.png" onclick="charge('references','','references')">
		<img class="bt_action" src="../images/eCray.png" onclick="charge('btns_ray','','references')">
    <?php
    }
    ?>    
    </div>
    </div>
      <div id="mysql" onclick="$(this).css('visibility', 'hidden')"> </div>
</div>
<div id='popup'></div>
<?php
//---------------------------protection d'écran--------------------------------------
if ($_SESSION['pass_'.$_SESSION[$dossier]] == 'oui') {
    ?>
<div id="voile" onclick="$('#pass').css('visibility','visible')">
<script>
$(document).ready(function() {
    function verif(req){
    $.ajax({
    type: "GET",
    url: "veille.php?req="+req,
    dataType : "html",
    success:function(data){
    $("#error").html(data);
        }});
    }
    
    function voir_clavier_pass(){
        $('#pass').append(clavierp);
        $('#annuler').css('visibility','hidden');   
        $('#effacer').css('visibility','hidden');   
        $('#keypad').css('top',50);
        $('#keypad').css('display','block');
    
        $("#keypad .key").click(function(){
            if ($(this).html() == 'Valider'){
                verif('<?php echo $_SESSION[$dossier]?>&pas='+$('#prot').html());
                }
            else if ($(this).html() == 'Effacer'){
                $('#prot').html('');        
                $('#valider').css('visibility', 'hidden');
                }
            else {
                $("#prot").append($(this).html());
                $('#valider').css('visibility', 'visible');
                $('#effacer').css('visibility', 'visible');
            }
        });
    }
    voir_clavier_pass();
});
</script>

<div id='pass'><div id="error"></div><span id='prot' style="visibility:hidden"></span></div>
</div>
<?php
} else {
        ?>
<div id="voile" onclick="$(this).css('visibility','hidden')">&nbsp;</div>
<?php
    }
//---------------------------fin de protection d'écran--------------------------------------

?>
  </body>
</html>
<script>
// var erreur;
function clic_reglement(id){
    if (id == 1){ //caisse
        if ($('#b_compte').attr('alt')=='comptes'){
            $("#references").html( "<h2>Le ticket est vide</h2>" );
            erreur = 1;
        } else {
            erreur ='';
        }
    } else if (id==2){ //chèque
        if (+$('#prix').html() < 0){
            $("#references").empty();
            $("#references").html("<h2>Un panier négatif ne peut être réglé en chèque ou Carte bancaire</h2>");
            erreur = 2;
        } else if ($('.panier_actif').attr('alt') > '0' ){
            erreur = '';
        } else {
            $("#references").html( "<h2>Le ticket est vide</h2> SI vous voulez enregistrer un don, saisissez l'article voulu dans la section Divers"  );
            erreur = 2;
        }
    } else if (id==3){ //carte bancaire
        if (+$('#prix').html() < 0) {
            $("#references").empty();
            $("#references").html("<h2>Un panier négatif ne peut être réglé en chèque ou Carte bancaire</h2>");
            erreur = 3;
        } else if ($('.panier_actif').attr('alt') > '0' ) {
            erreur = '';
        } else {
            $("#references").html( "<h2>Le ticket est vide</h2> SI vous voulez enregistrer un don, saisissez l'article voulu dans la section Divers"  );
            erreur = 3;
        }
    } else if (id==4) { //carte bancaire sans contact
        if (+$('#prix').html() < 0){
        $("#references").empty();
        $("#references").html("<h2>Un panier négatif ne peut être réglé en chèque ou Carte bancaire</h2>");
        erreur = 4;
        } else if ($('#b_compte').attr('alt')=='comptes'){
            $("#references").html( "<h2>Le ticket est vide</h2>" );
            erreur = 1;
        } else {
            erreur ='';
        }
    } else if (id==5) { //différé
        if ($('#b_compte').attr('alt')=='comptes'){
            $("#references").html( "<h2>Le ticket est vide</h2>" );
            erreur = 5;
        } else {
            erreur ='';
        }
    }
    if (!erreur) charge('reglement', id, 'references');
}

function compte_panier() {
    if ($('#b_compte').attr('alt') == 'panier'){ charge('comptes', 1, 'references')}
    else {charge('comptes', 2, 'references')}
}
clavierp='<div id="keypad" style="display: none;"><div class="keypad-row" id="valeur_min"></div><div class="keypad-row"><button type="button" class="key">1</button><button type="button" class="key">2</button><button type="button" class="key">3</button></div><div class="keypad-row"><button type="button" class="key">4</button><button type="button" class="key">5</button><button type="button" class="key">6</button></div><div class="keypad-row"><button type="button" class="key">7</button><button type="button" class="key">8</button><button type="button" class="key">9</button></div><div class="keypad-row"><div class="keypad-space"></div><button type="button" class="key">0</button><button type="button" class="key">.</button></div><div class="keypad-row"><button type="button" onclick="$(\'#keypad\').css(\'display\', \'none\')" class="key" id="annuler" style="background-color:red;text-shadow:white 2px 2px white;">Annuler</button><button type="button"  id="effacer" style="background-color:yellow;text-shadow:2px 2px white;" class="key">Effacer</button><button style="background-color:green;text-shadow: 2px 2px white;visibility:hidden" id="valider" type="button" onclick="$(\'#keypad\').css(\'display\', \'none\')" class="key" style="background-color:green;text-shadow: 2px 2px white;">Valider</button></div></div>';

clavierqt='<div id="keypad" style="display: none;"><div class="keypad-row"><button type="button" class="key">1</button><button type="button" class="key">2</button><button type="button" class="key">3</button></div><div class="keypad-row"><button type="button" class="key">4</button><button type="button" class="key">5</button><button type="button" class="key">6</button></div><div class="keypad-row"><button type="button" class="key">7</button><button type="button" class="key">8</button><button type="button" class="key">9</button></div><div class="keypad-row"><div class="keypad-space"></div><button type="button" class="key">-</button><button type="button" class="key">0</button></div><div class="keypad-row"><div class="keypad-space"></div><button type="button" onclick="$(\'#keypad\').css(\'display\', \'none\')" style="background-color:red;text-shadow: 2px 2px white;" class="key">Annuler</button><button style="background-color:yellow;text-shadow: 2px 2px white;" type="button" class="key">Effacer</button><button style="background-color:green;text-shadow: 2px 2px white;visibility:hidden" id="valider" type="button" onclick="$(\'#keypad\').css(\'display\', \'none\')" class="key">Valider</button></div></div>';

n_prix='<table id="tablePrix"><tr style="background-color:#ffffff"><tr onclick="action(\'prix\')"><TH style="background-color:<?php echo $coulCC?>">Nouv. prix</TH><TD id="n_prix" class="clavier"></TD><td style="width:20px"> €</td></td></tr><tr onclick="action(\'remise\')"><TH style="background-color:<?php echo $coulFF?>">Remise</TH><TD id="remise" class="align_d"></TD><td> %</td></tr></table>';

var w=$(window).height();
$('#conteneur').height(w);
$("#affichage").css('height', w-140);
$('#affichage').width($('#entete').width()-110);
$('#panier').width($('#affichage').width()-410);
//charge('panier', '', 'panier');
if($('#affichage').height()/<?php echo $nb_bt?> < 90) {
$('.bt_action').width($('#affichage').height()/<?php echo $nb_bt?>);
}
if ($('#tableTicket').height() < 10){
$("h2").append( "<br>Le ticket est vide" );
}

</script>
