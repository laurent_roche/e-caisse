<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
$req = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

require $incpath . "mysql/connect.php";
require $incpath . "php/fonctions.php";
connexobjet();
$req_favoris = "SELECT * FROM Affichage WHERE aff_parent=$req";
$r_favoris = $idcom->query($req_favoris);
//en début de liste on met les pseudos génériques en fonction des tva actives
$req_pseudo = "SELECT Vt1_nom, art_id, art_tva, tva_etat  
                    FROM Articles 
                        JOIN Vtit1 ON Vt1_article = art_id 
                        JOIN Tva ON art_tva = tva_id 
                            WHERE art_pseudo = 2
                            AND tva_etat = 1
                            AND art_statut < 3";
$r_pseudo = $idcom->query($req_pseudo);
while ($rq_pseudo = $r_pseudo->fetch_object()) {
    echo '<button class="boutref" style="width:45%;float:left" onclick="charge(\'insert\',\'&cb='.$rq_pseudo->art_id.'\',\'panier\')">'.$rq_pseudo->Vt1_nom.'</button>';
}

while ($rq_favoris = $r_favoris->fetch_object()) {
    echo '<button class="boutref abbaye" style="width:93%;background-image:url(\'/images/affichage/aff_' . $rq_favoris->aff_id . '.png\')" onclick="charge(\'divers_requete\',' . $rq_favoris->aff_id . ',\'panier\')">' . $rq_favoris->aff_nom . '</button>';
}
