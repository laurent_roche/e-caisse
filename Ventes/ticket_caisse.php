<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
// header('Content-Disposition: attachment');
//***********************************************Attention encodage cp1252******************************
// print_r($_SERVER); exit;
$ticket= filter_input( INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);//numero du ticket

require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";

connexobjet();

require $incpath.'/fpdf181/fpdf.php';
$Mois=array("", "janvier", "f�vrier", "mars", "avril", "mai", "juin", "juillet", "ao�t", "septembre", "octobre", "novembre", "d�cembre");
require "../info_facture.php";//fichier modifiable par l'utilisateur
$an = '';
$cemois = '';
$TT = '';
class PDF_FacturePF extends FPDF
{
    protected $Ligne_Hauteur = 10;

    function __construct($margeG, $margeH, $format)
    {
        parent::__construct();
        $this->SetAutoPageBreak(false);
        $this->SetFontSize(7);
        $this->FontSize = 7;
        $this->FontFamily = "Arial";
        $this->Ligne_Hauteur = $this->FontSize * .47;
        $this->AddPage('P', $format);
        $this->lMargin = $margeG;
        $this->tMargin = $margeH;
    }

    function addLigne($type, $valeurs)
    {
        if ($type == "entete") {
            //**********************en tete******************************
            //Cell(largeur, hauteur, contenu, bordure, fin de ligne ,alignement, couleur de fond, mixed link)
            $this->SetFont('Arial', 'B', 10.5);
            $this->SetX($this->lMargin);
            $this->SetFillColor(255, 255, 255);
            $this->Cell(66, 8, $valeurs['soc_nom'], 0, 1, 'C', 0);
            $this->SetFont('Arial', 'I', 10);
            $this->Cell(33, 5, $valeurs['soc_adr1'], 0, 0, 'L', 0);
            $this->Cell(33, 5, $valeurs['soc_adr2'], 0, 1, 'R', 0);
            $this->Cell(33, 5, $valeurs['soc_cp'].' '.$valeurs['soc_ville'], 0, 0, 'C', 0);
            $this->Cell(33, 5, $valeurs['soc_pays'], 0, 1, 'R', 0);

            //****************fin de l'entete***********************
        } elseif ($type == "titre_tableau") {
            $y_axis_initial=30;
            $this->SetFillColor(255, 255, 255);
            $this->SetFont('Arial', 'B', 10);
            $this->SetY($y_axis_initial);
            $this->SetX($this->lMargin);
            $this->Cell(66, 7, 'Ticket N� '.$valeurs['num'].' du '.$valeurs['jour']." ".$valeurs['mois']." ".$valeurs['annee'], 1, 0, 'C', 1);
            $this->SetY($y_axis_initial+6);
            $this->SetX($this->lMargin);
            $this->SetFont('Arial', 'B', 7);
            $this->Cell(46, 5, 'D�signation', 1, 0, 'C', 1);
            $this->Cell(7, 5, 'Qte', 1, 0, 'C', 1);
            $this->Cell(13, 5, 'Total', 1, 0, 'C', 1);
        } elseif ($type == "article") {
            $this->SetY(36+$valeurs["dec"]);
            $this->SetX($this->lMargin);
            $this->SetFont('Arial', '', 7);
            $this->Cell(46, 4, " ".$valeurs["titre"], 1, 0, 'L', 1);
            $this->Cell(7, 4, $valeurs["quantite"], 1, 0, 'R', 1);
            $this->Cell(13, 4, monetaireF($valeurs["quantite"]*$valeurs["prix"])." �", 1, 0, 'R', 1);
        } elseif ($type == "total") {
            //***************************************ligne tva*******************************
            $mar_g=$this->lMargin;
            $this->SetY(36+$valeurs["dec"]);
//             $this->SetY($bas);
            $this->SetX($mar_g);
            $this->SetFont('Arial', 'B', 7);
            $this->Cell(53, 4, 'Total TTC ', 1, 0, 'R', 1);
            $this->Cell(13, 4, monetaireF($valeurs['TT'])." �", 1, 0, 'R', 1);
            //*****************************affichage des tva utilis�s********************************
            $this->SetY(40+$valeurs["dec"]);
            $this->SetX($mar_g);
            $this->SetFont('Arial', 'I', 7);
            $this->Cell(53, 4, 'Montant TVA', 1, 0, 'R');
            $this->Cell(13, 4, monetaireF($valeurs['tva'])." �", 1, 1, 'R', 1);
            //*****************************modes de reglement********************************           
            $this->SetY(44+$valeurs["dec"]);
            $this->SetX($mar_g);
            $this->SetFont('Arial', '', 8);
            $this->Cell(66, 6, 'Mode de r�glement utilis� : '.$valeurs['mdr'], '', 0, 'L');
        } 
    }
}    
//coordonn�es de la soci�t�
$req_societe="SELECT * FROM Societe";
$r_societe=$idcom->query($req_societe);
$rq_societe=$r_societe->fetch_object();

$req_articles="SELECT tit_nom,
                        tic_prixS,
                        tic_quantite,
                        tic_tva,
                        tic_ntva,
                        tic_prix,
                        tic_tt,
                        (tic_tt-(tic_tt)/(1 + (tic_tva/100))) AS vl_tva,
                        tic_cp,
                        tic_num,
                        mdr_nom,
                        UNIX_TIMESTAMP(rst_validation) AS rst_validation,
                        art_unite
                            FROM Tickets_".ANNEE."
                            JOIN Titres ON tit_article = tic_article
                            JOIN Resume_ticket_".ANNEE." ON rst_id = tic_num
                            JOIN Mode_reglement ON mdr_id = rst_etat
                            JOIN Articles ON art_id = tic_article
                                WHERE tit_niveau = 1
                                AND tic_num =".$ticket."
                                    ORDER BY tic_id";
$r_articles=$idcom->query($req_articles);
$nombre_de_ligne=$r_articles->num_rows -1;
if ($r_articles->num_rows > 60 ) {
    $format = 'bande2';
} else {
    $format = 'bande1';
}
$FacturePF=new PDF_FacturePF(6, 6, $format);

//debut de facture, coordonn�es et lignes de titre
$FacturePF->addLigne("entete", array("soc_nom"=>utf8_decode($rq_societe->soc_nom), "soc_adr1"=>utf8_decode($rq_societe->soc_adr1), "soc_adr2"=>utf8_decode($rq_societe->soc_adr2), "soc_ville"=>utf8_decode($rq_societe->soc_ville), "soc_cp"=>utf8_decode($rq_societe->soc_cp), "soc_pays"=>$rq_societe->soc_pays));

// //************************ Ventes/ticket_caisse.php?req=N� ticket
// 
$dec=0;
$page = 1;
$numero_ligne = 1;//ligne incr�ment� de l'ensemble
$n_ligne = 1; //de la page
$rq_articles=$r_articles->fetch_object();
$num_ticket = $rq_articles->tic_num;
$mdr = utf8_decode($rq_articles->mdr_nom);

$FacturePF->addLigne("titre_tableau", array('jour'=>date('j', $rq_articles->rst_validation), 'mois'=>$Mois[date('n', $rq_articles->rst_validation)], 'annee'=>date('Y', $rq_articles->rst_validation), 'num'=>$num_ticket));
$r_articles->data_seek(0);
// 
$TVA = '';
while ($rq_articles=$r_articles->fetch_object()) {
    if ($rq_articles->art_unite == 1) {
        $quantite = sprintf("%d", $rq_articles->tic_quantite);
    } else {
        $quantite = $rq_articles->tic_quantite;
    }
// 
    $FacturePF->addLigne("article", array("titre"=>utf8_decode($rq_articles->tit_nom), "prixS"=>$rq_articles->tic_prixS, "prix"=>$rq_articles->tic_prix, "quantite"=>$quantite, "tva"=>$rq_articles->tic_tva, "dec"=>$dec, "numero_ligne"=>$numero_ligne));

    @$TT += $rq_articles->tic_tt;
    @$TVA +=$rq_articles->vl_tva;
    $dec += 4;
    $numero_ligne ++;
}
$FacturePF->addLigne('total', array("dec"=>$dec, 'TT'=>$TT, 'tva'=>$TVA, 'mdr'=>$mdr));
//supression de la variable d'impression du dernier ticket
unset($_SESSION['impression_'.$_SESSION[$dossier]]);
//****************************impression direct******************
//on recupere l'imprimante
$req_imprimante="SELECT * FROM Imprimantes WHERE imp_id=1";
$r_imprimante=$idcom->query($req_imprimante);
$rq_imprimante=$r_imprimante->fetch_object();
if ($r_imprimante-> num_rows != 0) {
    //verification de l'imprimante
    ini_set("display_errors", 0);
    $impr=array();
    
    exec('lpstat -v', $tab);//$tab=tableau contenant la description des imprimantes
    foreach ($tab as $v) {
        $ta=explode(" ", $v);
        if (isset($ta[2])) {
            array_push($impr, substr($ta[2], 0, -1));//on retire les : du nom de l'imprimante et on met dans un tableau
        }
    }
    if (in_array($rq_imprimante->imp_nom, $impr)) {
        $chemin = $incpath."pdf/jours/print.pdf";
        $FacturePF->Output('F', $chemin);
        $exec='lpr -P '.$rq_imprimante->imp_nom.' -o PageSize=RP80x297 -o PageRegion=RP80x297 -o Resolution=203x203dpi -o TmtSpeed=Auto -o TmtPaperReduction=Bottom -o TmtPaperSource=DocFeedCut -o TmtBuzzerControl=After -o TmtSoundPattern=A -o TmtBuzzerRepeat=1 -o TmtDrawer1=Off -o TmtDrawer2=Off '.$chemin;
        exec($exec);
    } else {
        $FacturePF->Output();//sortie pdf
    }
} else {
    $FacturePF->Output();//sortie pdf
}