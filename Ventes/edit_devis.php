<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
/*
les articles sont stockés dans la table Tickets_$an avec dans le champ tic_num,
le n° de l'utilisateur suivi du n° du panier. Au moment du règlement, 
une ligne est ajoutée à Resume_ticket_$an, on met à jour les numéros 
du panier avec cet ID et on ajuste les stocks.
Pour les panier mis en comptes, c'est la date du jour(timestamp) 
qui prend la palce du numéro du ticket
On incrémente le panier ou on récupère un panier en attente.
Le panier en cours d'utilisation est dans la variable 
$_SESSION['panier_'.$_SESSION[$dossier]].
Tous les boutons reglements, sélections etc se réfèrent à cette variable
*/
// print_r($_SESSION);
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require_once $incpath."mysql/connect.php";
require_once $incpath."php/fonctions.php";
connexobjet();
$req_panier="SELECT Vt1_nom,
                    tic_id,
                    tic_prix,
                    tic_quantite,
                    art_unite,
                    tic_cp,
                    tic_tt
                        FROM Tickets_".ANNEE." 
                        JOIN Vtit1 ON Vt1_article = tic_article 
                        JOIN Articles ON art_id = tic_article 
                        WHERE tic_devis =".$req." 
                            ORDER BY tic_id DESC";
$r_panier=$idcom->query($req_panier);
$nb = $r_panier->num_rows;
?>
<style>
.supprimer{
width:25px;
float:left;
}
</style>
<script>
function supprimer(id) {
// alert(id);
modif(id,20,'','id',2);
setTimeout(function(){ charge('edit_devis',<?php echo $req?>,'references');}, 300)

}
function sortir(id) {
// alert(id);
modif(id,20,'0','devis',1);
setTimeout(function(){ charge('edit_devis',<?php echo $req?>,'references');}, 300)
}
function annuler(id) {
$('#dev_'+id).html('');
}
$(document).ready(function() {
    $("img").click(function(){
    if($(this).hasClass('supprimer')) {
        id = $(this).attr('dev');
        $('#dev_'+id).html("<td><button onclick='supprimer("+id+")'>Supprimer</button><button style='float:right' onclick='annuler("+id+")'>Annuler</button></td></td><td colspan=3><button style='float:right' onclick='sortir("+id+")'>Sortir</button></td>");
        };
    });
});

</script>
<table id='tableRevoir'  class="pagination" number-per-page="5" current-page="0">
    <thead>
    <tr><th colspan="4">Devis N° <?php echo $req?></th></tr>
        <tr><Th>Article</Th><Th>Prix U.</Th><Th>Qt</Th><th>Total</th></tr>
  </tr>
    </thead>
  <tbody>
    <?php
    $n=1;
    $total = 0;
    while ($rq_panier=$r_panier->fetch_object()) {
        $coul = ($n % 2 == 0)? $coulCC:$coulFF;        
        $quantite = ($rq_panier->art_unite == 1)? sprintf('%d', $rq_panier->tic_quantite):$rq_panier->art_unite;
        // print_r($rq_panier);
        echo "<tr style='background-color:".$coul."' ><td><img src='/images/annuler.png' dev='".$rq_panier->tic_id."' class='supprimer'> <div class='libell'>".$rq_panier->Vt1_nom."</div></td><td class='droite'>".$rq_panier->tic_prix."&nbsp;€</td><td class='droite'>".$quantite." </td><td class='droite'>".monetaireF($rq_panier->tic_tt)."&nbsp;€</td></tr><tr id='dev_".$rq_panier->tic_id."'></tr>";
        
        $n++;
        $cp = $rq_panier->tic_cp;
        $total += $rq_panier->tic_tt;
    }
        //la sortie du compte ne vaut que pour les articles du devis
        ?>
        <tr><TH colspan="4">Total <?php echo monetaireF($total)?>&nbsp;€</TH></tr>
        </tbody>
        <tfoot>
<tr><TD colspan="4"><button class="boutref" style="width:45%;" onclick="charge('update','&dv=<?php echo $req."-".$cp?>','panier')">Panier</button><button  class="boutref" style="width:45%;float:right;" onclick="charge('panier','impression','panier')">Annuler</button></TD></tr>
        </table>
<script>
if($('#b_compte').attr('alt')=='panier') $('#panier_<?php echo $req?>').css('visibility','hidden');
$('table#tableRevoir.libelle').width($('#nom').width());
nb_ligne = Math.floor($("#affichage").height()/22)-6;
// alert(nb_ligne);
if(nb_ligne <= <?php echo ($nb -1)?>){
$("#tableRevoir").attr('number-per-page',nb_ligne);//nb_ligne
$("#tableRevoir").pagination();
$('.pager').css('margin-top',20);
}
</script>