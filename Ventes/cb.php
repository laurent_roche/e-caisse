<?php
session_start();
$tab=explode('/', $_SERVER['REQUEST_URI']);
$dossier=$tab[1];
// print_r($_SESSION);
?>
<script type="text/javascript">
function aff_rech(){
$(this).css('display','none');
$('#rechercheCB').css('display','block');
$('#insert_cb').css('display','none');
}

function focusCB(){
    $('#insert_cb').focus();
}
$('#insert_cb').on('keydown',function(event){
    if (event.which == 13) {
        charge('insert','&cb='+$('#insert_cb').val(),'panier');
        $('#insert_cb').val('');
    }
});
focusCB();

function clavierCB(){
    $('#insert_cb').width(200);
    _clavierqt();
}
</script>
<div id='saisie_cb'>
<button class="oeil" onclick="clavierCB()"></button><input id="insert_cb" type="text" max="13" style="float:right;border:0px;width:0px;height:40px;font-size:20px;background-color:<?php echo $_SESSION['surligne_'.$_SESSION[$dossier]]?>" onblur
="setTimeout(focusCB,500)"></div>
<div id='clavier' style="margin-top:40px">
<?php
$impression = isset($_SESSION['impression_'.$_SESSION[$dossier]]) ? $_SESSION['impression_'.$_SESSION[$dossier]] : null;
if ($impression) {//le panier est vide on affiche la possibilité d'imprimer le dernier panier s'il existe
    if ($_SESSION['droits_'.$_SESSION[$dossier]] == 1) {
        ?>
    <center>
    <button id="imp" onclick='$("#facture").css("visibility","hidden");charge("pre_caisse",<?php echo $_SESSION["impression_".$_SESSION[$dossier]]?>,"mysql")' class="boutref" style="width:45%;float:left">Impression</button>
    <button class="boutref" id="facture" style="width:45%;float:right;" onclick="charge('comptes','3&tic=<?php echo $_SESSION['impression_'.$_SESSION[$dossier]]?>','references');$('#imp').css('visibility','hidden')">Facture</button>
    <button class="boutref" style="width:100%;" onclick="charge('panier','impression','panier')">Terminer</button>
    </center>
        <?php
    } else {
        ?>
    <center><button id="imp" onclick='$("#facture").css("visibility","hidden");charge("pre_caisse",<?php echo $_SESSION["impression_".$_SESSION[$dossier]]?>,"mysql")' class="boutref" style="width:100%;float:left">Impression</button>
    <button class="boutref" style="width:100%;" onclick="charge('panier','impression','panier')">Terminer</button>
    </center>
        <?php
    }
}
?>
</div>
<button id="bt_recherche" class="boutref" style="width:100%;position: absolute;bottom: 0px;left:0px" onclick="charge('clavier1','','panier')">Recherche<br><span style='font-size:10pt'><?php echo $_SESSION['nom_'.$_SESSION[$dossier]]?></span></button>
<script>
function _clavierqt(){
    $('#clavier').html(clavierqt);
    $('#keypad').css('top',50);
    $('#keypad').css('display','block');

    $("#keypad .key").click(function(){
        if($(this).html() == 'Valider'){
            charge('insert','&cb='+$('#insert_cb').val(),'panier');
        } else if ($(this).html() == 'Annuler'){
            $('#insert_cb').val('');
            $('#keypad').css('display','none');
            $('#insert_cb').width(0);
        } else if ($(this).html() == 'Effacer'){
            $('#insert_cb').val('');
            $('#valider').css('visibility', 'hidden');
        } else {
            $("#insert_cb").val($("#insert_cb").val()+$(this).html());
            $('#valider').css('visibility', 'visible');
        }
    });
}

</script>