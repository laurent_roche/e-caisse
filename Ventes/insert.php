<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
require_once $incpath . "mysql/connect.php";
require_once $incpath . "php/fonctions.php";
connexobjet();
/*
Insertion des articles dans le panier
pour les élément qui sont envoyé en block, ils sont traité comme les cartes,
plusieurs articles avec leur quantité sont envoyé en même temps
Les articles par références sont insérés dans le fichier references.php
*/
$cb = filter_input(INPUT_GET, "cb", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$carte = filter_input(INPUT_GET, "carte", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
// print_r($_GET); exit;
if ($carte) {
    $tabcarte = explode(",", substr($carte, 0, -1));
    foreach ($tabcarte as $v) {
        //le numréro de l'article et la quantité sont sous la forme article-quantite
        $tabcart = explode("-", $v);
        print_r($tabcart);
        if ($tabcart[2] == 1) { //article-quantité
            $art_id = $tabcart[0];
            $quantite = $tabcart[1];
            //verification et insertion
            include 'insert_quantite.php';
        } elseif ($tabcart[2] > 1) { //pseudo article (1=remisable,2=non-remisable), qt=1, article-prix
            $req_insert = "INSERT INTO Tickets_" . ANNEE . " 
                        (tic_num,
                        tic_article,
                        tic_quantite,
                        tic_quantiteS,
                        tic_prix,
                        tic_prixS,
                        tic_pht,
                        tic_tt,
                        tic_tva,
                        tic_ntva)
                            SELECT " . $_SESSION['panier_' . $_SESSION[$dossier]] . ",
                                art_id,
                                1,
                                1,
                                " . $tabcart[1] . ",
                                " . $tabcart[1] . ",
                                art_pht,
                                $tabcart[1],
                                tva_nom,
                                art_tva
                                    FROM Articles 
                                    JOIN Tva ON tva_id = art_tva 
                                        WHERE art_id =" . $tabcart[0];
        }
        $r_insert = $idcom->query($req_insert);
        //echo $idcom->errno." ".$idcom->error;exit;
        unset($_SESSION['impression_' . $_SESSION[$dossier]]); //supression du dernier ticket
    }
    // exit;
    header('location:panier.php');
}
//verification du cb
if (strlen($cb) == 13) {
    if (!is_numeric($cb)) { // le code-barres ne doit contenir que des chiffres
        echo '<h1>erreur de lecture</h1>';
        exit;
    } else {
        ean13($cb);
        $req_id = "SELECT art_id, pro_mode FROM Articles LEFT JOIN Promo ON pro_article = art_id WHERE art_cb = " . $cb . " AND art_statut < 3";
        $r_id = $idcom->query($req_id);
        $rq_id = $r_id->fetch_object();
        if ($r_id->num_rows != 0) {
            if (($rq_id->pro_mode == 2) && ($_SESSION['soldes'] == 'non')) {
                echo '<h1>Cet article est en solde et n\'est donc pas disponible actuellement</h1>';
                exit;
            }
            $art_id = $rq_id->art_id;
        } else {
            $art_id = 0;
        }
    }
} elseif (strlen($cb) < 13) { //c'est l'id d'un article
    if (!is_numeric($cb)) {
        echo '<h1>erreur de saisie</h1>';
        exit;
    } else {
        $art_id = $cb;
    }
} else {
    echo '<h1>Le code n\'est pas cohérent</h1>';
}
//verification si l'article existe
echo $req_verif = "SELECT * FROM Articles WHERE art_id = " . $art_id;
$r_verif = $idcom->query($req_verif);

$r_verif->num_rows;
if ($r_verif->num_rows == 0) {
    echo "<h1>L'article recherché ne se trouve pas dans la base de données ou n'est pas en vente.<br> Vérifiez le code entré <br>ou effectuez une recherche sur le titre</h1>
    <center><button class=\"boutref\" style=\"width:200px\" onclick=\"charge('panier','','panier')\">Continuer</button></center>";
    exit;
}
require 'insert_quantite.php';

$r_insert = $idcom->query($req_insert);
// echo $idcom->errno." ".$idcom->error;exit;
if ($idcom->errno == 1064) { //erreur d'insertion = article non trouvé
    echo "<h1>L'article recherché ne se trouve pas dans la base de données.<br> Vérifiez le code entré <br>ou effectuez une recherche sur le titre</h1>
    <center><button class=\"boutref\" style=\"width:200px\" onclick=\"charge(\'panier\',\'\',\'panier\')\">Continuer</button></center>";
    ?>
    <script>
        $('#insert_cb').val('');
    </script>
    <?php
    exit;
}
//supression de la variable d'impression du dernier ticket
unset($_SESSION['impression_' . $_SESSION[$dossier]]);
// print_r($_SESSION);
header('location:panier.php');
?>