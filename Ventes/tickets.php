<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
require_once $incpath."mysql/connect.php";
require_once $incpath."php/fonctions.php";
if (substr($_SESSION['panier_'.$_SESSION[$dossier]], -4)==1999) {
    echo "<h1>Vous n'avez pas encore enregistré de ticket</h1>
    <center><button class='Bcartes' onclick=\"charge('panier','','panier')\">Reprendre</button></center>";
    exit;
}
connexobjet();
//on liste les tickets enregistré ce jour par ce vendeur dans l'ordre descendant
$req_ticket="SELECT rst_id, rst_num, rst_total, DATE_FORMAT(rst_validation, '%H:%i') AS date, mdr_nom, fac_ticket FROM Resume_ticket_".ANNEE." JOIN Mode_reglement ON mdr_id = rst_etat LEFT JOIN Factures_".ANNEE." ON fac_ticket = rst_id WHERE DATE(rst_validation) = DATE(NOW()) AND rst_utilisateur = $_SESSION[$dossier] ORDER BY rst_id DESC;";
$r_ticket=$idcom->query($req_ticket);
if ($idcom->error) {
    echo "<br>".$idcom->errno." ".$idcom->error."<br>";
}
$nb = $r_ticket->num_rows;
/*
+--------+---------+-----------+----------+------------+
| rst_id | rst_num | rst_total | date     | mdr_nom    |
+--------+---------+-----------+----------+------------+
|    291 |       2 |      8.00 | 14:23:49 | EspÃ¨ces   |
|    290 |       1 |     46.20 | 09:35:54 | EspÃ¨ces   |
+--------+---------+-----------+----------+------------+
*/
?>
<table id='tableTicket' class="pagination" number-per-page="6" current-page="0">
    <thead>
        <tr><TH>N° du ticket</TH><TH>Valeur</TH><TH>Mode de règlement</TH><TH>Heure</TH></tr>
    </thead>
<tbody>
<?php
$n=0;
while ($rq_ticket=$r_ticket->fetch_object()) {    
    $coul=($n % 2 == 0)?$coulCC:$coulFF;
    if ($rq_ticket->fac_ticket == $rq_ticket->rst_id) {//il y a une facture
        $comp = "<img src='/images/pdf.gif' style='float:right'>";
    } else {
        $comp = "";
    }
    // print_r($rq_ticket);
    echo "<tr onclick=\"charge('detail_ticket',".$rq_ticket->rst_id.",'references')\" style='background-color:".$coul."' ><td>".$rq_ticket->rst_id."/".$rq_ticket->rst_num."</td><td class='droite'>".$rq_ticket->rst_total."&nbsp;€</td><td>".$rq_ticket->mdr_nom.$comp." </td><td class='droite'>".$rq_ticket->date."</td></tr>";
    $n++;
}
?>
</tbody></table>
<script>
<?php
require 'tableTicketjs.php';
?>
</script>
