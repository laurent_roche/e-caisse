<?php
session_start();
$afficheur = isset($_SESSION['afficheur']) ? $_SESSION['afficheur'] : null;
$prix= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
if ($afficheur) {
    $ch = curl_init('http://'.$_SESSION['afficheur'].'/actualiser.php?&prix='.$prix);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 200);
    curl_exec($ch);
    curl_close($ch);
}
