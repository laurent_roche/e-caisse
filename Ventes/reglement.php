<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$mode= filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);

if ($mode == 5) {//différé, traitement spéciale car facture imposée
    header('location:differe.php');
    exit;
}
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
$req_reglement="SELECT mdr_nom, mdr_seuil FROM Mode_reglement WHERE mdr_id = $mode";
$r_reglement=$idcom->query($req_reglement);
$rq_reglement=$r_reglement->fetch_object();

if (($config['don'] == 1)&&($mode != 1)) {
    $rendre = "Don";
    $libel = "Confirmer la réception d'un don d'une valeur de";
} else {
    $rendre = "À rendre";
    $libel = "Confirmer la sortie de caisse d'une valeur de";
}
  ?>
  <span style="display:none" id='mode'><?php echo $mode?></span>
<h2 style='padding=0;margin=0'><img src='/images/reglements/mdr_<?php echo $mode?>.png'> <?php echo $rq_reglement->mdr_nom?></h2>

<script>
<?php
if ($rq_reglement->mdr_seuil != 0) {
      ?>
  if($('#prix').html() > <?php echo $rq_reglement->mdr_seuil?>){
  $('#references').html('<h2><?php echo $rq_reglement->mdr_nom?><br></h2>Avec ce mode de règlement, vous ne pouvez pas régler ce ticket qui dépasse le seuil prévu de <?php echo $rq_reglement->mdr_seuil?> €');
  }
  <?php
  }
?>

<?php
// echo $mode;
if ($mode == 1) {
    //on vérifie la présence dre la table tableTicket, si elle est absente on empèche l'affichage du reglement
?>
if($('.panier_actif').attr('alt') < 1){
    $('#tableReglement').css('visibility','hidden');
    $( "h2" ).append( "<br>Le ticket est vide" );
    }
else $('#tableReglement').css('visibility','visible');
<?php
}

?>
// ************************************gestion du clavier virtuel************************************
//**********affichage**********************
  $('.clavier').click(function(e){
  champ=$(this).attr('id');
//   alert(champ);
  $('#'+champ).empty();
  $('#'+champ).css('backgroundColor','<?php echo $_SESSION['surligne_'.$_SESSION[$dossier]]?>');
//   $('#bouton_enregistrer').html(clavierqt);

  $('#keypad').css('top',240);
  $('#keypad').css('display','block');
  
  //on cache le bouton enregistrer
  $('#enregistrer').css('visibility','hidden');
  });
//***********les boutons*********************
$('.key').unbind();
    $('.key').click(function(e){
        if ($(this).attr('id')=='bconfirm') {
            charge('validation',valeur+'&md='+$('#mode').html()+'&du='+$('#du').html(),'liste_panier');
        }
        switch ($(this).html()) {
            case 'Effacer' :{ $('#'+champ).html('');
                            $('#rendu').html('');
                            }
            break;
            case 'Annuler' : {
                $('#'+champ).html($('#donneA').html());//remettre la valeur initiale et du rendu et affichage du bouton
                pr=0;
                $('#rendu').html(pr.toFixed(2));
                $('#enregistrer').css('visibility','visible');
                $('#'+champ).css('backgroundColor','white');
            }
            break;
            case 'Valider' : {
                valeur=$('#'+champ).html();
                if (($('#du').html() > 0.00)&&(<?php echo $mode?> != 1)) {
                    $('#keypad').css('display','none');
                    $('#bconfirm').html($('#rendu').html()+' €<br> Confirmer');
                    $('#confirm').css('display','block');
                } else {
                    charge('validation',valeur+'&md='+$('#mode').html()+'&du='+$('#du').html(),'liste_panier');
                }
            }
            break;
            default : {
                $('#'+champ).append($(this).html());
                $('#rendu').empty();
                pr=($('#donne').html() - $('#prix').html());
                $('#rendu').append(pr.toFixed(2));
            }
        }
        if(+$('#donne').html() >= +$('#du').html()) $('#valider').css('visibility','visible');
        else $('#valider').css('visibility','hidden');

  });


// ************************************fin de la gestion du clavier virtuel************************************
</script>
<table id="tableReglement">
  <tr style='background-color:<?php echo $coulCC?>'><TH>Dû</TH><TH id="du" class='align_d'></TH></tr>
  <tr style='background-color:<?php echo $coulFF?>;height:60px'><TH>Donné</TH><TH id="donne" class="clavier" style='background-color:white;border:dotted 1px'></TH></tr>
  <tr><TH><?php echo $rendre?></TH><TH id="rendu" class='align_d'></TH></tr>
  <tr><TH colspan="2" id='bouton_enregistrer'><button onclick="charge('validation',$('#prix').html()+'&md='+$('#mode').html()+'&du='+$('#du').html(),'liste_panier');" id="enregistrer" class="Bcartes">Enregistrer</button></TH></tr>
  <tr style='display:none'><TH>Donné origine</TH><TD id="donneA"></TD></tr>
  
</table>
<div id="confirm" style="display: none;"><h3><?php echo $libel?><br>
<br><Button class='key' id='bconfirm' style="width:240px" class="Bcartes"></Button></h3></div>
<div id="keypad" style="display: none;">
    <div class="keypad-row">
        <button type="button" class="key">1</button>
        <button type="button" class="key">2</button>
        <button type="button" class="key">3</button>
    </div>
    <div class="keypad-row">
        <button type="button" class="key">4</button>
        <button type="button" class="key">5</button>
        <button type="button" class="key">6</button>
    </div>
    <div class="keypad-row">
        <button type="button" class="key">7</button>
        <button type="button" class="key">8</button>
        <button type="button" class="key">9</button>
    </div>
    <div class="keypad-row">
        <div class="keypad-space"></div>
        <button type="button" class="key">0</button>
        <button type="button" class="key">.</button>
    </div>
    <div class="keypad-row">
        <div class="keypad-space"></div>
        <button type="button" style="background-color:red;text-shadow: 2px 2px white;" onclick="$('#keypad').css('display','none')" class="key">Annuler</button>
        <button type="button" style="background-color:yellow;text-shadow: 2px 2px white;" class="key">Effacer</button>
        <button id='valider' type="button" style="background-color:green;text-shadow: 2px 2px white;visibility:hidden" onclick="$('#keypad').css('display','none')" class="key">Valider</button>
    </div>
</div>
<script>
// alert($('#prix').html()*10);
$('#du').append($('#prix').html());
$('#donne').append($('#prix').html());
$('#donneA').empty();
$('#donneA').append($('#prix').html());
pr=($('#donne').html() - $('#prix').html());
$('#rendu').append(pr.toFixed(2));
<?php
$afficheur = isset($_SESSION['afficheur_'.$_SESSION[$dossier]]) ? $_SESSION['afficheur_'.$_SESSION[$dossier]] : null;
if ($afficheur) {
    ?>
$(document).ready(function() {
    $('#donne').on("DOMSubtreeModified",function(){
    if($('#donne').html() !='') {
    if(+$('#donne').html()>=+$('#du').html())
        charge('actualiser_externe',$('#donne').html(),'myqsl');
    }
    });
});
<?php
}
?>
// $('#tableReglement').css('margin-left',($('#affichage').width()-400)/2);
</script>