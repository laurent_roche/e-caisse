<?php
session_start();
// print_r($_SESSION);
if (isset($config['mode'])) {
    ?>
    <script>$('.sp-container').remove();</script>
    <?php
    $mode = $config['mode'];
    include 'tableau_mois'.$mode.'.inc.php';
} else { //reinitialisation de la session
    ?>
    session_destroy();
    <script>
    top.location='/index.php'
    </script>
    <?php
}
exit;
?>