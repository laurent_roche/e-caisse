<?php
session_start();
define('ARCHIV_CSV_UPLD_FPN', 'archiveUpld.csv');

/**
 * Contrôler un fichier généré avec son empreinte
 * #e-Caisse_CTRL#
 */
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
require_once $incpath."mysql/connect.php";
require_once $incpath."php/fonctions.php";
connexobjet();

$psHash= filter_input(INPUT_POST, "h", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

require_once $incpath.'inc/hash_inc.php';

echo '<h3>Données archivées</h3>';

if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], ARCHIV_CSV_UPLD_FPN)) {
    echo "<p>Le fichier ". basename($_FILES["fileToUpload"]["name"]). " a été comparé.</p>";
} else {
    echo "<p>Il y a eu une erreur durant le téléchargement de fichier.</p>";
}
$sTheHash=hash_file(HASH_ALGO, ARCHIV_CSV_UPLD_FPN) ;
if ($sTheHash===$psHash) {
    echo '<p>Le fichier n\'a pas été modifié. &nbsp; <img src="/images/valider.png"/></p>';
} else {
    echo '<p>Le fichier a été <strong>modifié</strong>. &nbsp; <img src="/images/fermer.png"/></p>';
}
?>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p style="text-align: center"><button type="button" onclick="javascript:window.close()">Fermer</button></p>
