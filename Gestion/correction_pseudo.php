<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
// echo $incpath;
$ok = filter_input(INPUT_GET, "ok", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$article = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$ligne = filter_input(INPUT_GET, "ligne", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
// $_SESSION['aide_'.$_SESSION[$dossier]] = 'G1';
connexobjet();
if ($ok == 1) {
    //mise à jour des données
    $req_cor = 'UPDATE Articles, Tickets_'.ANNEE.' SET tic_article = '.$article.', 
                                                        art_stk = (art_stk-tic_quantite),
                                                        tic_prixS = art_ttc
                    WHERE art_id = '.$article.' AND tic_id = '.$ligne;
    $r_cor = $idcom->query($req_cor);
    $req_jour = 'SELECT DATE(rst_validation) AS date FROM Resume_ticket_'.ANNEE.' JOIN Tickets_'.ANNEE.' ON rst_id =tic_num WHERE tic_id ='.$ligne;
    $r_jour = $idcom->query($req_jour);
    $rq_jour =$r_jour->fetch_object();
    ?>
    <script>charge("pseudo_jour","<?php echo $rq_jour->date?>","panneau_d");</script>
    <?php
    exit;
}
$req_article = 'SELECT Vt1_nom, tic_prix, (SELECT CONCAT(tit_nom,"_",art_ttc) FROM Titres JOIN Articles ON art_id = tit_article WHERE tit_article = '.$article.' AND tit_niveau = 1) AS tit_nom FROM Articles JOIN Tickets_2021 ON tic_article = art_id JOIN Vtit1 ON vt1_article = tic_article WHERE tic_id='.$ligne;
// exit;
$r_article = $idcom->query($req_article);
$rq_article =$r_article->fetch_object();
$tab_change = explode("_",$rq_article->tit_nom);
?><br>
Voulez-vous vraiment remplacer l'article<br><br> <b><?php echo $rq_article->Vt1_nom."</b> au prix de <b>".$rq_article->tic_prix?> €</b><br> par<br>
<b><?php echo $tab_change[0]."</b> au prix de <b>".$tab_change[1]?> € ?</b><br><br>
Le prix de vente reste inchangé, l'article est modifié ainsi que son prix de base s'il est différent du prix de vente.<br>Le stock de l'article est mis à jour.
<br><br><button onclick='charge("correction_pseudo","<?php echo $article."&ligne=".$ligne."&ok=1"?>","panneau_d")'>Ok</button>
<script>
$("#panneau_d").height($("#affichage").height()-10);
</script>
