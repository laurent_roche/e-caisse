<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$an= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
if(!$an) $an = date("Y");
$req_devis="SELECT cpt_nom , dev_id, SUM(tic_tt) AS total, tic_num , fac_id, DATE(rst_validation) AS date_facture
                        FROM Devis_$an
                        LEFT JOIN Tickets_$an ON dev_id = tic_devis
                        LEFT JOIN Comptes ON tic_cp = cpt_id
                        LEFT JOIN Factures_$an ON fac_ticket = tic_num
                        LEFT JOIN Resume_ticket_$an ON rst_id = tic_num
                            GROUP BY dev_id";
$r_devis=$idcom->query($req_devis);
?>
<style>
#factures{width:90%}
table.tablesorter tbody td {
    font-size: 12px;
    text-align:left;
    color: #3D3D3D;
    padding: 4px;
    background-color: <?php echo $coulFF?>;
    vertical-align: top;
}
table.tablesorter tbody tr.odd td {
    text-align:left;
    border-top:solid 1px;
    background-color:<?php echo $coulCC?>;
}
table.tablesorter thead tr .headerSortDown, table.tablesorter thead tr .headerSortUp {
    background-color: #8dbdd8;
}
</style>
<script src="/js/jquery.tablesorter.js"></script>
<script>
$('#panneau_d').empty();

$('#factures td').click(function() {
    if($(this).html().slice(0,8)!='<a href=') {
        charge('detail_devis',$(this).parent().attr('fac')+'&an=<?php echo $an?>','panneau_d');
        $(this).parent().css('fontWeight','bold');
    }
})

$(document).ready(function(){
        $("#factures").tablesorter({ widgets: ['zebra']});
    }
);
</script>
<?php
if ($an > $config["debut"]) { 
    $bt1="<button class='pt_bt' style='float:left;' onclick=\"charge('devis',".($an-1).",'panneau_g')\"><&nbsp;".($an-1)."</button>";
} else {
    $bt1="<button class='pt_bt' style='float:left'></button>";
}

if($an < date("Y")) { 
    $bt2="<button class='pt_bt' style='float:right;' onclick=\"charge('devis',".($an+1).",'panneau_g')\">&nbsp;".($an+1)."></button>";
} else {
    $bt2="<button class='pt_bt' style='float:right'></button>";
}
?>
<h3><?php echo $bt1?>Devis de l'année <?php echo $an?><?php echo $bt2?></h3>
<center><table id='factures' class="tablesorter">
    <thead>
    <TR>
    <TH>N°</TH><TH>Nom</TH><TH>Valeur</TH><TH>N° facture</TH><th>Date</th>
    </TR>
    </thead>
    <tbody>
<?php
$numero_facture = "";
while ($rq_devis=$r_devis->fetch_object()) {
    if (strlen($rq_devis->tic_num) == 10) {//timestamp		
        $date = date("d/m/Y",$rq_devis->tic_num);
    } else if ($rq_devis->date_facture) {
        $date = dateFR($rq_devis->date_facture);
        $numero_facture = (($an*1000) + $rq_devis->fac_id);
    }
    else {
        $date = $rq_devis->tic_num;
    }
    echo "<tr fac='".$rq_devis->dev_id."'><TD>";
    if(file_exists($incpath."pdf/devis/devis_".(($an*1000) + $rq_devis->dev_id).".pdf")) {
        echo '<a href="'.$incpath."pdf/devis/devis_".(($an*1000) + $rq_devis->dev_id).'.pdf"><img src="../images/pdf.gif"></a> ';
    }
    echo "Dev".(($an*1000) + $rq_devis->dev_id)."</TD>
    <TD>".$rq_devis->cpt_nom."</TD>
    <TD style='text-align:right'>".monetaireF($rq_devis->total)."&nbsp;€</TD><TD>".$numero_facture."</TD>";
    echo "<TD>";
    if(file_exists($incpath."pdf/factures/facture_".(($an*1000) + $rq_devis->fac_id).".pdf")) echo '<a href="'.$incpath."pdf/factures/facture_".(($an*1000) + $rq_devis->fac_id).'.pdf"><img src="../images/pdf.gif" style="width:14px; float:right"></a>';
    echo $date."</TD></tr>";
    $numero_facture ='';
}
?>
</tbody>  
</table></center>

<script>
var b=$('#affichage').height() - 20;
$("#panneau_g").css('max-height', b);

</script>