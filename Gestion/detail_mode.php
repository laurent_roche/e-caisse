<?php
session_start();
$tab = explode('/', $_SERVER['REQUEST_URI']);
$dossier = $tab[1];
$_SESSION['aide_' . $_SESSION[$dossier]] = 'G1';
// print_r($_SESSION);
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
$req = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$tab = explode("-", $req);
$cemois = $tab[1];
$ceannee = $tab[0];
$cejour = $tab[2];
$mode = $tab[3];

$cjour = $tab[0] . "-" . $tab[1] . "-" . $tab[2];
require $incpath . "mysql/connect.php";
require $incpath . "php/fonctions.php";
connexobjet();
//on recupere les modes de reglements utilisés ou nom
$req_reg = "SELECT * FROM Mode_reglement WHERE mdr_etat = 1 ORDER BY mdr_ordre";
$r_reg = $idcom->query($req_reg);
$selctCombo = "<select class='monsel'><option></option>\n";
$selctComboC = "<select class='monsel'><option></option>\n"; //combo sans Espèce
$tbj_nom = '';
$tbj_id = '';
$tab_mode = array('');
while ($rq_reg = $r_reg->fetch_object()) {
    if ($mode == $rq_reg->mdr_id) {
        $sel = " selected";
    } else {
        $sel = "";
    }
    if ($rq_reg->mdr_id == 1) {
        $selctComboC .= "<option value='" . $rq_reg->mdr_id . "' disabled>" . $rq_reg->mdr_nom . "</option>\n";
    } else {
        $selctComboC .= "<option value='" . $rq_reg->mdr_id . "'" . $sel . ">" . $rq_reg->mdr_nom . "</option>\n";
    }
    $selctCombo .= "<option value='" . $rq_reg->mdr_id . "'" . $sel . ">" . $rq_reg->mdr_nom . "</option>\n";
    $tbj_nom .= "'" . $rq_reg->mdr_nom . "',";
    $tbj_id .= "'" . $rq_reg->mdr_id . "',";
    array_push($tab_mode, $rq_reg->mdr_nom);
}
$selctCombo .= "</select>";
$tbj_nom .= "''";
$tbj_id .= "''";
?>
<script>
    $('#panneau_d').empty();
    $(document).ready(function() {

        var tableau_nom = [<?php echo $tbj_nom ?>];
        var tableau_id = [<?php echo $tbj_id ?>];
        var Tableau = new Array(tableau_id, tableau_nom);
        for (var i = 0; i < tableau_id.length; i++) {
            Tableau[tableau_id[i]] = tableau_nom[i];
        }

        $('.monsel').change(function() {
            id = $(this).parent().attr('id');
            val = $(this).val();

            var dest = $('#m_' + val + '').html();
            if (dest === undefined) dest = '0,00';

            $('#panneau_d').html("<h3>Changement de mode de règlement pour le ticket " + id + "</h3>Attention, cette action est irréversible<br> Passage de vers " + Tableau[<?php echo $mode ?>] + " vers " + Tableau[val] + ".<br> Les nouvelles valeur seront : <br><b>" + Tableau[<?php echo $mode ?>] + " : " + ($('#m_<?php echo $mode ?>').html().replace(',', '.') - ($('#v_' + id + '').html().replace(',', '.'))).toFixed(2) + "<br>" + Tableau[val] + " : " + (+dest.replace(',', '.') + (+$('#v_' + id + '').html().replace(',', '.'))).toFixed(2) + "</b><br><button class='fdroite' onclick=\"charge('change_mode','<?php echo $req ?>&id=" + id + "&val=" + val + "','panneau_g');\">Confirmer</button>");
            //   
        });

        $('#jour tbody td').click(function() {
            if ($(this).html().slice(0, 8) != '<select ') {
                charge('detail_ticket', '<?php echo $ceannee ?>&tic=' + $(this).parent().attr('id'), "panneau_d");
                $('#jour tbody td').css('fontWeight', 'normal');
                $(this).css('fontWeight', 'bold');
            }
        });
    });
</script>

<table id='jour' class="jour" width="100%">
    <thead>
        <tr>
            <TH colspan="5">&nbsp;</TH>
        </tr>
        <TR>
            <TH>Ticket</TH>
            <TH>Valeur</TH>
            <TH>Heure</TH>
            <TH>Vendeur</TH>
            <TH>Règlement</TH>
        </TR>
    </thead>
    <tbody>
        <?php
        $valeur = 0.00;
        $req_jour = "SELECT tic_num, rst_id,
									rst_num,
									SUM(tic_tt) AS valeur,
									rst_validation,
									uti_nom,
									rst_mod,
									mdr_nom,
									mdr_correction,
									fac_id,
									rst_etat,
									rst_etatS,
                                    rst_total
									FROM Resume_ticket_$ceannee
										LEFT JOIN Tickets_$ceannee ON rst_id = tic_num
										JOIN Mode_reglement ON mdr_id = rst_etat
										JOIN Utilisateurs ON rst_utilisateur = uti_id
										LEFT JOIN Factures_$ceannee ON fac_ticket = rst_id
											WHERE DATE(rst_validation) = '$cjour' AND rst_etat='$mode' GROUP BY rst_id";
        $r_jour = $idcom->query($req_jour);
        $n = 0;
        while ($rq_jour = $r_jour->fetch_object()) {
            $coul = ($n % 2 == 0) ? $coulCC : $coulFF;
            $date = date_create($rq_jour->rst_validation);
            echo "<TR id='" . $rq_jour->rst_id . "' style=background-color:" . $coul . " >
		<td>" . $rq_jour->rst_id . "/" . $rq_jour->rst_num . "</td>
		<td id='v_" . $rq_jour->rst_id . "'>" . monetaireF($rq_jour->valeur) . " (" .monetaireF($rq_jour->rst_total).")</td>
		<td>" . date_format($date, 'H:i:s') . "</td>
		<td>" . $rq_jour->uti_nom . "</td>";
            //si rst_mod = 1, modification du contenu du ticket, on interdit le passage en caisse
            if (($rq_jour->rst_etat == $rq_jour->rst_etatS)) {
                if ($rq_jour->rst_mod == 1) {
                    echo "<td id='" . $rq_jour->rst_id . "'>" . $selctComboC . "</td>";
                } else {
                    echo "<td id='" . $rq_jour->rst_id . "'>" . $selctCombo . "</td>";
                }
            } else {
                echo "<td>" . $rq_jour->mdr_nom . "</td>";
            }
            echo "</TR>";
            $valeur += $rq_jour->rst_total;
            $n++; //onclick='charge(\"detail_ticket\",\"".$ceannee."&tic=".$rq_jour->rst_id."\",\"panneau_d\")'
        }

        ?>
        <tfoot><tr><td>Total</td><td><b><?php echo $valeur?></b></td><td colspan="3"></td></tr></tfoot>
        </tbody>
</table>

<script>
    $("#panneau_g").css('height', ($('#affichage').height() - 10));
    $("#panneau_d").css('height', $('#panneau_g').height());
</script>