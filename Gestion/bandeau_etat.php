<?php
session_start();
$tab=explode('/', $_SERVER['REQUEST_URI']);
$dossier=$tab[1];
?>
<script type="text/javascript">
$(document).ready(function(){
    $('.param').click(function() {
        $('.sp-container').remove();
        $('.param').removeClass('ombre');
        $(this).addClass('ombre');
    });
});

</script>
<link href="/simpleAutoComplete.css" rel="stylesheet" type="text/css">
<h3>États</h3>
<center>
  <table id='bandeau_etat' style="width:90%">
    <TR>
        <TH class="param" onclick="$('#affichage').empty();$('#affichage').html('<div id=panneau_d></div><div id=panneau_g></div>');charge('journal_factures',<?php echo date('Y')?>,'panneau_g')">Factures</TH>
        <TH class="param" onclick="voir('secteurs','panneau_g')">Ventes/secteurs</TH>
        <TH class="param" onclick="voire('ventes','affichage')">Ventes</TH>
        <TH class="param" onclick="voire('annees','affichage')">Années</TH>
        <TH class="param" onclick="voir('correction_stock','affichage')">Correction stock</TH>
        <TH class="param" onclick="charge('analyse','&fil=A','affichage')">Analyse/Export Achats</TH>
        <TH class="param" onclick="charge('analyse','&fil=V','affichage')">Analyse/Export Ventes</TH>
    </TR>
  </table>
</center>
<script>var w=$(window).height();
var b=$('#bandeau').height();
$("#affichage").height(w-b-40);
</script>