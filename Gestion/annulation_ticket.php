<?php
session_start();
$tab = explode('/', $_SERVER['REQUEST_URI']);
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
$ticket = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath . "mysql/connect.php";
require $incpath . "php/fonctions.php";
$_SESSION['aide_' . $_SESSION[$dossier]] = 'G2';
$ceannee = ANNEE;
connexobjet();
//copie négative de Resume_ticket
$req_tic = "INSERT INTO Resume_ticket_$ceannee (rst_num, rst_utilisateur, rst_etat, rst_etatS, rst_total, rst_totalS, rst_validation, rst_mod)
      SELECT rst_num, rst_utilisateur, rst_etat, rst_etatS, (rst_total * -1), (rst_totalS * -1), NOW(), rst_mod
        FROM Resume_ticket_$ceannee
          WHERE rst_id = $ticket";
$r_tic = $idcom->query($req_tic);
// echo $idcom->error;
$rsc_id = $idcom->insert_id;
//copie négative des articles
$req_art = "INSERT INTO Tickets_$ceannee (tic_num, tic_article, tic_quantite, tic_quantiteS, tic_prix, tic_prixS, tic_pht, tic_tt, tic_tva, tic_ntva)
                SELECT $rsc_id, tic_article, (tic_quantite * -1), (tic_quantiteS * -1), (tic_prix * -1), (tic_prixS * -1), tic_pht, (tic_tt * -1), tic_tva, tic_ntva FROM Tickets_$ceannee WHERE tic_num = $ticket";
$idcom->query($req_art);
// echo $idcom->error;
//gestion du stock
$req_stock = 'UPDATE Articles JOIN Tickets_'.$ceannee.' ON art_id = tic_article SET art_stk = (art_stk-tic_quantite) WHERE tic_num = '.$ticket;
$r_stock = $idcom->query($req_stock);
// echo $idcom->error;
?>
<script>
    charge('fin_jour','','panneau_g');
</script>