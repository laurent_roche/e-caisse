<?php
if (!isset($incpath)) {
    session_start();
    if (!isset($incpath)) {
        $p=preg_split("[/]", $_SERVER['PHP_SELF']);
        $incpath="";
        for ($i=1;$i<sizeof($p)-1;$i++) {
            $incpath='../'.$incpath;
        }
        unset($p, $i);
    }
    $date= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    include $incpath."mysql/connect.php";
    include $incpath."php/fonctions.php";
    connexobjet();

    if (!$date) {
        $date = ANNEE."-".date('m')."%";
    }//permet l'importation directe dans libreoffice
    ?>
<html>
<head>
       <TITLE></TITLE>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
   </style>
 </head><body>
    <?php
}

$an = substr($date, 0, 4);
// |     25 | 5800000  |        4 |espèces
// |     26 | 5112000  |        4 |chèques
// |     27 | 411CARTB |        4 |cartes bancaires
// |     28 | 4711001  |        4 |différés
// requete en deux temps : groupée pour tous les modes sauf 28 qui devra être détaillé
$req_recher="SELECT cod_id,
                    SUM(rst_total) AS rst_total,
                    rst_id,
                    mdr_nom,
                    cod_nom
                        FROM Resume_ticket_$an
                        JOIN Mode_reglement ON mdr_id = rst_etat
                        JOIN Codes ON cod_id = mdr_code
                            WHERE DATE(rst_validation) LIKE '$date%' 
                            AND cod_id != 28 
                                GROUP BY cod_id ORDER BY mdr_ordre";
// exit;
$r_recher=$idcom->query($req_recher);
// echo $r_recher->num_rows;
?>
<center id="resultat_analyse">
<?php
if ($r_recher->num_rows == 0) {
    echo "<h1>Il n'a pas de donnée à analyser</h1>";
    exit;
}

?>

<table style="width:900px"><tr style='background-color:white'><th colspan="8">Analyse des ventes</th></tr>
<TR><TH>Code J.</TH><TH>Jour</TH><TH>N° compte</TH><TH>Analytique</TH><TH>Libellé</TH><TH>Mvts débit</TH><TH>Mvts crédit</TH><th></th></TR>
<?php
$n = 1;
$m = 0;
// $ligne_csv = array('','','','','','');
// $csv = array();
$mode = 1;//valeur fictive de départ
$ttr = 0;
while ($rq_recher = $r_recher->fetch_object()) {
    // array_push($csv, $ligne_csv);
    if ($m%2 == 0) {
        $coul=$coulCC;
    } else {
        $coul=$coulFF;
    }
    // print_r($rq_recher);
    echo "<tr style='background-color:".$coul."'><td>VE</td><td>".dateFR($date)."</td><td>".$rq_recher->cod_nom."</td><td></td><td>Ventes par  -".$rq_recher->mdr_nom."</td><td class='droite'>".monetaireF($rq_recher->rst_total)."</td><td></td><td></td></tr>";
    $ttr += $rq_recher->rst_total;
    // $ligne_csv[$m][1]=$rq_recher->cod_nom;
    // $ligne_csv[$m][3]=$rq_recher->mdr_nom;
    $m++;
}
//--------------------------------différés----------------------------
$req_recher="SELECT cod_id,
                    rst_total,
                    rst_id,
                    mdr_nom,
                    cod_nom,
                    fac_id,
                    cpt_nom
                        FROM Resume_ticket_$an JOIN
                        Mode_reglement ON mdr_id = rst_etat
                        JOIN Codes ON cod_id = mdr_code
                        LEFT JOIN Factures_$an ON fac_ticket = rst_id
                        LEFT JOIN Comptes ON cpt_id = fac_cp
                            WHERE DATE(rst_validation) LIKE '$date%' AND
                                cod_id = 28 
                                    ORDER BY mdr_ordre, fac_id";
$r_recher=$idcom->query($req_recher);
$m = 0;
while ($rq_recher = $r_recher->fetch_object()) {
    if ($m%2 == 0) {
        $coul=$coulCC;
    } else {
        $coul=$coulFF;
    }
    echo "<tr style='background-color:".$coul."'><td>VE</td><td>".dateFR($date)."</td><td>".$m."-".$rq_recher->cod_nom."</td><td></td><td>".$rq_recher->cpt_nom." - F".$rq_recher->fac_id."</td><td class='droite'>".monetaireF($rq_recher->rst_total)."</td><td></td><td></td></tr>";
    $ttr += $rq_recher->rst_total;
    $m++;
}
//------------------------------total modes reglements----------------------
echo "<tr style='background-color:white'><td>VE</td><td>".dateFR($date)."</td><td>7001000</td><td>9999</td><td>Total des règlements</td><td></td><td id='ttr' class='droite'>".monetaireF($ttr)."</td><td></td></tr>";

//-------------------------Total les secteurs-----------------------------------------------
echo "<tr style='background-color:white;' id='navigateur'><td>VE</td><td>".dateFR($date)."</td><td>7001000</td><td>9999</td><td>Total Par secteurs</td><td class='droite' id='tts'></td><th>THT</th><th>TTC</th></tr>";
$req_secteurs = "SELECT COUNT(tic_id) as ct,
                        cod_id,
                        cod_nom,
                        sec_nom,
                        SUM(tic_tt) AS stts,
                        Vca_nom,
                        tva_nom,
                        ray_nom
                            FROM Tickets_$an
                            JOIN Resume_ticket_$an ON rst_id = tic_num
                            JOIN Articles ON art_id = tic_article
                            JOIN Rayons ON ray_id = art_rayon
                            JOIN Codes ON cod_id = ray_sort
                            JOIN Secteurs ON sec_id = ray_secteur
                            LEFT JOIN Vcana ON Vca_id = ray_ana
                            JOIN Tva ON tva_id = tic_ntva
                            WHERE DATE(rst_validation) LIKE '$date%'
                                    GROUP BY  Vca_nom,
                                        tic_ntva 
                                            ORDER BY tic_ntva DESC";
$r_secteurs=$idcom->query($req_secteurs);//$an-".date('m')."
$tts = 0.00;
while ($rq_secteurs = $r_secteurs->fetch_object()) {
    if ($m%2 == 0) {
        $coul=$coulCC;
    } else {
        $coul=$coulFF;
    }
    
    $compte = $rq_secteurs->cod_nom;
    if (($rq_secteurs->sec_nom == 'ABBAYE')||($rq_secteurs->sec_nom == 'ASA')) {
        $comp = "/<em>".$rq_secteurs->ray_nom."</em>";
    } else {
        $comp = '';
    }
    $val_HT = $rq_secteurs->stts / (1+($rq_secteurs->tva_nom/100));
    $tva_val=$rq_secteurs->stts - $val_HT;
    echo "<tr style='background-color:".$coul."'><td>VE</td><td>".dateFR($date)."</td><td>".$compte."</td><td>".str_pad($rq_secteurs->Vca_nom, 4, "0", STR_PAD_LEFT)."</td><td> ".$rq_secteurs->sec_nom.$comp."</td><td></td><td class='droite'> ".monetaireF($val_HT)."</td><td class='droite'>".$rq_secteurs->stts."</td></tr>";
    $tts += $val_HT;
    $m++;
}

$req_Tva = "SELECT COUNT(tic_id) as ct, SUM(tic_tt) AS TTC, SUM(tic_tt)-SUM(tic_tt)/(1+(tic_tva/100)) AS vl_tva, tic_tva, cod_nom, CONCAT('TVA à ',tva_nom) AS tva_nom FROM Tickets_$an JOIN Resume_ticket_$an ON rst_id = tic_num JOIN Tva ON tva_id = tic_ntva JOIN Codes ON cod_id = tva_code WHERE DATE(rst_validation) LIKE '$date%' GROUP BY tic_tva";
$r_Tva=$idcom->query($req_Tva);
$total_tva = 0.00;
while ($rq_Tva = $r_Tva->fetch_object()) {
    if ($m%2 == 0) {
        $coul=$coulCC;
    } else {
        $coul=$coulFF;
    }
    echo "<tr style='background-color:".$coul."'><td>VE</td><td>".dateFR($date)."</td><td>".$rq_Tva->cod_nom."</td><td></td><td> ".$rq_Tva->tva_nom."</td><td></td><td id='".$rq_Tva->cod_nom."' class='droite'>".monetaireF($rq_Tva->vl_tva)."</td><td></td></tr>";
    $total_tva += $rq_Tva->vl_tva;
    $m++;
}
echo "<tr style='background-color:white' id='libreoffice'><td>".str_replace("%", "", $date)."</td><td>7001000</td><td>9999</td><td>Total Par secteurs</td><td class='droite' >".monetaireF($tts+$total_tva)."</td><td></td></tr>";
// print_r($csv);

?>
</table>
<div id="export_ebp" style='margin:10px;background-color:white;width:900px;text-align:left'><button onclick="charge('export_ebp','<?php echo $date?>','export_ebp')">Export ebp</button></div>
<script>$('#tts').html('<?php echo monetaireF($tts+$total_tva)?>');
if ($('#tts').html() != $('#ttr').html()){
    $('#tts').css('backgroundColor','yellow');
    }
$('#libreoffice').css('display','none');
</script>
<?php
