<?php
session_start();
// print_r($_SESSION);
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$an= filter_input(INPUT_GET, "an", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$req= filter_input(INPUT_GET, "id", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();

$req_comptes="SELECT edi_nom, uti_nom, jrn_facture, LEFT(jrn_date,10) AS jrn_date,jrn_id,rsc_ttc
                        FROM Journal_factures_$an
                        JOIN Resume_commande_$an ON rsc_id = jrn_facture
                        JOIN Editeurs ON edi_id = rsc_serveur
                        JOIN Utilisateurs ON uti_id = edi_utilisateur
                          WHERE edi_id=$req ORDER BY jrn_id DESC";
$r_comptes=$idcom->query($req_comptes);
$nb=$r_comptes->num_rows;
$resu=$r_comptes->fetch_object();
?>
<script>
$('#liste_facture tr').click(function(){
  charge('detail_factureC',$(this).attr('id')+'&an=<?php echo $an?>','panneau_d');
    $('#liste_facture td').css('fontWeight','normal');
    $(this).parent().css('fontWeight','bold');
})

$(document).ready(function() {
  $('#recherche').focus();
  $('#recherche').simpleAutoComplete('recherche.php',{
    autoCompleteClassName: 'autocomplete',
    selectedClassName: 'sel',
    attrCallBack: 'rel',
    identifier: '<?php echo $an?>'
      },nomCallback);
});
function nomCallback( par ) {
  var options = {                   
  success: function(data){
    //on met à jour le div suggest avec les données reçus
        $('#panneau_g').empty();
        $('#panneau_g').append(data)},
        url:      'liste_facture.php?',
        type:     'GET',
        data:       {an:par[0],id:par[1]}
        };
  $.ajax(options);
  };
</script>
<input autocomplete="off" onclick="this.value=''" class="demi" id="recherche" type="text">
<?php
if ($nb > 1) {
    $titre= "Les ".$nb." factures de ".$resu->edi_nom.", année ".$an;
} else {
    $titre="La facture de ".$resu->edi_nom.", année ".$an;
}
?>
<h3><?php echo $titre?></h3>
<table width="98%" id='liste_facture'>
    <thead>
    <TR>
    <TH>N°&nbsp;J.</TH><TH>N°&nbsp;com.</TH><TH>TTC</TH><TH>Date</TH><TH>Serveur</TH><TH>Utilisateur</TH>
    </TR>
    </thead>
    <tbody>
    <?php
    $n = 0;
    $r_comptes->data_seek(0);
    while ($resu=$r_comptes->fetch_object()) {
        $coul=($n % 2 == 0)?$coulCC:$coulFF;
        echo "<tr style='background-color:".$coul." 'id='". $resu->jrn_facture."'><td>".$resu->jrn_id."</td><td>".$resu->jrn_facture."</td><td class='droite'>".$resu->rsc_ttc."</td><td class='centre'>".dateFR($resu->jrn_date)."</td><td>".$resu->edi_nom."</td><td>".$resu->uti_nom."</td></tr>\n";
        $n++;
    }
    ?>
    </tbody>
</table>
<script>
$("#panneau_g").css('max-height', $('#affichage').height());
</script>
