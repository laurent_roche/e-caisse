<?php
session_start();
$tab=explode('/', $_SERVER['REQUEST_URI']);
$dossier=$tab[1];
require "../php/config.php";
// print_r($_SESSION);
?>
<script type="text/javascript">
$(document).ready(function(){
    $('.param').click(function(){
        $('.sp-container').remove();
        $('.param').removeClass('ombre');
        $(this).addClass('ombre');
    });
});
$('.sp-container').remove();
</script>
<link href="/simpleAutoComplete.css" rel="stylesheet" type="text/css">
<h3>Controle des données</h3>
<center>
  <table style="width:90%">
    <TR>
    <?php
    if ($config['mode']==2) {
        ?>
        <TH class="param" onclick="voir('tva_secteurs','panneau_g')">Articles Tva/secteurs</TH>
        <?php
    }
    ?>
    <TH class="param" onclick="voire('tva_ventes','affichage')">Articles Tva/Ventes</TH>
    <TH class="param" onclick="voir('tickets_corriges','panneau_g')">Ventes valeurs modifiées</TH>
    <TH class="param" onclick="charge('recup_md5',<?php echo date('m')?>,'panneau_g')">Intégrité</TH>
    <TH class="param" onclick="charge('extracts','Aff','panneau_g')">Archivages/Extractions</TH>
    <?php
    if (isset($_SESSION['afficheur'])) {
        ?>
    <TH class="param" onclick="voir('afficheur','panneau_g')">Afficheur Externe</TH>
    <?php
    }
    ?>
    </TR>
  </table>
</center>
<script>var w=$(window).height();
var b=$('#bandeau').height();
$("#affichage").height(w-b-40);
</script>
