<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$an= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();

$req_recher="SELECT rsc_date,
              SUM(art_pht  * com_quantite)*(1+(tva_nom/100)) as vl,
              edi_nom,
              edi_id,
              uti_nom
                FROM Commandes_$an
                JOIN Resume_commande_$an ON rsc_id = com_numero
                LEFT JOIN Articles ON art_id =com_article
                JOIN Editeurs ON edi_id = rsc_serveur
                JOIN Utilisateurs ON uti_id = edi_utilisateur
                JOIN Tva ON tva_id = art_tva
                  WHERE rsc_etat = 2
                    GROUP By edi_id
                    ORDER BY edi_utilisateur";
$r_recher=$idcom->query($req_recher);
echo "<h3>Commandes en attente de réception</h3>";
if ($r_recher->num_rows == 0) { 
    echo "Il n'y a pas de commande en attente.";
    exit;
}
echo "<center><table width=500 id='resume'><tr><th>Date d'envoi</th><th>Éditeur</th><th>Valeur</th>";
$n =0;
$utilidateur = '';
$tt = 0.00;
while ($resu=$r_recher->fetch_object()) {
    $coul=($n%2 == 0)?$coulCC:$coulFF;
    if ($utilidateur != $resu->uti_nom) {
        if ($n > 0) {
            echo "<tr><tH colspan='2'>Sous-total</th><td style='border:solid 1px'>".monetaireF($stt)." €</td></tr>";
        }
        $stt="";
        echo "<tr><tH colspan='3'>".$resu->uti_nom."</th></tr>";
    }
    $utilidateur = $resu->uti_nom;
    $date=strtotime($resu->rsc_date);
    @$stt += $resu->vl;
    echo "<tr  style='background-color:".$coul."'><td>".date('d/m/Y', $date)."</td><td>".$resu->edi_nom."</td><td><strong>".monetaireF($resu->vl)." €</strong></td></tr>";
    $edi = $resu->edi_nom;
    $tt = $resu->vl + $tt;
    $n++;
}
echo "<tr><tH colspan='2'>Sous-total</th><td style='border:solid 1px'>".monetaireF($stt)." €</td></tr>";
?>
<tr><TH colspan="2">Total</TH><Td style="border:solid 1px"><strong><?php echo monetaireF($tt)?> €</strong></Td></tr></table></center>
