<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$date= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();

$tab_annee = explode('-', $date);
$an = $tab_annee[0];

$tab_tva =  array();
$tab_tvaCode =  array();
//recherche des tva utilisées et doublage des colonne pour les avoir 5.5,5_5...
$req_tva = "SELECT tic_tva,
            cod_nom
                FROM Tickets_$an
                JOIN Resume_ticket_$an ON rst_id = tic_num
                JOIN Tva ON tic_ntva = tva_id
                JOIN Codes ON tva_code = cod_id
                    WHERE MONTH(rst_validation) = '$tab_annee[1]'
                    AND tic_tva != '0.00'
                        GROUP BY tic_tva";
$r_tva = $idcom->query($req_tva);
while ($rq_tva =$r_tva->fetch_object()) {
    // echo $rq_tva->tic_tva."<br>";
    $tab_tva[] = $rq_tva->tic_tva;
    $tab_tvaCode[] = $rq_tva->cod_nom;
    $tab_tva[] = str_replace('.', '_', $rq_tva->tic_tva);
    $tab_tvaCode[] = $rq_tva->cod_nom;
}
// print_r($tab_tva);exit;


$tab_tva = array_fill_keys($tab_tva, '0.00');
// print_r($tab_tvaCode);
// echo '<br>';
// print_r($tab_tva);
$memoire ='';
// exit;
ob_start();

if (!$tab_annee[2]) {
    //export sur le mois sélectionné
    $req_mois = "SELECT rsj_date AS date FROM Resume_jour_".$tab_annee[0]." 
        WHERE MONTH(rsj_date) = ".($tab_annee[1]*1)." 
            GROUP BY rsj_date";
    $r_mois = $idcom->query($req_mois);
    while ($rq_mois =$r_mois->fetch_object()) {
        $date = $rq_mois->date;
        include "include_ebp.php";
    }
} else {
    //export du jour sélectionné
    include "include_ebp.php";
}
$memoire = ob_get_contents();
$id_file=fopen("export_ebp.csv", "w+b");
$memoire = iconv("UTF-8", "Windows-1252", $memoire);
fwrite($id_file, $memoire);
fclose($id_file);
ob_end_flush();
// exit;
?>
<script>
$('#export_ebp').empty();
location.href=('export_ebp.csv');</script>

