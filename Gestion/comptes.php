<?php
session_start();
// print_r($_SESSION);
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$nom= filter_input(INPUT_GET, "nom", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$sup= filter_input(INPUT_GET, "sup", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require_once $incpath."mysql/connect.php";
require_once $incpath."php/fonctions.php";
connexobjet();
if ($sup) {
    $idcom->query("DELETE FROM Comptes WHERE cpt_id=$sup");
    echo '<h1>Le compte à bien été supprimé</h1>'; ?>
    <script>
    $('#C_<?php echo $sup?>').empty();
    </script>
    <?php
    exit;
}
if ($req == "000") {//creation de compte
    $insert=" INSERT INTO Comptes (cpt_nom) VALUE ('$nom')";
    $r_comptes=$idcom->query($insert);
    $req=$idcom->insert_id;
}

$req_comptes="SELECT * FROM Comptes WHERE cpt_id=$req";
$r_comptes=$idcom->query($req_comptes);
$resu=$r_comptes->fetch_object();
?>
<h3><?php echo $resu->cpt_nom." N° ".$resu->cpt_id?></h3>
<?php
//--------le détail du compte est commun avec le module Vente
require_once $incpath."inc/detail_compte.inc.php";
exit;
//***********************************historique des factures****************************************

//on recherche toutes les factures de ce comptes
require $incpath."php/config.php";
$deb = $config['debut'];

$req_facture = '';
for ($i=$deb; $i <= ANNEE; $i++) {
    $req_facture.="SELECT fac_cp, rst_id, $i AS fac_annee, fac_id, fac_ticket, rst_total ,DATE(rst_validation) AS rst_jour, mdr_nom, uti_nom 
                                    FROM Factures_$i 
                                        JOIN Resume_ticket_$i ON rst_id=fac_ticket 
                                        JOIN Mode_reglement ON mdr_id = rst_etat 
                                        JOIN Utilisateurs ON uti_id=rst_utilisateur 
                                            WHERE fac_cp=$req";
    if ($i < ANNEE) {
        $req_facture.=" UNION ALL ";
    }
}
$req_facture.=" ORDER BY fac_annee DESC, rst_id DESC";
$r_facture=$idcom->query($req_facture);
if ($idcom->errno != 0) {
    echo $idcom->errno." ".$idcom->error."<br>";
}
$nb = $r_facture->num_rows;
//***********************************historique des devis****************************************
//$deb = 2017; //la fonction n'existait pas avant !
$req_devis = '';
for ($i=$deb; $i <= ANNEE; $i++) {
    $req_devis.="SELECT dev_compte, dev_id, $i AS dev_annee,dev_date 
                                    FROM Devis_$i 
                                        WHERE dev_compte=$req";
    if ($i < ANNEE) {
        $req_devis.=" UNION ALL ";
    }
}
$req_devis.=" ORDER BY dev_annee DESC";
// echo $req_devis ;
$r_devis = $idcom->query($req_devis);
$nb += $r_devis->num_rows;
if ($resu->cpt_id != 1) {
    if (($nb == 0) && ($resu->cpt_compte == 0)) {
        echo "Ce compte n'a pas de facture ou devis associée, il peut être supprimé"; ?><br>
    <button onclick="charge('comptes','&sup=<?php echo $resu->cpt_id?>','panneau_d')">Supprimer ce compte</button>
    <?php
    } else {
        echo "<h3>Il y a $nb facture".($nb > 1?"s":"")."/devis sur ce compte</h3>"; ?>
    <style type="text/css">
    #factures td {
    text-align:right
    }
     </style>
<script>
$('#factures td').click(function() {
    // alert($(this).parent().attr('cpt'));
    if (($(this).html().slice(0,8)!='<a href=')&&($(this).parent().attr('cpt')!='undefined')) {
        charge('detail_facture',$(this).parent().attr('cpt'),'detail_facture');
    }
    $('#factures tr').css('fontWeight','normal');
    $(this).parent().css('fontWeight','bold');
});

</script> 
<center>
<table class='generique'>
<thead>
<TR><TH>N° ticket</TH><TH>N° Facture</TH><TH>Date</TH><TH>px TTC</TH><TH>Règlement</TH><th>Vendeur</th></TR>
</thead>
<tbody id="factures" >
    <?php
    $n=0;
        $fac = '';
        while ($rq_facture=$r_facture->fetch_object()) {
            if (file_exists($incpath."pdf/factures/facture_".(($rq_facture->fac_annee*1000)+$rq_facture->fac_id).".pdf")) {
                $fac= '<a href="'.$incpath."pdf/factures/facture_".(($rq_facture->fac_annee*1000)+$rq_facture->fac_id).'.pdf"><img src="../images/pdf.gif" style="float:left"></a> ';
            }
            $coul=  ($n%2 == 0)?$coulCC:$coulFF;
            echo "<tr cpt='".$rq_facture->fac_id."&an=".$rq_facture->fac_annee."' style='background-color:".$coul.";'><td>".$fac.$rq_facture->fac_id."/".$rq_facture->rst_id."</td><td>Lib".(($rq_facture->fac_annee*1000)+$rq_facture->fac_id)."</td><td>".dateFR($rq_facture->rst_jour)."</td><td>".$rq_facture->rst_total." €</td><td>".$rq_facture->mdr_nom."</td><td>".$rq_facture->uti_nom."</td></tr>";
            $n++;
        }
    }
}
?>
</tbody>
<tbody>
<thead>
<?php
if ($r_devis->num_rows != 0) {
    ?>
    <tr><th colspan=6>Devis</th>
    <tr><TH></TH><TH>N° devis</TH><TH>Date</TH><TH colspan=3></th>
    <?php
    while ($rq_devis=$r_devis->fetch_object()) {
        if (file_exists($incpath."pdf/devis/devis_".(($rq_devis->dev_annee*1000)+$rq_devis->dev_id).".pdf")) {
            $dev= '<a href="'.$incpath."pdf/devis/devis_".(($rq_devis->dev_annee*1000)+$rq_devis->dev_id).'.pdf"><img src="../images/pdf.gif" style="float:left"></a> ';
        }
        $coul=  ($n%2 == 0)?$coulCC:$coulFF;
        echo "<tr style='background-color:".$coul.";'><td>".$dev."</td><td>".(($rq_devis->dev_annee*1000)+$rq_devis->dev_id)."</td><td>".dateFR($rq_devis->dev_date)."</td><td colspan=3></td></tr>";
        $n++;
    }
}
?>
</thead>
</tbody></table></center>
<div id='detail_facture'></div>
<script>
// var b=$('#panneau_g').height();
$("#panneau_d").css('max-height', $('#affichage').height());
</script>
