<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
$jour = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath . "mysql/connect.php";
require $incpath . "php/fonctions.php";
// $_SESSION['aide_'.$_SESSION[$dossier]] = 'G1';
connexobjet();
?>
<table class="generique">
    <thead>
        <tr>
            <TH colspan="5">Article(s) à corriger</TH>
        </tr>
        <TR>
            <TH>Ligne / N° ticket</TH>
            <TH>Article</TH>
            <TH>Valeur</TH>
            <TH>Vendeur</TH>
            <TH>Heure</TH>
        </TR>
    </thead>
    <tbody>
        <?php
        $n = 0;
        $req_pseudo = "SELECT tic_num,
                      Vt1_nom,
                      tic_prix,
                      uti_nom,
                      tic_id,
                      tic_ntva,
                      TIME(rst_validation) AS heure
                        FROM Tickets_" . ANNEE . "
                        JOIN Resume_ticket_" . ANNEE . " ON rst_id = tic_num
                        JOIN Articles ON art_id = tic_article
                        JOIN Vtit1 ON Vt1_article = tic_article
                        JOIN Utilisateurs ON uti_id = rst_utilisateur
                            WHERE DATE(rst_validation) LIKE '" . $jour . "'
                            AND art_pseudo = '4'";
        $r_pseudo = $idcom->query($req_pseudo);
        if ($r_pseudo->num_rows == 0) {
            //corrections terminées, on recharge fin_jour
            ?>
            <script>charge("fin_jour",'',"panneau_g")</script>
            <?php
        }
        while ($rq_pseudo = $r_pseudo->fetch_object()) {
            $coul = ($n % 2 == 0) ? $coulCC : $coulFF;
            echo "<tr style='background-color:" . $coul . "' onclick=\"charge('cor_article','" . $rq_pseudo->tic_id . "&tva=" . $rq_pseudo->tic_ntva . "','correction')\"><td>" . $rq_pseudo->tic_id . " / " . $rq_pseudo->tic_num . '</td><td>' . $rq_pseudo->Vt1_nom . '</td><td>' . $rq_pseudo->tic_prix . '</td><td>' . $rq_pseudo->uti_nom . '</td><td>' . $rq_pseudo->heure . '</td></tr>';
            $n++;
        }
        ?>
    </tbody>
</table>
<span id='ce_jour' style='display:none'><?php echo $jour ?></span>
<div id='correction'></div>