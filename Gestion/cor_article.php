<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
// echo $incpath;
$id_ticket = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$tva = filter_input(INPUT_GET, "tva", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath . "mysql/connect.php";
require $incpath . "php/fonctions.php";
// $_SESSION['aide_'.$_SESSION[$dossier]] = 'G1';
connexobjet();
$req_article = 'SELECT tic_id, tic_tva, tic_ntva, tic_prix FROM Tickets_' . ANNEE . ' WHERE tic_id=' . $id_ticket;
$r_article = $idcom->query($req_article);
$rq_article = $r_article->fetch_object();
// print_r($rq_article);
?>
<link rel="stylesheet" type="text/css" href="/simpleAutoComplete.css">
<script type='text/javascript' src="/js/simpleAutoComplete.js"></script>
<script>
    $(document).ready(function() {
        //---------------------------recherche textetuelle-----------
        $('#recherche').focus();
        $('#recherche').simpleAutoComplete('recherche_pseudo.php', {
            autoCompleteClassName: 'autocomplete',
            selectedClassName: 'sel',
            attrCallBack: 'rel',
            identifier: '<?php echo $tva."_".$id_ticket ?>'
        }, nomCallback);
    });

    function nomCallback(par) {
        var options = {
            success: function(data) {
                //on met à jour le div suggest avec les données reçus
                $('#cor').empty();
                $('#cor').append(data)
            },
            url: 'correction_pseudo.php?',
            type: 'GET',
            data: {
                req: par[0],
                ligne: par[1]
            }
        };
        $.ajax(options);
    };
    //---------------------------fin recherche textetuelle-------------
</script>
<h3>Correction de la ligne <?php echo $id_ticket ?></h3>
L'article à une tva de <strong><?php echo $rq_article->tic_tva ?> %</strong> et un prix de <strong><?php echo $rq_article->tic_prix ?> €.</strong><br>
Si ce n'est pas la bonne tva, on ne peut corriger l'article.<br>
<br>
<input autocomplete="off" placeholder="Recherche sur le nom de l'article" onclick="this.value=''" id="recherche" type="text">
<div id='cor'></div>
<script>
    $("#panneau_d").height($("#affichage").height() - 10);
</script>