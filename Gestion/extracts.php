<?php
session_start();
/**
 * Contrôler les archivages déjà effectuées (et en créer un ponctuellement)
 * #e-Caisse_CTRL#
 */ 
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$psAct=filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$psYear= filter_input(INPUT_GET, "year", FILTER_SANITIZE_FULL_SPECIAL_CHARS); 
if (is_null($psYear)) {
    $psYear=date('Y');
}

/**
 * Principe
*/
require_once $incpath."mysql/connect.php";
require_once $incpath.'php/fonctions.php';
//on verifie si le serveur est sous contrôle
connexobjet();
$req_token = 'SELECT LENGTH(con_token) AS ln FROM Config;';
$r_token = $idcom->query($req_token);
$rq_token =$r_token->fetch_object();
if ($rq_token->ln < 30 ) {
    echo "<h3>Ce seveur n'est pas sous contrôle</h3>";
    exit;
}

require_once $incpath.'inc/hash_inc.php';

?>
<script>
$(document).ready(function(){
    $('.generique td').css('cursor','pointer');
    $('.generique tr').click(function(){
    $('.generique tr').css('font-weight','normal');
    $(this).css('font-weight','bold');
    })
})
</script>
<style>
button {
    width: inherit;
}
input[type=text] {
    width: inherit;
}
</style>
<h3>Pour les archivages/extractions de l'année <input type="text" size="10" name="DtYear" value="<?php echo $psYear?>"/> 
<br>
<button type="button" onclick="charge('extracts', 'Aff&year='+$('input[name=DtYear]').val() ,'panneau_g');">Afficher</button>
<button type="button" onclick="charge('extract_nouv', 'Nouv&year='+$('input[name=DtYear]').val(),'panneau_d');">Nouveau</button></h3>

<table class="generique"><thead>
<tr><th id="analyse" colspan="5"> archivages déjà effectués</th><th id="resultat"></th></tr>
<tr><th>Début</th><th>Fin</th><th>Nom donné</th><th>Enregistré à</th><th>Commentaires</th><th>Empreinte</th></tr>
</thead>
<tbody>
<?php

$n = 0;
switch ($psAct) {
case 'Aff':
    $aPost=['j'=>$gsJeton, 'bgn'=>$psYear .'-01-01', 'end'=>$psYear .'-12-31'];
    $sRes = mc_callUrlPost(CTRL_URL . '/getXtr', $aPost);
    $aoRes = json_decode($sRes);

    // Fix pour CoulFF/CoulCC
    $coulCC='silver';
    if (! is_null($aoRes)) {
        foreach ($aoRes as $o1Extract) {
            $coul=($n%2 == 0)?$coulCC:$coulFF;
            echo "<tr style='background-color:{$coul}' onclick=\"charge('extract_ctrl','Ctrl&h={$o1Extract->xtr_hash}','panneau_d');\"><td>" . date_format(new DateTime($o1Extract->xtr_dtm_bgn), 'd/m/Y') . "</td><td>" . date_format(new DateTime($o1Extract->xtr_dtm_end), 'd/m/Y') . "</td>"
                ."<td>{$o1Extract->xtr_nm}</td><td>{$o1Extract->xtr_dtm_log}</td><td>{$o1Extract->xtr_cmt}</td><td>{$o1Extract->xtr_hash}</td></tr>";
            $n++;
        }
    }
    break;
case 'Nouv':
    break;
}
?>
</tbody></table>
<script>
$('#analyse').prepend('(<?php echo $n ?>)');
<?php
if ($psAct=='Ctrl') {
    echo "$('#resultat').prepend('{$nbCtrlOK} OK');";
} 

if ($psAct=='Mdf') {
    echo "$('#analyse').hide(); $('#resultat').prepend('{$n} modifiés lors de cloture de journée');"; 
}
?>
$('#panneau_d').empty();
</script>
