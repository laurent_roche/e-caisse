<?php
session_start();
/**
 * Différents contrôle possible à l'écran
 * #e-Caisse_CTRL#
 */ 
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$psAct=filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$pBgn= DateTime::createFromFormat('d/m/Y', filter_input(INPUT_GET, "bgn", FILTER_SANITIZE_FULL_SPECIAL_CHARS)); 
$pEnd= DateTime::createFromFormat('d/m/Y', filter_input(INPUT_GET, "end", FILTER_SANITIZE_FULL_SPECIAL_CHARS));
$sMsg = '';
if (gettype($pEnd)=="object") {
    if ($pBgn->format('Y')==$pEnd->format('Y')) {
        $pEndNextDay = clone($pEnd);
        $pEndNextDay->modify('+1 day');
        $pAnnee = $pBgn->format('Y');
    } else {
        $sMsg = ' &nbsp; Les années de la date de début et de fin doivent être identiques !';
        $psAct = 'KO';
    }
} else {
    $pBgn = $pEnd = $pEndNextDay = null;
}
//if (gettype($pEnd)=="object") echo "DBG begin =" .$pBgn->format('Y-m-d') ."  end =" .$pEnd->format('Y-m-d') ." <br />";

/**
 * Principe
 * On récupère les données du mois sur le site distant et on compare avec le hash des données locales
*/

require_once $incpath.'mysql/connect.php';
require_once $incpath.'php/fonctions.php';
connexobjet();
//on verifie si le serveur est sous contrôle
$req_ctrl = 'SELECT con_token FROM Config';
$r_ctrl = $idcom->query($req_ctrl);
$rq_ctrl =$r_ctrl->fetch_object();
if (strlen($rq_ctrl->con_token) != 30) {
    echo "<h3>Ce seveur n'est pas sous contrôle</h3>";
    exit;
}
require_once $incpath.'inc/hash_inc.php';

function dtDeft($pDate, $pDeft)
{
    return is_null($pDate)?$pDeft:$pDate->format('d/m/Y');
}

// charge les différents mode de paiements dans un tableau indexés
$oQuery = $idcom->query('SELECT mdr_nom, mdr_id FROM Mode_reglement ORDER BY mdr_id');
for ($aTabMode = array(''); $oTmp = $oQuery->fetch_object();) $aTabMode[] = $oTmp->mdr_nom;

switch ($psAct) {
case 'Aff':
    $sSQL = 'SELECT rst_id, rst_validation, CONCAT(rst_id,"/",rst_num) AS rst_num, rst_dtm_log FROM Resume_ticket_'.$pAnnee.'
            WHERE rst_validation BETWEEN "'.$pBgn->format('Y-m-d').'" AND "'.$pEndNextDay->format('Y-m-d').'" ORDER BY rst_id';
    $oQuery = $idcom->query($sSQL);
    break;
case 'Mdf':
    $sSQL = 'SELECT rst_id, rst_validation, rst_etatS, rst_etat, rst_total, rst_totalS, rst_num, rst_dtm_log FROM Resume_ticket_'.$pAnnee.'
            WHERE (rst_etat <> rst_etatS OR rst_total <> rst_totalS) AND rst_validation BETWEEN "'.$pBgn->format('Y-m-d').'" AND "'.$pEndNextDay->format('Y-m-d').'" ORDER BY rst_id';
    $oQuery = $idcom->query($sSQL);
    break;

case 'Ctrl':
    $sSQL = 'SELECT rst_id, rst_num, rst_total, rst_etatS, rst_validation, rst_dtm_log FROM Resume_ticket_'.$pAnnee.'
            WHERE rst_validation BETWEEN "'.$pBgn->format('Y-m-d').'" AND "'.$pEndNextDay->format('Y-m-d').'" ORDER BY rst_id';
    $oQuery = $idcom->query($sSQL);
    $aPost=['j'=>$gsJeton, 'bgn'=>$pBgn->format('Y-m-d'), 'end'=>$pEndNextDay->format('Y-m-d')];
    $sRes = mc_callUrlPost(CTRL_URL, $aPost);
    $aoRes = json_decode($sRes);
    //var_dump($aoRes);
    break;
}
// echo "SQL = '{$sSQL}'";

?>
<script>
$(document).ready(function(){
    $('.generique td').css('cursor','pointer');
    $('.generique tr').click(function(){
    $('.generique tr').css('font-weight','normal');
    $(this).css('font-weight','bold');
    })
})
</script>
<style>
button {
    width: inherit;
}
input[type=text] {
    width: inherit;
}
</style>
<h3>Pour les tickets entre le <input type="text" size="10" name="DtBgn" value="<?php echo dtDeft($pBgn, '1/1/'.date('Y')) ?>"/> et le <input type="text" value ="<?php echo dtDeft($pEnd, date('d/m/Y'));?>" size="10" name="DtEnd">
<br>
<button type="button" onclick="charge('recup_md5', 'Aff&bgn='+$('input[name=DtBgn]').val() +'&end='+$('input[name=DtEnd]').val(),'panneau_g');">Afficher</button>
<?php
if (! empty($gsJeton) ) { ?>
    <button type="button" onclick="charge('recup_md5', 'Ctrl&bgn='+$('input[name=DtBgn]').val() +'&end='+$('input[name=DtEnd]').val(),'panneau_g');">Contrôler</button> 
<?php 
}
?>
<button type="button" onclick="charge('recup_md5', 'Mdf&bgn='+$('input[name=DtBgn]').val() +'&end='+$('input[name=DtEnd]').val(),'panneau_g');">Modifiés ?</button></h3>

<p style="color:red"><strong><?php echo $sMsg ?></strong></p>
<table class="generique"><thead>
<tr><th id="analyse" colspan="2"> tickets analysés</th><th id="resultat"></th></tr>
<tr><th>Date & Heure </th><th>N° ticket</th><th><?php echo ($psAct=='Aff')?'Date & Heure de Contrôle':'Analyse'?></th></tr>
</thead>
<tbody>
<?php

$n = 0;
switch ($psAct) {
case 'Aff':
    while ($oTkt = $oQuery->fetch_object()) {
        $coul=($n%2 == 0)?$coulCC:$coulFF;
        echo "<tr style='background-color:{$coul}' onclick=\"charge('detail_ticket','{$pAnnee}&tic={$oTkt->rst_id}','panneau_d');\"><td>{$oTkt->rst_validation}</td><td>{$oTkt->rst_id}</td><td>{$oTkt->rst_dtm_log}</td></tr>";
        $n++;
    }
    break;
case 'Mdf':
    while ($oTkt = $oQuery->fetch_object()) {
        $coul=($n%2 == 0)?$coulCC:$coulFF;
        if ($oTkt->rst_etatS <> $oTkt->rst_etat) {
            echo "<tr style='background-color:{$coul}' onclick=\"charge('detail_ticket','{$pAnnee}&tic={$oTkt->rst_id}','panneau_d');\"><td>{$oTkt->rst_validation}</td><td>{$oTkt->rst_id}</td><td>{$oTkt->rst_dtm_log}"
            ."</td><td>{$aTabMode[$oTkt->rst_etatS]} ==> {$aTabMode[$oTkt->rst_etat]}</td></tr>";
        } else {
            echo "<tr style='background-color:{$coul}' onclick=\"charge('detail_ticket','{$pAnnee}&tic={$oTkt->rst_id}','panneau_d');\"><td>{$oTkt->rst_validation}</td><td>{$oTkt->rst_id}</td><td>{$oTkt->rst_dtm_log}"
            ."</td><td>Changement du montant{$oTkt->rst_totalS} ==> {$oTkt->rst_total}</td></tr>";
        }
        $n++;
    }
    break;
case 'Ctrl':
    $nbCtrlOK = 0;
    $nbTktAbsCtrl = 0;
    $iCntAoRes = count($aoRes);
    while (($oTkt = $oQuery->fetch_object()) AND ($n < $iCntAoRes) ) {
        debut_boucle:
        $coul=(($n-$nbCtrlOK)%2 == 0)?$coulCC:$coulFF;
        if ($oTkt->rst_id == $aoRes[$n]->tkt_ec_id AND mc_geneCtrl($oTkt) == $aoRes[$n]->tkt_hash ) {
            //echo "<tr style='background-color:{$coul}'><td>".$oTkt->rst_validation.'</td><td>'.$oTkt->rst_id.'</td><td>OK</td></tr>';
            $nbCtrlOK++;
            $n++; 
        } else {
            if ($oTkt->rst_id == $aoRes[$n]->tkt_ec_id) {
                // hash différent
                echo "<tr style='background-color:{$coul}' onclick=\"charge('detail_ticket','{$pAnnee}&tic={$oTkt->rst_id}','panneau_d');\"><td>".$oTkt->rst_validation.'</td><td>'.$oTkt->rst_num.'</td><td>Ticket modifié dans e-Caisse !</td></tr>';
                $n++; 
            } else {
                // Décalage de numéro
                if ($oTkt->rst_id < $aoRes[$n]->tkt_ec_id) {
                    // Ticket manquant sur Serveur de Contrôle
                    echo "<tr style='background-color:{$coul}' onclick=\"charge('detail_ticket','{$pAnnee}&tic={$oTkt->rst_id}','panneau_d');\"><td>".$oTkt->rst_validation.'</td><td>'.$oTkt->rst_num."</td><td>Ticket manquant sur le serveur ! N° {$oTkt->rst_id} <> {$aoRes[$n]->tkt_ec_id}</td></tr>";
                    $nbTktAbsCtrl++;
                } else {
                    // Ticket manquant dans e-Caisse
                    echo "<tr style='background-color:{$coul}' onclick=\"alert('Impossible d\'afficher le ticket : il est absent !');\"><td>".$aoRes[$n]->tkt_dtm_gen.'</td><td>'.$aoRes[$n]->tkt_ec_id."</td><td>Ticket manquant dans e-Caisse ! N° {$oTkt->rst_id} <> {$aoRes[$n]->tkt_ec_id}</td></tr>";
                    $n++;
                    if ($n < $iCntAoRes) {
                        goto debut_boucle;
                    }
                }
            }
        }
    }
    $n += $nbTktAbsCtrl;
    break;
}
?>
</tbody></table>
<script>
$('#analyse').prepend('(<?php echo $n ?>)');
<?php if ($psAct=='Ctrl') {
    echo "$('#resultat').prepend('{$nbCtrlOK} OK');";
} 
?>
<?php if ($psAct=='Mdf') {
    echo "$('#analyse').hide(); $('#resultat').prepend('{$n} modifiés lors de cloture de journée');"; 
}
?>
$('#panneau_g').height($("#affichage").height()-10);
$('#panneau_d').empty();
</script>
