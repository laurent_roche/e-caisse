<?php
session_start();
// print_r($_SESSION);
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$req = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
require $incpath."php/config.php";
connexobjet();

$req_factures="SELECT edi_nom,
                        uti_nom,
                        jrn_facture,
                        jrn_id,
                        jrn_date,
                        rsc_ttc
                            FROM Journal_factures_$req
                            JOIN Resume_commande_$req ON rsc_id = jrn_facture
                            JOIN Editeurs ON edi_id = rsc_serveur
                            JOIN Utilisateurs ON uti_id = edi_utilisateur
                                ORDER BY jrn_id DESC LIMIT 0,30";
$r_factures=$idcom->query($req_factures);
?>
<script type="text/javascript" src="/js/simpleAutoComplete.js"></script>

<script>
$('#liste_facture tr').click(function() {
    charge('detail_factureC',$(this).attr('id')+'&an=<?php echo $req?>','panneau_d');
    $('#liste_facture tr').css('fontWeight','normal')
    $(this).css('fontWeight','bold');
});

$(document).ready(function() {
    $('#recherche').focus();
    $('#recherche').simpleAutoComplete('recherche.php',{
        autoCompleteClassName: 'autocomplete',
        selectedClassName: 'sel',
        attrCallBack: 'rel',
        identifier: '<?php echo $req?>'
        },nomCallback);
});
function nomCallback( par ) {
    var options = {                   
    success: function(data){
//         alert(par);
                //on met à jour le div suggest avec les données reçus
                $('#panneau_g').empty();
                $('#panneau_g').append(data)},
                url:      'liste_facture.php?',
                type:     'GET',
                data:       {an:par[0],id:par[1]}
    };
    $.ajax(options);
    };
</script>

<?php
if ($req > $config['debut']) {
    $bt1="<button class='pt_bt' style='float:left;' onclick=\"charge('journal_factures',".($req-1).",'panneau_g')\"><&nbsp;".($req-1)."</button>";
} else {
    $bt1="<button class='pt_bt' style='float:left'></button>";
}
if ($req < date("Y")) {
    $bt2="<button class='pt_bt' style='float:right;' onclick=\"charge('journal_factures',".($req+1).",'panneau_g')\">&nbsp;".($req+1)."></button>";
} else {
    $bt2="<button class='pt_bt' style='float:right'></button>";
}

if ($req == date("Y")) {//uniquement pour l'année en cours
//valeur de commande en attente de réception
    /*echo*/ $req_attente="SELECT rsc_date,
                            SUM(art_pht  * com_quantite)*(1+(tva_nom/100)) as vl
                                FROM Commandes_$req
                                JOIN Resume_commande_$req ON rsc_id = com_numero
                                JOIN Articles ON art_id =com_article
                                JOIN Tva ON tva_id = art_tva
                                    WHERE rsc_etat = 2;";
    $r_attente=$idcom->query($req_attente);
    $rq_attente=$r_attente->fetch_object();
}
?>
<div style="margin-top:10px">
<input autocomplete="off" placeholder="Éditeur"  onclick="this.value=''" id="recherche" class="demi" type="text">
<?php

if ($req == date('Y')) {
    ?>
    <span style="float:right">
    Valeur estimée des commandes en attente : <strong><?php echo sprintf("%01.2f", $rq_attente->vl)?> € TTC</strong><img src="/images/edit.png" style="height:30px;float:right" onclick="charge('com_attente','<?php echo $req?> ','panneau_d')"><br> Les nouveautés ne sont pas prises en compte.</span>
    <?php
}
?>
 <h3><?php echo $bt1?>Journal de facture de l'année <?php echo $req?><?php echo $bt2?></h3> 
</div>
<table width="98%" id='liste_facture'>
    <thead>
    <tr><TH colspan="5">30 Dernières factures</TH></tr>
    <TR>
    <TH>N°&nbsp;J.</TH><TH>N°&nbsp;com.</TH><TH>TTC</TH><TH>Date</TH><TH>Serveur</TH><TH>Utilisateur</TH>
    </TR>
    </thead>
    <tbody>
    <?php
    $n = 0;
    while ($resu=$r_factures->fetch_object()) {
        if ($n%2 == 0) {
            $coul=$coulCC;
        } else {
            $coul=$coulFF;
        }
        echo "<tr style='background-color:".$coul."' id= '". $resu->jrn_facture."'>
        <td>L".str_pad($resu->jrn_id, 3, "0", STR_PAD_LEFT)."</td>
        <td>".$resu->jrn_facture."</td><td class='droite' style=padding-right:20px>".monetaireF($resu->rsc_ttc)."</td>
        <td>".dateFR($resu->jrn_date)."</td>
        <td>".$resu->edi_nom."</td><td>".$resu->uti_nom."</td>
        </tr>\n";
        $n++;
    }
    ?>
    </tbody>
</table>
<script>
$("#panneau_g").height($('#affichage').height());
</script>

