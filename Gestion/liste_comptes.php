<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
$cpt = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$req_comptes="SELECT cpt_id, LEFT(cpt_nom,1) AS cpt_abr, cpt_nom 
                    FROM Comptes GROUP BY LEFT(cpt_nom,1) 
                        ORDER BY cpt_nom";
$r_comptes=$idcom->query($req_comptes);
?>
<script>
$(document).ready(function() {
    $('.comptes').click(function(){
        $('.comptes').removeClass('ombre');
        $(this).addClass('ombre');
        charge('filtre_comptes',$(this).html(),'liste_comptes');
    });
    //---------------------------recherche textetuelle-----------
    $('#recherche').focus();
    $('#recherche').simpleAutoComplete('recherche_compte.php',{
        autoCompleteClassName: 'autocomplete',
        selectedClassName: 'sel',
        attrCallBack: 'rel',
        identifier: 'cp'
        },nomCallback);
});
function nomCallback( par ) {
    var options = {                   
    success: function(data){
        //on met à jour le div suggest avec les données reçus
        $('#panneau_d').empty();
        $('#panneau_d').append(data)},
        url:      'comptes.php?',
        type:     'GET',
        data:       {req:par[0],nom:$('#recherche').val()}
    };
    $.ajax(options);
};
//---------------------------fin recherche textetuelle-------------


</script>
<script type="text/javascript" src="/js/simpleAutoComplete.js"></script>
<h3>Les comptes</h3>
<?php
$bout='';
while ($resu=$r_comptes->fetch_object()) {
    $bout .="<button class='comptes'>".$resu->cpt_abr."</button>";
}
// $bout .="<br><button class='comptes' id='NV'>Nouveau compte</button>";
$bout .=" <br><input autocomplete=\"off\" placeholder=\"Recherche compte\"  onclick=\"this.value=''\" id=\"recherche\" type=\"text\">";
echo $bout;

?>
<div id='liste_comptes'>
</div>
<script>
$(".comptes").width(30);
$('#NV').width(150);
$("#panneau_g").css('height', $('#affichage').height());

</script>
