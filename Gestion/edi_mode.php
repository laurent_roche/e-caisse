<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$mode= filter_input(INPUT_GET, "mod", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$mode= substr($mode, 1);
$tab_jour=explode('-', $req);
$date=mktime(0, 0, 0, $tab_jour[1], $tab_jour[2], $tab_jour[0]);
// echo  date("j", $date)."<br>";
if (date("j", $date) < 7)$dif= -date("j", $date)+1;//debut du mois
elseif (date("w", $date) == 0)$dif= -6 + date("w", $date);
else  $dif= -date("w", $date) +1;
// if ($dif < 1 )$dif = 1;
$jour=$tab_jour[2];
// exit;
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
$an=$tab_jour[0];
connexobjet();
/*
tva tva_id + 100
secteur sec_id + 200
reglement mdr_id + 300
*/
if ($mode > 300) {//mode de reglement
    $req_mdr="SELECT mdr_id, mdr_nom FROM Mode_reglement WHERE mdr_id = $mode - 300";
    $r_mdr=$idcom->query($req_mdr);
    $rq_mdr=$r_mdr->fetch_object();
    $titre=$rq_mdr->mdr_nom;

    $req="SELECT rst_validation, rst_total FROM Resume_ticket_$an WHERE rst_etat ='".$rq_mdr->mdr_id."' AND DATE(rst_validation) >= ADDDATE('$req', $dif) AND DATE(rst_validation) <= '$req'";
    $r=$idcom->query($req);
    echo "<h3>Ventes par ".$titre." du ".str_pad(($jour+$dif), 2, "0", STR_PAD_LEFT)." au ".$jour."</h3>
    <center><table><tr><td>Date</td><td>Valeur</td>";
    $n = 0;
    $tt = 0.00;
    while ($rq=$r->fetch_object()) {
       $coul=($n%2 == 0)?$coulCC:$coulFF;
        echo "<tr  style='background-color:".$coul."'><td>".$rq->rst_validation."</td><td class='droite'><strong>".$rq->rst_total." €</strong></td></tr>";
        $tt = $rq->rst_total + $tt;
        $n++;
    }
    ?>
    <tr><TH>Total</TH><TH class="droite"><?php echo monetaireF($tt)?> €</TH></tr></table></center>
    <?php
    } elseif ($mode > 200) {//secteur
        $req="SELECT Vt1_nom, tic_quantite, tic_prix, tic_num, tic_tt, sec_nom FROM Tickets_$an JOIN Resume_ticket_$an ON rst_id=tic_num LEFT JOIN Vtit1 ON Vt1_article=tic_article JOIN Varticles_secteurs ON Vas_article = tic_article JOIN Secteurs ON sec_id = Vas_secteur WHERE Vas_secteur= '".($mode - 200)."' AND DATE(rst_validation) >= ADDDATE('$req', $dif) AND DATE(rst_validation) <= '$req'";
        $r=$idcom->query($req);
        $rq=$r->fetch_object();
        echo "<h3>Ventes du ".str_pad(($jour+$dif), 2, "0", STR_PAD_LEFT)." au ".$jour." pour le secteur ".$rq->sec_nom."</h3>
        <center><table><tr><th>N° Ticket</th><th>Titre</th><th>Prix</th><th>Quantité</th></tr>";
        $r->data_seek(0);
        $n = 0;
        $tt = 0.00;
        while ($rq=$r->fetch_object()) {
            $coul=($n%2 == 0)?$coulCC:$coulFF;
            echo "<tr  style='background-color:".$coul."'><td>".$rq->tic_num."</td><td>".$rq->Vt1_nom."</td><td class='droite'>".$rq->tic_prix."&nbsp;€</td><td class='droite'>".$rq->tic_quantite."</td></tr>";
            $tt += $rq->tic_tt;
            $n++;
        }
        ?>
        <tr><TH>Total</TH><TH class="droite" colspan="2"><?php echo monetaireF($tt)?>&nbsp;€</TH></tr></table></center>
        <?php
    } else {//tva
        $tt = 0.00;
        $req="SELECT Vt1_nom, tic_quantite, tic_prix, tic_num, tic_tt FROM Tickets_$an JOIN Resume_ticket_$an ON rst_id=tic_num LEFT JOIN Vtit1 ON Vt1_article=tic_article WHERE tic_ntva= '".($mode - 100)."' AND DATE(rst_validation) >= ADDDATE('$req', $dif) AND DATE(rst_validation) <= '$req'";
        $r=$idcom->query($req);
        echo "<h3>Ventes par taux de tva du ".str_pad(($jour+$dif), 2, "0", STR_PAD_LEFT)." au ".$jour."</h3>
        <center><table><tr><th>N° Ticket</th><th>Titre</th><th>Prix</th><th>Quantité</th></tr>";
        $n = 0;
        while ($rq=$r->fetch_object()) {
            $coul=($n%2 == 0)?$coulCC:$coulFF;
            echo "<tr  style='background-color:".$coul."'><td>".$rq->tic_num."</td><td>".$rq->Vt1_nom."</td><td class='droite'>".$rq->tic_prix."&nbsp;€</td><td class='droite'>".$rq->tic_quantite."</td></tr>";
            $tt += $rq->tic_tt;
            $n++;
        }
        ?>
        <tr><TH>Total</TH><TH class="droite" colspan="2"><?php echo monetaireF($tt)?>&nbsp;€</TH></tr></table></center>
        <?php
    }
?>