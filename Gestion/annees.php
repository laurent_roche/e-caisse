<?php
session_start();
?>
<script type="text/javascript">
    $(document).ready(function() {
        $('.annees').click(function(e) {
            id = $(this).attr('id');
            if ($("#svg_" + id).length) {
                x = $("#svg_" + id).position();
                curs = Math.round(e.pageX - x.left);
                if ((curs > 69) && (curs < 898)) charge("detail_mois", curs + "&an=" + id, id);
            }
        });
    });
</script>
<?php

// print_r($_SESSION);
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
$filtre = filter_input(INPUT_GET, "f", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$req = filter_input(INPUT_GET, "id", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
if ($req == "") {
    $req = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
}
require $incpath . "mysql/connect.php";
require $incpath."php/config.php";
connexobjet();

    $debut = $config['debut'];

$annee = array();
$valeurV = array();
$paq = array();
for ($i = date('Y'); $i >= $config['debut']; $i--) {
    if (file_exists("svg/annee_" . $i . ".svg")) {
        include "svg/annee_" . $i . ".svg";
    } else { //on affiche les années passée et on calcule l'année en cours
        ob_start();
        //****************************************ventes**********************************************
        // $cem = $i . "-" . date('m') . "-" . date('j') . " " . date('H') . ":" . date('i');
        $req_ventes = "SELECT SUM(tic_tt) as CT FROM Tickets_$i JOIN Resume_ticket_$i ON rst_id = tic_num  GROUP BY MONTH(rst_validation)";
        $r_ventes = $idcom->query($req_ventes);
        if ($idcom->error) {
            echo "<br>" . $idcom->errno . " " . $idcom->error . "<br>";
        }
        while ($resu = $r_ventes->fetch_object()) {
            array_push($annee, $i);
            array_push($valeurV, $resu->CT);
        }
        //****************************************commandes**********************************************
        $valeurC = array();
        $req_commandes = "SELECT SUM(rsc_ttc) as CT FROM Resume_commande_$i WHERE rsc_etat = 3 GROUP BY MONTH(rsc_date)";
        $r_commandes = $idcom->query($req_commandes);
        if ($idcom->error) {
            echo "<br>" . $idcom->errno . " " . $idcom->error . "<br>";
        }
        while ($resu = $r_commandes->fetch_object()) {
            array_push($valeurC, $resu->CT);
        }
        
        include "annees.svg.php";
        $valeurC = array();
        $valeurV = array();
        $valC = 0.00;
        $valV = 0.00;
        $val = 0.00;
        if ($i < date('Y')) { //on enregistre les années passées
            $fp = fopen("svg/annee_" . $i . ".svg", "w+");
            fputs($fp, ob_get_contents());
        }
        ob_end_flush();
        ob_end_clean();
    }
}
exit;
?>