<?php
if (!isset($incpath)) {
    session_start();
    if (!isset($incpath)) {
        $p=preg_split("[/]", $_SERVER['PHP_SELF']);
        $incpath="";
        for ($i=1;$i<sizeof($p)-1;$i++) {
            $incpath='../'.$incpath;
        }
        unset($p, $i);
    }
    // $date= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    include $incpath."mysql/connect.php";
    include $incpath."php/fonctions.php";
    connexobjet();
}
$date= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
if (!$date) {
    $date = ANNEE."-".date('m')."%";
}
$an = substr($date, 0, 4);
?>
<script>
$(document).ready(function() {
    function ctrl() {
        cp = 0;
        $("#export_achat input[type=checkbox]").each(function() {
        id = $(this).attr('id');
        if($(this).prop('checked')==true) {
        cp++;
        }                        
    });        
        if (cp == 0) {
            $('#export_ebp').css('visibility','hidden');
        } else {
            $('#export_ebp').css('visibility','visible');
        }
    }
    $('#export_achat input[type=checkbox]').click(function(){
        if ($(this).prop('checked') == false) {
            $(this).prop('checked',false);
            modif($(this).attr('id'),25,1,'export','1&an=<?php echo $an?>');
            ctrl();
        } else {
            $(this).prop('checked',true);
            modif($(this).attr('id'),25,0,'export','1&an=<?php echo $an?>');
            ctrl();
        }
    });
    ctrl();
});
</script>
<?php
$req_recher="SELECT def_tvavaleur,
                    def_artvaleur,
                    edi_nom,
                    rsc_ttc,
                    rsc_id,
                    Journal_factures_$an.*,
                    edi_code,                    
                    tva_code,
                    tva_codeachat,
                    Vce_nom,
                    Vana_nom,
                    Vct_nom,
                    uti_nom
                        FROM Detail_facture_$an
                        JOIN Journal_factures_$an ON def_facture = jrn_facture
                        JOIN Resume_commande_$an ON rsc_id = jrn_facture
                        JOIN Editeurs ON edi_id = rsc_serveur
                        JOIN Utilisateurs ON uti_id = edi_utilisateur
                        JOIN Tva ON tva_id = def_tva
                        LEFT JOIN Codes ON tva_code = cod_id
                        LEFT JOIN Vcodes_entree ON Vce_id = def_codeent
                        LEFT JOIN Vcodes_tvadeductible ON Vct_id = tva_codeachat                 
                        JOIN Vanalytique ON Vana_id = def_codeana
                            WHERE DATE(jrn_date ) LIKE '$date%'
                                ORDER BY jrn_date, rsc_id , tva_code DESC";
// exit;
$r_recher=$idcom->query($req_recher);
// echo $r_recher->num_rows;
?>
<center id="resultat_analyse">
<?php
if ($r_recher->num_rows == 0) {
    echo "<h1>Il n'a pas de donnée à analyser</h1>";
    exit;
}
//recherche des tautaux par tva

$editeur = "";
?>

<table id="export_achat" style="width:900px"><tr style='background-color:white'><th colspan="8">Analyse des achats</th></tr>
<TR><TH>Journal</TH><TH>Jour</TH><TH>Document</TH><TH>Compte général</TH><TH>Libellé</TH><TH>Débit</TH><TH>Crédit</TH><th>Poste analytique</th></TR>
<?php
$n = 1;
$m = 0;
// $ligne_csv = array('','','','','','');
// $csv = array();
$mode = 1;//valeur fictive de départ
$ttr = 0.00;//total cumulé de resume_commande
$ttr_facture = 0.00;//resume_commande
$ttd = 0.00;//total cumulé du détail des factures
$ttd_facture = 0.00;//total de la facture
$ttd_tva = 0.00;//total tva de la facture
$journal = "";
$nfacture = '';
$utilisateur = '';
while ($rq_recher =$r_recher->fetch_object()) {
    // array_push($csv, $ligne_csv);
    // print_r($rq_recher);
    if ($rq_recher->rsc_ttc > 0.00) {
        if ($journal != $rq_recher->jrn_id) {
            //si $rq_recher->rsc_ttc != $ttd_facture on affiche une ligne d'erreur
            if (sprintf('%01.2f', $ttr_facture) != sprintf('%01.2f', $ttd_facture + $ttd_tva)) {
                echo "<th colspan=8>Erreur, le détail ne correspond pas au total : ".sprintf('%01.2f', $ttd_facture + $ttd_tva).", module ".$utilisateur.", facture n° ".$nfacture."</th>";
            }
            $ttd_facture = 0.00;
            $ttd_tva = 0.00;
            $coul=($m % 2 == 0)?$coulCC:$coulFF;
            $check=($rq_recher->jrn_export == 1)?"":" checked";
            // $m++;
            echo "<tr style='background-color:".$coul."'><td>AP</td>
            <td>".dateFR($rq_recher->jrn_date)."</td>
            <td>".$rq_recher->jrn_numero."</td>
            <td>".$rq_recher->edi_code."</td><td>".$rq_recher->edi_nom."</td>
            <td></td>
            <td class='droite'>".monetaireF($rq_recher->rsc_ttc)."</td>
            <td><input id='".$rq_recher->jrn_id."' type='checkbox' ".$check."></td></tr>";
            $ttr += $rq_recher->rsc_ttc;
            $ttr_facture = $rq_recher->rsc_ttc;
            $journal = $rq_recher->jrn_id;
            $m++;
        }
        //pour chaque ligne retournée, deux affichées : article pht et tva
        echo "<tr style='background-color:".$coul."'><td>AP</td><td>".dateFR($rq_recher->jrn_date)."</td><td></td><td>".$rq_recher->Vce_nom."</td><td>".$rq_recher->edi_nom."</td><td class='droite'>".monetaireF($rq_recher->def_artvaleur)."</td><td></td><td>".$rq_recher->Vana_nom."</td></tr>";
        $ttd += $rq_recher->def_artvaleur;
        $ttd_facture += $rq_recher->def_artvaleur;
        if ($rq_recher->def_tvavaleur != 0.00) {
            echo "<tr style='background-color:".$coul."'><td>AP</td><td>".dateFR($rq_recher->jrn_date)."</td><td></td><td>".$rq_recher->Vct_nom."</td><td>".$rq_recher->edi_nom."</td><td class='droite'>".monetaireF($rq_recher->def_tvavaleur)."</td><td></td><td></td></tr>";
            $ttd += $rq_recher->def_tvavaleur;
            $ttd_tva += $rq_recher->def_tvavaleur;
        }
        $utilisateur = $rq_recher->uti_nom;
        $nfacture = $rq_recher->jrn_facture;
    } else { //--------------------avoir--------------------------------------------
        if ($journal != $rq_recher->jrn_id) {
            $check=($rq_recher->jrn_export == 1)?"":" checked";
            $coul=($m % 2 == 0)?$coulCC:$coulFF;
            // $m++;
            echo "<tr style='background-color:".$coul."'><td>AP</td>
            <td>".dateFR($rq_recher->jrn_date)."</td>
            <td>".$rq_recher->jrn_numero."</td>
            <td>".$rq_recher->edi_code."</td><td>".$rq_recher->edi_nom."</td>
            <td class='droite'>".monetaireF(($rq_recher->rsc_ttc*-1))."</td>
            <td></td>
            <td><input id='".$rq_recher->jrn_id."' type='checkbox' ".$check."></td></tr>";
            $ttr += ($rq_recher->rsc_ttc*-1);
            $journal = $rq_recher->jrn_id;
            $m++;
        }
        //pour chaque ligne retournée, deux affichées : article pht et tva
        echo "<tr style='background-color:".$coul."'><td>AP</td><td>".dateFR($rq_recher->jrn_date)."</td><td></td><td>".$rq_recher->Vce_nom."</td><td>".$rq_recher->edi_nom."</td><td></td><td class='droite'>".monetaireF(($rq_recher->def_artvaleur*-1))."</td><td>".$rq_recher->Vana_nom."</td></tr>";
        $ttd += ($rq_recher->def_artvaleur*-1);
        if ($rq_recher->def_tvavaleur != 0.00) {
            echo "<tr style='background-color:".$coul."'><td>AP</td><td>".dateFR($rq_recher->jrn_date)."</td><td></td><td>".$rq_recher->Vct_nom."</td><td>".$rq_recher->edi_nom."</td><td></td><td class='droite'>".monetaireF(($rq_recher->def_tvavaleur*-1))."</td><td></td></tr>";
            $ttd += ($rq_recher->def_tvavaleur*-1);
        }
    }
    // $m++;


}
if (sprintf('%01.2f', $ttr_facture) != sprintf('%01.2f', $ttd_facture + $ttd_tva)) {
    echo "<th colspan=8>Erreur, le détail ne correspond pas au total : ".sprintf('%01.2f', $ttd_facture + $ttd_tva).", module ".$utilisateur.", facture n° ".$nfacture."</th>";                
}
$cl = '';
if (sprintf('%01.2f', $ttd) != sprintf('%01.2f', $ttr)) {
    $cl = ' style="background-color:red"';
}
?>
<tr<?php echo $cl?>><th colspan = '5'></th><th><?php echo $ttd?></th><th><?php echo $ttr?><th></th></tr>
</table>
<div id="export_ebp" style='margin:10px;background-color:white;width:900px;text-align:left'>
<button onclick="charge('exportachat_ebp','<?php echo $date?>','mysql')">Export ebp Achat</button>
</div>
