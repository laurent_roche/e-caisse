<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$action= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
if ($action == 1) {
    ?>
    <h3>Arrêt de l'afficheur externe</h3>
    Veuillez patienter...
    <?php
    $ch = curl_init('http://'.$_SESSION['afficheur'].'/arret.php');
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 200);
    curl_exec($ch);
    curl_close($ch);
    ?>
    <script>
    setTimeout(() => {
        charge('afficheur',3,'panneau_d');
    }, 15000);
    </script>
    <?php
    exit;
} elseif ($action == 2) {
    ?>
    <h3>Redémarrage de l'afficheur</h3>
    Veuillez patienter... environ 30s
    <?php
    $ch = curl_init('http://'.$_SESSION['afficheur'].'/reboot.php');
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 200);
    curl_exec($ch);
    curl_close($ch); 
    ?>
    <script>
    setTimeout(() => {
        charge('afficheur',4,'panneau_d');
    }, 30000);
    </script>
    <?php
    exit;
} elseif ($action == 3) {
    echo "<h3>Arrêt de l'afficheur externe</h3>Vous pouvez maintenant débrancher l'afficheur";
    exit;
} elseif ($action == 4) {
    echo "<h3>Redémarage de l'afficheur</h3>L'afficheur externe doit être maintenant disponible";
    exit;
}
//on vérifie s'il y a une valeur de stockée sur l'afficheur
$valeur = @fopen("http://".$_SESSION['afficheur']."/totalprice.txt", "r");
if ($valeur) {
      $prix = fgets($valeur);
      if($prix =="") {
        unset($prix);
      }
    if (!feof($valeur)) {
        echo "L'afficheur n'est pas accessible\n";
    } 
    fclose($valeur);
}
?>
<h3>Afficheur externe</h3>
<?php
if(isset($prix)) {
    echo 'Il y a une valeur non enregistrée dans l\'afficheur : '.$prix;
    ?>
    <script>$('#arret').prop("disabled", true); </script>
    <?php
}
?>
<center>
<button id='arret' onclick=charge('afficheur',1,'panneau_d')>Arrêt de l'afficheur</button><br><br>
<button onclick=charge('afficheur',2,'panneau_d')>Redémmarage de l'afficheur</button></center>
