<?php
session_start();
/**
 * Générer un nouvel archivage
 * #e-Caisse_CTRL#
 */
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
require_once $incpath . "mysql/connect.php";
connexobjet();
$psYear = filter_input(INPUT_GET, "year", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

function csvOutTable($psTable, $psFilePN, $psSep)
{ 
    global $idcom, $incpath;
    // echo $incpath.'pdf/archives/'.$psFilePN;
    $oOut = fopen($incpath.'pdf/archives/'.$psFilePN, 'w');
    $extract = "SELECT rst_id AS N°_ticket, SUM(tic_tt) AS Total_ttc, tva_nom AS TVA, mdr_nom AS Mode_règle, rst_validation AS h_enregistrement  FROM Tickets_$psTable JOIN Tva ON tva_id = tic_ntva JOIN Resume_ticket_$psTable ON rst_id = tic_num JOIN Mode_reglement ON mdr_id = rst_etat  GROUP BY tic_ntva , tic_num  ORDER BY rst_id";
    $oQuery = $idcom->query($extract);
    // $oQuery = $idcom->query("SELECT * FROM {$psTable}");
    while ($aLig = $oQuery->fetch_array(MYSQLI_NUM)) {
        fputcsv($oOut, $aLig, $psSep);
    }
    fclose($oOut);
}

$psAct = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$psNm = filter_input(INPUT_GET, "nm", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$psCmt = filter_input(INPUT_GET, "cmt", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

switch ($psAct) {
case 'Nouv':
    ?>
        <style>
            label {
                width: 8em;
                display: block;
                float: left;
                text-align: right;
            }
        </style>
        <h3>Nouvel Archivage pour l'année <?php echo $psYear ?></h3>
        <form>
            <p>
                <label>Nom : </label><input type="text" size="60" name="nm" /><br />
                <label>Commentaire : </label><textarea name="cmt" rows="5" cols="60" /></textarea>
            <p style="text-align:center; clear: both;">
                <input type="button" value="Générer" onclick="charge('extract_nouv', 'Sav&nm='+$('input[name=nm]').val()+'.csv'+'&cmt='+$('textarea[name=cmt]').val()+'&year='+$('input[name=DtYear]').val(),'panneau_d')" />
            </p>
        </form>
    <?php
    break;
case 'Sav':
        include_once $incpath . 'inc/hash_inc.php';
        csvOutTable($psYear, $psNm, '|');
        // $sTheHash=hash_file(HASH_ALGO, ARCHIV_CSV_FILEPN) ;
        // // Appel API pour enregistrer l'archivage sur le serveur de contrôle
        // $aPost=['j'=>$gsJeton, 'bgn'=>$psYear .'-01-01', 'end'=>$psYear .'-12-31', 'h'=>$sTheHash, 'nm'=>$psNm, 'cmt'=>$psCmt ];
        // $sRes = mc_callUrlPost(CTRL_URL . '/addXtr', $aPost);
        // $aoRes = json_decode($sRes);
    ?>
        <h3>Données archivées</h3>
        <p>Les données ont été archivées. Enregistrer le fichier : il faudra le produire en cas de contrôle.</p>
        <script>
            document.location.href = '<?php echo $incpath.'pdf/archives/'.$psNm ?>';
        </script>
<?php
    break;
}
?>