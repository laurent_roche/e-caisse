<?php
session_start();
// print_r($_SESSION);
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$filtre= filter_input(INPUT_GET, "f", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$an= filter_input(INPUT_GET, "an", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
// if($req == "") $req= filter_input( INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require_once $incpath."php/fonctions.php";
connexobjet();
if (!$an) {
    $an = date("Y");
}
?>
<script>
$(document).ready(function(){
    $('.generique td').css('cursor','pointer');
    $('.generique tr').click(function(){
    $('.generique tr').css('font-weight','normal');
    $(this).css('font-weight','bold');
    })
    $("#remise tr").click(function(){
        art=$(this).attr('id');
        charge("detail_ticket","<?php echo $an?>&tic="+art,"panneau_d");
    })
})
</script>
<h3>Tickets corrigés pour l'année <?php echo $an?></h3>
<center>
    <div id='cor' style='width:100%;overflow:auto'>
        <table id='remise' class="generique">
            <thead>
            <TR><TH>N° ticket</TH><th>Date</th><TH>Valeur&nbsp;remisée</TH><TH>Mode&nbsp;règlement</TH></TR></thead>
            <tbody>
            <?php
            $req_vente="SELECT tic_num, 
                        SUM(tic_quantite * tic_prix) - SUM(tic_prixS * tic_quantiteS) AS dif, 
                        rst_mod, 
                        mdr_nom,
                        rst_validation
                            FROM Tickets_$an 
                            JOIN Resume_ticket_$an ON rst_id=tic_num 
                            JOIN Mode_reglement ON rst_etat = mdr_id 
                                GROUP BY tic_num 
                                    HAVING (SUM(tic_prixS * tic_quantiteS)- SUM(tic_quantite * tic_prix)) !=0 
                                    AND tic_num < 14500000
                                        ORDER BY rst_id DESC";
            $r_vente=$idcom->query($req_vente);
            $n = 0;
            $tt =0.00;
            while ($rq_vente=$r_vente->fetch_object()) {
                if ($n%2 == 0) {
                    $coul=$coulCC;
                } else {
                    $coul=$coulFF;
                }
                echo "<tr id=".$rq_vente->tic_num." style='background-color:".$coul."'>
                <TD>".$rq_vente->tic_num."</TD>
                <TD>".$rq_vente->rst_validation."</TD>
                <TD class='droite'style='padding-right:20px'>".monetaireF($rq_vente->dif)."</TD>
                <td>".$rq_vente->mdr_nom."</td></tr>";
                $n++;
                $tt += $rq_vente->dif;
            }
            ?></tbody>
            <tfoot>
            <tr><TD colspan='2'>Valeur remisée pour l'année</TD><td align="right"><?php echo $tt?> €</td><td></td></tr>
            </tfoot>
        </table>
    </div>
</center>
<script>
$("#panneau_g").height(($('#affichage').height())-10);
</script>
