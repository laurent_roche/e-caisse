<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "query", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$an= filter_input(INPUT_GET, "identifier", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
connexobjet();

$req_recher="SELECT edi_id, edi_nom, COUNT(jrn_id) AS qt
              FROM Journal_factures_$an
                JOIN Resume_commande_$an ON rsc_id = jrn_facture
                JOIN Editeurs ON edi_id = rsc_serveur
                  WHERE edi_nom LIKE '%$req%'
                  GROUP BY edi_id
                    ORDER BY edi_nom";
$r_recher=$idcom->query($req_recher);
echo "<ul>\n";
while ($resu=$r_recher->fetch_object()) {
    echo "\t" . '<li id="autocomplete_' . $an . '" rel="'.$an.'_' .$resu->edi_id. '">' .$resu->edi_nom." (".$resu->qt.")".'</li>' . "\n";
}
echo '</ul>';
