<?php
session_start();
// print_r($_SESSION);
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$an= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
if (!$an) {
    $an = date("Y");
}
$req_art="SELECT art_id, Vt1_nom, sec_tva, art_tva, art_stk, sec_nom, ray_nom
													FROM Articles 
														JOIN Rayons ON ray_id = art_rayon 
														JOIN Secteurs ON sec_id = ray_secteur 
														JOIN Vtit1 ON art_id = Vt1_article 
															WHERE sec_tva != art_tva";
$r_art=$idcom->query($req_art);
$nb = $r_art->num_rows;

$req_tva = "SELECT tva_id, tva_nom FROM Tva ORDER BY tva_id";
$r_tva=$idcom->query($req_tva);
while ($rq_tva = $r_tva->fetch_object()) {
    $tab_tva[]=$rq_tva->tva_nom;
}
// echo $r_art->num_rows;
// print_r($resu);
?>
<style>
/*.pt_bt{
height:25px;width:80px;margin-top:-5px;
}*/

#factures{width:90%}
table.tablesorter tbody td {
  font-size: 12px;
  text-align:left;
  color: #3D3D3D;
  padding: 4px;
  background-color: <?php echo $coulFF?>;
  vertical-align: top;
 }
table.tablesorter tbody tr.odd td {
  text-align:left;
  border-top:solid 1px;
  background-color:<?php echo $coulCC?>;
 }
 table.tablesorter thead tr .headerSortDown, table.tablesorter thead tr .headerSortUp {
background-color: #8dbdd8;}
</style>
<script src="/js/jquery.tablesorter.js"></script>
<script>
function facture(id){
charge('detail_facture',id+'&an='+<?php echo $an?>,'panneau_d');
}

$(document).ready(function(){
     $("#factures").tablesorter({ widgets: ['zebra']});
    }
);
</script>

<h3>Contrôle de cohérence des taux de tva ( <?php echo $nb?> erreurs )</h3>
<center><table id='factures' class="tablesorter">
  <thead>
  <TR>
  <TH>N°</TH><TH>Article</TH><TH>Tva article</TH><TH>Tva secteur</TH><TH>Secteur</TH><th>Rayon</th>
  </TR>
  </thead>
  <tbody>
<?php
while ($resu=$r_art->fetch_object()) {
    if ($resu->Vt1_nom == '') {
        $nom = '';
    } else {
        $nom = stripslashes($resu->Vt1_nom);
    }
    echo "<tr onclick=\"charge('/Saisie/article',".$resu->art_id.",'panneau_d')\"><TD>".$resu->art_id."</TD><TD>".$nom."</TD><TD>".$tab_tva[$resu->art_tva]."&nbsp;%</TD><TD>".$tab_tva[$resu->sec_tva]."&nbsp;%</TD><TD>".$resu->sec_nom." </TD><td>".$resu->ray_nom."</td></tr>";
}
?>
</tbody>  
</table></center>

<script>
var b=$('#affichage').height() - 20;
$("#panneau_g").css('max-height', b);

</script>