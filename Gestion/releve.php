<?php
session_start();
$tab=explode('/', $_SERVER['REQUEST_URI']);
$dossier=$tab[1];

if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();

$an = date("Y");
$req_comptes="SELECT cpt_id, COUNT(tic_cp) AS nb, SUM(tic_tt) AS valeur, cpt_nom FROM Tickets_$an JOIN Comptes ON cpt_id = tic_cp WHERE tic_num > 100000 GROUP BY tic_cp ORDER BY cpt_nom";
$r_comptes=$idcom->query($req_comptes);

$nb = $r_comptes->num_rows;
$s = $nb > 1?"s":"";

?>
<script>
$(document).ready(function(){
  $('.generique tr').click(function(){
  $('.generique tr').css('font-weight','normal');
  $(this).css('font-weight','bold');
  charge('detail_releve',$(this).attr('id'),'panneau_d');
  });
});
</script>
<h3> Compte<?php echo $s?> client<?php echo $s?> en attente</h3>
<table class="generique"><thead><TR><TH>Nom</TH><TH>Nb d'article</TH><TH>Valeur</TH></TR></thead><tbody>
<?php
//***********************************liste des articles en compte****************************************
$n = 0;
while ($rq_compte=$r_comptes->fetch_object()) {
    $coul=($n % 2 == 0)?$coulCC:$coulFF;
    echo "<tr id='". $rq_compte->cpt_id."' style='background-color:".$coul.";'><td>".$rq_compte->cpt_nom."</td><td>".$rq_compte->nb."</td><td>".monetaireF($rq_compte->valeur)." €</td></tr>";
    $n++;
}
?></tbody></table>
