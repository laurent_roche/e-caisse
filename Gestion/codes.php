<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$date= filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();

if (!$date) {
    $date = ANNEE."-".date('m')."-".date('j');
}
$tab_date=explode('-', $date);
require $incpath."php/config.php";
//combo année
$option_annee = '';
$anneeB = '';
for ($i = ANNEE; $i >= $config['debut']; $i--) {
    $sel= ($tab_date[0] == $i)?" selected":"";
    
    $option_annee .= "<option".$sel.">".$i."</option>";
    //tableau javascript de années bisextilles
    $anneeB = "'".date('L', mktime(1, 1, 1, 1, 1, $i))."',".$anneeB;
}

//combo mois

$option_mois = '';
for ($i = 0; $i <= 12; $i++) {
    if (($tab_date[1]*1) == $i) {
        $cemois = $i;
        $sel= " selected";
    } else {
        $sel = "";
    }
    $option_mois .= "<option value='".str_pad($i, 2, 0, STR_PAD_LEFT)."'".$sel.">".$mois[$i]."</option>";
}
//combo jour pour la date par défaut, en cas de modification, construction javascript
$limit= date("t", strtotime($date));
// echo $tab_date[2];
if (($cemois == date('m')) && ($tab_date[0] == date('Y'))) {
    $limit = date('d');
}
$option_jour = "<option></option><option value=''>Mois</option>";
for ($i = 0; $i <= $limit; $i++) {
    if (isset($tab_date[2])) {
        $sel =(($tab_date[2]*1) == $i)?" selected":"";         
    } 
    $option_jour .= "<option value='".str_pad($i, 2, 0, STR_PAD_LEFT)."'".$sel.">".$i."</option>";
}

$nb_jour="'',31,28,31,30,31,30,31,31,30,31,30,31";
$nb_jourB="'',31,29,31,30,31,30,31,31,30,31,30,31";
?>
<script>
var tab_jour=[<?php echo $nb_jour?>];

//il faudrait n'afficher que les jours passés du mois actif

var debut = <?php echo $_SESSION['debut']?>;
var tab_annee=[<?php echo $anneeB?>];
function jour(id) {
if($('#mois').val() == 2){//fevrier, on adapte si bisextille
    id = id + (+tab_annee[$('#annee').val() - debut]);
    // alert(id);
}
    
cont = '<option></option><option value="">Mois</option>';
for( i = 1 ; i <= id; i++){
    cont += '<option value='+("00" + i).slice(-2)+'>'+i+'</option>';
    }
    $('#jour').html(cont);
}
function analyse(jour){
    charge('resultat_analyse',$('#annee').val()+'-'+$('#mois').val()+'-'+jour,'resultat_analyse');
}
    </script>
<h3>Analyse <select id="annee" onchange="$('#mois option[value=00]').prop('selected', true);">
<?php echo $option_annee?></select>
<select id="mois" onchange=jour(tab_jour[(this.value*1)])><?php echo $option_mois?></select>
<select id="jour" onchange=analyse(this.value)><?php echo $option_jour?></select>
</h3>
<?php require "resultat_analyse.php";?>

</center>
<script>

</script>