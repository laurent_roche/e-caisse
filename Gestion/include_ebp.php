<?php
// requete groupée pour tous les modes sauf 28 qui devra être détaillé par ticket
//recalcul du total pour résoudre les problèmes d'arrondi
$req_reglement="(SELECT 'TZ' AS code_J,
            DATE_FORMAT(DATE(rst_validation),'%d/%m/%y') AS date,
            mdr_id,
            mdr_nom AS libelle,
            cod_nom AS code,
            SUM(rst_total) AS total,
            1 AS rst_id,
            \"Ventes par\" AS tit
                FROM Resume_ticket_$an                
                JOIN Mode_reglement ON mdr_id = rst_etat
                LEFT JOIN Codes ON mdr_code = cod_id
                    WHERE DATE(rst_validation) = '$date'
                    AND mdr_id != 5
                        GROUP BY rst_etat,
                        mdr_nom ORDER BY rst_etat)                        
        UNION ALL
            (SELECT 'TZ' AS code_J,
            DATE_FORMAT(DATE(rst_validation),'%d/%m/%y') AS date,
            mdr_id,
            CONCAT(cpt_nom, '-F',fac_id) AS libelle,
            cod_nom AS code,
            rst_total AS total,
            rst_id,
            \"Ventes pour\" AS tit
                FROM Resume_ticket_$an
                JOIN Mode_reglement ON mdr_id = rst_etat
                LEFT JOIN Codes ON mdr_code = cod_id
                LEFT JOIN Factures_$an ON fac_ticket = rst_id
                LEFT JOIN Comptes ON cpt_id = fac_cp
                    WHERE DATE(rst_validation) = '$date' 
                    AND mdr_id = 5 )";
// exit;
$r_reglement=$idcom->query($req_reglement);
$mdr = '';
$val_HT='';
while ($rq_reglement =$r_reglement->fetch_object()) {
    $jour=$rq_reglement->date;
    $libelle = str_replace("&#39;", "'", $rq_reglement->libelle);
    $tit = str_replace("&#39;", "'", $rq_reglement->tit);
    if ($rq_reglement->total < 0.00) {
        echo "\"".$rq_reglement->code_J."\"\t".$jour."\t\"".$rq_reglement->code."\"\t\t\"".$tit." ".$libelle."\"\t\t".monetaireF($rq_reglement->total*-1)."\t\n";
    } else {
        echo "\"".$rq_reglement->code_J."\"\t".$jour."\t\"".$rq_reglement->code."\"\t\t\"".$tit." ".$libelle."\"\t".monetaireF($rq_reglement->total)."\t\t\n";
    }
    if ($rq_reglement->mdr_id <> 5) { //autres que différé
        $req_detail="SELECT 'TZ' AS code_J,
                    DATE_FORMAT(DATE(rst_validation),'%d/%m/%y') AS date,
                    cod_nom,
                    Vca_nom,
                    ray_nom,                    
                    SUM(tic_tt) AS stts,
                    tva_nom
                        FROM Resume_ticket_$an 
                        JOIN Tickets_$an ON rst_id = tic_num
                        JOIN Articles ON art_id = tic_article
                        JOIN Rayons ON ray_id = art_rayon
                        JOIN Codes ON cod_id = ray_sort
                        JOIN Secteurs ON sec_id = ray_secteur
                        LEFT JOIN Vcana ON Vca_id = ray_ana
                        JOIN Tva ON tva_id = tic_ntva
                            WHERE DATE(rst_validation) = '$date'
                            AND rst_etat = ".$rq_reglement->mdr_id."
                                GROUP BY  Vca_nom,
                                tic_ntva,
                                rst_etat
                                    ORDER BY tic_ntva DESC;";
    } else { //différé
        // print_r($rq_reglement)."<br>";
        $req_detail="SELECT rst_id,
                        'TZ' AS code_J,
                        DATE_FORMAT(DATE(rst_validation),'%d/%m/%y') AS date,
                        cod_nom,
                        Vca_nom,
                        ray_nom,
                        SUM(tic_tt) AS stts,
                        tva_nom
                            FROM Resume_ticket_$an
                                JOIN Tickets_$an ON rst_id = tic_num
                                JOIN Articles ON art_id = tic_article
                                JOIN Rayons ON ray_id = art_rayon
                                JOIN Codes ON cod_id = ray_sort
                                JOIN Secteurs ON sec_id = ray_secteur
                                LEFT JOIN Vcana ON Vca_id = ray_ana
                                JOIN Tva ON tva_id = tic_ntva
                                    WHERE rst_id = ".$rq_reglement->rst_id."
                                    AND rst_etat = 5
                                        GROUP BY rst_id,
                                        Vca_nom,
                                        tic_ntva
                                            ORDER BY rst_id DESC";
    }
    $plan = '';       
    $r_detail = $idcom->query($req_detail);
    while ($rq_detail =$r_detail->fetch_object()) {
        if ($rq_detail->stts != 0.00) {
            if (substr($rq_detail->cod_nom, 0, 3) == 467 ) {
                $plan = '2';
            } else {
                $plan = '';
            }
            $code = array("&#39;","'");
            $remplac = array("'","_");
            $ray_nom = str_replace($code, $remplac, $rq_detail->ray_nom);
            $val_HT = sprintf("%01.2f", $rq_detail->stts / (1+($rq_detail->tva_nom/100)));
            if ($val_HT < 0.00) {
                $val_HT = ($val_HT *-1);
                echo "\"".$rq_detail->code_J."\"\t".$jour."\t\"".$rq_detail->cod_nom."\"\t\"".$rq_detail->Vca_nom."\"\t\"".$ray_nom."\"\t".monetaireF($val_HT)."\t".$plan."\t\n";
                if ($rq_detail->tva_nom != 0.00) {
                    $ht = ($rq_detail->stts * -1) - $val_HT;
                    $tab_tva[str_replace('.', '_', $rq_detail->tva_nom)] += $ht;
                }
            } else {
                echo "\"".$rq_detail->code_J."\"\t".$jour."\t\"".$rq_detail->cod_nom."\"\t\"".$rq_detail->Vca_nom."\"\t\"".$ray_nom."\"\t\t".monetaireF($val_HT)."\t".$plan."\n";
                if ($rq_detail->tva_nom != 0.00) {
                    $ht = $rq_detail->stts - $val_HT;
                    $tab_tva[$rq_detail->tva_nom] += $ht;
                }
            }
            //mise en tableau de la valeur tva
        }
        $n = 0;
        // print_r($tab_tva);
        foreach ($tab_tva as $key => $value) {
            if ($value != 0.00) {
                $tv = $key;
                if (strrpos($tv, ".")=== false) {//avoir
                    $tv = str_replace('_00', '', $tv);
                    $tv = str_replace('_', '.', $tv);
                    echo "\"TZ\"\t".$jour."\t\"".$tab_tvaCode[$n]."\"\t\t\"TVA coll. ventes à ".str_replace('.', ',', $tv)."%\"\t".monetaireF($value)."\t\t\n";
                    $tab_tva[$key] = 0.00;    
                } else {
                    // echo "<br>-----".$key."-------".$tab_tvaCode[$n]."-----<br>";
                    $tv = str_replace('.00', '', $tv);
                    echo "\"TZ\"\t".$jour."\t\"".$tab_tvaCode[$n]."\"\t\t\"TVA coll. ventes à ".str_replace('.', ',', $tv)."%\"\t\t".monetaireF($value)."\t\n";
                    $tab_tva[$key] = 0.00;
                }
                
            }
            $n++;
        }
    }
    //lecture du tableau tva
}
// exit;
