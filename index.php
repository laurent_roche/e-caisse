<?php
session_start();
/*
entrée de l'application, elle ne fait que lister les différentes entrées du
programme.
Ces entrées correspondent aux dossiers de l'application. Une fois entré dans un
dossier, on ne peut manuellement aller dans un autre. Il faut se déconnecter et
reconnecter.
Suite dans le fichier index.php du dossier choisi

Un seul utilisateur par dossier dans le même navigateur. Si on veut travailler
sur plusieurs en même temps, ouvrir autant de navigateurs compatibles : firefox,
opera, chromium, midori, arora... mais pas IE et peut-être pas safari
*/
$ip = $_SERVER['REMOTE_ADDR'];
require_once "mysql/connect.php";
$idcom=connexobjet();
$req_util="SELECT * FROM Fonctions";
$r_util=$idcom->query($req_util);
$nm=$r_util->num_rows;
//definition des variables de l'application, voir aide dans /Administration

$req_config="SELECT * FROM Config LEFT JOIN Externe ON ext_poste ='$ip'";
$r_config=$idcom->query($req_config);
$rq_config=$r_config->fetch_object();
// print_r($rq_config);
//l'historique est de 6 années + année en cours
/* En administration on peut garder l'année de départ si on veut conserver l'historique complet
ou supprimer les années supplémentaire, la date de départ s'ajustera à la valeur historique légale
 */
if ($rq_config -> ext_poste!= "") {
    $_SESSION['afficheur']=$rq_config->ext_ip;
}
if ($rq_config -> ext_auto == "1") {
    $_SESSION['afficheur_auto']=1;
}

// print_r($_SESSION);
?>
<!DOCTYPE html>
<html>
<head><TITLE>Magasin</TITLE>
<script type="text/javascript" src="/js/jquery.js"></script>
</head>
<body style="background-color:#ab5e0d">
<center>
  <div align="center" style="width:250px;margin-top:20px">
  <style>
  .bout{
  width:200px;
  font: 1.5em bold small-caps serif;
  margin:10px;
  }
  </style>
<?php
while ($rq_util=$r_util->fetch_object()) {
    ?>
<button class="bout" onclick="location.href='/<?php echo
$rq_util->fon_nom?>/index.php'"><?php echo $rq_util->fon_nom?></button>
<?php
}
?>

</center>
<script>
<?php
// hauteur de la fenêtre -40 - (20(espace entre bouton) * nombre de boutons)
?>
$('.bout').css('height',($(document).height()-40-(20*<?php echo $nm?>))/<?php
echo $nm?>);
</script>
</body>
</html>
