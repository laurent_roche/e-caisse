<?php
session_start();
$dos = filter_input(INPUT_GET, "dos", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$uti = filter_input(INPUT_GET, "uti", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$tt = filter_input(INPUT_GET, "tt", FILTER_SANITIZE_FULL_SPECIAL_CHARS); //retour à la racine
function detruire($uti)
{
    foreach ($_SESSION as $k => $v) {
        $tab = explode("_", $k);
        if (isset($tab[1])) {
            if ($tab[1] == $uti) {
                $champ = $tab[0] . "_" . $uti;
                unset($_SESSION[$champ]);
            }
        }
        if (isset($dos)) {
            unset($_SESSION[$dos]);
        }
    }
}
switch ($dos) {
case "Ventes": echo 'ventes';
    if (!isset($incpath)) {
        $p = preg_split("[/]", $_SERVER['PHP_SELF']);
        $incpath = "";
        for ($i = 1; $i < sizeof($p) - 1; $i++) {
            $incpath = '../' . $incpath;
        }
        unset($p, $i);
    }
    include $incpath . "mysql/connect.php";
    connexobjet();
    $req_uti = "UPDATE Utilisateurs SET uti_etat='0' WHERE uti_id=" . $_SESSION['Ventes'];
    $idcom->query($req_uti);
    detruire($uti);
    unset($_SESSION['Ventes']);
    if ($tt == 1) {
        $dos = '';
    }
    // exit;
    break;

case "Comptes":
    detruire($_SESSION[$dos]);
    unset($_SESSION['Comptes']);
    break;
case "Gestion":
    detruire($_SESSION[$dos]);
    unset($_SESSION['Gestion']);
    break;

case "Saisie":
    detruire($_SESSION[$dos]);
    unset($_SESSION['Saisie']);
    break;

case "Administration":
    detruire($_SESSION[$dos]);
    unset($_SESSION['Administration']);
    break;
default:
    header('location:/index.php');
}
header('location:/' . $dos);
