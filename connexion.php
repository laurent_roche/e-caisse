<?php
session_start();
// print_r($_SESSION);
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}

$nom = filter_input(INPUT_POST, "nom", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$pass = filter_input(INPUT_POST, "pass", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
// print_r($_POST);

/* if ($pass!= "vide") {//il y a un mot de passe
    $value = md5($pass);
    $req_util = "SELECT uti_id, uti_pass FROM Fonctions JOIN Utilisateurs ON uti_fonction = fon_id WHERE uti_nom = '$nom'";
} elseif ($pass == "vide") {//pas de mot de passe
    $req_util = "SELECT * FROM Fonctions JOIN Utilisateurs ON uti_fonction = fon_id WHERE uti_nom = '$nom'";
} else {//ne devrait jamais arriver
    echo "erreur système";
    exit;
} */

require_once $incpath . "mysql/connect.php";
$idcom = connexobjet();
$req_util = "SELECT * FROM Fonctions JOIN Utilisateurs ON uti_fonction = fon_id WHERE uti_nom = '$nom'";
$r_util = $idcom->query($req_util);
$rq_util = $r_util->fetch_object();
// exit;
if ($pass != "vide") {
    //verification du mot de passe
    if ($rq_util->uti_pass != md5($pass)) {
        $nb = $_SESSION[$_SERVER['REMOTE_ADDR'] . '_' . $rq_util->uti_id];
        ++$nb;
        $_SESSION['message_' . $rq_util->uti_id] = "erreur de connexion. Vous pouvez essayer encore " . (3 - $nb) . " fois.";
        $utilisateur = '';
        // exit;
    } else {
        $utilisateur = $rq_util->uti_id;
    }
} else {
    $utilisateur = $rq_util->uti_id;
}

if ($utilisateur != '') {
    //definition des variables utilisateurs, les variables de l'application ont été définies dans la page /index.php
    $_SESSION['pass_' . $rq_util->uti_id] = ($rq_util->uti_pass != '') ? 'oui' : 'non';
    $_SESSION['nom_' . $rq_util->uti_id] = $rq_util->uti_nom; //nom de l'utilisateur
    $_SESSION['id_' . $rq_util->uti_id] = $rq_util->uti_id; //id de l'utilisateur
    $_SESSION['titre_' . $rq_util->uti_id] = $rq_util->uti_titre; //couleur de la ligne de titre
    $_SESSION['fondF_' . $rq_util->uti_id] = $rq_util->uti_fondF; //couleur foncée de l'interface
    $_SESSION['fondC_' . $rq_util->uti_id] = $rq_util->uti_fondC; //couleur clair de l'interface
    $_SESSION['droits_' . $rq_util->uti_id] = $rq_util->uti_droits; //gestion des permissions
    $surligne = $_SESSION['surligne_' . $rq_util->uti_id] = $rq_util->uti_surligne; //couleur de surlignage
    $_SESSION['dossier_' . $rq_util->uti_id] = $rq_util->fon_nom; //dossier racine de l'utilisateur
    $_SESSION[$rq_util->fon_nom] = $rq_util->uti_id; // le nom du dossier est associé à l'id de l'utilisateur
    if (!isset($_SESSION[$_SERVER['REMOTE_ADDR'] . '_' . $rq_util->uti_id])) {
        $_SESSION[$_SERVER['REMOTE_ADDR'] . '_' . $rq_util->uti_id] = 0;
    }
    if ($rq_util->fon_nom == 'Ventes') {
        $req_solde = "SELECT * FROM Soldes WHERE sol_debut <= '" . date('Y-m-d') . "' AND sol_fin >= '" . date('Y-m-d')."'";
        $r_solde = $idcom->query($req_solde);
        $_SESSION['soldes']=($r_solde->num_rows == 0)?'non':'oui';
    }
    //on efface la
    // header("location:/".$rq_util->fon_nom."/");
}
// exit;
?>
<script>
    setTimeout(function() {
        location.reload()
    }, 100);
</script>