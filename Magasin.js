function charge(page,req,div){
	//on affiche la div mysql pour indiquer un travail en cours
	$("#mysql").empty();//au cas ou il y aurait une erreur enregistrée
	$('#mysql').css('visibility','visible');
	//si div = panneau_g, on verifie si la div liste est présente, si oui  on remplace par confirmation qu'on affiche
	if((div == 'panneau_g')&&($('#liste').height() > 1)){
	div = 'popup_g';
	$('#popup_g').css('visibility','visible');
	}
	$.ajax({
	type: "GET",
	url: page+".php?req="+req,
	dataType : "html",
	//affichage de l'erreur en cas de problème
	error:function(msg, string){
	alert( "Error !: " + string );
	},
	success:function(data){
	$('#mysql').css('visibility','hidden');
	$("#"+div).empty();
	$("#"+div).append(data);
	if(div == 'popup_g')$('#fermer').css('display','block');
	}});
}

function recharge(){
	window.location.reload();
}

function modif(id,tb,vl,cp,my){
	$('#mysql').css('visibility','visible');
	$.ajax({
	type: "GET",
	url: "/mysql/mysql.php?my="+my+"&id="+id+"&tb="+tb+"&vl="+vl+"&cp="+cp,
	dataType : "html",
	//affichage de l'erreur en cas de problème
	error:function(msg, string){
	alert( "Error !: " + string );
	},
	success:function(data){
	$("#mysql").empty();
	$("#mysql").append(data);	
	}});
}
