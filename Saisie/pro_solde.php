<?php
session_start();

if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
$req = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$art = filter_input(INPUT_GET, "art", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

require $incpath . "mysql/connect.php";
require $incpath . "php/fonctions.php";
// echo "<br>".$dossier;
connexobjet();
/*
on recherche si l'article existe, si oui on met à jour, si non on crée
*/
$req_prom = "SELECT * FROM Promo 
                        JOIN Varticle_editeur ON Vart_id = pro_article
                        JOIN Rayons ON ray_id = Vart_rayon
                            WHERE Vart_utilisateur = $_SESSION[$dossier] ORDER BY pro_mode";
$r_prom = $idcom->query($req_prom);
if ($r_prom->num_rows == 0) {
    echo "<h3>Il n'y a pas d'article en promotion ou solde dans votre domaine.</h3>";
    exit;
}
?>
<script>
    $(document).ready(function() {
        $("#pro_solde tr").click(function() {
            id = $(this).attr("id");
            // alert(id);
            // modif (id,11,$(this).val(),"statut",1);
            charge('article', id, 'panneau_g')
        });
        $("#articles").tablesorter({
            widgets: ['zebra']
        });
    });
</script>
<h3>Articles en promotion ou solde</h3>
Remarque : seul l'administrateur peut retirer les articles des soldes s'ils y ont été mis par erreur
<table id='pro_solde' class='generique'>
    <tr>
        <th>Titre</th>
        <th>Rayon</th>
    </tr>
    <?php
    $modeV ="test";
    $n = 0;
    while ($rq_prom = $r_prom->fetch_object()) {
        $mode = ($rq_prom->pro_mode == 1) ? 'Promotion' : 'Solde';
        // echo $modeV . " != " . $mode;
        if ($modeV != $mode) {
            echo "<tr style='background-color:white'><th colspan=2>" . $mode . "</th></tr>";
        }
        $coul = ($n % 2 == 0) ? $coulCC : $coulFF;
        echo "<tr id='" . $rq_prom->Vart_id . "' style='background-color:" . $coul . "'><td>" . $rq_prom->Vart_titre . "</td><td>" . $rq_prom->ray_nom . "</td></tr>";
        $n++;
        $modeV = $mode;
    }
    ?>
</table>