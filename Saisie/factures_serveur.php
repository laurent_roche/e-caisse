<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$serveur= filter_input(INPUT_GET, "serveur", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$an = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
if ($an == '') {
    $an = ANNEE;
}

$req_recher="SELECT rsc_id,
                    rsc_date,
                    edi_nom,
                    DATE(jrn_date) AS jrn_date ,
                    jrn_id,
                    jrn_numero,
                    rsc_ttc 
                        FROM Resume_commande_$an 
                        LEFT JOIN Journal_factures_$an ON rsc_id = jrn_facture 
                        JOIN Editeurs ON edi_id = rsc_serveur 
                            WHERE rsc_serveur = $serveur
                            AND rsc_etat = 3
                                ORDER BY jrn_id DESC";
    $r_recher=$idcom->query($req_recher);
    if ($idcom->error) {
        echo "<br>".$idcom->errno." ".$idcom->error."<br>";
    }
    $nb = $r_recher->num_rows;
    $rq_recher=$r_recher->fetch_object();
    $r_recher->data_seek(0);
    ?>
    <script>
$(document).ready(function(){
    $('#factures td').click(function(){
    $('#factures tr').css('font-weight','normal');
    $(this).parent().css('font-weight','bold');
        if ($(this).html().slice(0,8) == '<img src') {
            charge('journal_facture',$(this).parent().attr('id')+'&an=<?php echo $an?>','panneau_d');
        } else if($(this).html().slice(0,8)!='<a href='){
            charge('detail_commande_validee',$(this).parent().attr('id')+'&an=<?php echo $an?>','panneau_d');
        }
    });
});

function comptes(id){

}

</script>
<style>#factures td{
cursor : pointer;
}
</style>
    <h3>Les factures  de <?php echo $rq_recher->edi_nom?></h3>
    <div style = "">
    <table id='factures' class='generique'><TR><TH>N° com</TH><TH>N° Fac</TH><TH>Date réception</TH><TH>Journal</TH><TH>Montant</TH><TH>Date facture</TH></TR>
    <?php
    $n = 0;
    while ($rq_recher=$r_recher->fetch_object()) {
        $coul=($n % 2 == 0)?" class='clair'":" class='fonce'";
        
        if (file_exists($incpath."pdf/commandes/com_".(($an*1000) + $rq_recher->rsc_id).".pdf")) {
            $pdf= '<a href="'.$incpath."pdf/commandes/com_".(($an*1000) + $rq_recher->rsc_id).'.pdf"><img src="../images/pdf.gif" style="float:left"></a> ';
        } else {
            $pdf = '';
        }

        echo "<tr id='".$rq_recher->rsc_id."' style='text-align:right' ".$coul."><td>".$pdf." ".$rq_recher->rsc_id."</td>
            <td>".$rq_recher->jrn_numero."</td>
            <td>".$rq_recher->rsc_date."</td><td>".$rq_recher->jrn_id."</td>
            <td>".monetaireF($rq_recher->rsc_ttc)."</td>
            <td>".dateFR($rq_recher->jrn_date)."</td>            
            <td><img src='/images/button_edit.png'></td></tr>\n";
        $n++;
    }
    ?>
</div></table>
<script>$("#panneau_g").height($("#affichage").height()-10);</script>