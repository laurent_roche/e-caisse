<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
require $incpath."php/config.php";
if (date('Y') == $config['debut']) {
    echo "<h3>Il n'y a pas d'article ancien la première année</h3>";
    exit;
}
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
require "ventes_historique.php";

$an=ANNEE;
$req_=utf8_decode($req);
$req_recher="SELECT art_id, Vt1_nom, art_stk, art_id FROM Articles
                        JOIN Vtit1 ON art_id=Vt1_article
                        JOIN Varticles_utilisateur ON Vart_id=art_id
                        WHERE LEFT(art_creation,4) < $_SESSION[debut]
                        OR art_creation IS NULL
                        AND Vart_utilisateur = $_SESSION[$dossier]
                        AND art_statut = 1
                        ORDER BY art_stk";
$r_recher=$idcom->query($req_recher);
if ($idcom->error) {
    echo "<br>".$idcom->errno." ".$idcom->error."<br>";
}
$nb = $r_recher->num_rows;
?>
<script type="text/javascript" src="/js/jquery.tablesorter.js"></script>
<style>
table.tablesorter tbody td {
    font-size: 12px;
    text-align:left;
    color: #3D3D3D;
    padding: 4px;
    background-color: #f7ffd3;
    vertical-align: top;
}
table.tablesorter tbody tr.odd td {
    text-align:left;
    border-top:solid 1px;
    background-color:#f8f8f4;
}
table.tablesorter thead tr .headerSortDown, table.tablesorter thead tr .headerSortUp {
background-color: #8dbdd8;}
</style>  <script type="text/javascript">
$(document).ready(function(){
    $("table").tablesorter();
    } 
);

</script>
<h3><?php echo $nb?> articles en liste commandé avant <?php echo $_SESSION['debut']?></h3>
<script>
function article(id){
charge('article',id,'panneau_g');
}
</script>
<table  cellpadding="0" id='liste_articles' cellspacing="0" border="1" style='width:90%;margin-left:5%'>
    <thead>
    <TR>
        <TH style="text-align:center">Titre</TH>
        <TH style="text-align:center">Stock</TH>
        <TH style="text-align:center">Vente/ans</TH>
        <TH style="text-align:center"></TH>
      </TR>
      </thead>
      <tbody>
  <?php
$r_recher->data_seek(0);
require "liste_articles.php";

?>
</tbody>
</table>

<script>
$("#panneau_d").height($('#affichage').height()-10);
</script>
