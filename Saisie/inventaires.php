<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
// $req= filter_input( INPUT_GET, "id", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
// if($req == "") $req= filter_input( INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
$req_inventaire = 'SELECT con_inventaire FROM Config';
$r_inventaire = $idcom->query($req_inventaire);
$rq_inventaire = $r_inventaire->fetch_object();
// if($idcom->error) echo $idcom->errno." ".$idcom->error."<br>";

?>
<h3>Les inventaires</h3>
<table width="100%">
    <TR>
        <TH style="cursor:pointer" onclick="charge('inv_theorique',<?php echo $_SESSION[$dossier]?>,'panneau_g')">Théorique</TH>
        <?php
        if (date('n') != $rq_inventaire->con_inventaire) {
            ?>
        <TH>Résultat <?php echo(ANNEE -1)?> <a href="/pdf/inventaires/inv_<?php echo(ANNEE -1)."_".$_SESSION[$dossier]?>.pdf"><img src="/images/pdf.gif"></a></TH>
        <?php
        } else {
        ?>
        <TH onclick="charge('inv_annuel',<?php echo $_SESSION[$dossier]?>,'panneau_g')">Annuel</TH>
        <?php
        }
        ?>
    </TR>
</table>
<script>
// $("#panneau_d").height($('#affichage').height()-10);
$("#panneau_g").height($('#affichage').height()-10);
</script>
