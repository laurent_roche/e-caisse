<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$edi_id= filter_input(INPUT_GET, "edi", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
$tab_req = explode('_', $req);
if ($tab_req[1] == 1) {
    //compte protégé par mot de passe, à faire
    ?>
    <script>alert('La gestion du mot de passe n\'est encore en service')</script>
    <?php
}
echo $req."".$edi_id;
?>
