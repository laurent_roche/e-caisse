<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$sem1= filter_input(INPUT_GET, "sem", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
// echo $dossier;
$an=ANNEE;

connexobjet();
/*
pas défaut, c'est la date du jour
On affiche les données de ce jour et à partir de cette date on construit le combo de selection
La date est au format US pour la requete mysql
*/
$sel1 = '';
$sel = '';

if (!$req) {
    //cacul de la semaine
    /*echo */$req = dateUS(date('U'));
    /*echo */$ladate=date('U');
// echo date("w");
} else {
    $ladate=timestampA($req);
    $an=($an > date('Y', $ladate))?date('Y', $ladate):$an;

}
/*echo "<br>jour de la semaine ".*/$j=date("w", $ladate);
/*echo "<br>debut de semaine = ".*/$deb_sem=dateUS(mktime(0, 0, 0, date('n', $ladate), (date('j', $ladate)-($j-1)), date('Y', $ladate)));
/*echo "<br>".*/$fin_semaine=dateUS(mktime(0, 0, 0, date('n', $ladate), (date('j', $ladate)-$j)+6, date('Y', $ladate)));
 
/*echo */$req=dateUS($ladate);
// echo (date('j',$ladate)-7).'<br>';
/*echo '<br>sem pre '.*/$pre=dateUS($_pre=mktime(0, 0, 0, date('n', $ladate), (date('j', $ladate)-7), date('Y', $ladate)));//semaine précédente
/*echo '<br>sem suiv '.*/$suiv=dateUS($_suiv=mktime(0, 0, 0, date('n', $ladate), (date('j', $ladate)+7), date('Y', $ladate)));

    ?>
<script type="text/javascript">
$(document).ready(function(){
    $('#ventes td').click(function(){
        if ($(this).html().slice(0,6) != '<input') {
            var tr =  $(this).parent().attr('id').split('_');
            $('#ventes td').css('fontWeight','normal');
            $(this).css('fontWeight','bold');
            charge('article',tr[1],'panneau_g');
        }
    });
    $('#ventes input').on('keydown',function(event) {
    $(this).addClass('jaune');
    // alert($(this).parent().parent().attr('id'));
        if(event.which == 13){
            var tr =  $(this).parent().parent().attr('id').split('_');
            // alert($(this).parent().html().slice(0,8));
            if($(this).parent().html().slice(0,6) == '<input') {
                if($(this).hasClass("com") === true){//commande en cours, mise à jour
                    modif($(this).attr('com'),16,$(this).val(),'quantite',1);
                } else {//insertion
                    charge('/Saisie/commandes/insert','&art='+tr[1]+'&qt='+$(this).val(),'mysql');
                    $(this).parent().parent().css('backgroundColor','orange');
                }
            $(this).removeClass('jaune');
            }
        }
    });
});
</script>
    <h3>
    <button style="margin-right:50px;" onclick="charge('ventes','<?php echo $pre?>','panneau_d')">< <?php echo(date("W", $_pre))?></button>&nbsp;Ventes de la semaine&nbsp;<?php echo date("W", $ladate)?>&nbsp;<select onchange="charge('ventes',this.value,'panneau_d')">  
    <option value="<?php echo $deb_sem."&sem=1"?>" <?php echo $sel1?>>Semaine <?php echo date("W", $ladate)?></option>
    <?php
    for ($i=0;$i< 7;$i++) {
        $datejour=dateUS($cejour=mktime(0, 0, 0, date('n', $ladate), (date('j', $ladate)-($j-1)+$i), date('Y', $ladate)));
        if (!$sem1) {
            $sel=($datejour == "$req")?"selected":"";
        }
        echo "<option ".$sel." value='".$datejour."'>".$sem[date("w", $cejour)]." ".date("j", $cejour)." ".$mois[date("n", $cejour)]."</option>\n";
    }
    ?>
    </select>&nbsp;
    <?php
    if (date("W", $ladate) < date("W")) {
        echo "<button style='float:right' onclick=\"charge('ventes','".$suiv."','panneau_d')\">".(date("W", $_suiv))." ></button>";
    }
    ?>

    </h3>
    <style>
    #ventes{
    text-align:right;
    width:90%;
    }
    #ventes td{
    height:25px;
    vertical-align:middle;
    }
    #ventes tbody th{
    padding-left:25px;
    text-align:left;
    }
    </style>
    <center><table id="ventes">
    <thead>
    <TR style="text-align:center;"><TH>Titre</TH><TH>Quantité</TH><TH>Stock</TH><TH>Com.</TH></TR>
    </thead>
    <tbody>
    <?php
if ($sem1 == 1) {
        $comp = "DATE(rst_validation)>='".$req."' AND DATE(rst_validation) <= '".$fin_semaine."'";
        $sel1=" selected";
    } else {
        $comp = "DATE(rst_validation)='".$req."'";
    }
//   exit;
//table temporaire des articles en cours de commande
    $tempo = "CREATE VIEW Com AS SELECT com_article,
                            rsc_etat,
                            com_id,
                            com_quantite
                                FROM Commandes_$an 
                                JOIN Resume_commande_$an ON com_numero = rsc_id 
                                    WHERE rsc_etat = 2 OR rsc_etat = 1 AND com_article != 1";
$idcom->query($tempo);
    $req_vente="SELECT tic_article, 
                DATE(rst_validation), 
                SUM(tic_quantite) AS tic_quantite, 
                Vt1_nom, 
                Vart_stk, 
                Vart_unite, 
                ray_nom , 
                rsc_etat, 
                com_quantite,
                com_id
                    FROM Tickets_$an 
                        JOIN Resume_ticket_$an ON rst_id = tic_num 
                        JOIN Vtit1 ON Vt1_article=tic_article 
                        JOIN Varticle_editeur ON Vart_id=tic_article  
                        JOIN Rayons ON ray_id=Vart_rayon
                        LEFT JOIN Com ON com_article = tic_article
                            WHERE $comp AND Vart_utilisateur=$_SESSION[$dossier] 
                                GROUP BY tic_article 
                                    ORDER BY ray_nom";
    $r_vente=$idcom->query($req_vente);
    $rq_vente=$r_vente->fetch_object();
    // print_r($rq_vente);
    $n=0;
    $rayon = "";
    $r_vente->data_seek(0);
    while ($rq_vente=$r_vente->fetch_object()) {
        // echo $rq_vente->rsc_id."<br>";
        $coul=($n % 2 == 0)?$coulCC:$coulFF;
        
        if ($rq_vente->Vart_unite == 1) {
            $com_quantite = sprintf('%d', $rq_vente->com_quantite);
            $tic_quantite=sprintf('%d', $rq_vente->tic_quantite);
            $Vart_stk=sprintf('%d', $rq_vente->Vart_stk);
        } else {
            $com_quantite = $rq_vente->com_quantite;
            $tic_quantite=$rq_vente->tic_quantite;
            $Vart_stk=$rq_vente->Vart_stk;
        }

        switch ($rq_vente->rsc_etat) {
        case 1: $coul= "orange";$cellule="<input type='text' class='pt com' com='".$rq_vente->com_id."' value='".$com_quantite."'>";
            
            break;
        case 2: $coul= "red";$cellule=$com_quantite;
            break;
        default:$cellule="<input type='text' value='' class='pt'>";
            break;
        }
            //   echo $cellule;  
        if ($rayon != $rq_vente->ray_nom) {
            echo "<tr><th colspan='4'>".$rq_vente->ray_nom."</th>";
        }
        echo "<tr id='A_".$rq_vente->tic_article."' style='background-color:".$coul."'>";
        echo "<td style='cursor:pointer'>".stripslashes($rq_vente->Vt1_nom)."</td>
    <td>".$tic_quantite."</td>
    <td>".$Vart_stk."</td>";
        
        echo "<td id='C".$rq_vente->tic_article."' style='background-color:".$coul."'>".$cellule."</td></tr>\n";
        $n++;
        $rayon = $rq_vente->ray_nom;
    }
    $idcom->query("DROP VIEW Com");
    ?></tbody></table></center>
<script>
$("#panneau_d").height($("#affichage").height()-10);
</script>
