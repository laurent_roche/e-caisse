<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p,$i);
}
// print_r($_SESSION);
require_once '../fpdf181/fpdf.php';
//*************attention ecodage cp-1252******************************************
class PDF_TicketsFinJournee extends FPDF
{
    // Propri�t�s priv�es
    public $Fonte_Taille = 7;
    public $Fonte_Nom = "Arial";

    public $Ligne_Hauteur = 15;
    // public $_Line_Height = 10;

    public $nbColonnes;
    public $interColonne;
    public $nbLignes;
    public $Colonne_Largeur;
    public $ligne=0;
    public $colonne=0;
    public $N_Page=0;
    public $nbPages=0;
    public $Titre_Page=false;
    public $total_ligne;
    // Give the height for a char size given.
    public function _Get_Height_Chars($pt)
    {
        // Tableau de concordance entre la hauteur des caract�res et de l'espacement entre les lignes
        $pt=7;
        return $pt*.52;
    }

    public function __construct($nbColonnes, $margeG, $margeH, $interColonne)
    {
        parent::__construct();
//    $this->Set_Font_Name($this->Fonte_Nom);
//    $this->SetMargins($this->_Marge_Gauche,$this->_Marge_Haut);
        $this->SetAutoPageBreak(false);
        $this->Set_Font_Size($this->Fonte_Taille);
        $this->nbColonnes=$nbColonnes;
        $this->lMargin=$margeG;
        $this->tMargin=$margeH;
        $this->Colonne_Largeur=($this->w - $this->lMargin - $this->rMargin -($nbColonnes-1)*$interColonne) / $this->nbColonnes;
        $this->nbLignes=intval(($this->h - $this->tMargin- $this->bMargin) / 5)+18;
        $this->interColonne=$interColonne;
        //     $this->Open();
        $this->initPage(true);
        // print_r($this);
    }

    // M�thode qui permet de modifier la taille des caract�res
    // Cela modifiera aussi l'espace entre chaque ligne
    public function Set_Font_Size($pt)
    {
        if ($pt > 3) {
            $this->Fonte_Taille = $pt;
            $this->Ligne_Hauteur = $this->_Get_Height_Chars($pt);
            $this->SetFontSize($this->Fonte_Taille);
        }
    }

    // Method to change font name
    public function Set_Font_Name($fontname)
    {
        if ($fontname != '') {
            $this->Fonte_Nom = $fontname;
            $this->SetFont($this->Fonte_Nom);
        }
    }

    public function initColonne($nCol)
    {
        $this->colonne=$nCol;
        $this->ligne=0;
        if (($this->Titre_Page)&&($nCol>1)) {
            $this->ligne++;
        }
    }

    public function initPage($titre=false)
    {
        //init des variables de lignes et colonnes
        $this->AddPage();

        $this->ligne=0;
        $this->initColonne(1);
        $this->N_Page+=1;

        if ($titre) {
            // affichage de la ligne de titre si besoin
            $this->SetFillColor(0, 255, 255);
            $this->SetFont('Arial', 'B', 10);

            $this->SetX($this->lMargin);
            $this->SetY(($this->ligne)*$this->Ligne_Hauteur+$this->tMargin);
            $this->Cell($this->w-$this->lMargin-$this->rMargin-27, $this->Ligne_Hauteur, 'INVENTAIRE au 31 d�cembre '.date("Y"), 1, 0, 'C', 1);
            $this->SetFont('Arial', 'I', 8);
            $this->Cell(27, 5, ' page '.$this->N_Page, 1, 0, 'C', 1);
            $this->ligne+=1;
            $this->Titre_Page=true;
        }
    }

    public function addLigne($type, $valeurs)
    {
        if ($type=="titreTicket") {
            if (($this->ligne==$this->nbLignes-1)||($this->ligne==$this->nbLignes-2)) {
                $this->initPage(true);
            }
            $this->SetFillColor(100, 255, 100);
            $this->setY(($this->ligne)*$this->Ligne_Hauteur+$this->tMargin);
            $this->setX(($this->colonne-1)*$this->Colonne_Largeur+$this->lMargin+($this->interColonne*($this->colonne-1)));
            $this->cell($this->Colonne_Largeur, $this->Ligne_Hauteur, $valeurs, 1, 0, 'C', 1);
        } elseif ($type=="article") {
            //affichage d'une ligne article
            $this->SetFillColor(255, 255, 255);
            $largeurRef=25;
            $largeurTitre=70;
            $largeurPrix=15;
            $largeurTotal=17;
            $largeurQte=15;
            $largeurTva=7;
            if ($valeurs['unite'] == 1) {
                $qt = sprintf("%d", $valeurs['qt']);
            }
            $this->setY(($this->ligne)*$this->Ligne_Hauteur+$this->tMargin);
            $this->setX(($this->colonne-1)*$this->Colonne_Largeur+$this->lMargin+($this->interColonne*($this->colonne-1)));
            $this->cell($this->Colonne_Largeur-$largeurQte-$largeurTva-$largeurPrix-$largeurTotal-$largeurTitre-$largeurRef, $this->Ligne_Hauteur, $valeurs['nom'], 1, 0, 'L', 1);
            $this->cell($largeurRef, $this->Ligne_Hauteur, $valeurs['cb'], 1, 0, 'C', 1);
            $this->cell($largeurTitre, $this->Ligne_Hauteur, $valeurs['titre'], 1, 0, 'L', 1);
            $this->cell($largeurPrix, $this->Ligne_Hauteur, sprintf("%01.2f", $valeurs['pht'])." �", 1, 0, 'R', 1);
            // $this->cell($largeurPrix, $this->Ligne_Hauteur,  $valeurs['pht']." �", 1, 0, 'R', 1);
            $this->cell($largeurQte, $this->Ligne_Hauteur, $qt, 1, 0, 'R', 1);
            $this->cell($largeurTotal, $this->Ligne_Hauteur, sprintf("%01.2f", $valeurs['tt'])." �", 1, 0, 'R', 1);
            // $this->cell($largeurTotal, $this->Ligne_Hauteur, $valeurs['tt']." �", 1, 0, 'R', 1);
            $this->cell($largeurTva, $this->Ligne_Hauteur, $valeurs['tva'], 1, 0, 'R', 1);
            $this->total_ligne--;
        } elseif ($type=="libelle") {
            //affichage des titres colonnes
            $this->SetFillColor(220, 220, 220);
            $largeurRef=25;
            $largeurTitre=70;
            $largeurPrix=15;
            $largeurTotal=17;
            $largeurQte=15;
            $largeurTva=7;
            $this->setY(($this->ligne)*$this->Ligne_Hauteur+$this->tMargin);
            $this->setX(($this->colonne-1)*$this->Colonne_Largeur+$this->lMargin+($this->interColonne*($this->colonne-1)));
            $this->cell($this->Colonne_Largeur-$largeurQte-$largeurTva-$largeurPrix-$largeurTotal-$largeurTitre-$largeurRef, $this->Ligne_Hauteur, "�diteur", 1, 0, 'C', 1);
            $this->cell($largeurRef, $this->Ligne_Hauteur, "R�f�rence/CB", 1, 0, 'C', 1);
            $this->cell($largeurTitre, $this->Ligne_Hauteur, "D�signation", 1, 0, 'C', 1);
            $this->cell($largeurPrix, $this->Ligne_Hauteur, "Prix HT", 1, 0, 'C', 1);
            $this->cell($largeurQte, $this->Ligne_Hauteur, "Qt.", 1, 0, 'C', 1);
            $this->cell($largeurTotal, $this->Ligne_Hauteur, "Total HT", 1, 0, 'C', 1);
            $this->cell($largeurTva, $this->Ligne_Hauteur, "TVA", 1, 0, 'C', 1);
        } elseif ($type=="stotal") {
            //*****************************affichage des stotaux par tva********************************
            $this->SetFillColor(200, 200, 200);
            $this->setY(($this->ligne)*$this->Ligne_Hauteur+$this->tMargin);
            $this->setX(($this->colonne-1)*$this->Colonne_Largeur+$this->lMargin+($this->interColonne*($this->colonne-1)));
            $taille_stt = 17+15+7;//$largeurTotal+$largeurQte+$largeurTva
            $taille_cellule = ($this->Colonne_Largeur - $taille_stt)/(COUNT($valeurs['afftva']));
            $stt = 0.00;
            $n = 0;
            foreach ($valeurs['afftva'] as $key => $value) {//ligne titre tva+$largeurQte+$largeurTva
        $this->Cell($taille_cellule, 4, $key.' ('.$valeurs['n_tva'][$n].')', 'LR', 0, 'C');//
        $n++;
            }
            $this->Cell($taille_stt, 4, 'Sous Total', 'R', 1, 'C');
            $this->ligne++;
            $this->total_ligne--;
        
            $this->setY(($this->ligne)*$this->Ligne_Hauteur+$this->tMargin);
            foreach ($valeurs['afftva'] as $key => $value) {//ligne valeur tva
                $this->Cell($taille_cellule, 4, monetaireF($value).'  �', 1, 0, 'C');
                $stt += $value;
            }
            $this->Cell($taille_stt, 4, monetaireF($stt).'  �', 'BR', 1, 'C');
            $this->ligne++;
            $this->total_ligne--;
        //*****************************fin affichage  des stotaux par tva********************************
        } elseif ($type=="TOTAL") {
            //*****************************affichage des totaux par tva********************************
            $this->SetFillColor(200, 200, 200);
            $this->setY(($this->ligne)*$this->Ligne_Hauteur+$this->tMargin);
            $this->setX(($this->colonne-1)*$this->Colonne_Largeur+$this->lMargin+($this->interColonne*($this->colonne-1)));
            $taille_stt = 17+15+7;//$largeurTotal+$largeurQte+$largeurTva
            $taille_cellule = ($this->Colonne_Largeur - $taille_stt)/(COUNT($valeurs['TTtva']));
            $tt = 0.00;
            $n = 0;
            foreach ($valeurs['TTtva'] as $key => $value) {//ligne titre tva
                $this->Cell($taille_cellule, 4, $key.' ('.$valeurs['n_tva'][$n].')', 'LR', 0, 'C');
                $n++;
            }
            $this->Cell($taille_stt, 4, 'Total', 'R', 1, 'C');
            $this->ligne++;
            $this->total_ligne--;
        
            $this->setY(($this->ligne)*$this->Ligne_Hauteur+$this->tMargin);
            foreach ($valeurs['TTtva'] as $key => $value) {//ligne valeur tva
                $this->Cell($taille_cellule, 4, monetaireF($value).'  �', 1, 0, 'C');
                $tt += $value;
            }
            $this->Cell($taille_stt, 4, monetaireF($tt).'  �', 'BR', 1, 'C');
            $this->ligne++;
            $this->total_ligne--;
        //*****************************fin affichage  des totaux par tva********************************
        } elseif ($type=="blanche") {
            //affichage d'une ligne blanche
            $this->SetFillColor(255, 255, 255);
            $this->setY(($this->ligne)*$this->Ligne_Hauteur+$this->tMargin);
            $this->setX(($this->colonne-1)*$this->Colonne_Largeur+$this->lMargin+($this->interColonne*($this->colonne-1)));
            $this->cell($this->Colonne_Largeur, $this->Ligne_Hauteur, " ", 0, 0, 'L', 1);
        } elseif ($type=="commentaire") {
            //affichage d'une ligne blanche
            $this->SetFillColor(255, 255, 255);
            $this->setY(($this->ligne)*$this->Ligne_Hauteur+$this->tMargin);
            $this->setX(($this->colonne-1)*$this->Colonne_Largeur+$this->lMargin+($this->interColonne*($this->colonne-1)));
            $this->cell($this->Colonne_Largeur, $this->Ligne_Hauteur, $valeurs['note'], 0, 0, 'L', 1);
        }

        //on passe  la ligne
        $this->ligne+=1;

        // maintenant on test si on doit changer de colonne ou de page
        if ($this->ligne>=$this->nbLignes) {
            // on change de colonne
            $this->initColonne($this->colonne+1);


            if ($this->colonne>$this->nbColonnes) {
                //on change de page
        $this->initPage(true); // ou initPage($titrePage); si tu peux le rafficher
            }
        }
    }
}
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
//supression des articles de l'utilisateur entr�s dans Stock_ANNEE
$idcom->query("DELETE  Stock_".ANNEE.".* FROM Stock_".ANNEE." JOIN Articles ON art_id = stk_article JOIN Inventaire ON inv_cb = art_cb WHERE inv_utilisateur = ".$_SESSION[$dossier]);
// echo $idcom->errno." ".$idcom->error;
//insertion des articles dans Stock_ANNEE
$idcom->query("INSERT INTO Stock_".ANNEE." (stk_article,stk_stk,stk_pht) SELECT art_id, art_stk, art_pht FROM Articles JOIN Inventaire ON inv_cb = art_cb WHERE art_stk > 0 AND inv_utilisateur =".$_SESSION[$dossier]);
// echo "<br>".$idcom->errno." ".$idcom->error;
//mise � jour avec les donn�es en compte
// $idcom->query("CREATE TABLE Art_encomtpe TEMPORARY AS SELECT tic_article AS arc_article , SUM(tic_quantite) AS arc_quantite FROM Tickets_".ANNEE." JOIN Articles ON tic_article = art_id JOIN Inventaire ON  inv_cb = art_cb WHERE LENGTH(tic_num)=10 GROUP BY tic_article");
// $idcom->query("UPDATE Stock_".ANNEE." JOIN Art_encomtpe SET stk_stk = (stk_stk - arc_quantite) WHERE stk_article = arc_article");

// $an = ANNEE;
$pdfTickets=new PDF_TicketsFinJournee(1, 10, 10, 5);
$i=0;
//recherche des tvas utilis�es
$req_tva = "SELECT art_tva, tva_nom,tva_id FROM Tva JOIN Articles ON tva_id = art_tva GROUP BY tva_id ORDER BY tva_ordre";
$r_tva=$idcom->query($req_tva);
$afftva=array();
$n_tva = array();
while ($rq_tva=$r_tva->fetch_object()) {
    array_push($afftva, $rq_tva->tva_nom);
    array_push($n_tva, $rq_tva->tva_id);
}
$afftva = array_fill_keys($afftva, '0.00');
$afftvaVide = $afftva;
$TTtva = $afftva;

//impression par secteur
$req_serveur="SELECT sec_id, sec_nom, sec_abrege FROM Secteurs";
$r_serveur=$idcom->query($req_serveur);
while ($rq_serveur=$r_serveur->fetch_object()) {
    $req_sel="SELECT Vt1_nom,
                    art_cb, 
                    Vt3_nom, 
                    edi_nom, 
                    stk_pht, 
                    art_tva,
                    stk_stk,
                    tva_nom,
                    art_unite
                        FROM Stock_".ANNEE." 
                        JOIN Articles ON art_id = stk_article 
                        JOIN Vtit1 ON Vt1_article = art_id 
                        LEFT JOIN Vtit3 ON Vt3_article = art_id 
                        JOIN Editeurs ON edi_id = art_editeur 
                        JOIN Editeur_serveur ON eds_editeur = art_editeur 
                        JOIN Tva ON tva_id = art_tva
                        JOIN Rayons ON ray_id = art_rayon
                        JOIN Secteurs ON ray_secteur = sec_id
                            WHERE sec_id = ".$rq_serveur->sec_id."
                            AND edi_utilisateur = $_SESSION[$dossier] 
                                AND stk_stk!=0 
                                    ORDER BY edi_nom, Vt1_nom";
    $r_sel=$idcom->query($req_sel);
    $nom_edi ='';
    if ($r_sel->num_rows!=0) {
        $pdfTickets->addLigne("blanche", "");
        $pdfTickets->addLigne("titreTicket", $rq_serveur->sec_abrege." - ".utf8_decode($rq_serveur->sec_nom));
        $pdfTickets->addLigne("libelle", "");
        $afftva = $afftvaVide;
        while ($rq_sel=$r_sel->fetch_object()) {
            if ($nom_edi!=$rq_sel->edi_nom) {
                $n=1;
                if ($i != 0) {//insertion des sous-totaux editeur sauf en d�but de permi�re page
                    $pdfTickets->addLigne("stotal", array('afftva'=>$TTediteur,'n_tva'=>$n_tva));
                    $TTediteur = $afftvaVide;
                }
                $i= 1;
            }
                    
            if ($rq_sel->Vt3_nom!="") {
                $ref=$rq_sel->Vt3_nom;
            } elseif (substr($rq_sel->art_cb, 0, 3)==200) {
                $ref="";
            } else {
                $ref=$rq_sel->art_cb;
            }
            $pdfTickets->addLigne("article", array("nom"=>utf8_decode($rq_sel->edi_nom),"titre"=>stripslashes(utf8_decode($rq_sel->Vt1_nom)),"cb"=>$ref,"pht"=>$rq_sel->stk_pht,"qt"=>$rq_sel->stk_stk,"tt"=>(($rq_sel->stk_stk)*($rq_sel->stk_pht)),"tva"=>$rq_sel->art_tva,"unite"=>$rq_sel->art_unite));
            @$afftva[$rq_sel->tva_nom] += ($rq_sel->stk_pht * $rq_sel->stk_stk);
            @$TTtva[$rq_sel->tva_nom] += ($rq_sel->stk_pht * $rq_sel->stk_stk);
            @$TTediteur[$rq_sel->tva_nom] += ($rq_sel->stk_pht * $rq_sel->stk_stk);
            $nom_edi=$rq_sel->edi_nom;
            $n++;
        }
        $pdfTickets->addLigne("stotal", array('afftva'=>$afftva,'n_tva'=>$n_tva));
    }
}
$pdfTickets->addLigne("blanche", "");
$pdfTickets->addLigne("titreTicket", "RESULTAT");
$pdfTickets->addLigne("TOTAL", array('TTtva'=>$TTtva,'n_tva'=>$n_tva));
$pdfTickets->addLigne("commentaire", array("note"=>"NOTE : Les articles en moyens pond�r�s regroupant des commandes de plusieurs �diteurs, apparaissent sous l'�diteur ou ils sont r�f�renc�s."));

//***********inventaire depot*************************************
$afftva = $afftvaVide;
$TTtva = $afftva;
$pdfTickets->addLigne("blanche", "");
$pdfTickets->addLigne("titreTicket", "Articles en d�pot");
$req_serveur="SELECT sec_id, sec_nom, sec_abrege FROM Secteurs";
$r_serveur->data_seek(0);
$n = 0;
while ($rq_serveur=$r_serveur->fetch_object()) {
    $req_sel="SELECT Vt1_nom,
                    art_cb, 
                    Vt3_nom, 
                    edi_nom, 
                    stk_pht, 
                    art_tva,
                    stk_stk,
                    tva_nom,
                    art_unite
                        FROM Stock_".ANNEE." 
                        JOIN Articles ON art_id = stk_article 
                        JOIN Vtit1 ON Vt1_article = art_id 
                        LEFT JOIN Vtit3 ON Vt3_article = art_id 
                        JOIN Editeurs ON edi_id = art_editeur 
                        JOIN Editeur_serveur ON eds_editeur = art_editeur 
                        JOIN Tva ON tva_id = art_tva
                        JOIN Rayons ON ray_id = art_rayon
                        JOIN Secteurs ON ray_secteur = sec_id
                        JOIN Depot ON dep_article = art_id
                            WHERE sec_id = ".$rq_serveur->sec_id."
                            AND edi_utilisateur = $_SESSION[$dossier] 
                                AND stk_stk!=0 
                                AND dep_article IS NOT NULL
                                    ORDER BY edi_nom, Vt1_nom";
    $r_sel=$idcom->query($req_sel);
    $nom_edi ='';
    if ($r_sel->num_rows!=0) {
        $pdfTickets->addLigne("blanche", "");
        $pdfTickets->addLigne("titreTicket", $rq_serveur->sec_abrege." - ".utf8_decode($rq_serveur->sec_nom));
        $pdfTickets->addLigne("libelle", "");
        $afftva = $afftvaVide;
        while ($rq_sel=$r_sel->fetch_object()) {
            if ($nom_edi!=$rq_sel->edi_nom) {
                $n=1;
            }
                    
            if ($rq_sel->Vt3_nom!="") {
                $ref=$rq_sel->Vt3_nom;
            } elseif (substr($rq_sel->art_cb, 0, 3)==200) {
                $ref="";
            } else {
                $ref=$rq_sel->art_cb;
            }
            $pdfTickets->addLigne("article", array("nom"=>"[".$n."] ".utf8_decode($rq_sel->edi_nom),"titre"=>utf8_decode($rq_sel->Vt1_nom),"cb"=>$ref,"pht"=>$rq_sel->stk_pht,"qt"=>$rq_sel->stk_stk,"tt"=>(($rq_sel->stk_stk)*($rq_sel->stk_pht)),"tva"=>$rq_sel->art_tva,"unite"=>$rq_sel->art_unite));
            @$afftva[$rq_sel->tva_nom] += ($rq_sel->stk_pht * $rq_sel->stk_stk);
            @$TTtva[$rq_sel->tva_nom] += ($rq_sel->stk_pht * $rq_sel->stk_stk);
            
            $nom_edi=$rq_sel->edi_nom;
            $n++;
        }
        $pdfTickets->addLigne("stotal", array('afftva'=>$afftva,'n_tva'=>$n_tva));
    }
}
    if ($n == 0) {
        $pdfTickets->addLigne("commentaire", array("note"=>"Pas d'articles en d�pot"));
    } else {
        $pdfTickets->addLigne("titreTicket", "RESULTAT");
        $pdfTickets->addLigne("TOTAL", array('TTtva'=>$TTtva,'n_tva'=>$n_tva));
    }

//***********commandes non reglees*************************************
$pdfTickets->addLigne("blanche", "");
$pdfTickets->addLigne("titreTicket", "VALEUR HT estim�e des commandes non regl�es");

$req_commandes="SELECT rsc_id,
                        jrn_facture, 
                        SUM(com_quantite* com_pht) AS valeur, 
                        com_tva,
                        tva_nom
                            FROM Resume_commande_".ANNEE." 
                            LEFT JOIN Journal_factures_".ANNEE." ON rsc_id=jrn_facture 
                            JOIN Commandes_".ANNEE." ON com_numero = rsc_id 
                            JOIN Tva ON tva_id = com_tva 
                                WHERE rsc_etat=3 
                                AND rsc_util= $_SESSION[$dossier]
                                AND jrn_facture IS NULL 
                                    GROUP BY com_tva";
$r_commandes=$idcom->query($req_commandes);
if ($r_commandes->num_rows == 0) {
    $pdfTickets->addLigne("commentaire", array("note"=>"Pas de commande en attente"));
} else {
    while ($rq_commandes = $r_commandes->fetch_object()) {
        $afftvaVide[$rq_commandes->tva_nom] += $rq_commandes->valeur;
    }
    $pdfTickets->addLigne("stotal", array('afftva'=>$afftvaVide,'n_tva'=>$n_tva));
}
    
//***********article non vendu depuis 1 an *************************************
$afftva = $afftvaVide;
$pdfTickets->addLigne("blanche", "");
$pdfTickets->addLigne("titreTicket", "Articles non vendus depuis 1 an");
$pdfTickets->addLigne("libelle", "");
$req_ancien = "SELECT art_cb AS ref, 
                        Vt1_nom , 
                        stk_stk,
                        stk_pht, 
                        art_tva,  
                        edi_nom,
                        tva_nom,
                        art_unite
                            FROM Articles  
                            LEFT JOIN Tickets_".ANNEE." ON art_id = tic_article 
                            JOIN Stock_".(ANNEE-1)." ON stk_article=art_id 
                            LEFT JOIN Commandes_".ANNEE." ON com_article=art_id 
                            JOIN Editeurs ON edi_id = art_editeur 
                            JOIN Vtit1 ON Vt1_article = art_id 
                            JOIN Tva ON tva_id = art_tva
                                WHERE tic_quantite IS NULL 
                                AND com_numero IS NULL 
                                AND edi_utilisateur=$_SESSION[$dossier] 
                                AND stk_stk>'0' 
                                AND tic_quantite IS NULL 
                                                        ORDER BY art_editeur";
$r_ancien=$idcom->query($req_ancien);
while ($rq_ancien = $r_ancien->fetch_object()) {
    if ($nom_edi!=$rq_ancien->edi_nom) {
        $n=1;
    }
    $pdfTickets->addLigne("article", array("nom"=>"[".$n."] ".utf8_decode($rq_ancien->edi_nom),"titre"=>utf8_decode($rq_ancien->Vt1_nom),"cb"=>$rq_ancien->ref,"pht"=>$rq_ancien->stk_pht,"qt"=>$rq_ancien->stk_stk,"tt"=>(($rq_ancien->stk_stk)*($rq_ancien->stk_pht)),"tva"=>$rq_ancien->art_tva,"unite"=>$rq_ancien->art_unite));
    $nom_edi=$rq_ancien->edi_nom;
    @$afftva[$rq_ancien->tva_nom] += ($rq_ancien->stk_pht * $rq_ancien->stk_stk);
    @$TTtva[$rq_ancien->tva_nom] += ($rq_ancien->stk_pht * $rq_ancien->stk_stk);
    $n++;
}
$pdfTickets->addLigne("TOTAL", array('TTtva'=>$TTtva,'n_tva'=>$n_tva));

$chemin = $incpath."pdf/inventaires/inv_".ANNEE."_".$_SESSION[$dossier].".pdf";
$pdfTickets->Output('F', $chemin);//
// exit;
?>
<script>
location.href='<?php echo $chemin?>';
</script>

