<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
$req_exclus = "SELECT art_id, 
                    Vt1_nom, 
                    edi_nom,
                    ray_nom,
                    art_stk
                        FROM Articles 
                        JOIN Vtit1 ON vt1_article = art_id 
                        LEFT JOIN Rayons ON ray_id = art_rayon
                        LEFT JOIN Editeurs ON edi_id = art_editeur
                            WHERE art_statut = $req
                            AND(edi_utilisateur IS NULL OR edi_utilisateur = $_SESSION[$dossier])";
$r_exclus = $idcom->query($req_exclus);
if ($r_exclus->num_rows == 0) {
    echo "Il n'y a pas d'article dans cette catégorie";
    exit;
}
$n = 0;
?>
<script>
$(document).ready(function() {
    $('table#exclus tbody tr').css('cursor','pointer');
    $('table#exclus tbody td').click(function(){
        charge('article',$(this).parent().attr('id'),'panneau_g');
        });
});
$('#panneau_d').height($('#affichage').height()-10)
</script>

<table id='exclus' class="generique">
<thead><TR><TH>Titre</TH><TH>Editeur</TH><TH>Rayon</TH><TH>Stock</TH></TR></thead>
<tbody>
<?php
while ($rq_exclus =$r_exclus->fetch_object()) {
    $coul=($n % 2 == 0)?$coulCC:$coulFF;
    echo "<tr id='".$rq_exclus->art_id."' style='background-color:".$coul."'><td>".$rq_exclus->Vt1_nom."</td><td>".$rq_exclus->edi_nom."</td><td>".$rq_exclus->ray_nom."</td><td>".$rq_exclus->art_stk."</td></tr>";
    $n++;
}
?>
</tbody>
</table>