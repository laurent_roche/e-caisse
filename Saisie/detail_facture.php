<?php
if (!isset($incpath)) {
    session_start();
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
    include $incpath."mysql/connect.php";
    include $incpath."php/fonctions.php";
    connexobjet();
}
$plus = filter_input(INPUT_GET, "plus", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$id = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$an = filter_input(INPUT_GET, "an", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
//recherche des details déjà entrés
$req_facture = "SELECT * FROM Detail_facture_$an WHERE def_facture = $id";
?>
<script>
$(document).ready(function() {
    $('#export input').on('keydown',function(event){
    id=$(this).parent().parent().attr('id');
    chp = $(this).attr('alt');
    $(this).addClass('jaune');
        if (event.which == 13) {
            if($(this).attr('type') == 'text') {
            var ttverif = 0.00;
                //verification du total
                $('.tt').each(function(){
                    ttverif += +$(this).val();                    
                })
                resul = ttverif.toFixed(2);
                if (resul != $('#valeurF').val()) {
                    $('#ctrl').html("erreur sur le total : "+resul);
                    $('#valeurF').css('backgroundColor','orange');
                } else {
                    $('#ctrl').html('');
                    $('#valeurF').css('backgroundColor','');
                }
                modif(id,31,$(this).val(),chp,'1&an=<?php echo $an?>');
                $(this).removeClass('jaune');
            }
        }
    });
});
</script>
<style>#export input {
    border : dotted 1px;
    width : 100px;
}</style>
<br>
<table><tr><th>Tva</th><th>Code entrée</th><th>Valeurs articles</th><th>Valeur tva</th><th>Poste analytique</th><th></th></tr>
<?php
$req_tva="SELECT * FROM Tva WHERE tva_etat = 1 ORDER BY tva_ordre";
$r_tva=$idcom->query($req_tva);

$req_entree = "SELECT * FROM Vcodes_entree";
$r_entree = $idcom->query($req_entree);
$rq_entree =$r_entree->fetch_object();


$req_code = 'SELECT * FROM Vanalytique';
$r_code = $idcom->query($req_code);
$ttart = 0.00;
$tttva = 0.00;
$r_facture = $idcom->query($req_facture);
$r_facture->num_rows;
if ($r_facture->num_rows > 0) {
    while ($rq_facture =$r_facture->fetch_object()) {
        echo "<tr id=".$rq_facture->def_id."><td><select onchange=\"modif(".$rq_facture->def_id.", 31, $(this).val(),'tva','1&an=".$an."')\">";
        $r_tva->data_seek(0);
        while ($rq_tva=$r_tva->fetch_object()) {
            $sel=($rq_tva->tva_id == $rq_facture->def_tva)?" selected":"";
            echo "<option value='".$rq_tva->tva_id."'".$sel.">".$rq_tva->tva_nom."</option>";
        }
        echo "</select></td><td><select onchange=\"modif(".$rq_facture->def_id.", 31, $(this).val(),'codeent','1&an=".$an."')\">
        <option value='0'></option>";
        $r_entree->data_seek(0);
        while ($rq_entree=$r_entree->fetch_object()) {
            $sel=($rq_entree->Vce_id == $rq_facture->def_codeent)?" selected":"";
            echo "<option value='".$rq_entree->Vce_id."'".$sel.">".$rq_entree->Vce_description."</option>";
        }
        echo "</select></td>
        <td><input type='text' class='tt' alt='artvaleur' value='".$rq_facture->def_artvaleur."'></td>
        <td><input type='text' class='tt' alt='tvavaleur' value='".$rq_facture->def_tvavaleur."'></td>
        <td><select onchange=\"modif(".$rq_facture->def_id.", 31, $(this).val(),'codeana','1&an=".$an."')\"><option value='0'></option>";
        $r_code->data_seek(0);
        while ($rq_code =$r_code->fetch_object()) {
            $sel=($rq_code->Vana_id == $rq_facture->def_codeana)?" selected":"";
            echo "<option value='".$rq_code->Vana_id."'".$sel.">".$rq_code->Vana_nom."</option>";
        }
        echo "</select></td>
        <td><button onclick=\"modif(".$rq_facture->def_id.", 31, '','id',2);$('#panneau_d').empty()\"><img src='/images/supprimer.png'></button></td></tr>";
        $ttart += $rq_facture->def_artvaleur;
        $tttva += $rq_facture->def_tvavaleur;
    }//                            id,tb,vl,cp,my
} else {
    //recherche des tva utilisé dans la commande
    $req_com = "SELECT SUM(com_pht * com_quantite) AS tht,
                        ROUND(SUM(com_pht* com_quantite)*(1+(tva_nom/100)),2) AS ttc,
                        com_tva,
                        ray_ana,
                        ray_ent
                            FROM Commandes_$an
                            JOIN Articles ON art_id = com_article
                            JOIN Rayons ON ray_id = art_rayon
                            JOIN Tva ON tva_id = com_tva
                            JOIN Vanalytique ON Vana_id = ray_ana
                                WHERE com_numero = $id
                                    GROUP BY ray_ent";
    $r_com = $idcom->query($req_com);
    while ($rq_com =$r_com->fetch_object()) {
        $req = "INSERT INTO Detail_facture_$an (def_tva, def_facture, def_artvaleur, def_tvavaleur,def_codeana, def_codeent) 
                        VALUE($rq_com->com_tva, $id, $rq_com->tht, ($rq_com->ttc - $rq_com->tht),$rq_com->ray_ana,$rq_com->ray_ent)";
        $res = $idcom->query($req);
        if (!$res) {
            echo "<b>Erreur n° ".$idcom->errno."</b><br> <em>".$idcom->error."</em>";
            break;
        }
    } ?>
    <script>charge('detail_facture','<?php echo $id."&an=".$an?>','export')</script>
    <?php
}

if ($plus == 1) {
    //insertion d'une ligne
    $req_insert = "INSERT INTO Detail_facture_$an (def_facture, def_codeana) VALUE($id, 0)";
    $idcom->query($req_insert);
    $n_id = $idcom->insert_id;
    echo "<tr id=".$n_id."><td><select onchange=\"modif(".$n_id.", 31, $(this).val(),'tva','1&an=".$an."')\">";
    $r_tva->data_seek(0);
    while ($rq_tva=$r_tva->fetch_object()) {
        echo "<option value='".$rq_tva->tva_id."'>".$rq_tva->tva_nom."</option>";
    }
    echo "</select></td><td><select onchange=\"modif(".$n_id.", 31, $(this).val(),'codeent','1&an=".$an."')\">
    <option value='0'></option>";
    $r_entree->data_seek(0);
    while ($rq_entree=$r_entree->fetch_object()) {
        $sel=($rq_entree->Vce_id == $rq_facture->def_codeent)?" selected":"";
        echo "<option value='".$rq_entree->Vce_id."'".$sel.">".$rq_entree->Vce_description."</option>";
    }
    echo "</select></td>
    <td><input type='text' class='tt' alt='artvaleur'></td>
    <td><input type='text' class='tt' alt='tvavaleur'></td>
    <td><select onchange=\"modif(".$n_id.", 31, $(this).val(),'codeana','1&an=".$an."')\"><option value='0'></option>";
    $r_code->data_seek(0);
    while ($rq_code =$r_code->fetch_object()) {
        $sel=($rq_code->Vana_id == 0)?" selected":"";
        echo "<option value='".$rq_code->Vana_id."'".$sel.">".$rq_code->Vana_nom."</option>";
    }
    echo "</select>
    <td><button onclick=\"charge(".$n_id.", 31, '','id',2);$('#panneau_d').empty()\"><img src='/images/supprimer.png'></button></td></tr>";
}
?>
<tr><th colspan='5' style='background:orange' id='ctrl'></th></tr>
</table>
<script>
var verif = <?php echo($ttart + $tttva)?>;
if (verif != $('#valeurF').val()) {
    $('#ctrl').html("erreur sur le total : "+verif);
    $('#valeurF').css('backgroundColor','orange');
} else {
    $('#valeurF').css('backgroundColor','');
}
</script>
<button onclick="$('#export').empty();charge('detail_facture','<?php echo $id."&an=".$an?>&plus=1','export')">Ajouter une ligne d'export</button>