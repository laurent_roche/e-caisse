<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$qt= filter_input(INPUT_GET, "qt", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$inv= filter_input(INPUT_GET, "inv", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$my= filter_input(INPUT_GET, "my", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$id= filter_input(INPUT_GET, "id", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
// print_r($_GET);
// exit;
require $incpath."mysql/connect.php";
require$incpath."php/fonctions.php";
connexobjet();
if ($id) {
    $req = $id;
}

if ($inv) {
    if (strlen($inv) < 13) {//c'est un numéro d'article
        $comp = "art_id = ".$inv;
        $requete ="INSERT INTO Inventaire (inv_cb,inv_utilisateur,inv_date) VALUES((SELECT art_cb FROM Articles WHERE art_id = $inv),$_SESSION[$dossier],NOW())";
    } else {
        $comp = "art_cb = ".$inv;
        $requete="INSERT INTO Inventaire (inv_cb,inv_utilisateur,inv_date) VALUES($inv,$_SESSION[$dossier],NOW())";
    }

    if ($my == 1) {//insertion dans la table Inventaire
        if ($qt != 0) {
            $idcom->query($requete);
            $req = '';
        }
        $idcom->query("UPDATE Articles SET art_stk = $qt WHERE $comp");
        $req = '';
    } elseif ($my == 2) {
        if (strlen($inv) < 13 ) {
            // echo "UPDATE Articles SET art_stk = art_stk+$qt WHERE art_id = $inv";
            $idcom->query("UPDATE Articles SET art_stk = art_stk+$qt WHERE art_id = $inv");
        } else {
            // echo "UPDATE Articles SET art_stk = art_stk+$qt WHERE art_cb = $inv";
            $idcom->query("UPDATE Articles SET art_stk = art_stk+$qt WHERE art_cb = $inv");
        }
        $req = '';
    }
    // echo $idcom->errno." ".$idcom->error."<br>";

}
if ($req) {
    $_an = ANNEE -1;
    if (strlen($req) < 13) {//c'est un numéro d'article
        $comp = "art_id = ".$req; $comp1 = "WHERE tic_article = ".$req;
    } else {
        $comp = "art_cb = ".$req; $comp1 = "LEFT JOIN Articles ON art_id = tic_article WHERE art_cb= ".$req;
    }
    $req_recher="SELECT art_stk, 
                        (SELECT SUM(tic_quantite) FROM Tickets_".ANNEE." $comp1 AND LENGTH(tic_num) = 10) AS art_cp,
                        Vt1_nom,
                        art_unite,
                        art_id,
                        IFNULL(stk_stk,0) AS stk_stk,
                        inv_id,
                        SUM(tic_quantite) AS qtv
                            FROM Articles 
                            JOIN Vtit1 ON Vt1_article = art_id
                            LEFT JOIN Stock_$_an ON stk_article = art_id
                            LEFT JOIN Tickets_".ANNEE." ON tic_article = art_id
                            LEFT JOIN Inventaire ON inv_cb = art_cb
                                WHERE $comp  
                                AND LENGTH(IFNULL(tic_num,0)) < 10
                                    GROUP BY art_id";
    $r_recher=$idcom->query($req_recher);
    if ($r_recher->num_rows == 0 ) {
        //quantité vendu = 0 car pas encore vendu dans cette requête
        $req_recher="SELECT art_stk, 
        (SELECT SUM(tic_quantite) FROM Tickets_".ANNEE." $comp1 AND LENGTH(tic_num) = 10) AS art_cp,
        Vt1_nom,
        art_unite,
        art_id,
        IFNULL(stk_stk,0) AS stk_stk,
        inv_id,
        0 AS qtv
            FROM Articles 
            JOIN Vtit1 ON Vt1_article = art_id
            LEFT JOIN Stock_$_an ON stk_article = art_id
            LEFT JOIN Tickets_".ANNEE." ON tic_article = art_id
            LEFT JOIN Inventaire ON inv_cb = art_cb
                WHERE $comp  
                AND LENGTH(IFNULL(tic_num,0)) = 10
                    GROUP BY art_id";
    }
    $r_recher=$idcom->query($req_recher);
    $rq_recher = $r_recher->fetch_object();
    if ($rq_recher->art_unite == 1) {
        $art_cp =sprintf('%d', $rq_recher->art_cp);
        $art_stk =sprintf('%d', $rq_recher->art_stk);
    } else {
        $art_cp =$rq_recher->art_cp;
        $art_stk =$rq_recher->art_stk;
    }
    
    $art_cp =($rq_recher->art_unite == 1)?sprintf('%d', $rq_recher->art_cp):$rq_recher->art_cp;
    // echo $art_stk;
    $prevu = '';
    $req_commande = "SELECT SUM(com_quantite) AS qtc FROM Commandes_".ANNEE." WHERE com_article = ".$rq_recher->art_id;
    $r_commande=$idcom->query($req_commande);
    $rq_commande = $r_commande->fetch_object();
    // echo $rq_commande->qtc." + ".$rq_recher->stk_stk." - ".$rq_recher->art_cp;
    $prevu = ($rq_commande->qtc + $rq_recher->stk_stk) - $rq_recher->qtv;
    //-----------------pas encore vu
    if (!$rq_recher->inv_id) {
        ?>
        <h3><?php echo $rq_recher->Vt1_nom?></h3>
        <table width="100%"><TR><th>Éditer</th><TH>Stock</TH><TH>Stock prévu</TH><th>En compte</th><th>Valider</th></TR>
        <tr align="center">
        <TD><img tabindex='4' onclick="charge('article',<?php echo $rq_recher->art_id?>,'panneau_g')" src="../images/edit.png" width="40"></TD>
        <TD><input id='stk' inv='1' type="text" onclick="this.value=''" class="mille" value="<?php echo $art_stk?>" tabindex='3'></TD>
        <td><button onclick="charge('inv_article','&qt=<?php echo $prevu?>&inv=<?php echo $req?>&my=1','panneau_d')" tabindex='2'><?php echo $prevu?></button></td>
        <td><?php echo $art_cp?></td>
        <td><button style="width:40px;height:40px;background-size: contain;background-image:url('/images/valider.png')" tabindex='1' id='valider' onclick="charge('inv_article','&qt='+$('#stk').val()+'&inv=<?php echo $req?>&my=1','panneau_d')"></button></td>
        </tr>
        <tr><TD colspan="5" align="center"><button onclick="charge('inv_article','','panneau_d')">Annuler</button></TD></tr>
        </table>
        <?php
        if ($art_cp != 0) {
            echo "<p class='attention'>Cet article est en compte. Si ce compte doit-être réglé en fin d'année, 
            ajuster le stock en fonction de la quantité en compte qui correspond normalement au stock prévu.</p>";
        }
        ?>
        <script>
        $("#valider").focus();
        </script>
        <?php
    } else {
        //-----------------déjà vu
        ?>
        <h3><?php echo $rq_recher->Vt1_nom?></h3>
        <p class='attention'>Cet article à déjà été validé. La valeur saisie sera ajoutée au stock indiqué. Il est possible de mettre une valeur négative pour corriger le stock mais cela peut aussi être fait directement dans la fenêtre d'édition</p>
        <table width="100%">
        <TR><th>Éditer</th><TH>Stock</TH><TH>Ajouter</TH><th>En compte</th><th>Valider</th></TR>
        <tr align="center">
        <TD><img onclick="charge('article',<?php echo $rq_recher->art_id?>,'panneau_g')" src="../images/edit.png" width="40"></TD>
        <TD><?php echo $art_stk?></TD>
        <td><input id='stk' inv='2' type="text" class="mille" value=""></td>
        <td><?php echo $art_cp?></td>
        <td><img src="../images/valider.png" width="40" onclick="charge('inv_article','&qt='+$('#stk').val()+'&inv=<?php echo $req?>&my=2','panneau_d')"></td>
        </tr>
        <tr><TD colspan="4" align="center"><button onclick="charge('inv_article','','panneau_d')">Annuler</button></TD></tr>
        </table>
        <?php
        if ($art_cp != 0) {
            echo "<p class='attention'>Cet article est en compte. Si ce compte doit-être réglé en fin d'année, 
            ajuster le stock en fonction de la quantité en compte.</p>";
        }
        ?>
        <script>
        $("#stk").focus();
        </script>
        <?php
    }
}

?>
<script type="text/javascript" src="/js/simpleAutoComplete.js"></script>
<script>
$(document).ready(function(){
$("#CB").focus();

$('#CB').on('keydown',function(event){
      if(event.which == 13){
            charge('inv_article',$('#CB').val(),'panneau_d');
            }
        });
        
$('.param').click(function(){
    $('.param').removeClass('ombre');
    $(this).addClass('ombre');
    $('#popup_g').css('visibility','hidden');$('#fermer').css('visibility','hidden');
    });
    
$('#stk').on('keydown',function(event){
      if(event.which == 13){
            charge('inv_article','&qt='+$('#stk').val()+'&inv=<?php echo $req?>&my='+$('#stk').attr('inv'),'panneau_d');
            }
        });

  $('#rechercheTitre').simpleAutoComplete('inv_recherche.php',{
    autoCompleteClassName: 'autocomplete',
    selectedClassName: 'sel',
    attrCallBack: 'rel',
    identifier: 'auteur'
      },nomCallback);
// 
function nomCallback( par ) {
  var options = {
  success: function(data){
        //on met à jour le div suggest avec les données reçus
        $('#panneau_d').empty();
        $('#panneau_d').append(data)},
        url:      'inv_article.php?',
        type:     'GET',
        data:       {id:par[0],f:par[1]}
        };
  $.ajax(options);
  };
});
</script>
<link href="/simpleAutoComplete.css" rel="stylesheet" type="text/css">
<?php
if ($req == '') {
    echo '<center><input style ="margin-top:50px" placeholder="EAN13/id" type="text" id="CB"><br>
    <input style ="margin-top:150px" autocomplete="off" placeholder="recherche sur Titre ou référence" value="" type="text" maxlength="13" id="rechercheTitre"></input></center>';
}
?>


