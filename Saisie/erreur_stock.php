<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
/*
Creation et remplissage en fin d'inventaire de la table Correction_ANNEE qui contient les erreurs de stock de l'année
*/
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();

$req_commandes = "CREATE TEMPORARY TABLE Vcommandes AS select Commandes_".ANNEE.".com_article AS Vcom_art,
                            SUM(com_quantite) AS Vcom_quantite 
                                FROM Commandes_".ANNEE." 
                                JOIN Resume_commande_".ANNEE." on rsc_id = com_numero 
                                    WHERE rsc_etat = '3'
                                        GROUP BY com_article";
$idcom->query($req_commandes);
$idcom->query("ALTER TABLE Vcommandes
  ADD KEY Vcom_art (Vcom_art)");


// echo $idcom->errno." ".$idcom->error."<br>";
//Les ventes, on exclus les articles en compte LENGTH(10) et les tickets en cours LENGTH(9)
$req_ventes = "CREATE TEMPORARY TABLE `Vvente` AS SELECT `tic_article` AS `Vven_art`,
        SUM(`tic_quantite`) AS `Vven_quantite` 
            FROM `Tickets_".ANNEE."`
                WHERE LENGTH(tic_num) < 9
                    GROUP BY `tic_article`";
$idcom->query($req_ventes);
$idcom->query("ALTER TABLE `Vvente`
  ADD KEY Vven_art (Vven_art)");

/*echo*/ $Corrections = "CREATE TEMPORARY TABLE `Corrections` AS SELECT art_id,
                            Vt1_nom,
                            art_pht,
                            art_stk,
                            art_unite,
                            0 AS ven_qt,
                            0 AS vcom_qt,
                            0 AS stk,
                            art_pseudo
                                FROM Articles
                                    JOIN Editeurs ON edi_id = art_editeur
                                     JOIN Vtit1 ON Vt1_article = art_id
                                        WHERE edi_utilisateur =".$_SESSION[$dossier];
$idcom->query($Corrections);
$idcom->query("ALTER TABLE `Corrections`
  ADD KEY art_id (art_id)");
// echo $idcom->errno." ".$idcom->error."<br>";
$idcom->query($Corrections);
// $nb = $r_erreur->num_rows;
//commandes
$precedent= ANNEE - 1;
$idcom->query("UPDATE Corrections, Vcommandes SET vcom_qt = Vcom_quantite WHERE Vcom_art = art_id");
// $r_erreur=$idcom->query($req_erreur);
$idcom->query("UPDATE Corrections, `Vvente` SET ven_qt = Vven_quantite WHERE Vven_art = art_id");
// $r_erreur=$idcom->query($req_erreur);
$idcom->query("UPDATE Corrections, Stock_$precedent SET stk = stk_stk WHERE stk_article = art_id");


$req_erreur="SELECT art_id,
                    Vt1_nom,
                    vcom_qt,
                    stk,
                    ven_qt,
                    art_stk,
                    art_unite
                        FROM Corrections
                            WHERE ((vcom_qt + stk) - ven_qt)!= art_stk AND art_pseudo = '1'";
$r_erreur=$idcom->query($req_erreur);
$nb = $r_erreur->num_rows;
?>
<script type="text/javascript">
$(document).ready(function(){
    $('#liste_article tr').click(function(){
    $('#liste_article tr').css('font-weight','normal');
    $(this).css('font-weight','bold');
    charge('article',$(this).attr('id'),'panneau_g');
    });
});
</script>

<h3 align="center"> Erreur stock actuel (<?php echo $nb?>)</h3>

<center><table class="generique" id='liste_article'>
<thead>
<th colspan=6><i>Attention</i> : les articles en compte ne sont pas gérés</th>
<TR><Th>Titre</Th><Th>Acheté</Th><Th>Vendu</Th><th>Stock-1</th><th>Stock</th><th>erreur</th></thead><tbody>
<?php
$n = 0;
while ($rq_erreur=$r_erreur->fetch_object()) {
    // 	print_r($rq_erreur);
    $res=(($rq_erreur->vcom_qt + $rq_erreur->stk) - $rq_erreur->ven_qt -  $rq_erreur->art_stk);
    $coul=($n % 2 == 0)?$coulCC:$coulFF;
    
    $Vart_stk=($rq_erreur->art_unite == 1)?sprintf('%d', $rq_erreur->art_stk):$rq_erreur->art_stk;
    
    echo "<tr id='".$rq_erreur->art_id."' style='background-color:".$coul."'>
    <td>".$rq_erreur->Vt1_nom."</td>
    <td align='right'>".$rq_erreur->vcom_qt."</td>
    <td align='right'>".$rq_erreur->ven_qt."</td>
    <td align='right'>".$rq_erreur->stk."</td>
    <td align='right'>".$Vart_stk."</td>
    <td align='right'>".$res."</td></tr>";
    $n++;
}
?>
<!-- <tr><th colspan="3" align="right">Valeur perte estimée :</th><th colspan="2"><?php echo monetaireF($TT)?> € HT</th></tr> -->
</tbody></table></center>
<script>$('#panneau_d').height($("#affichage").height()-10);
</script>