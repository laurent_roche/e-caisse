<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$art= filter_input(INPUT_GET, "art", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$nouv_id=$req;
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
$req_nouveau = "SELECT Vt1_nom, art_id FROM Vtit1 JOIN Articles ON Vt1_article = art_id WHERE art_statut = 4";
$r_nouveau = $idcom->query($req_nouveau);
?>
<script type="text/javascript">
$(document).ready(function(){
    $('.generique tr').click(function(){
    $('.generique tr').css('fontWeight','normal');
    $(this).css('fontWeight','bold');
    charge('article',$(this).attr('id'),'panneau_g');
    $('#panneau_d').height($('#affichage').height());
    });
    } 
);
</script>
<table class="generique"><thead>
<TR><TH colspan="2">Articles ayant le statut "Nouveau"</TH></TR>
<TR><TH>N°</TH><TH>Titre</TH></TR></thead>
<tbody>
<?php
$n = 0;
while ($rq_nouveau =$r_nouveau->fetch_object()) {
    if ($n%2 == 0) {
        $coul=$coulCC;
    } else {
        $coul=$coulFF;
    }
    echo "<tr style='background-color:".$coul."' id='".$rq_nouveau->art_id."'><TD>".$rq_nouveau->art_id."</TD><TD>".$rq_nouveau->Vt1_nom."</TD></tr>";
    $n++;
}
?></tbody></table>
