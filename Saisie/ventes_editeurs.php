<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$an= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
if (! $an) {
    $an = date('Y');
}
require $incpath."mysql/connect.php";
connexobjet();
require $incpath."php/fonctions.php";
require $incpath."php/config.php";
$req_recher="SELECT SUM(tic_quantite * tic_pht) AS vlht, 
                    SUM(tic_tt) AS vlttc,
                    tic_tva,
                    edi_nom, 
                    edi_id
                        FROM Tickets_$an 
                        JOIN Articles ON art_id = tic_article 
                        JOIN Editeurs ON edi_id = art_editeur 
                            WHERE edi_utilisateur =".$_SESSION[$dossier]." 
                            AND art_pseudo <= '1'
                                GROUP BY edi_id
            ORDER BY edi_nom ";
$r_recher=$idcom->query($req_recher);
$resu=$r_recher->fetch_object();
$nb = $r_recher->num_rows;
$r_recher->data_seek(0);
$annees='<option></option>';
for ($i = ANNEE; $i >= $config['debut']; $i--) {
    $sel=($i == $an)?" selected":"";
    $annees .= "<option".$sel.">".$i."</option>";
}
?>
<script>
$(document).ready(function() {
    $('table#ventes_editeur tbody tr').css('cursor','pointer');
    $('table#ventes_editeur tbody td').click(function(){
    $('table#ventes_editeur tbody tr').css('fontWeight','normal');
    $(this).parent().css('fontWeight','bold');
        charge('detail_ventes',$(this).parent().attr('id')+'&an=<?php echo $an?>','panneau_d');
        });
});
</script>

<h3 id="liste">Valeur des ventes par Éditeurs <select id='annees' onchange="force('ventes_editeurs',this.value,'panneau_g')"><?php echo $annees?></select></h3>
<?php
if ($r_recher->num_rows == 0) {
    echo "<h1>Il n'a pas de donnée à analyser</h1>";
    exit;
}
?>
<table id="ventes_editeur" class="generique">
<thead><tr><TH>Éditeurs</TH><TH>Valeur achat HT</TH><TH>Valeur vente TTC</TH><th>Gain</th></tr>
<?php
if ($an < 2018) {
    echo "<tr><td colspan=4>Les valeurs sont estimées à partir du pht inscrit en 2018...</td></tr>";
}
?>
</thead>
<tbody>

<?php
$n=0;
$ttc = 0.00;
$tht = 0.00;
$gain = 0.00;
while ($resu=$r_recher->fetch_object()) {
    // Prix HT = prix TTC/(1 + taux de TVA)
    $coul=($n % 2 == 0)?$coulCC:$coulFF;
    echo '<tr style="background-color:'.$coul.'" id="'.$resu->edi_id.'"><td>'.$resu->edi_nom."</td>
<td class='droite'>".monetaireF($resu->vlht)."</td><td class='droite'>".monetaireF($resu->vlttc)."</td>
<td class='droite'>".$g = monetaireF(($resu->vlttc / (1+($resu->tic_tva/100)))-$resu->vlht)."<td/></tr>";
    $n++;
    $ttc += $resu->vlttc;
    $tht += $resu->vlht;
    $gain += ($resu->vlttc / (1+($resu->tic_tva/100)))-$resu->vlht;
    }
?></tbody>
<tfoot>
    <tr><TH>Éditeurs</TH><TH>Valeur achat HT</TH><TH>Valeur vente TTC</TH><th>Gain</th></tr>
    <tr><th>Total</th>
        <td class='droite'><?php echo monetaireF($tht)?></td>
        <td class='droite'><?php echo monetaireF($ttc)?></td>
        <td class='droite'><?php echo monetaireF($gain)?></td>
    </tr>
</tfoot>
</table>
<script>
$("#panneau_d").empty();
$("#panneau_g").height($("#affichage").height()-10);
</script>
