<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);

require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";

connexobjet();
// echo 'ici';
if (is_numeric($req)) {
    if (strlen($req) == 13) {
        //verification du cb
        ean13($req);
        $champ = 'art_cb';
    } elseif (strlen($req) > 13) {
        echo '<img src="/images/attention.png"><br><h3>Problème sur l\'article n° : '.$req.', un EAN13 ne peut avoir plus de 13 chiffres</h3>';
    } else {
        $champ = 'art_id';
    }
    //recherche sur l'ensemble des articles, si l'article est trouvé chez un autre utilisateur, une alerte sera affichée : article.php ligne 190
    $req_recher="SELECT art_id, edi_utilisateur, uti_nom FROM Articles JOIN Editeurs ON edi_id = art_editeur JOIN Utilisateurs ON uti_id = edi_utilisateur WHERE $champ='$req'";
    $r_recher=$idcom->query($req_recher);
    $resu=$r_recher->fetch_object();
    
    if ($r_recher->num_rows== 0) {
        echo "<h3>Cet article n'existe pas</h3>
        <center><button onclick=\"charge('creation_cb',$('#rechercheCB').val(),'panneau_g');\" class='button'>Ajouter l'article</button></center>";
        exit;
    } else {
        $id = $resu->art_id;
        $uti = $resu->edi_utilisateur;
        $nom = $resu->uti_nom;
        header('location:article.php?id='.$id.'&uti='.$uti.'&nom='.$nom);
    }
} elseif ($req == "undefined") {
    //redirection anormal depuis la page d'envoi de la commande?>
<script>
$("#popup_g").empty();$("#popup_g").css("visibility","hidden");
</script>
<?php
exit;
} else {
    echo 'Seul des chiffres sont admis';
}
                                        

?>
