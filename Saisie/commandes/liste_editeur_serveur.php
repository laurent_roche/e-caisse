<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
// exit;//
$com= filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);

require $incpath."mysql/connect.php";
connexobjet();
require $incpath."php/fonctions.php";

$req_liste = "SELECT edi_id, edi_nom FROM (
    (SELECT edi_id, edi_nom  
        FROM Editeurs 
            WHERE edi_utilisateur = ".$_SESSION[$dossier]." AND edi_etat != 1)
    UNION ALL
    (SELECT edi_id, edi_nom
        FROM Editeur_serveur 
            JOIN Editeurs ON edi_id = eds_editeur 
                WHERE edi_utilisateur = ".$_SESSION[$dossier]."
                    AND eds_editeur != eds_serveur  AND edi_etat != 1)) 
AS UnionTable GROUP BY edi_id  ORDER BY edi_nom";
$r_liste = $idcom->query($req_liste);
$max = round($r_liste->num_rows/2);
$n = 0;
?>
<script>
$(document).ready(function() {
    $('.editeurs p').click(function(){
        charge('commandes/commande','<?php echo $com?>&srv='+$(this).attr('id'),'panneau_d');
    });
    
    
});

</script>
<style>
.editeurs p {
   padding: 10px;
   cursor:pointer;
}
</style>
<h3>Liste des éditeurs</h3><div id='editeurs'></div><div class='editeurs' style='float:left'>
<?php
while ($rq_liste =$r_liste->fetch_object()) {
    $coul=($n % 2 == 0)?$coulCC:$coulFF;
    echo "<p style='background-color:".$coul."' id='".$rq_liste->edi_id."'>".$rq_liste->edi_nom."</p>";
    if($n == $max) {
        echo "</div><div class='editeurs' style='float:right'>";
    }
    $n++;
}
echo "</div>";
?>