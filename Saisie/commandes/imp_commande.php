<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
require $incpath."php/fonctions.php";
require $incpath."mysql/connect.php";
connexobjet();
$n_commande= filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);
// echo $n_commande; exit;
//*************ATTENTION ENCODAGE DE LA PAGE CP-1252*****************
connexobjet();

require_once $incpath.'/fpdf181/fpdf.php';
$Mois=array("","janvier","f�vrier","mars","avril","mai","juin","juillet","ao�t","septembre","octobre","novembre","d�cembre");

class PDF_FacturePF extends FPDF
{

    // Properties

    public $_ligne = 0;
    protected $_nbLignes;
    protected $_Colonne_Largeur;
    protected $_colonne = 1;
    protected $_Titre_Page = false;

    public function __construct($margeG, $margeH)
    {
        parent::__construct();

        $this->SetAutoPageBreak(false);
        $this->SetFontSize(7);
        $this->FontSize = 7;
        $this->FontFamily = "Arial";

        $this->_Ligne_Hauteur = $this->FontSize * .47;
        $this->AddPage('P', 'A4');

        $this->lMargin = $margeG;
        $this->tMargin = $margeH;
        $this->_Colonne_Largeur = ($this->w - $this->lMargin - $this->rMargin);
        $this->_ligne = 0;
        $this->_colonne = 1;
    }

    // Give the height for a char size given.
    protected function _get_height_chars($pt = null)
    {
        // Tableau de concordance entre la hauteur des caract�res et de l'espacement entre les lignes
        if ($pt === null) {
            $pt = $this->FontSize;
        }

        return $pt * .52;
    }

    public function addLigne($type, $valeurs)
    {
        if ($type == "entete") {
            //**********************en tete soci�t�******************************
            $this->SetFont('Arial', 'B', 14);
            $mar_g = $this->SetX($this->lMargin);
            $this->MultiCell(100, 6, $valeurs['soc_nom'].'
'.$valeurs['soc_adr1'].'
'.$valeurs['soc_adr2'].'
'.$valeurs['soc_cp'].' '.$valeurs['soc_ville'].'
'.$valeurs['soc_pays']);
            $this->SetFont('Arial', 'I', 8);
            $this->SetX($this->lMargin);
            $this->MultiCell(100, 4, 'SIRET : '.$valeurs['soc_siret'].'
N TVA : '.$valeurs['soc_tva']);
            $this->SetFont('Arial', 'B', 10);
            $this->SetX($this->lMargin);
            $this->MultiCell(100, 4, 'TEL. : '. $valeurs['soc_tel'].'
FAX. : '.$valeurs['soc_fax'].'

Date de commande : '.date('j/n/Y').'
N� Client '.$valeurs['edi_client']);

            
            //**************on affiche les coordonnes de l'�diteur*****************
            if (($valeurs['totalC'] < 0)&&($valeurs['edi_type'] == 1)) {
                $libel="Mouvement de retour ";
            } elseif ($valeurs['totalC'] < 0) {
                $libel="Bon de retour ";
            } elseif ($valeurs['edi_type'] == 1) {
                $libel = "Mouvement d'entr�e ";
            } else {
                $libel="Bon de commande ";
            }
        
            //   print_r($rq_facture);
            $debut=30;
            $this->SetFont('Arial', 'B', 16);
            $this->SetY(20);
            $this->SetX($mar_g+100);
            $this->Cell(100, 6, $libel.'N� '.$valeurs['com_id']);
            $this->SetFont('Arial', 'I', 10);
            $this->SetY($debut);
            $this->SetX($mar_g+100);
            $this->Cell(80, 6, $valeurs['edi_nom']);
            $this->SetY($debut+6);
            $this->SetX($mar_g+100);
            if ($valeurs['edi_adr2'] !="") {
                $this->Cell(80, 6, $valeurs['edi_adr1']);
                $this->SetY($debut+12);
                $this->SetX($mar_g+100);
                $this->Cell(80, 6, $valeurs['edi_adr2']);
                $this->SetY($debut+18);
                $this->SetX($mar_g+100);
                $this->Cell(80, 6, $valeurs['edi_cp']);
                $this->SetY($debut+24);
                $this->SetX($mar_g+100);
                $this->Cell($debut, 6, $valeurs['edi_ville']);
                $this->SetY($debut+30);
                $this->SetX($mar_g+100);
                $this->Cell($debut, 6, $valeurs['edi_tel']);
                $this->SetY($debut+36);
                $this->SetX($mar_g+100);
                $this->Cell($debut, 6, $valeurs['edi_fax']);
            } else {
                $this->Cell(80, 6, $valeurs['edi_adr1']);
                $this->SetY($debut+12);
                $this->SetX($mar_g+100);
                $this->Cell(80, 6, $valeurs['edi_cp']);
                $this->SetY($debut+18);
                $this->SetX($mar_g+100);
                $this->Cell($debut, 6, $valeurs['edi_ville']);
                $this->SetY($debut+24);
                $this->SetX($mar_g+100);
                $this->Cell($debut, 6, "N� Tel ".$valeurs['edi_tel']);
                $this->SetY($debut+30);
                $this->SetX($mar_g+100);
                $this->Cell($debut, 6, "N� Fax ".$valeurs['edi_fax']);
            }
        } elseif ($type == "titre_tableau") {
            $y_axis_initial=75;
            $this->SetFillColor(255, 255, 255);
            $this->SetFont('Arial', 'B', 10);
            $this->SetY($y_axis_initial);
            $this->SetX($this->lMargin);
            $this->Cell(8, 10, 'N� l', 1, 0, 'C', 1);
            $this->Cell(30, 10, 'C.Barre/ref Ed.', 1, 0, 'C', 1);
            $this->Cell(37, 10, 'Auteur/ref Int.', 1, 0, 'C', 1);
            $this->Cell(92, 10, 'D�signation', 1, 0, 'C', 1);
            $this->Cell(13, 10, 'Quant.', 1, 0, 'C', 1);
        } elseif ($type == "titre_suivant") {
            $y_axis_initial=10;
            $h_tableau = 350;
            $this->SetFillColor(255, 255, 255);
            $this->SetFont('Arial', 'B', 12);
            $this->SetY($y_axis_initial);
            $this->SetX($this->lMargin);
            $this->Cell(8, 8, '', 1, 0, 'C', 1);
            $this->Cell(30, 8, 'C.Barre/ref Ed.', 1, 0, 'C', 1);
            $this->Cell(37, 8, 'Auteur/ref Int.', 1, 0, 'C', 1);
            $this->Cell(92, 8, 'D�signation', 1, 0, 'C', 1);
            $this->Cell(13, 8, 'Quant.', 1, 0, 'C', 1);
        } elseif ($type == "article") {
            // print_r($valeurs);
            $this->SetFont('Arial', '', 10);
            $this->SetY($valeurs["entete"]+$valeurs["dec"]);
            $this->SetX($this->lMargin);
            $this->Cell(8, 8, $valeurs['lign'], 1, 0, 'C', 1);
            $this->Cell(30, 8, $valeurs['cb'], 1, 0, 'C', 1);
            $this->Cell(37, 8, $valeurs['auteur'], 1, 0, 'L', 1);
            $this->Cell(92, 8, $valeurs['titre'], 1, 0, 'L', 1);
            $this->Cell(13, 8, $valeurs['qt'], 1, 0, 'R', 1);
        } elseif ($type == "finTableau") {
            $h_tableau = 270;
            $debut = $valeurs['debut'];
            $this->Line(15, $debut, 15, $h_tableau);
            $this->Line(23, $debut, 23, $h_tableau);
            $this->Line(53, $debut, 53, $h_tableau);
            $this->Line(90, $debut, 90, $h_tableau);
            $this->Line(182, $debut, 182, $h_tableau);
            $this->Line(195, $debut, 195, $h_tableau);
            $this->Line(15, $h_tableau, 195, $h_tableau);
        } elseif ($type == "signature") {
            $this->SetY(280);
            $this->Cell(0, 8, $valeurs['nom'], 0, 0, 'R', 0);
        }
    }
}
//coordonn�es de la soci�t�
$req_societe="SELECT * FROM Societe";
$r_societe=$idcom->query($req_societe);
$rq_societe=$r_societe->fetch_object();



//coordonn�es du serveur
$req_serveur="SELECT  edi_nom, 
                    edi_adr1, 
                    edi_adr2, 
                    edi_cp, 
                    edi_ville, 
                    edi_tel, 
                    edi_fax,
                    edi_client, 
                    edi_type,
                    uti_nom 
                        FROM Editeurs 
                        JOIN Resume_commande_".ANNEE." ON rsc_serveur = edi_id 
                        JOIN Utilisateurs ON edi_utilisateur = uti_id
                        WHERE rsc_id=".$n_commande;
$r_serveur=$idcom->query($req_serveur);
$rq_serveur=$r_serveur->fetch_object();

//v�rification du total de la commande : si n�gatif, c'est un retour
$req_total = "SELECT SUM(com_quantite) AS totalC FROM Commandes_".ANNEE." WHERE com_numero=".$n_commande;
$r_total=$idcom->query($req_total);
$rq_total=$r_total->fetch_object();

// exit;
$commandePDF=new PDF_FacturePF(15, 6);

//debut de facture, coordonn�es et lignes de titre
$commandePDF->addLigne("entete", array("soc_nom"=>utf8_decode($rq_societe->soc_nom),"soc_adr1"=>utf8_decode($rq_societe->soc_adr1),"soc_adr2"=>utf8_decode($rq_societe->soc_adr2),"soc_cp"=>$rq_societe->soc_cp,"soc_ville"=>utf8_decode($rq_societe->soc_ville),"soc_pays"=>utf8_decode($rq_societe->soc_pays),"soc_siret"=>$rq_societe->soc_siret,"soc_tva"=>$rq_societe->soc_tva,"soc_banque"=>$rq_societe->soc_banque,"soc_iban"=>$rq_societe->soc_iban,"soc_tel"=>$rq_societe->soc_tel,"soc_fax"=>$rq_societe->soc_fax,"edi_nom"=>utf8_decode($rq_serveur->edi_nom),"com_id"=>$n_commande,"edi_adr1"=>utf8_decode($rq_serveur->edi_adr1),"edi_adr2"=>utf8_decode($rq_serveur->edi_adr2),"edi_cp"=>$rq_serveur->edi_cp,"edi_ville"=>utf8_decode($rq_serveur->edi_ville),"edi_tel"=>$rq_serveur->edi_tel,"edi_fax"=>$rq_serveur->edi_fax,"edi_client"=>$rq_serveur->edi_client,"totalC"=>$rq_total->totalC,"edi_type"=>$rq_serveur->edi_type));

$commandePDF->addLigne("titre_tableau", array(""));

$req_commande = "SELECT Vt1_nom, 
                        art_cb, 
                        com_quantite, 
                        Vt2_nom, 
                        Vt3_nom, 
                        Vt4_nom,
                        art_unite 
                            FROM Commandes_".ANNEE." 
                                JOIN Articles ON art_id = com_article
                                JOIN Vtit1 ON Vt1_article = com_article 
                                LEFT JOIN Vtit2 ON Vt2_article = com_article 
                                LEFT JOIN Vtit3 ON Vt3_article = com_article 
                                LEFT JOIN Vtit4 ON Vt4_article = com_article 
                                    WHERE com_numero = ".$n_commande." AND com_article != 1";//on cache la ligne de port qui ne sert que pour la r�ception
//exit;
$r_commande=$idcom->query($req_commande);
$nb_commande = $r_commande->num_rows;
$n = 1;//nombre de lignes affich�es
$l_page = 1;//ligne affich�e dans la page
$page = 1;
$reste = $nb_commande;
$entete = 85;//d�calage por la premi�re page
$dec = 0;
$cb = '';
while ($rq_commande = $r_commande->fetch_object()) {
    print_r($rq_commande);
    if (substr($rq_commande->art_cb, 0, 3)!= 200) {
        $cb = $rq_commande->art_cb;
    } 
    if ($cb == '') {
        $cb = $rq_commande->Vt2_nom;
    }
    if ($rq_commande->Vt4_nom) {
        $auteur = ucfirst(substr($rq_commande->Vt4_nom, 0, 19));
    } else {
        $auteur = ucfirst(utf8_decode($rq_commande->Vt3_nom));
    }
    if ($rq_commande->art_unite == 1) {
        $qt = sprintf("%d", $rq_commande->com_quantite);
    } else {
        $qt = $rq_commande->com_quantite;
    }
    $commandePDF->addLigne("article", array("lign"=>$n,"entete"=>$entete,"dec"=>$dec,"cb"=>$cb,"auteur"=>ucfirst(utf8_decode($auteur)),"titre"=>ucfirst(utf8_decode($rq_commande->Vt1_nom)),"qt"=>$qt));
    $reste--;
    /*echo "<br>".*/$dec += 8;
    if ($page == 1) {
        if (($n < 24)&&($reste == 0)) {//page incompl�te
            $dec += 8;
            $commandePDF->addLigne("signature", array("debut"=>$dec,"nom"=>$rq_serveur->uti_nom));
        } elseif (($n == 24)&&($reste == 0)) {//page complete
            //$dec += 8;
            //$commandePDF->addLigne("signature",array("debut"=>$dec,"nom"=>$rq_serveur->uti_nom));
        } elseif ($n == 24) {
            $dec += 8;
            $page++;
            $commandePDF->addLigne("signature", array("debut"=>$dec,"nom"=>"Suite � la page ".$page));
            $commandePDF->AddPage();
            $commandePDF->addLigne("titre_suivant", array(""));
            $entete = 0;
            $dec = 18;
            $l_page = 1;
        }
        //$l_page++;
    } else {//page 2 et suivantes
        $l_page++;
        if (($l_page == 33)&&($reste > 0)) {//saut de page, il reste des articles
            $page++;
            $commandePDF->addLigne("signature", array("debut"=>$dec,"nom"=>"Suite � la page ".$page));
            $commandePDF->AddPage();
            $commandePDF->addLigne("titre_suivant", array(""));
            $dec = 18;
            $l_page = 1;
        }
        if ($reste == 0) {
            $commandePDF->addLigne("finTableau", array("debut"=>$dec));
            $dec += 8;
            $commandePDF->addLigne("signature", array("debut"=>$dec,"nom"=>$rq_serveur->uti_nom));
        }
        //if($l_page == 1) $dec = 10;
    }
    $cb = '';
    $n++;
}

$chemin = $incpath."pdf/commandes/com_".((ANNEE * 1000)+$n_commande).".pdf";
$commandePDF->Output('F', $chemin);//
?>
<script>
$('#popup_g').css('visibility','hidden');
location.href=('<?php echo $chemin?>');
</script>