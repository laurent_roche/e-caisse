<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$serveur= filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);
$limit = filter_input(INPUT_GET, "lim", FILTER_SANITIZE_STRING);
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();

$req_com = 'INSERT INTO Resume_commande_'.ANNEE.' (rsc_date, rsc_serveur,rsc_etat, rsc_util) 
                        VALUES(NOW(),'.$serveur.',1,'.$_SESSION[$dossier].')';
$r_com = $idcom->query($req_com);
$rsc_id=$idcom->insert_id;
//recherche des articles au stock négatif sur cet éditeur
$req_vente ="SELECT (art_stk * -1) AS qt,
                        art_id
                        FROM Articles
                            WHERE art_editeur = ".$serveur." AND art_stk < 0";
$r_vente = $idcom->query($req_vente);
while ($rq_vente = $r_vente->fetch_object()) {
    $req_comm = "INSERT INTO Commandes_".ANNEE." (com_article,com_ttc,com_remise,com_pht,com_tva,com_utilisateur,com_statut,com_numero,com_quantite) SELECT art_id,art_ttc,art_remise,art_pht,art_tva,$_SESSION[$dossier],0,$rsc_id,".$rq_vente->qt." FROM Articles WHERE art_id = ".$rq_vente->art_id;
    $res = $idcom->query($req_comm);
    if (!$res) {
        ?>
        <script>$('#mysql').css('visibility','visible')</script>
        <?php
        echo "<b>Erreur n° ".$idcom->errno."</b><br> <em>".$idcom->error."</em>";
        exit;
    } else {
        ?>
        <script>
        $('.jaune').removeClass('jaune');
        $('#mysql').css('visibility','hidden')</script>
        <?php
        // echo "<br>".$req_comm;
    }
}
?><button onclick="charge('commandes/commande',<?php echo $rsc_id?>,'panneau_d')">Voir la commande</button>