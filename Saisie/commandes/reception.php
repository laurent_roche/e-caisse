<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);
require $incpath."mysql/connect.php";
connexobjet();
require $incpath."php/fonctions.php";
$_SESSION['aide_'.$_SESSION[$dossier]] = 'S3_5';
// print_r($_SESSION);
$an=ANNEE;
$req_recher="SELECT Commandes_$an.*,
                    vt1_nom,
                    art_id,
                    art_cb,
                    art_rayon,
                    art_editeur,
                    art_tva,
                    art_unite,
                    edi_commande,
                    edi_nom,
                    com_quantite,
                    com_numero,
                    com_remise,
                    com_ttc,
                    com_pht,
                    rsc_date,
                    tva_nom,
                    com_id
                        FROM Commandes_$an
                        JOIN Articles on art_id=com_article
                        JOIN Vtit1 ON Vt1_article  = art_id
                        JOIN Resume_commande_$an ON rsc_id=com_numero
                        JOIN Editeurs ON rsc_serveur = edi_id
                        LEFT JOIN Tva ON tva_id = art_tva
                          WHERE com_numero = $req
                          ORDER BY com_statut, art_unite DESC, com_id";
// $req_recher=
$r_recher=$idcom->query($req_recher);
$resu=$r_recher->fetch_object();
$nb = $r_recher->num_rows;
if ($nb == 0) {
    echo "<img src='/images/attention.png'>Il n'y a pas d'article dans cette commande";
    exit;
}
$r_recher->data_seek(0);
$tab=explode("/", $_SERVER['PHP_SELF']);
if ($tab[3]) {
    $page=$tab[2]."/".substr($tab[3], 0, -4);
}
else {
    $page=substr($tab[2], 0, -4);
}
?>
<style>
#attente td{
min-height:30px;
}
.vert{
background-color:green;
}
.clair{
background-color:<?php echo $coulCC?>;
}
.fonce{
background-color:<?php echo $coulFF?>;
}
.jaune{
background-color:yellow;
}
</style>
<?php

require $incpath.'Saisie/commandes/reception.inc.php';
?>
<h3 id="liste" style="position:fixed;width:40%;height:40px"> Commande en attente n° <SPAN id='numero_commande'><?php echo $resu->com_numero?></SPAN> <?php echo $resu->edi_nom?> <br> <input id='select_art' type="text" style="float:left">Valeur estimée : <span style="width:80px;font-size:18px;font-weight:bold" id=valeur></span></h3>

<table id="commande" style='width:96%;margin-left:2%;margin-top:60px' class="tablesorter">
<thead>
<tr><TH>Titre</TH><TH>Quantité</TH><TH>PuTTC</TH><TH>Rem</TH><th>PU. HT</th><td></td></tr>
</thead>
<tbody>
<?php
$desactiver=($resu->edi_commande == "")?" style='float:left;color:#ffffff'":" style='float:left'";
$n=0;
$bt_validation = 0;
while ($resu=$r_recher->fetch_object()) {
    $coul=($n % 2 == 0)?" class='clair'":" class='fonce'";
    if ($resu->com_statut == 1) {
        $coul=" class='vert'";
        $bt_validation = 1;
    }
    $qt=($resu->art_unite == 1)?sprintf("%d", $resu->com_quantite):$resu->com_quantite;
    // id,tb,vl,cp,my
    ?>
    <tr <?php echo $coul.' id="'.$resu->art_cb?>">
        <td><?php echo $resu->Vt1_nom?></td>
        <td><input id="qt_<?php echo $resu->com_id?>" type="text" class="mille" value="<?php echo $qt?>">
    </td>
    <td><input id="px_<?php echo $resu->com_id?>" type="text" class="cent px" class="cent" value="<?php echo $resu->com_ttc?>">
    </td>
    <?php
    if ($resu->com_remise > 0.00) {
        $comp1 = 'style="background-color:transparent;border:0;height:32px"';
        $comp = '';
    } else {
        $comp = 'style="background-color:transparent;border:0;height:32px"';
        $comp1 = '';
    }
    ?>

    <td><input <?php echo $comp?> id="rem_<?php echo $resu->com_id?>"  type="text" class="cent rem" class="cent" value="<?php echo $resu->com_remise?>">
    </td>
    <td><input <?php echo $comp1?> id="pht_<?php echo $resu->com_id?>" type="text" class="cent rem" class="cent" value="<?php echo $resu->com_pht?>">
    </td>
    
    <?php
    if ((($resu->art_rayon == '')||($resu->art_editeur == '')||($resu->art_tva == ''))&&($resu->art_id != 1)) {
        ?>
        <td><button id="b_<?php echo $resu->com_id?>" onclick="charge('nouveau','<?php echo $resu->art_cb."&art=".$resu->art_id?>','panneau_g')">Complèter</button></td>
        <?php
    } else {
        ?>
        <td><button id="b_<?php echo $resu->com_id?>">Ok</button></td>
        <?php
    }
    ?>    
    </tr>
    <?php
    $n++; 
}

//***********************les nouveautes sont dans la table Articles avec art_unite = 0******************
//elles ont été stockées en cours de requete et sont affichées maintenant
// echo "<tr><th colspan=6>Les nouveautés</th></tr>
// 			<tr><th></th><th>CB</th><th colspan=2>Titre</th><th>Quantite</th><th></th></tr>";
while ($resu=$r_recher->fetch_object()) {
    $coul=($n % 2 == 0)?" class='clair'":" class='fonce'";
    echo '<tr'.$coul.'><td><img onclick="charge(\''.$incpath.'Saisie/commandes/supression\',\'art&art='.$resu->com_id.'\',\'popup_g\')"; src="/images/button_drop.png"></td><td><input type="text" class=_cb value="'.$resu->nou_cb.'" onchange=modif('.$resu->nou_id.',21,this.value,\'cb\',\'1\')></td><td colspan=2>'.stripslashes($resu->nou_titre).'</td><td>'.$resu->nou_qt.'</td><td><button onclick="charge(\'nouveau\','.$resu->nou_id.',\'panneau_g\')">Créer</button></td></tr>';
    $n++;
}
$nb = $r_recher->num_rows;
?>
<tr>
    <TD colspan="2">
    <?php
    if ($bt_validation == 1) {
        ?>
        <button onclick="force('commandes/validation',<?php echo $req?>,'panneau_g')">Valider la réception</button>
        <?php
    }
    ?>
    </TD>
<td colspan="2"></td><td colspan="2" align="right"><button onclick="modif(<?php echo $req?>,23,1,'etat',1);force('commandes/attente',<?php echo $req?>,'panneau_g')">Rééditer</button></td></tr>
</tbody>
</table>

<script>
charge('commandes/calcul','<?php echo $req?>','valeur');
$("#panneau_d").height($("#affichage").height()-10);
</script>
