<?php
session_start();
//page appelée par /Saisie/commandes/reception.php ligne 155
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);
require $incpath."mysql/connect.php";
connexobjet();
require $incpath."php/fonctions.php";
// $_SESSION['aide_'.$_SESSION[$dossier]] = 'S3_5';
/*
Validation de la commande : rsc_etat = 3
mise à jour du stock et des prix
si l'article est en moyen pondéré, il faut faire le calcul

1 - un premier passage en l'excluant
2 - un second traitant les m p

on ne valide que les articles
*/
$an=ANNEE;
$req_valid="UPDATE Resume_commande_$an
                    SET rsc_etat = 3,
                        rsc_date = DATE(NOW()),
                        rsc_ttc = (SELECT SUM(com_pht * com_quantite)*(1+(tva_nom / 100)) FROM Commandes_$an JOIN Tva ON tva_id = com_tva WHERE com_numero = $req)
                            WHERE rsc_id = $req";
$idcom->query($req_valid);
//1
$req_stock = "UPDATE Articles, Commandes_$an
                            SET art_stk = art_stk + com_quantite,
                            art_pht = com_pht,
                            art_ttc = com_ttc,
                            art_remise = com_remise
                            WHERE art_id = com_article
                                AND com_numero = $req
                                AND com_statut = 1
                                AND art_mp = '0'";
$idcom->query($req_stock);
//2 : (art_stk*pht + com_quantite*com_pht) / (art_stk + com_quantite)
$req_stock = "UPDATE Articles, Commandes_$an SET art_stk = art_stk + com_quantite, art_pht = (art_stk*art_pht + com_quantite*com_pht) / (art_stk + com_quantite), art_ttc = com_ttc, art_remise = com_remise WHERE art_id = com_article AND com_numero = $req AND com_statut = 1 AND art_mp = '1'";
$idcom->query($req_stock);
// echo "<b>Erreur n° ".$idcom->errno."</b><br> <em>".$idcom->error."</em>";
//on vérifie si tous les articles ont été validés
$req_val = "SELECT * FROM Commandes_$an WHERE com_numero = $req AND com_statut = 0";
$r_val=$idcom->query($req_val);
// echo "<b>Erreur n° ".$idcom->errno."</b><br> <em>".$idcom->error."</em><br>";
if ($r_val->num_rows > 0) {//creation d'une nouvelle commande et mise à jour des articles restant
    //on récupère le numéro du serveur
    $req_serveur = "SELECT rsc_serveur FROM Resume_commande_$an WHERE rsc_id = $req";
    $r_serveur = $idcom->query($req_serveur);
    $rq_serveur =$r_serveur->fetch_object();
    // echo "<b>Erreur n° ".$idcom->errno."</b><br> <em>".$idcom->error."</em><br>";
    $serveur = $rq_serveur->rsc_serveur;
    $req_insert = "INSERT INTO Resume_commande_$an (rsc_date,rsc_serveur,rsc_etat,rsc_util,rsc_ttc,rsc_tax) VALUES (DATE(NOW()),$serveur,2,".$_SESSION[$dossier].",0.00,0.00)";
    $res = $idcom->query($req_insert);
    $rsc_id=$idcom->insert_id;
    // echo "<b>Erreur n° ".$idcom->errno."</b><br> <em>".$idcom->error."</em><br>";
    echo $req_update = "UPDATE Commandes_$an SET com_numero = $rsc_id WHERE com_numero = $req AND com_statut = 0";
    $idcom->query($req_update);
    // echo "<b>Erreur n° ".$idcom->errno."</b><br> <em>".$idcom->error."</em><br>";
}
// exit;
header('location:liste_reception.php');
