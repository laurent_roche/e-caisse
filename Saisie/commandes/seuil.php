<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);
require $incpath."mysql/connect.php";
connexobjet();
require $incpath."php/fonctions.php";
// print_r($_SESSION);
$an=ANNEE;
require $incpath."Saisie/ventes_annuelles.php";
// exit;
$req_max="SELECT MAX(ven_ct) as maxi                      
						FROM Articles
						LEFT JOIN Ventes ON ven_article = art_id
						WHERE art_stk <= art_seuil AND art_seuil != 0";
$r_max=$idcom->query($req_max);
$rq_max=$r_max->fetch_object();
// print_r($rq_max);
$req_recher="SELECT Vt1_nom,
					art_id, 
					art_stk,
					ven_ct, 
					com_statut, 
					com_quantite, 
					art_unite, 
					rsc_etat,
					art_statut AS Vart_afficher,
					com_id
						FROM Articles 
						JOIN Vtit1 ON Vt1_article = art_id
						JOIN Editeurs ON art_editeur = edi_id 
						LEFT JOIN Ventes ON ven_article = art_id
						LEFT JOIN Commandes_$an ON com_article = art_id
						LEFT JOIN Resume_commande_$an ON rsc_id = com_numero
								WHERE art_stk <= art_seuil 
								AND art_seuil != 0 
								AND com_statut IS NULL
								AND edi_utilisateur = $_SESSION[$dossier]";
$r_recher=$idcom->query($req_recher);
$resu=$r_recher->fetch_object();
$nb = $r_recher->num_rows;
$r_recher->data_seek(0);
?>
<?php
$nom = 'à réapprovisionner';
require $incpath."Saisie/liste_articles.php";
?>
<script>
$("#panneau_d").height($("#affichage").height()-10);
</script>
