<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p,$i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_STRING);
require $incpath."mysql/connect.php";
connexobjet();
require $incpath."php/fonctions.php";
// print_r($_SESSION);
$an = ANNEE;
$req_recher="SELECT rsc_id, rsc_date, edi_nom FROM Resume_commande_$an JOIN Editeurs ON edi_id = rsc_serveur WHERE rsc_etat =2 AND rsc_util=".$_SESSION[$dossier];
$r_recher=$idcom->query($req_recher);
// $resu=$r_recher->fetch_object();
$nb = $r_recher->num_rows;
if ($nb == 0) {
    echo '<h3>Pas de commande en attente de réception</h3>';
    exit;
}
?>
<script>
$(document).ready(function() {
    $('table#attente td').click(function() {	
    $('table#attente tr').css('fontWeight','normal');
    if($(this).html().slice(0,8)!='<a href='){
        charge('commandes/reception',$(this).parent().attr('id'),'panneau_d');
    }
    $(this).parent().css('fontWeight','bold');
    });
});
$("#panneau_d").height($("#affichage").height()-10);
$("#panneau_d").empty();$("#popup_g").css("visibility","hidden");
</script>
<h3> Commandes (<?php echo $nb?>) en attente de réception </h3>

<table id="attente" style='width:90%;margin-left:5%' class="generique">
<thead>
<tr><TH>N°</TH><TH>Date</TH><TH>Serveurs</TH></tr>
</thead>
<tbody> 
<?php
$n=0;//onclick="charge(\'commandes/reception\','.$resu->rsc_id.',\'panneau_d\')"
while ($resu=$r_recher->fetch_object()) {
    if ($n%2 == 0) {
        $coul=$coulCC;
    } else {
        $coul=$coulFF;
    }

    if (file_exists($incpath."pdf/commandes/com_".(($an*1000) + $resu->rsc_id).".pdf")) {
        $pdf= '<a href="'.$incpath."pdf/commandes/com_".(($an*1000) + $resu->rsc_id).'.pdf"><img src="../images/pdf.gif"></a> ';
    } else {
        $pdf='';
    }

    echo '<tr  id="'.$resu->rsc_id.'" style="background-color:'.$coul.'"><td>'.$pdf.' '.$resu->rsc_id.'</td>
<td>'.dateFR($resu->rsc_date).'</td><td>'.$resu->edi_nom.'</td></tr>';
    $n++;
}
?>
</tbody>
</table>