<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
connexobjet();
require $incpath."php/fonctions.php";

$req_recher="SELECT * FROM Statut WHERE sta_id > 2 ORDER BY sta_nom ";
$r_recher=$idcom->query($req_recher);
?>
<h3>Les articles masqués</h3>
<select onchange="charge('articles_masques',this.value,'articles_masques')">
<option>Categories</option>
<?php
while ($resu=$r_recher->fetch_object()) {
    echo "<option value='".$resu->sta_id."'>".$resu->sta_nom."</option>";
}
?></select>
<div id='articles_masques'>
</div>
