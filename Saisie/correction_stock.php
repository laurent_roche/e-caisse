<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
/*
Creation et remplissage en fin d'inventaire de la table Correction_ANNEE qui contient les erreurs de stock de l'année
*/
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
$an = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
if ($an == '') {
    $an = ANNEE -1;
}

$req_erreur = "SELECT Vt1_nom, 
                    cor_stkpre,
                    cor_commande, 
                    cor_vente, 
                    cor_stock, 
                    cor_pht, 
                    ((cor_commande + cor_stkpre) - cor_vente)-cor_stock AS cor_quantite, 
                    art_unite, 
                    art_id FROM
                        Corrections_$an 
                            JOIN Vtit1 ON Vt1_article = cor_article 
                            JOIN Articles ON art_id = cor_article 
                                JOIN Editeurs ON edi_id = art_editeur 
                                    WHERE edi_utilisateur =".$_SESSION[$dossier];
$r_erreur = $idcom->query($req_erreur);
// echo "<br>".$idcom->errno." ".$idcom->error;
?>
<script type="text/javascript">
$(document).ready(function(){
    $('#liste_article tr').click(function(){
    $('#liste_article tr').css('font-weight','normal');
    $(this).css('font-weight','bold');
    charge('article',$(this).attr('id'),'panneau_g');
    });
});
</script>
<h3 align="center"> Erreur stock de l'année <?php echo $an?></h3>

<center><table class="generique" id='liste_article'>
<thead><TR><Th>Titre</Th><Th>Acheté</Th><Th>Vendu</Th><th>Stock-<?php echo($an-1)?></th><th>Stock</th><th>erreur</th></thead><tbody>
<?php
$n = 0;
while ($rq_erreur=$r_erreur->fetch_object()) {
    if ($n%2 == 0) {
        $coul=$coulCC;
    } else {
        $coul=$coulFF;
    }
    
    if ($rq_erreur->art_unite == 1) {
        $cor_quantite = sprintf('%d', $rq_erreur->cor_quantite);
        $cor_vente =  sprintf('%d', $rq_erreur->cor_vente);
        $cor_commande = sprintf('%d', $rq_erreur->cor_commande);
        $cor_stkpre =  sprintf('%d', $rq_erreur->cor_stkpre);
        $cor_stock =  sprintf('%d', $rq_erreur->cor_stock);
    } else {
        $cor_quantite = $rq_erreur->cor_quantite;
        $cor_vente = $rq_erreur->cor_vente;
        $cor_commande = $rq_erreur->cor_commande;
        $cor_stkpre = $rq_erreur->cor_stkpre;
        $cor_stock = $rq_erreur->cor_stock;
    }
    
    echo "<tr id='".$rq_erreur->art_id."' style='background-color:".$coul."'>
    <td>".$rq_erreur->Vt1_nom."</td>
    <td align='right'>".$cor_commande."</td>
    <td align='right'>".$cor_vente."</td>
    <td align='right'>".$cor_stkpre."</td>
    <td align='right'>".$cor_stock."</td>
    <td align='right'>".$cor_quantite."</td></tr>";
    $n++;
}
?>
</tbody></table></center>
<script>$('#panneau_d').height($("#affichage").height()-10);
</script>