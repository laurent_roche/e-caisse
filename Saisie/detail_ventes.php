<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$an= filter_input(INPUT_GET, "an", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
connexobjet();
require $incpath."php/fonctions.php";

$req_recher="SELECT Vt1_nom, SUM(tic_quantite) as qt, SUM(tic_tt) AS vl, art_id, art_unite FROM Tickets_$an JOIN Articles ON art_id =tic_article JOIN Vtit1 ON Vt1_article = tic_article WHERE art_editeur = $req GROUP BY art_id ORDER BY SUM(tic_quantite) DESC";
$r_recher=$idcom->query($req_recher);
$resu=$r_recher->fetch_object();
$nb = $r_recher->num_rows;
$r_recher->data_seek(0);
?>
<script>
$(document).ready(function() {
    $('table#ventes tbody tr').css('cursor','pointer');
    $('table#ventes tbody td').click(function(){
    $('table#ventes tbody tr').css('fontWeight','normal');
    $(this).parent().css('fontWeight','bold');
        charge('article',$(this).parent().attr('id'),'panneau_g');
        });
});
</script>

<h3>Ventes en <?php echo $an?> </h3>

<table id="ventes" class="generique">
<thead><tr><TH>Titres</TH><TH>Quantité</TH><th>Valeur TTC</th></tr>
<?php
if ($an < 2017) {
    echo "<tr><td colspan=3>Les valeurs sont une estimation...</td></tr>";
}
?>
</thead>
<tbody>

<?php
$n=0;
$ttc = 0.00;
while ($resu=$r_recher->fetch_object()) {
    $coul=($n % 2 == 0)?$coulCC:$coulFF;
    $quantite=($resu->art_unite == 1)?sprintf('%d', $resu->qt):$resu->qt;
    echo '<tr style="background-color:'.$coul.'" id="'.$resu->art_id.'"><td>'.$resu->Vt1_nom."</td><td class='droite'>".$quantite."</td><td class='droite'>".monetaireF($resu->vl)."</td></tr>";
    $n++;
    $ttc += $resu->vl;
}
?></tbody>
<tfoot>
    <tr><TH>Titres</TH><TH>Quantité</TH><th>Valeur TTC</th></tr>
    <tr><TH></TH><th class='droite'>Total</th><th class='droite'><?php echo monetaireF($ttc)?></th></tr>
</tfoot>
</table>
<script>
$("#panneau_d").height($("#affichage").height()-10);
</script>
