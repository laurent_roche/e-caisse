<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$id= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$tva= filter_input(INPUT_GET, "tva", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$rem= filter_input(INPUT_GET, "rem", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$ttc= filter_input(INPUT_GET, "ttc", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$cp= filter_input(INPUT_GET, "cp", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
// echo $ttc."/(1+".($tva/100).")<br>";
$ht= $ttc/(1+($tva/100));//ht sur prix fixe
// echo $ht."*(1-(".$rem."/100))";
$pht = $ht*(1-($rem/100));//ht sur remise
// echo (17.061611374408 * (1-(30.00/100)));
require $incpath."mysql/connect.php";
connexobjet();
require $incpath."php/fonctions.php";
//changement de remise : mise à jour de art_remise et recalcul de art_pht
if ($cp == "remise") {
    $req_com="UPDATE Articles SET art_remise = $rem, art_pht = $pht WHERE art_id = $id";
} elseif ($cp == "ttc") { //changement de ttc si remise : mise à jour de art_pht
    $req_com="UPDATE Articles SET art_ttc = $ttc WHERE art_id = $id";
}
$req_remise = "SELECT eds_remise FROM Editeur_serveur JOIN Articles ON eds_editeur = art_editeur WHERE art_id = $id";
$r_remise = $idcom->query($req_remise);
$rq_remise =$r_remise->fetch_object();

$res=$idcom->query($req_com);
// echo $idcom->errno." ".$idcom->error;exit;
    if (!$res) {
        ?>
        <script>$('#mysql').css('visibility','visible')</script>
        <?php
        echo $idcom->errno." ".$idcom->error;
        echo "<br>".$req_com;
    }
?>

<input style="float:left" class="cent" onchange="remise(this.value,'ttc')" type="text" id='ttc' value="<?php echo $ttc?>">
            <?php
            if ($rem > 0.00) {
                $comp1 = 'style="background-color:transparent;border:0;height:32px"';
                $comp = '';
            } else {
                $comp = 'style="background-color:transparent;border:0;height:32px"';
                $comp1 = '';
            }
            
            //si la remise est utilisée, le pht ne peut être modifié
            if ($rem == 0.00) {
                $modif= " onchange=modif(".$id.",11,this.value,'pht',1)";
            } else {
                $modif = "onclick=\"alert('On ne peut modifier ce prix HT lié à la remise. Modifer la remise ou la mettre à 0.00')\"";
            }

            ?>
            <span><strong>Remise</strong> 
            <input <?php echo $comp?> placeholder='0.00' class="cent" onchange="remise(this.value,'remise')" type="text" id='rem' value="<?php echo $rem?>"> % <em>(<?php echo $rq_remise->eds_remise?>)</em></span>
            <span style="float:right;"><strong>Prix HT</strong> <input <?php echo $comp1?> class="demi" <?php echo $modif?> type="text" id='pht' value="<?php echo number_format($pht, 4)?>"></span>
