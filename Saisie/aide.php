<?php
session_start();
$tab=explode('/',$_SERVER['REQUEST_URI']);
$dossier=$tab[1];
?>
<img style="float: right; margin: 0 0 0 20px;" alt="Lil bomb dude" src="/images/annuler.png" onclick='$("#aide").css("visibility","hidden")'>
<?php
switch($_SESSION['aide_'.$_SESSION[$dossier]])
	{
	case 'S1_1' : echo "<h3>Catalogage</h3> 
							<p>Sous ce terme, on retrouve tout ce qui concerne l'article en lui-même : création, modification, historique</p>
							<p>La <strong>zone de saisie</strong> du 'bandeau' permet une recherche simultanée sur :
							 <ul>
							 <li>les titres</li>
							 <li>les références internes</li>
							 <li>les références éditeurs</li>
							 <li>les auteurs</li>
							 </ul></p>
							 <p>La recherche est insensible à la casse et aux accents ce qui veut dire que <em>François</em> et <em>francois</em> donneront le même résultat. La recherche se fait sur la suite de caractères, quelque soit la place dans le mot, espace compris, exemple pour recherche <em>Jésus de Nazareth</em> il suffit de tape  : <em>s de n</em>, on trouvera aussi <em>temps de Noël</em>.<br><strong>Note</strong> Actuellement l'apostrophe n'est pas utilisable, on trouvera donc pas <em>l'évènement</em> mais on trouvera <em>évènement</em>.</p>
							 <p>On ne doit pas utiliser la touche 'Entrée' (sauf pour les CB) mais sélectionner l'élément recherché dans la liste.</p>
							 <p>On peut 'doucher' les articles dans ce champ de saisie, on peut aussi saisir un CB et appuyer sur la touche 'Entrée' du clavier pour valider la sasie. Si le CB n'existe pas, la création de l'article sera proposée.</p>
							 <p><strong>Nouvel article</strong> permet la création d'article avec ou sans code barre. Les codes barres internes commencent tous par 200. La création se fait normalement au moment de la réception, quand on a réellement l'article en main.</p>
							 <p><strong>Impression des CB</strong> Liste des code barre à imprimer.<br>
							 </p> 	<p><strong>Grandes Étiquettes</strong> Il s'agit des étiquettes de présentation avec un format prédéfini qui permet d'unifier le rendu visuel du magasin.</p> ";break;
		case 'S1_2' : echo "<h3>Articles</h3>
							Les articles sont retirés du stock au moment de la validatino de la vente ce qui signifie que les articles en compte ne sont pas réellemetn retirés du stock., ";break;					 
		case 'S3_5' : echo "<h3>Réception de commande</h3>
							<p>À l'ouverture de cette fenêtre, le curseur est dans le champ de saisie prêt à recevoir les données de la douchette. Une fois douché, le titre de l'article sélectionné est surligné.</p> <p><strong>Tous les articles doivent-être validé un par un</strong> après avoir vérifié que les prix, quantité et remise sont corrects. Le total HT s'affiche à droite du champ de saisie et, quand tous les articles ont été vérifiés, doit correspondre à mois d'un € près du total HT du fournisseur.</p> <p>Si l'écart est plus important, il faut rechercher la cause.</p><p>Si le code barre d'un article à changé, éditer l'article en cliquant sur l'icone à gauche du titre. Modifier le cb puis redoucher l'article.</p> <p><strong>Il est important de doucher un exemplaire de chaque article</strong> de la commande pour vérifier que les codes barres sont bien lisibles.</p><p>Si un article n'est pas livré et ne sera pas livré, le supprimer. Si on le laisse, une nouvelle commande en atente de réception sera générée.</p>";break;					 
	default : echo "<h3>Aide sur la création et gestion des articles</h3><p>L'aide est dynamique, elle affiche donc ce qui concerne la dernière fenêtre ouverte</p><p>On peut déplacer la fenêtre d'aide en la saisissant pas la bande grise supérieur.</p>";
	}
// 	
?>
