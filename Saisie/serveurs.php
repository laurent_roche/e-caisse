<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$req= filter_input(INPUT_GET, "query", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
connexobjet();
require $incpath."php/fonctions.php";
$req_recher="SELECT eds_serveur, edi_nom, edi_etat, edi_id FROM Editeurs 
                        JOIN Editeur_serveur ON eds_serveur = edi_id 
                            WHERE edi_utilisateur = $_SESSION[$dossier]                            
                                GROUP BY edi_id
                                    ORDER BY edi_nom";
$r_recher=$idcom->query($req_recher);
$resu=$r_recher->fetch_object();
$nb = $r_recher->num_rows;
$r_recher->data_seek(0);
?>
<h3 id="liste"><?php echo $nb?> Serveurs actifs</h3>
<script>
$(document).ready(function(){
  $('td').click(function(){
    $('td').removeClass('ombre');
    $(this).addClass('ombre');
    charge('detail_editeur',$(this).parent().attr('id')+'&f=serveur','panneau_d');
  });
});
</script>
<table cellpadding="0" cellspacing="0" border="1" id="liste_editeur">
<?php
$n=0;
while ($resu=$r_recher->fetch_object()) {
    if ($n%2 == 0) {
        $coul=$coulCC;
    } else {
        $coul=$coulFF;
    }
    if ($resu->edi_etat == '1') {
        $coul = $_SESSION['surligne_'.$_SESSION[$dossier]];
    }
    echo '<tr id='.$resu->eds_serveur.' style="background-color:'.$coul.'"><td>'.$resu->edi_nom."</td><td></td></tr>";
    $n++;
}
?>
</table>
<script>
$("#panneau_g").height($("#affichage").height()-10);
</script>