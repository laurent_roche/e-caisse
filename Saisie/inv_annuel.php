<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1; $i < sizeof($p) - 1; $i++) {
        $incpath = '../' . $incpath;
    }
    unset($p, $i);
}
// $req= filter_input( INPUT_GET, "id", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
// if($req == "") $req= filter_input( INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath . "mysql/connect.php";
require $incpath . "php/fonctions.php";
connexobjet();
$req_inventaire = 'SELECT con_inventaire FROM Config';
$r_inventaire = $idcom->query($req_inventaire);
$rq_inventaire = $r_inventaire->fetch_object();
if (date('n') == $rq_inventaire->con_inventaire) {
    $req_reini = "SELECT YEAR(inv_date) AS an FROM Inventaire";
    $r_reini = $idcom->query($req_reini);
    if ($r_reini->num_rows != 0) { //vidange de la table inventaire si elle contient des données de l'année précédente
        $rq_reini = $r_reini->fetch_object();
        if ($rq_reini->an == (ANNEE - 1)) {
            $idcom->query("TRUNCATE Inventaire");
        }
        $Nstcok = "CREATE TABLE IF NOT EXISTS `Stock_" . ANNEE . "` (
                        `stk_id` int(4) NOT NULL AUTO_INCREMENT,
                        `stk_article` int(4) NOT NULL,
                        `stk_stk` decimal(8,2) NOT NULL,
                        `stk_pht` decimal(8,4) NOT NULL,
                        PRIMARY KEY (`stk_id`)
                        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
        $idcom->query($Nstcok);
    }
}
$conseil = '';
$req_recher = "SELECT * FROM Inventaire WHERE inv_utilisateur = " . $_SESSION[$dossier];
$r_recher = $idcom->query($req_recher);
if ($r_recher->num_rows == 0) {
    //début de l'inventaire pour cet utilisateur, on affiche les conseils
    $conseil = "<p class='attention'>Il est préférable de terminer les ventes en attente dans les comptes avant de commencer l'inventaire, sinon vous serez obligé d'en tenir compte dans le contrôle du stock.</p>";
}
?>
<script>
    $(document).ready(function() {
        $('#inventaire tr').click(function() {
            if ($(this).hasClass('rayon') == true) {
                $('.inv_art').empty();
                ray = $(this).attr('id');
                charge('inv_rayon', ray, 'A' + ray);
            }
        });
    })
</script>
<style>
    .inv_art p {
        text-indent: 30px;
        height: 20px;
    }
</style>
<h3 id='liste'>Inventaire annuel</h3>
<?php echo $conseil ?>
<!--En cas de grosse erreur, il est possible de supprimer l'inventaire déjà réalisé à partir d'une certaine date et heure .<br><br>
L'impression de l'inventaire sera réalisable quand tous les articles auront été contrôlés.<br>
Recherchez les articles par le code barre ou le titre dans le panneau droit ou lister un rayon ci_dessous<br>-->
<table id='inventaire' width="98%">
    <?php
    $nb_articles = 0;
    $req_rayon = "SELECT Var_raynom,
                    Var_rayon,
                    COUNT(Var_article) AS articles,
                    inv_cb 
                        FROM Varticles_rayon 
                            LEFT JOIN Inventaire ON inv_cb = Var_cb 
                                WHERE Var_utilisateur = $_SESSION[$dossier]
                                AND Var_stk <> 0.00
                                AND inv_cb IS NULL
                                    GROUP BY Var_rayon
                                    ORDER BY Var_raynom";
    $r_rayon = $idcom->query($req_rayon);
    $nb = $r_rayon->num_rows;
    if ($nb == 0) { //inventaire terminé, on imprime
    ?>
        <tr>
            <tH colspan="2">Inventaire terminé</tH>
        </tr>
        <tr>
            <td align=center><a href="inv_impression.php"><button> Impression du résultat</button></a></td>
            <td><button onclick="window.open('inv_simulation.php')"> Simulation du résultat</button></td>
        </tr>
        <?php
    } else {
        ?>
        <thead>
            <tr>
                <TH colspan="2">Il y a <span id='nb'></span> articles à vérifer<button style='float:right' onclick="$('#liste').empty();charge('inv_annuel','','panneau_g')">Actualiser</button></TH>
            </tr>
            <TR>
                <TH>Rayons</TH>
                <TH>Nb articles</TH>
            </TR>
        </thead>
        <tbody>

            <?php

            $n = 0;
            while ($rq_rayon = $r_rayon->fetch_object()) {
                $coul = ($n % 2 == 0) ? $coulCC : $coulFF;
                echo '<tr class="rayon" style="background-color:' . $coul . '" id="' . $rq_rayon->Var_rayon . '"><td>' . $rq_rayon->Var_raynom . '</td><td>' . $rq_rayon->articles . '</td></tr><tr class="inv_art" id="A' . $rq_rayon->Var_rayon . '"></tr>';
                $n++;
                $nb_articles += $rq_rayon->articles;
            } ?></tbody>
        <tr>
            <td align=center colspan="2"><a href="inv_simulation.php"><button> Simulation du résultat</button></a></td>
        </tr>
</table>
<script>
    $('#nb').html(<?php echo $nb_articles ?>);
    charge("inv_article", '', 'panneau_d');
    $("#panneau_g").height($('#affichage').height() - 10);
</script>
        <?php
    }
    ?>