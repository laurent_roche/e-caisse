<?php
session_start();
if (!isset($incpath)) {
    $p=preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath="";
    for ($i=1;$i<sizeof($p)-1;$i++) {
        $incpath='../'.$incpath;
    }
    unset($p, $i);
}
$art= filter_input(INPUT_GET, "art", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$typ= filter_input(INPUT_GET, "typ", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
// echo "ici";exit;
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();

if ($typ=="ven") {
    $req_vente="SELECT rst_validation,
                     tic_num,
                     tic_quantite,
                     Vt1_nom 
                       FROM Tickets_$req 
                         JOIN Resume_ticket_$req ON rst_id = tic_num 
                         JOIN Vtit1 ON Vt1_article=tic_article 
                          WHERE tic_article=$art 
                            ORDER BY rst_validation DESC";
    $r_vente=$idcom->query($req_vente);
    $rq_vente=$r_vente->fetch_object(); ?>
    <h3>Historique des ventes de <?php echo $rq_vente->Vt1_nom?></h3>
    <table class='droite' width="90%"><TR><TH>Date</TH><TH>N° du ticket</TH><TH>Quantité</TH></TR>
    <?php
    $n=0;
    $r_vente->data_seek(0);
    while ($rq_vente=$r_vente->fetch_object()) {
        $coul=($n % 2 == 0)?$coulCC:$coulFF;
        echo "<tr style='background-color:".$coul."'><td>".$rq_vente->rst_validation."</td><td>".$rq_vente->tic_num."</td><td>".$rq_vente->tic_quantite."</td></tr>";
        $n++;
    } ?></table>
    <?php
} else {
        $req_vente="SELECT rsc_date ,
                  Vt1_nom,
                  com_numero,
                  com_quantite,
                  art_unite,
                  rsc_serveur
                    FROM Resume_commande_$req
                    JOIN Commandes_$req ON com_numero=rsc_id
                    JOIN Articles ON art_id = com_article
                    JOIN Vtit1 ON Vt1_article =com_article
                      WHERE com_article=$art 
                        ORDER BY rsc_id DESC";

        $r_vente=$idcom->query($req_vente);
        $rq_vente=$r_vente->fetch_object(); ?>
    <h3>Historique des commandes de <?php echo $rq_vente->Vt1_nom?></h3>
    <table id='comm' class='droite' width="90%"><TR><TH>Date</TH><TH>N° de commande</TH><TH>Quantité</TH></TR>
    <?php
    $n=0;
        $r_vente->data_seek(0);
    while ($rq_vente=$r_vente->fetch_object()) {
        $coul=($n % 2 == 0)?$coulCC:$coulFF;
        $quantite=($rq_vente->art_unite == 1)?sprintf("%d", $rq_vente->com_quantite):$rq_vente->com_quantite;
        $dateFR=($rq_vente->rsc_serveur == 2)?"Pilon":dateFR($rq_vente->rsc_date);
        echo "<tr com='".$rq_vente->com_numero."&an=".$req."' style='background-color:".$coul."'><td>".$dateFR."</td><td>".$rq_vente->com_numero."</td><td>".$quantite."</td></tr>";
        $n++;
    } ?></table>
    <?php
}
?>
<div id='detail'></div>
<script>
$(document).ready(function() {
    $('#comm tr').click(function(){    
        $('#comm tr').css('font-weight','normal');
        $(this).css('font-weight','bold');            
        charge("detail_commande_validee",$(this).attr('com'),"detail");
        });
});
$("#panneau_d").height($('#affichage').height()-10);
</script>
