<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}

require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
require $incpath."php/config.php";
// echo "<br>".$dossier;
connexobjet();
//insertion dans une table temposraire des articles achetés et vendus depuis $_SESSION['debut']
$deb = $config['debut'];
$req_article = 'CREATE TEMPORARY TABLE Vhistorique AS SELECT art FROM (';
for ($i=$deb; $i <= ANNEE; $i++) {
        $req_article.="SELECT tic_article AS art
                        FROM Tickets_$i 
                        JOIN Articles ON art_id = tic_article
                        JOIN Editeurs ON art_editeur = edi_id 
                            WHERE edi_utilisateur = $_SESSION[$dossier] AND art_stk != 0.00
                        UNION ALL
                        SELECT com_article AS art
                            FROM Commandes_$i
                            JOIN Articles ON art_id = com_article
                            JOIN Editeurs ON art_editeur = edi_id
                                WHERE edi_utilisateur = $_SESSION[$dossier] AND art_stk != 0.00";
    if ($i < ANNEE) {
        $req_article.=" UNION ALL ";
    }
}
    $req_article.=") AS UnionTable GROUP BY art";
$idcom->query($req_article);

// echo "<br>".$req_article;
$req_purge = 'SELECT art_id, art, Vt1_nom 
                FROM Articles
                LEFT JOIN Vhistorique ON art = art_id
                JOIN Editeurs ON edi_id = art_editeur
                JOIN Vtit1 ON Vt1_article = art_id
                    WHERE art IS NULL 
                        AND edi_utilisateur = '.$_SESSION[$dossier].'
                        AND art_stk = 0.00';
$r_purge = $idcom->query($req_purge);
$nb = $r_purge->num_rows;
echo "<h3>Recherche des articles non utilisés (".$nb.")</h3><table class='generique' id='purge'><tr><th>ID</th><th>Titre</th>";
$n = 0;
while($rq_purge = $r_purge->fetch_object()) {
    $coul=($n % 2 == 0)?$coulCC:$coulFF;
    echo "<tr style='background-color:".$coul."' art = '".$rq_purge->art_id."'><td> ".$rq_purge->art_id." </td><td> ".$rq_purge->Vt1_nom."</td></tr>";
    $n++;
}
// echo "<br>".$r_article->num-rows;
if ($idcom->error) {
    echo $idcom->errno." ".$idcom->error."<br>";
}
// $idcom->query("DROP VIEW Vhistorique");
?>
</tr></table>
<script type="text/javascript">
$(document).ready(function(){
    $('#purge tr').css('cursor','pointer');
    $('#purge tr').click(function(){
    $('#purge tr').css('fontWeight','normal');
    $(this).css('fontWeight','bold');
    charge('article',$(this).attr('art'),'panneau_g');
    });
    } 
);
$("#panneau_d").height($("#affichage").height()-10);
</script>