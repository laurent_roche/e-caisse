<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$req = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$an = filter_input(INPUT_GET, "an", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
require $incpath."mysql/connect.php";
connexobjet();
require $incpath."php/fonctions.php";
// print_r($_SESSION);
if ($an == '') {
    $an=ANNEE;
}
$req_recher="SELECT Commandes_$an.*,
                vt1_id,
                vt1_nom,
                art_id,
                art_cb,
                art_unite, 
                edi_commande, 
                com_quantite,
                com_numero,
                com_remise,
                com_pht,
                com_ttc,
                art_pht,
                edi_nom, 
                rsc_date, 
                edi_id,
                rsc_id,
                art_statut 
                    FROM Resume_commande_$an
                        LEFT JOIN Commandes_$an ON rsc_id=com_numero
                        LEFT JOIN Articles on art_id = com_article
                        LEFT JOIN Vtit1 ON Vt1_article = art_id
                        JOIN Editeurs ON rsc_serveur = edi_id
                            WHERE rsc_id = $req
                                ORDER BY art_statut DESC, vt1_nom";
$r_recher=$idcom->query($req_recher);
$nb = $r_recher->num_rows;
if ($nb == 0) {
    echo '<h3>Problème, la commande n° '.$req.' est vide</h3>';
    exit;
}
$resu=$r_recher->fetch_object();
$serveur = $resu->edi_id;
$r_recher->data_seek(0);
// modif(id,tb,vl,cp,my)
?>
<script>
$(document).ready(function() {
    $('#commande td').click(function(){
        $('#commande tr').css('font-weight','normal');
        $(this).parent().css('font-weight','bold');
        if($(this).html().slice(0,4) != "<img") {
        charge('article',$(this).parent().attr('id'),'panneau_g');
        } else {
            id = $(this).parent().attr("id");
            charge("cor_detail_commande_validee",id+'&an=<?php echo $an."&com=".$req?>',id);
        }
    });
    function commande() {

    }

});

</script>
<style>
.jaune{
background-color:yellow;
}
</style>
<?php

?>
<h3 id="liste"> Commande n° <?php echo $req?><br> <?php echo $resu->edi_nom?> <br> <?php echo dateFR($resu->rsc_date)?></h3>

<table id='commande' class='generique' style='width:90%;margin-left:5%'>
<thead>
<tr><TH>CB</TH><TH>Titre</TH><TH>Quantité</TH><th>P. Vente</th><th>Remise</th><th>P.ht</th><td></td></tr>
</thead>
<tbody>
<?php
if ($resu->edi_commande == "") {
    $desactiver=" style='float:left;
    color:#ffffff'";
} else {
    $desactiver=" style='float:left'";
}
$n=0;
$tt = 0.00;
while ($resu=$r_recher->fetch_object()) {
    $num_commande=$resu->rsc_id;
    $coul=($n % 2 == 0)?$coulCC:$coulFF;
    
    $com_quantite=($resu->art_unite == 1)?sprintf("%d", $resu->com_quantite):$resu->com_quantite;
    echo '<tr id="'.$resu->art_id.'" quant="'.$resu->com_quantite.'" style="background-color:'.$coul.'"><td>'.$resu->art_cb.'</td><td>&nbsp;'.$resu->Vt1_nom.'</td><td style="text-align:center">'.$com_quantite.'</td><td class="droite">'.$resu->com_ttc.'</td><td class="droite">'.$resu->com_remise.'</td><td class="droite">'.$resu->com_pht.'</td><td><img src="/images/button_edit.png"></td></tr>'."\n";
    $n++;
    $tt += ($com_quantite*$resu->com_pht);    
}
?>
</tbody>
<tfoot>
<tr><th colspan=4></th><th>Total</th><th><?php echo $tt?> HT</th><th></th></tr>
<tr><th colspan=7><button onclick="charge('dupliquer','<?php echo $req."&an=".$an."&srv=".$serveur?>','panneau_d')">Recommander ces articles</button></th></tr>
</tfoot>
</table>
<table style='width:90%;margin-left:5%'>
<tr id='correction'></tr></table>
<script>
$("#panneau_d").height($("#affichage").height()-10);
$("#panneau_g").height($("#affichage").height()-10);
</script>
