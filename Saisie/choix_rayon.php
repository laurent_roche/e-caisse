<?php
session_start();
if (!isset($incpath)) {
    $p = preg_split("[/]", $_SERVER['PHP_SELF']);
    $incpath = "";
    for ($i = 1;$i<sizeof($p)-1;$i++) {
        $incpath = '../'.$incpath;
    }
    unset($p, $i);
}
$req = filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$sec = filter_input(INPUT_GET, "sec", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
if ($req == "") {
    $req= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
}
require $incpath."mysql/connect.php";
require $incpath."php/fonctions.php";
connexobjet();
?>
<span id='article' style='visibility:hidden'><?php echo $req?></span>
<?php
if ($sec==1) {
    ?>
<script>
function aller(id){
    modif($('#article').html(),11,id,'rayon',1);
    charge('article',$('#article').html(),'panneau_g');
    $('#popup_g').css('visibility','hidden');
}
</script>
<?php
 $req_recher="SELECT ray_id, ray_nom,sec_nom 
                FROM Rayons 
                JOIN Secteurs ON sec_id=ray_secteur 
                    WHERE ray_secteur = $req ORDER BY ray_nom";
    $r_recher=$idcom->query($req_recher);
    if ($r_recher->num_rows == 0) {
        echo "<img src='/images/attention.png'> <h3>Il n'y a pas de rayon dans ce secteur<br>Il faut en créer dans le module Administration</h3>";
        exit;
    }
    $resu=$r_recher->fetch_object();
    $r_recher->data_seek(0);
    $n = 0;
    echo '<h3>Rayons de '.$resu->sec_nom.'</h3>';
    while ($resu=$r_recher->fetch_object()) {
        $coul=($n % 2 == 0)?$coulCC:$coulFF;
        echo "<div style='background-color:".$coul.";height:30px'><img src='../images/valider.png' width='20' height='20' onclick=\"aller(".$resu->ray_id.")\" > ".$resu->ray_nom."</div>";
        $n++;
        // print_r($resu);
    }
} else {
    // print_r($_SESSION);
    $req_recher="SELECT sec_id, sec_nom FROM Secteurs
                              ORDER BY sec_nom";
    $r_recher=$idcom->query($req_recher);
    if ($idcom->error) {
        echo $idcom->errno." ".$idcom->error."<br>";
    } ?>
    <img style="float: right; margin: 0 0 0 20px;" id="fermer" src="/images/annuler.png" onclick='$("#popup_g").css("visibility","hidden");$("#popup_g").empty()'>

    <div style="float:left" id="secteurs">
    <h3>Secteurs / rayons</h3>
    <?php
    $n=0;
    while ($resu=$r_recher->fetch_object()) {
        if ($n%2 == 0) {
            $coul=$coulCC;
        } else {
            $coul=$coulFF;
        }
        echo "<div onclick=\"charge('choix_rayon','".$resu->sec_id."&sec=1','rayon')\" style='background-color:".$coul.";height:20px'>".$resu->sec_nom."</div>";
        $n++;
        // print_r($resu);
    } ?>
    </div>
    <div style="float:left;margin-left:20px" id="rayon">
    </div>
    <?php
}
?>
<script>$('#popup_g').css('visibility','visible');
</script>