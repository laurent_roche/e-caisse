<style>
#articles{width:90%}
.jaune{
background-color:yellow;
}
#articles.tablesorter tbody td {
  font-size: 11pt;
  color: #3D3D3D;
  padding: 0 4px 0 4px;
  background-color: <?php echo $coulFF?>;
  vertical-align: middle;
 }
#articles.tablesorter tbody tr.odd td {
/*   text-align:left; */
  background-color:<?php echo $coulCC?>;
  vertical-align: middle;
 }
#articles.tablesorter tbody tr.odd.orange td {
/*   text-align:left; */
  background-color:orange;
  vertical-align: middle;
 }
 #articles.tablesorter thead tr .headerSortDown, table.tablesorter thead tr .headerSortUp {
background-color: #8dbdd8;}
</style>
<script type="text/javascript" src="/js/jquery.tablesorter.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $("#articles").tablesorter({ widgets: ['zebra']});
    $('#articles td').click(function(){
    $('#articles td').css('fontWeight','normal');
    $(this).css('fontWeight','bold');
    });
    } 
);


$(document).ready(function() {
    $('#articles input').on('keydown',function(event) {
    $(this).addClass('jaune');
    // alert($(this).parent().parent().attr('id'));
    if(event.which == 13){
        var tr =  $(this).parent().parent().attr('id').split('_');
        if($(this).parent().html().slice(0,6) == '<input') {
            if($(this).parent().parent().hasClass("com") === true){//commande en cours
                modif($(this).parent().parent().attr('com'),16,$(this).val(),'quantite',1);
            } else {//insertion
                charge('/Saisie/commandes/insert','&art='+tr[1]+'&qt='+$(this).val(),'mysql');
                $(this).css('backgroundColor','orange');
            }
        $(this).removeClass('jaune');
        }
    }
});
    
    $('#articles tbody td').click(function(){
        // alert($(this).html().slice(0,8));
        if(($(this).html().slice(0,6)) != '<input'){
            var art=$(this).parent().attr('id').split('_');
            charge('article',art[1],'panneau_g');
            }
        });
});
</script>

<h3><?php echo $nb?> articles <?php echo $nom?></h3>
<table cellpadding="0" id="articles" class="tablesorter">
  <thead>
    <TR>
      <TH style="text-align:center">Titre</TH>
      <TH style="text-align:center">Stock</TH>
      <TH style="text-align:center">Ventes/an</TH>
      <TH style="text-align:center">Comm.</TH>
    </TR>
  </thead>
  <tbody>
<?php
//cette page est incluse dans toute les listes.
// les requètes doivent renvoyer : art_unite, art_stk, unv_abrege, rsc_etat, com_quantite, Vt1_nom, art_id,
$n=0;
while ($resu=$r_recher->fetch_object()) {
    // 	print_r($resu);
    if (!$rq_max->maxi) {
        $graph=$resu->ven_ct;
    } elseif (($rq_max->maxi)&&($resu->ven_ct != 0)) {
        $largeur=round((60/$rq_max->maxi)*$resu->ven_ct);
        if ($largeur < 5) {
            $largeur = 5;
        }
        $largeur=($largeur > 200)?200:$largeur;
        
        $graph=$resu->ven_ct."<div style='width:".$largeur."px;background-color:".$_SESSION['surligne_'.$_SESSION[$dossier]]."'>&nbsp;</div>";
    } else {
        $graph="";
    }
    
    if ($resu->art_unite == 1) {
        $stock= sprintf("%d", $resu->art_stk);
    } else {
        $stock = $resu->art_stk." ".$resu->unv_abrege;
    }
    
    // print_r($resu);
    $coul = '';

    if ($resu->rsc_etat == 1) {//commandes en préparation
        $coul= " style='background-color:orange'";
    } elseif ($resu->rsc_etat == 2) {//commandes en attente de réception
        $coul= " style='background-color:red'";
    }
    
    if ($resu->rsc_etat != 3) {
        $quantite = $resu->com_quantite;
    } else {
        $quantite ="";
    }
    
    if ($resu->com_id != "") {
        $com = " com='".$resu->com_id."' class ='com'";
    } else {
        $com = "";
    }
    $statut = '';
    echo '<tr id="A_'.$resu->art_id.'"'.$com.'>';
    if ((isset($resu->sta_nom))&&(isset($resu->art_statut))) {
        if (($resu->art_statut) != 1) {
            $statut = ' <em style="float:right">'.$resu->sta_nom.'</em>';        
        } else {
            $statut = '';       
        }
    }
    echo '<td style="cursor:pointer">'.stripslashes($resu->Vt1_nom).$statut.'</td>';    
    echo '<td class="droite">'.$stock.'</td>
        <td align="right">'.$graph.'</td>';
    if ($quantite !='') {
        $quantite = sprintf("%d", $quantite);
    } else {
        $quantite = '';
    }
    if (($resu->rsc_etat == 2)) {
        echo "<td class='centre'>".$quantite."</td></tr>";
    } else {
        echo "<td class='centre'><input type='text' class='mille' $coul value='".$quantite."'></td>
    </tr>";
    }
    $quantite="";
    $n++;
}
?>
</tbody>
</table>
<script>
$("#panneau_d").height($("#affichage").height()-10);
</script>
