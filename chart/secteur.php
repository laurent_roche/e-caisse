<?php
$annee= filter_input(INPUT_GET, "an", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$secteur= filter_input(INPUT_GET, "req", FILTER_SANITIZE_FULL_SPECIAL_CHARS);
$tab = explode('/', $_SERVER['REQUEST_URI']);
$dossier = $tab[1];
$comp = ($dossier == "Saisie")? "AND Vart_utilisateur = ".$_SESSION[$dossier]: "" ;
$req_secteurs = "SELECT SUM(tic_quantite) AS ct, 
                      (SUM(tic_prix * tic_quantite)) AS vl,
                      sec_nom,
                      sec_couleur,
                      ray_nom,
                      Vart_id,
                      Vt1_nom,
                      Vart_unite
                        FROM Tickets_".$annee."
                          JOIN Varticle_editeur ON Vart_id = tic_article
                          JOIN Rayons ON ray_id = Vart_rayon
                          JOIN Secteurs ON sec_id = ray_secteur
                          JOIN Vtit1 ON Vt1_article = Vart_id
                            WHERE ray_secteur = $secteur
                            $comp
                                GROUP BY Vart_id 
                                ORDER BY ray_nom";
$r_secteurs = $idcom->query($req_secteurs);
$rq_secteurs =$r_secteurs->fetch_object();
$r_secteurs->data_seek(0);
?>
<h3 id="secteur">Ventes dans le secteur <?php echo $rq_secteurs->sec_nom?> en <?php echo $annee?></h3>
<?php
if ($dossier == 'Saisie') {
    ?>
<script>
$(document).ready(function() {    
$('#articles tbody tr').click(function(){
        $('#articles tr').css('font-weight','normal');
        $(this).css('font-weight','bold');
        charge('article',$(this).attr('id').slice(4),'panneau_g');
    });
});
</script>
    <?php
}
?>
<table class="generique" id="articles">
    <thead>
        <tr>
            <th>Titre</th><th>Quantite</th><th>Valeur</th>
        </tr>
    </thead>
    <tbody>
<?php
$rayon = "";
$n = 0;
$vl = 0.00; //valeur total afficher par jquery dans le titre
while ($rq_secteurs =$r_secteurs->fetch_object()) {
    $coul=($n % 2 == 0)?$coulCC:$coulFF;
    $unite = ($rq_secteurs->Vart_unite == 1) ? sprintf('%d', $rq_secteurs->ct):$rq_secteurs->ct;
    if ($rayon != $rq_secteurs->ray_nom) {
        echo "<tr><th colspan='3'>".$rq_secteurs->ray_nom."</th></tr>";
        $rayon = $rq_secteurs->ray_nom;
    }
    echo "<tr id='art_".$rq_secteurs->Vart_id."' style='background-color:".$coul."'><td>".$rq_secteurs->Vt1_nom."</td><td class='droite'>".$unite."</td><td class='droite'>".sprintf('%01.2f', $rq_secteurs->vl) ." €</td></tr>";
    $vl += $rq_secteurs->vl;
    $n++;
}

?></tbody>
</table>
<script>
    $("#secteur").append("<?php echo " (".sprintf('%01.2f', $vl)." €)"?>");
    $("#panneau_d").height($("#affichage").height()-10);
</script>

